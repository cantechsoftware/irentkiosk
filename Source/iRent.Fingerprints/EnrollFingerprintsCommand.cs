﻿using System;
using System.Collections.Generic;
using Castle.Core;
using iRent.Framework;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Models;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;
using iRentKiosk.Core.Util;

namespace iRentKiosk.Fingerprints
{
    [CastleComponent]
    public class EnrollFingerprintsCommand : BaseCommand
    {
        private readonly Func<FingerprintScannerControl> _factory;
        private readonly FingerprintApi _fpApi;

        public EnrollFingerprintsCommand(FingerprintApi fpApi,
            Func<FingerprintScannerControl> factory)
        {
            _fpApi = fpApi;
            _factory = factory;
            MinFingers = 2;
            Verify = true;
        }

        public string Next { get; set; }
        public string Back { get; set; }

        public int MinFingers { get; set; }
        public bool Verify { get; set; } 

        public override void Invoke()
        {
            var personId = ContextData.DriverID;
            var fingers = _fpApi.GetPrints(personId);
            Enroll(personId, fingers);
        }

        private void Return()
        {
            Proceed(Back);
        }

        private void Proceed()
        {
            Proceed(Next);
        }

        private void Enroll(IdInfo personId, IList<FingerprintModel> fingers)
        {
            var control = _factory();
            IMainForm main = (IMainForm)MainForm;
            main.AddToFormPanel(control);

            control.Init();
            control.Next += (o, e) => Proceed();
            control.Back += (o, e) => Proceed();
            control.Enroll(personId, ContextData.DriverName, MinFingers, fingers, Verify);
        }
    }
}