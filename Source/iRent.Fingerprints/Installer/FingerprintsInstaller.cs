﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using CanTech.Common.Extensions;

namespace iRentKiosk.Fingerprints.Installer
{
    public class FingerprintsInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.RegisterComponents<FingerprintsInstaller>();            
        }
    }
}