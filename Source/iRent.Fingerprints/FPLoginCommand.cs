﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.Core;
using iRent.Framework;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Models;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;
using iRentKiosk.Core.Util;

namespace iRentKiosk.Fingerprints
{
    [CastleComponent]
    public class FPLoginCommand : BaseCommand
    {
        private readonly Func<FingerprintScannerControl> _factory;
        private readonly FingerprintApi _fpApi;

        public FPLoginCommand(FingerprintApi fpApi,
            Func<FingerprintScannerControl> factory)
        {
            _fpApi = fpApi;
            _factory = factory;
            MinFingers = 2;
            VerifyOnEnroll = false;
        }

        public string Registered { get; set; }
        public string NotRegistered { get; set; }

        public int MinFingers { get; set; }
        public bool VerifyOnEnroll { get; set; }

        public override void Invoke()
        {
            EnrollOrVerify();
        }

        private void Proceed()
        {
            if (ContextData.Driver.IsRegistered)
                Proceed(Registered);
            else
                Proceed(NotRegistered);
        }

        private void EnrollOrVerify()
        {
            var personId = ContextData.DriverID;
            var fingers = _fpApi.GetPrints(personId);
            if (fingers.Any())
                Verify(personId, fingers);
            else
                Enroll(personId, fingers);
        }

        private void Verify(IdInfo personId, IList<FingerprintModel> fingers)
        {
            var control = _factory();
            IMainForm main = (IMainForm)MainForm;
            main.AddToFormPanel(control);
            control.Init();
            control.Next += (o, e) =>
            {
                if (fingers.Count < MinFingers)
                    Enroll(personId, fingers);
                else
                    Proceed();
            };

            control.Verify(ContextData.DriverName, fingers);
        }

        private void Enroll(IdInfo personId, IList<FingerprintModel> fingers)
        {
            var control = _factory();
            IMainForm main = (IMainForm)MainForm;
            control.Init();
            main.AddToFormPanel(control);

            control.Next += (o, e) => Proceed();
            control.Enroll(personId, ContextData.DriverName, MinFingers, fingers, VerifyOnEnroll);
        }
    }
}