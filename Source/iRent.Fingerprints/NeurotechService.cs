﻿using System;
using Castle.Core;
using Neurotec.Biometrics;
using Neurotec.Biometrics.Client;
using Neurotec.Licensing;
using System.Windows.Forms;
using log4net;


namespace iRentKiosk.Fingerprints
{
    [CastleComponent]
    public class NeurotechService : IStartable
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(NeurotechService));

        private static string _address { get; set; }
        private static int _port { get; set; }

        public NeurotechService(string address = "/local", int port = 5000, bool disabled = false)
        {
            try
            {
                if (disabled)
                    return;

                _address = address;
                _port = port;

            }
            catch (Exception ex)
            {
                _log.Error("Error During NeurotechService.Create :", ex);
                MessageBox.Show("Error occurred during NeurotechService.Create: \n" + ex.Message);
            }
        }
               
        public NBiometricClient BiometricClient { get; private set; }

        private void GetLicenses(string address, int port)
        {
            try
            {
                const string components =
                    "Biometrics.FingerExtraction,Biometrics.FingerMatching,Devices.FingerScanners,Images.WSQ,Biometrics.FingerSegmentation,Biometrics.FingerQualityAssessmentBase";
                foreach (var component in components.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    bool res = NLicense.ObtainComponents(address, port, component);
                    if (!res)
                    {
                        _log.Error("Error During NeurotechService.GetLicenses :"+ component);
                        MessageBox.Show("Error occurred during NeurotechService.GetLicenses: \n" + component);
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error("Error During NeurotechService.GetLicenses :", ex);
                MessageBox.Show("Error occurred during NeurotechService.GetLicenses: \n" + ex.Message);
            }
        }

        public void Start()
        {
            try
            {
                //GetLicenses(_address, _port);
                BiometricClient = new NBiometricClient
                {
                    UseDeviceManager = true,
                    BiometricTypes = NBiometricType.Finger
                };
                
                BiometricClient.Initialize();
            }
            catch (Exception ex)
            {
                _log.Error("Error During NeurotechService.Start :", ex);
                MessageBox.Show("Error occurred during NeurotechService.Start: \n" + ex.Message);
            }
        }

        public void Stop()
        {
            BiometricClient.Dispose();
        }
    }
}