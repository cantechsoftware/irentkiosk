﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using Neurotec.Biometrics;
using Neurotec.Biometrics.Client;
using Neurotec.Biometrics.Gui;
using iRent2.Contracts.Models;
using SmartFormat;
using Humanizer;

namespace iRentKiosk.Fingerprints
{
    public class VerifyMode : IFingerprintMode
    {
        private CancellationTokenSource _cancel = new CancellationTokenSource();
        private FingerprintScannerControl _control;
        private IList<FingerprintModel> _fingers;
        private NFPosition _position;
        private NSubject _verifySubject;
        private NFinger _finger;
        private NSubject _subject;
        private NBiometricClient _client;

        public VerifyMode(FingerprintScannerControl control, 
            IList<FingerprintModel> fingers, 
            NBiometricClient client)
        {
            _control = control;
            _fingers = fingers;
            _client = client;
            _control.SetRemaining("{0} {0:finger|fingers} enrolled".FormatSmart(fingers.Count));
            foreach (var fn in fingers)
                _control.SetFingerColour(fn.Code.ToNFPosition(), Color.DarkGreen);
            Task.Factory.StartNew(ChooseFinger, _cancel.Token).ConfigureAwait(false);
        }

        private void ChooseFinger()
        {
            if (_cancel.IsCancellationRequested)
                return;

            _control.SetStatus("Randomly selecting a finger to verify, please be patient...");
            FingerprintModel finger = null;
            var rand = new Random();
            var pos = rand.Next(_fingers.Count);
            for (var i = 0; i < 10; i++)
            {
                if (_cancel.IsCancellationRequested)
                    return;

                finger = _fingers[(pos + rand.Next(_fingers.Count - 1))%_fingers.Count];
                _control.SetFingerColour(_position, Color.DarkGreen);
                _position = finger.Code.ToNFPosition();
                _control.SetFingerColour(_position, Color.YellowGreen);
                Thread.Sleep(300);
            }
            _control.SetStatus("Place your {0} on the fingerprint reader"
                                   .FormatWith(CurrentFinger));
            _subject = NSubject.FromMemory(finger.Template);            
            BeginVerify();
        }

        private string CurrentFinger => _position.Humanize().Titleize();

        public void Dispose()
        {
            _cancel.Cancel();
            _client.Cancel();            
            _control = null;
        }

        private void OnFingerPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Status")
                _control.SetQuality(_finger.Status);
        }

        private void BeginVerify()
        {
            _control.SetStatus("Please place your {0} on the fingerprint reader"
                                   .FormatWith(CurrentFinger));
            _verifySubject = new NSubject();
            _finger = new NFinger {Position = _position};
            _verifySubject.Fingers.Add(_finger);

            _finger.PropertyChanged += OnFingerPropertyChanged;
            _control.ShowFinger(_finger, ShownImage.Original);

            // Begin capturing
            _client.FingersReturnBinarizedImage = true;
            var task = _client.CreateTask(NBiometricOperations.Capture
                                         | NBiometricOperations.CreateTemplate,
                                         _verifySubject);
            _client.BeginPerformTask(task, OnVerifyCaptured, null);
        }

        private void OnVerifyCaptured(IAsyncResult ar)
        {
            var task = _client.EndPerformTask(ar);
            var status = task.Status;
            var finger = task.Subjects[0].Fingers[0];
            finger.PropertyChanged -= OnFingerPropertyChanged;

            // Check if extraction was canceled
            if (status == NBiometricStatus.Canceled)
            {
                Reset(0);
                return;
            }

            if (status == NBiometricStatus.Ok)
            {
                _control.SetQuality($"Quality: {_finger.Objects[0].Quality}");

                var verifyStatus = _client.Verify(_subject, _verifySubject);
                if (verifyStatus == NBiometricStatus.Ok)
                {
                    _control.ShowFinger(null, ShownImage.None);
                    _control.SetFingerColour(_finger.Position, Color.LimeGreen);
                    _control.SetStatus($"Successfully verified {CurrentFinger}, Click Next to Proceed");
                    _control.ToggleNext(true);                    
                }
                else
                {
                    _control.SetStatus(
                        $"{CurrentFinger} fingerprint did not match ({verifyStatus.Humanize()}), please try again");
                    Reset(2500);
                }
            }
            else
            {
                _control.SetStatus($"Failed to scan fingerprint: {status.Humanize()}");
                Reset(2500);
            }
        }

        private void Reset(int delay)
        {
            _finger.PropertyChanged -= OnFingerPropertyChanged;
            _finger = null;
            _subject = null;
            _verifySubject = null;

            if (!_cancel.IsCancellationRequested)
            {
                Task.Delay(delay, _cancel.Token)
                    .ContinueWith(x => ChooseFinger())
                    .ConfigureAwait(false);
            }
        }
    }
}