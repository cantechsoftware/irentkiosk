﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CanTech.Common.Extensions;
using Humanizer;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Models;
using iRentKiosk.Core.Services.Api;
using log4net;
using Neurotec.Biometrics;
using Neurotec.Biometrics.Client;
using Neurotec.Biometrics.Gui;
using SmartFormat;
using System.Windows.Forms;

namespace iRentKiosk.Fingerprints
{
    public class EnrollMode : IFingerprintMode
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(EnrollMode));

        private readonly CancellationTokenSource _cancel = new CancellationTokenSource();
        private NBiometricClient _client;
        private NeurotechService _svc;
        private FingerprintScannerControl _control;
        private FingerprintApi _fpApi;
        private readonly int _minFingers;
        private readonly IdInfo _personId;
        private readonly IList<NFPosition> _positions = new List<NFPosition>();

        private readonly bool _verify;

        private NFinger _finger;
        private NSubject _subject;
        private NSubject _verifySubject;

        public EnrollMode(FingerprintScannerControl control,
            FingerprintApi fpApi,
            IdInfo personId,
            int minFingers,
            IList<FingerprintModel> alreadyEnrolled,
            NeurotechService svc,
            bool verify)
        {
            _log.DebugFormat("Created EnrollMode for {0}, min fingers {1}, alreadyEnrolled: {2}, verify: {3}",
                personId, minFingers, alreadyEnrolled.Count, verify);

            _control = control;
            _fpApi = fpApi;
            _personId = personId;
            _control.FingerClick += OnFingerClick;
            _minFingers = minFingers;
            _svc = svc;
            _client = svc.BiometricClient;
            _verify = verify;
            foreach (var fn in alreadyEnrolled)
            {
                _positions.Add(fn.Code.ToNFPosition());
                _control.SetFingerColour(fn.Code.ToNFPosition(), Color.YellowGreen);
            }
            UpdateRemaining();
        }

        private string CurrentFinger => Describe(_finger.Position);

        public void Dispose()
        {
            _log.Debug("Disposing EnrollMode");
            _cancel.Cancel();
            _client.Cancel();
            _control.FingerClick -= OnFingerClick;
        }

        private NBiometricTask _activetask = null;
        private bool inProgress = false;

        private void OnFingerClick(object sender, FingerSelector.FingerClickArgs e)
        {
            if (inProgress)
            {
                inProgress = false;
                _client.Cancel();
            }

            _log.DebugFormat("Clicked on finger {0} (scanned: {1})", e.Position, _positions.Contains(e.Position));
            if (_positions.Contains(e.Position))
            {
                _control.SetStatus($"{e.Position} has already been scanned!");
                return;
            }

            _subject = new NSubject();
            _finger = new NFinger {Position = e.Position};
            _subject.Fingers.Add(_finger);

            _finger.PropertyChanged += OnFingerPropertyChanged;
            _control.ShowFinger(_finger, ShownImage.Original);
            _control.SetStatus($"Please put your {CurrentFinger} on the fingerprint scanner");

            // Begin capturing
            _client.FingersReturnBinarizedImage = true;

            _activetask = _client.CreateTask(NBiometricOperations.Capture
                                          | NBiometricOperations.CreateTemplate,
                _subject);
            inProgress = true;
            _client.BeginPerformTask(_activetask, OnEnrollCompleted, null);
            _log.DebugFormat("Started Capture & CreateTemple task");

            
            //ManualResetEvent evt = (ManualResetEvent)_activetaskresult.AsyncWaitHandle;
        }

        private void OnEnrollCompleted(IAsyncResult ar)
        {
            _finger.PropertyChanged -= OnFingerPropertyChanged;

            var task = _client.EndPerformTask(ar);
            var status = task.Status;

            _log.DebugFormat("OnEnrollCompleted, status {0}", status);
            // Check if extraction was canceled
            if (status == NBiometricStatus.Canceled)
            {
                Reset(0,task);
                inProgress = false;
                return;
            }

            if (status == NBiometricStatus.Ok)
            {
                _log.DebugFormat("Captured finger {0}, quality: {1}", _finger.Position, _finger.Objects[0].Quality);
                _control.SetQuality($"Quality: {_finger.Objects[0].Quality}");
                if (_verify)
                {
                _control.SetFingerColour(_finger.Position, Color.Gold);
                _control.SetStatus($"Please scan your {Describe(_finger.Position)} again to verify your fingerprint");
                Task.Delay(2000, _cancel.Token)
                    .ContinueWith(x => WaitForFingerLift(), _cancel.Token)
                    .ContinueWith(x => BeginVerify(), _cancel.Token)
                    .ConfigureAwait(false);
            }
            else
            {
                    _control.ShowFinger(null, ShownImage.None);
                    _control.SetFingerColour(_finger.Position, Color.LimeGreen);
                    _control.SetStatus($"Successfully captured {CurrentFinger} fingerprint");
                    _positions.Add(_finger.Position);

                    SaveFinger(_subject);
                    UpdateRemaining();
                    Reset(3000,task);
                }
            }
            else
            {
                _log.DebugFormat("Template not extracted: {0}", status);
                _control.SetStatus($"Failed to capture finger: {status}.");
                _control.SetFingerColour(_finger.Position, Color.DarkGray);
                Reset(3000,task);
            }
            inProgress = false;
        }

        private void WaitForFingerLift()
        {
            _control.SetStatus($"Please lift your {CurrentFinger}");
            Thread.Sleep(2000);
        }

        private string Describe(NFPosition pos)
        {
            return pos.Humanize().Titleize();
        }

        private void BeginVerify()
        {
            _log.DebugFormat("BeginVerify: CancelledRequested: {0}", _cancel.IsCancellationRequested);
            if (_cancel.IsCancellationRequested)
                return;

            _control.SetStatus($"Please place your {CurrentFinger} down again");

            _verifySubject = new NSubject();
            var finger = new NFinger {Position = _finger.Position};
            _verifySubject.Fingers.Add(finger);

            finger.PropertyChanged += OnFingerPropertyChanged;
            _control.ShowFinger(finger, ShownImage.Original);

            // Begin capturing
            _client.FingersReturnBinarizedImage = true;
            var task = _client.CreateTask(NBiometricOperations.Capture
                                          | NBiometricOperations.CreateTemplate,
                _subject);
            _client.BeginPerformTask(task, OnVerifyCaptured, null);
            _log.DebugFormat("Created Task Capture & CreateTemplate for {0}", _finger.Position);
        }

        private void OnVerifyCaptured(IAsyncResult ar)
        {
            var task = _client.EndPerformTask(ar);
            _log.DebugFormat("OnVerifyCaptured: Cancelled: {0}", _cancel.IsCancellationRequested);
            if (_cancel.IsCancellationRequested)
                return;

            _log.DebugFormat("OnVerifyCaptured: ({0}) {1}", task.Status,
                task.Subjects.SelectMany(x => x.Fingers)
                    .Select(x => $"{x.Position}, {x.Status}")
                    .Join("\r\n"));

            var status = task.Status;
            var finger = task.Subjects[0].Fingers[0];
            finger.PropertyChanged -= OnFingerPropertyChanged;

            // Check if extraction was canceled
            if (status == NBiometricStatus.Canceled)
            {
                _log.DebugFormat("OnVerifyCaptured: status: {0}", status);
                Reset(0,task);
                return;
            }

            if (status != NBiometricStatus.Ok)
            {
                _log.DebugFormat("OnVerifyCaptured: Failed to capture fingerprint {0}: {1}", _finger.Position, status);
                _control.SetStatus("Failed to capture fingerprint, please try again.");
                _control.SetFingerColour(_finger.Position, Color.OrangeRed);
                Task.Delay(2000, _cancel.Token)
                    .ContinueWith(x => WaitForFingerLift(), _cancel.Token)
                    .ContinueWith(x => BeginVerify(), _cancel.Token)
                    .ConfigureAwait(false);
                inProgress = false;
                return;
            }

            _log.DebugFormat("OnVerifyCaptured: Captured finger: {0}, quality: {1}",
                _finger.Position, _finger.Objects[0].Quality);

            _control.SetQuality($"Quality: {_finger.Objects[0].Quality}");

            try
            {
                _log.DebugFormat("OnVerifyCaptured: Verifying finger {0}:\r\nSubject: {1}\r\nVerifySubject: {2}",
                    _finger.Position, _subject.Describe(), _verifySubject.Describe());

                var bstat =_client.Clear();
                _log.DebugFormat("OnVerifyCapture: status after clear: {0}", bstat);

                var verifyStatus = _client.Verify(_subject, _verifySubject);
                _log.DebugFormat("OnVerifyCaptured: Verify Status for {0} = {1}", _finger.Position, verifyStatus);
                if (verifyStatus == NBiometricStatus.Ok)
                {
                    _log.DebugFormat("OnVerifyCaptured: Verified finger {0}", CurrentFinger);
                    _control.ShowFinger(null, ShownImage.None);
                    _control.SetFingerColour(_finger.Position, Color.LimeGreen);
                    _control.SetStatus($"Successfully captured {CurrentFinger} fingerprint");
                    _positions.Add(_finger.Position);

                    SaveFinger(_subject);
                    UpdateRemaining();
                    Reset(3000,task);
                }
                else
                {
                    _log.DebugFormat("OnVerifyCaptured: Failed to verify finger {0}: {1}", CurrentFinger, verifyStatus);
                    _control.SetStatus($"{verifyStatus.Humanize()} fingerprint did not match, please try again");
                    Reset(2500,task);
                }
                inProgress = false;
            }
            catch (Exception ex)
            {
                _log.Error($"OnVerifyCaptured: Error verifying finger {_finger.Position}", ex);
                inProgress = false;
            }
        }

        private void SaveFinger(NSubject subject)
        {
            _log.DebugFormat("SaveFinger: {0}", _finger.Position);
            var buffer = subject.GetTemplateBuffer().ToArray();
            _fpApi.Enroll(_personId, _finger.Position.ToFingerCode(), buffer);
        }

        private void Reset(int delay, NBiometricTask task)
        {
            _log.DebugFormat("Reset, delay: {0}", delay);

            if (task != null)
            {
                foreach (var subject in task.Subjects)
                {
                    foreach (var finger in subject.Fingers)
                    {
                        finger.Dispose();
                    }
                    subject.Dispose();
                }
                task.Dispose();
            }

            //            if (inProgress)

            //_client.Cancel();
            _client.Reset();
            //_client.Clear();
            Task.Delay(delay, _cancel.Token)
                .ContinueWith(x => _control.SetStatus("Please click on a finger to scan"))
                .ConfigureAwait(false);
        }

        private void OnFingerPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            _log.DebugFormat("OnFingerPropertyChanged: {0} ({1})", e.PropertyName, _finger.Status);
            if (e.PropertyName == "Status")
                _control.SetQuality(_finger.Status);
        }

        private void UpdateRemaining()
        {
            var captured = _positions.Count;
            var remaining = Math.Max(0, _minFingers - captured);
            _log.DebugFormat("UpdateRemaining fingers, captured: {0}, remaining: {1}", captured, remaining);
            if ((remaining > 0) && (captured == 0))
                _control.SetRemaining(
                    "You need to scan at least {0} {0:finger|fingers} to continue."
                        .FormatSmart(remaining));
            else if (remaining > 0)
                _control.SetRemaining("Well done, {0} {0:finger|fingers} captured, {1} to go!"
                    .FormatSmart(captured, remaining));
            else
                _control.SetRemaining(
                    "You have captured {0} {0:finger|fingers}, click on another finger to capture it, or click on Next to proceed"
                        .FormatSmart(captured));
            if(remaining == 0)
                
            _control.ToggleNext(remaining == 0);
        }
    }
}