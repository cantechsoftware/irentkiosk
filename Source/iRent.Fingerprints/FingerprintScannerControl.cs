﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Castle.Core;
using Humanizer;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Models;
using iRentKiosk.Core.Services.Api;
using log4net;
using Neurotec.Biometrics;
using Neurotec.Biometrics.Client;
using Neurotec.Biometrics.Gui;

namespace iRentKiosk.Fingerprints
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class FingerprintScannerControl : UserControl
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(FingerprintScannerControl));

        private readonly FingerprintApi _fpApi;
        private readonly NeurotechService _neurotech;
        public FingerSelector _fingerSelector;
        public NFingerView _fingerView;
        private IFingerprintMode _mode;

        public FingerprintScannerControl(NeurotechService neurotech, FingerprintApi fpApi)
        {
            _neurotech = neurotech;
            _neurotech.Stop();
            _neurotech.Start();
            _fpApi = fpApi;
            InitializeComponent();
        }

        private NBiometricClient Client => _neurotech.BiometricClient;

        public event EventHandler<FingerSelector.FingerClickArgs> FingerClick;
        public event EventHandler<EventArgs> Next;
        public event EventHandler<EventArgs> Back;

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (_fingerView != null)
            {
                _fingerView.Dispose();
            }
            if (_fingerSelector != null)
            {
                _fingerSelector.Dispose();
            }

            _fingerView = new NFingerView
            {
                Dock = DockStyle.Fill
            };
            _fingerSelector = new FingerSelector
            {
                Dock = DockStyle.Fill
            };
            _fingerSelector.FingerClick += OnFingerClick;
            splitContainer.Panel1.Controls.Add(_fingerSelector);
            splitContainer.Panel2.Controls.Add(_fingerView);
#if DEBUG
            lblStatus.DoubleClick += lblStatus_DoubleClick;
#endif
        }

        private void lblStatus_DoubleClick(object sender, EventArgs e)
        {
            btnNext_Click(btnNext, e);
        }

        private void OnFingerClick(object sender, FingerSelector.FingerClickArgs e)
        {
            _log.DebugFormat("OnFingerClick ({0}): {1}", Client.FingerScanner, e.Position);
            if (Client.FingerScanner == null)
            {
                lblStatus.Text = "Fingerprint Scanner Not Detected!";
                return;
            }

            FingerClick?.Invoke(sender, e);
        }

        public void Init()
        {
            _neurotech.Stop();
            _neurotech.Start();
        }

        public void Enroll(IdInfo personId, string name, int minFingers,
            IList<FingerprintModel> alreadyEnrolled, bool verify)
        {
            _log.DebugFormat("Enroll: {0}, {1}, {2}, {3}, {4}", 
                personId, name, minFingers, alreadyEnrolled.Count, verify);

            lblHeading.Text = $"Enrolling {name}'s Fingerprints";
            _mode = new EnrollMode(this, _fpApi, personId,
                minFingers, alreadyEnrolled, _neurotech, verify);
        }

        public void Verify(string name, IList<FingerprintModel> fingers)
        {
            _log.DebugFormat("Verify: {0}, {1}", name, fingers.Count);
            lblHeading.Text = $"Verifying {name}'s Fingerprints";
            _mode = new VerifyMode(this, fingers, _neurotech.BiometricClient);
        }

        public void SetStatus(string message)
        {
            InvokeIfRequired(() =>
            {
                _log.DebugFormat("SetStatus: {0}", message);
                lblStatus.Text = message;
            });
        }

        private void InvokeIfRequired(Action callback)
        {
            if (InvokeRequired)
                BeginInvoke(callback);
            else
                callback();
        }

        public void ShowFinger(NFinger finger, ShownImage shownImage)
        {
            InvokeIfRequired(() =>
            {
                try
                {
                    _fingerSelector.SelectedPosition = finger?.Position ?? NFPosition.Unknown;
                    _fingerView.Finger = finger;
                    _fingerView.ShownImage = shownImage;
                }
                catch (Exception ex)
                {
                    _log.Warn($"Error in ShowFinger, finger: ${finger}, shownImage: ${shownImage}", ex);
                }
            });
        }

        public void SetQuality(string message)
        {
            InvokeIfRequired(() =>
            {
                _log.DebugFormat("SetQuality: {0}", message);
                lblQuality.Text = message;
            });
        }

        public void SetFingerColour(NFPosition position, Color colour)
        {
            InvokeIfRequired(() =>
            {
                _log.DebugFormat("SetFingerColour: {0}, {1}", position, colour);
                _fingerSelector.SetPositionColour(position, colour);                
            });
        }

        public void SetRemaining(string message)
        {
            InvokeIfRequired(() =>
            {
                _log.DebugFormat("SetRemaining: {0}", message);
                lblRemaining.Text = message;
            });
        }

        public void ToggleNext(bool enabled)
        {
            InvokeIfRequired(() =>
            {
                _log.DebugFormat("ToggleNext: {0}", enabled);
                btnNext.Enabled = enabled;
                btnNext_Click(null, new EventArgs());
            });
        }

        public void SetHeading(string msg)
        {
            InvokeIfRequired(() =>
            {
                _log.DebugFormat("SetHeading: {0}", msg);
                lblHeading.Text = msg;
            });
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            _log.DebugFormat("btnNext_Click");
            _mode.Dispose();
            _mode = null;
            Next?.Invoke(this, e);
        }

        /// <summary>
        ///     Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            _mode?.Dispose();
            _mode = null;
            if (disposing)
                components?.Dispose();
            base.Dispose(disposing);
        }

        public void SetQuality(NBiometricStatus status)
        {
            SetQuality(Translate(status));
        }

        private string Translate(NBiometricStatus status)
        {
            switch (status)
            {
                case NBiometricStatus.ObjectNotFound:
                    return "Finger not detected";
                default:
                    return status.Humanize();
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            _log.DebugFormat("btnBack_Click");
            _mode.Dispose();
            _mode = null;
            Back?.Invoke(this, e);

        }

        private void lblRemaining_DoubleClick(object sender, EventArgs e)
        {
            _log.DebugFormat("btnNext_Click");
            _mode.Dispose();
            _mode = null;
            Next?.Invoke(this, e);

        }
    }
}