﻿using System;
using System.Linq;
using CanTech.Common.Extensions;
using Neurotec.Biometrics;
using Neurotec.IO;
using iRent2.Contracts.Enums;

namespace iRentKiosk.Fingerprints
{
    public static class NeurotechExtensions
    {
        public static byte[] Serialize(this NFinger self)
        {
            var template = self.Objects[0].GetTemplate(false);
            var buff = new NBuffer(template.GetSize());
            template.Save(buff);
            return buff.ToArray();
        }

        public static NFPosition ToNFPosition(this FingerCode code)
        {
            switch (code)
            {
                case FingerCode.L1:
                    return NFPosition.LeftThumb;
                case FingerCode.L2:
                    return NFPosition.LeftIndexFinger;
                case FingerCode.L3:
                    return NFPosition.LeftMiddleFinger;
                case FingerCode.L4:
                    return NFPosition.LeftRingFinger;
                case FingerCode.L5:
                    return NFPosition.LeftLittleFinger;
                case FingerCode.R1:
                    return NFPosition.RightThumb;
                case FingerCode.R2:
                    return NFPosition.RightIndexFinger;
                case FingerCode.R3:
                    return NFPosition.RightMiddleFinger;
                case FingerCode.R4:
                    return NFPosition.RightRingFinger;
                case FingerCode.R5:
                    return NFPosition.RightLittleFinger;
                default:
                    throw new ArgumentOutOfRangeException("code", code, "Unexpected code " + code);
            }
        }

        public static FingerCode ToFingerCode(this NFPosition code)
        {
            switch (code)
            {
                case NFPosition.LeftThumb:
                    return FingerCode.L1;
                case NFPosition.LeftIndexFinger:
                    return FingerCode.L2;
                case NFPosition.LeftMiddleFinger:
                    return FingerCode.L3;
                case NFPosition.LeftRingFinger:
                    return FingerCode.L4;
                case NFPosition.LeftLittleFinger:
                    return FingerCode.L5;
                case NFPosition.RightThumb:
                    return FingerCode.R1;
                case NFPosition.RightIndexFinger:
                    return FingerCode.R2;
                case NFPosition.RightMiddleFinger:
                    return FingerCode.R3;
                case NFPosition.RightRingFinger:
                    return FingerCode.R4;
                case NFPosition.RightLittleFinger:
                    return FingerCode.R5;
                default:
                    throw new ArgumentOutOfRangeException("code", code, "Unexpected position " + code);
            }
        }

        public static string Describe(this NSubject self)
        {
            return $"{self.Id} - {self.QueryString}: {self.Status}\r\n"
                   + self.Fingers.Select(x => "\t" + x.Describe()).Join("\r\n");
        }

        public static string Describe(this NFinger self)
        {
            return $"{self.Position} ({self.BiometricType}, {self.Error})";
        }
    }
}