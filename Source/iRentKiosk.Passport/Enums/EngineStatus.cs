﻿namespace iRentKiosk.Passport
{
    public enum EngineStatus
    {
        EngineLoaded = 0,
        EngineFailedToLoad = 1,
        EngineInitialized= 2,
        EngineFailedToInitialise = 3,
        ScanSizeInvalid = 4,
        ImageAcquisitionFailure = 5,
        ScanSuccess = 6,
        InvalidCardSize = 7,
        RecognitionFailure = 8,
        RecognitionSuccess = 9,
        FailedSavingImage = 10,
        ImageSaved = 11,
        HeaderImageSaved = 12,
        FailedSaveImageHeader = 13,
        Complete = 14,
        Failed = 15,
        GrabSignalTypeFailed = 16
    }
}