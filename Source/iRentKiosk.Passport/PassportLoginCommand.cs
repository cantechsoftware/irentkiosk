﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CanTech.Common.Extensions;
using Castle.Core;
using iRent.Framework;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Util;

namespace iRentKiosk.Passport
{
    [CastleComponent]
    public class PassportLoginCommand : BaseCommand
    {
        private readonly Func<PassportLoginControl> _factory;

        public string NotRegistered { get; set; }
        public string ExistingCard { get; set; }
        public string Registered { get; set; }
        public string Back { get; set; }

        public PassportLoginCommand(Func<PassportLoginControl> factory)
        {
            _factory = factory;
        }
        
        public override void Invoke()
        {
            var control = _factory();
            control.ClearControls();
            control.Next += OnNext;
            control.Back += OnBack;

            IMainForm main = (IMainForm)MainForm;
            main.AddToFormPanel(control);
        }

        private void OnBack(object sender, DriverModel driver)
        {
            Proceed(Back);
        }

        private void OnNext(object sender, DriverModel driver)
        {
            ContextData.SetDriver(driver);
            if ((driver.Licence == null) || !driver.IsRegistered)
                Proceed(NotRegistered);
            else if (ExistingCard.HasValue() && (driver.RfId != null))
                Proceed(ExistingCard);
            else
                Proceed(Registered);
        }
    }
}
