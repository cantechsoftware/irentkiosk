﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CanTech.Common.Extensions;
using Castle.Core;
using iRent2.Contracts.Drivers;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Models;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;

namespace iRentKiosk.Passport
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class PassportLoginControl : UserControl
    {
        private readonly RecognitionEngine _engine;
        private readonly DriverApi _driverApi;

        public string Name => txtName.Text;
        public string PassportNumber => txtPassportNumber.Text;
        public string BDate => txtBDate.Text;
        public string Country => txtCountry.Text;
        public string Expirydate => txtExpiryDate.Text;
        public string IssueDate => txtIssueDate.Text;
        public string IdNumber => txtIDNumber.Text;
        private string _nationalName;
        private string _sex;
        private string _englishSurname;
        private string _englishFirstName;
        private string _mrz1;
        private string _mrz2;
        private string _nationalityCode;
        private string _placeOfBirth;
        private string _placeOfIssue;
        private string _rfidMrz;
        private string _ocrMrz;
     
        public PassportLoginControl(RecognitionEngine engine, DriverApi driverApi)
        {
            _engine = engine;
            _driverApi = driverApi;
            InitializeComponent();
        }

        private void btnRetry_Click(object sender, EventArgs e)
        {
            _engine.LoadDll();
            var result = _engine.Process("21");
            
            txtName.Text = result.Status.ToString();
            if (result.Result != null)
                PopulateForm(result);
            btnNext.Enabled = true;
        }

        private void PopulateForm(Response result)
        {
            txtName.Text = result.Result["English name"];
            txtBDate.Text = result.Result["Date of birth"];
            txtCountry.Text = result.Result["Issuing country code"];
            txtExpiryDate.Text = result.Result["Date of expiry"];
            txtIDNumber.Text = result.Result["ID Number"];
            txtIssueDate.Text = result.Result["Date of issue (only PRC)"];
            txtPassportNumber.Text = result.Result["Passport number"];

            _englishFirstName = result.Result["English first name"];
            _englishSurname = result.Result["English surname"];
            _sex = result.Result["Sex"];
            _mrz1 = result.Result["MRZ1"];
            _mrz2 = result.Result["MRZ2"];
            _nationalName = result.Result["National name"];
            _nationalityCode = result.Result["Nationality code"];
            _placeOfBirth = result.Result["Place of birth (only PRC)"];
            _placeOfIssue = result.Result["Place of issue (only PRC)"];
            _rfidMrz = result.Result["RFID MRZ"];
            _ocrMrz = result.Result["OCR MRZ"];

            using (var ms = new MemoryStream(_engine.ImageHead ))
                pictureBox1.Image= Image.FromStream(ms);
            
            pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
         }

        public event EventHandler<DriverModel> Next;
        public event EventHandler<DriverModel> Back;

        protected override void OnLoad(EventArgs e)
        {
            var result = _engine.Process("21");
            lblResponse.Text = result.Status.ToString();
            btnBack.Enabled = true;
            if (result.Result != null)
            {
                PopulateForm(result);
                btnNext.Enabled = true;
            }
        }

        public void ClearControls()
        {
            txtName.Text = "";
            txtPassportNumber.Text = "";
            txtBDate.Text = "";
            txtCountry.Text = "";
            txtExpiryDate.Text = "";
            txtIssueDate.Text = "";
            txtIDNumber.Text = "";
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            var request = new SavePassportRequest
            {
                Country = Country,
                EnglishFirstName = _englishFirstName,
                EnglishSurname = _englishSurname,
                IdNumber = IdNumber,
                Mrz1 = _mrz1,
                Mrz2 = _mrz2,
                NationalName = _nationalName,
                NationalityCode = _nationalityCode,
                OcrMrz = _ocrMrz,
                PassportNumber = PassportNumber,
                PlaceOfBirth = _placeOfBirth,
                RfidMrz = _rfidMrz,
                Sex = _sex,
                PlaceOfIssue = _placeOfIssue,
                Name = Name,
                Photo = _engine.Image,
                PhotoHead =_engine.ImageHead,
                PhotoIr =_engine.ImageIr,
                PhotoUv = _engine.ImageUv
            };
            DateTime expiryDate;
            if (DateTime.TryParse(Expirydate, out expiryDate))
                request.Expirydate = expiryDate;

            DateTime birthDate;
            if (DateTime.TryParse(BDate, out birthDate))
                request.BirthDate = birthDate ;

            DateTime issueDate;
            if (DateTime.TryParse(IssueDate , out issueDate))
                request.IssueDate = issueDate;
            
            var model = _driverApi.SavePassport(request);
            Next(this, model);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            Back(this, null);
        }
    }
}
