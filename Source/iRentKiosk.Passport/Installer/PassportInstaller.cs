﻿using CanTech.Common.Extensions;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace iRentKiosk.Passport.Installer
{
    public class PassportInstaller:IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.RegisterComponents<PassportInstaller>();
        }
    }
}