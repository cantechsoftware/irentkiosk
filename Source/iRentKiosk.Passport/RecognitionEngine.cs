﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Castle.Core;
using iRent.Login;
using iRentKiosk.Core;

namespace iRentKiosk.Passport
{
    [CastleComponent(Lifestyle = LifestyleType.Singleton)]
    public class RecognitionEngine
    {
        private  bool _idCardLoaded = false;
        private Dictionary<string,string> _recognitionResult;
        public string UserId { get; set; }
        public byte[] Image { get; set; }
        public byte[] ImageHead { get; set; }
        public byte[] ImageIr { get; set; }
        public byte[] ImageUv { get; set; }

        public RecognitionEngine(string userId)
        {
            UserId = userId;
            LoadDll();
        }

        public Response Process(string scannedText)
        {
            if(!_idCardLoaded)
                LoadDll();
            var res = ScanAndRecognize(scannedText);
            ReleaseIdCard();
            return res;
        }

        public void ReleaseIdCard()
        {
            if (!_idCardLoaded) return;
            Dlls.FreeIDCard();
            _idCardLoaded = false;
        }

        public void LoadDll()
        {
            var ret = LoadDll(UserId.ToCharArray());
            if (ret.Status != EngineStatus.EngineInitialized)
                throw new ApplicationException($"Engine failed to initialize Finger Print recognition engine : {ret.Status}");
        }

        #region Private

        private Response LoadDll(char[] userId)
        {
            try
            {
                var dict = new Dictionary<string, string>();
                if (_idCardLoaded)
                    return   new Response { Result =null, Status = EngineStatus.EngineInitialized };

                var res = Dlls.LoadLibrary("IDCard");
                if (res == 0)
                {
                    dict.Add("Error", Marshal.GetLastWin32Error().ToString());
                    return new Response
                    {
                        Result = dict,
                        Status = EngineStatus.EngineFailedToLoad
                    };
                }

                var initCard = Dlls.InitIDCard(userId, 0, null);
                if (initCard != 0)
                {
                    dict.Add("Error", Marshal.GetLastWin32Error().ToString());
                    return new Response
                    {
                        Result= dict,
                        Status = EngineStatus.EngineFailedToInitialise
                    };
                }

                Dlls.SetSpecialAttribute(1, 1);
                _idCardLoaded = true;
                return new Response { Result = null, Status = EngineStatus.EngineInitialized };
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }

        private Response ScanAndRecognize(string scannedText)
        {
            if (!_idCardLoaded)
                new Response { Result = null, Status = EngineStatus.EngineFailedToLoad };

            var res = Scan(scannedText);
            if (res != EngineStatus.ScanSuccess)
                new Response { Result =null, Status = res };

            var recogRes = Recognize(scannedText);
            if (recogRes != EngineStatus.RecognitionSuccess)
                new Response { Result = _recognitionResult, Status = recogRes };

            var saveRes = SaveImage();
            if (saveRes != EngineStatus.ImageSaved)
                new Response { Result = _recognitionResult, Status = saveRes };

            return new Response { Result = _recognitionResult, Status = EngineStatus.Complete }; ;
        }

        private EngineStatus SaveHeaderImage(string strRunPath)
        {
            var strHeadPath = strRunPath + "test_Head.jpg";
            var carrHeadPath = strHeadPath.ToCharArray();
            var res = Dlls.SaveHeadImage(carrHeadPath);
            if (res != 0) return  EngineStatus.FailedSaveImageHeader;

            ImageHead = StoreImageInternally(strHeadPath);

            return EngineStatus.HeaderImageSaved;
        }

        private EngineStatus Scan(string scannedText)
        {
            var scanSizeType = int.Parse(scannedText);

            if (int.Parse(scannedText) <= 0)
                return EngineStatus.ScanSizeInvalid;

            var nRet = Dlls.AcquireImage(scanSizeType);
            return nRet != 0 ? EngineStatus.ImageAcquisitionFailure : EngineStatus.ScanSuccess;
        }

        private EngineStatus Recognize(string scannedText)
        {
            var cardType = int.Parse(scannedText);
            if (cardType <= 0)
                return EngineStatus.InvalidCardSize;

            var subId = new int[1];
            subId[0] = 0;
            Dlls.SetIDCardID(cardType, subId, 1);
            Dlls.AddIDCardID(13, subId, 1);

            if (Dlls.RecogIDCard() <= 0)
                return EngineStatus.RecognitionFailure;

            var maxChNum1 = 128;
            var maxChNum2 = 128;
            var arrFieldValue = new char[maxChNum1];
            var arrFieldName = new char[maxChNum2];
            var dictionary = new Dictionary<string, string>();
            for (int i = 1; ; i++)
            {
                if(Dlls.GetRecogResult(i, arrFieldValue, ref maxChNum1) == 3)
                    break;
                
                Dlls.GetFieldName(i, arrFieldName, ref maxChNum2);

                var fieldName = new string(arrFieldName, 0, Array.IndexOf(arrFieldName, '\0'));
                var fieldValue = new string(arrFieldValue, 0, Array.IndexOf(arrFieldValue, '\0'));
                dictionary[fieldName] = fieldValue;                
            }
            if(dictionary != null)
            {
                string idnumber = "";
                dictionary.TryGetValue("Nationality code", out idnumber);
                if(idnumber == "ZAF")
                {
                    dictionary.TryGetValue("MRZ2", out idnumber);
                    idnumber = idnumber.Substring(idnumber.Length - 16, 13);
                    dictionary["ID Number"] = idnumber;
                }
            }
            _recognitionResult = dictionary;
            return EngineStatus.RecognitionSuccess;
        }

        private EngineStatus SaveImage()
        {
            var strRunPath = System.Windows.Forms.Application.StartupPath + "\\";
            var strImgPath = strRunPath + "test.jpg";
            var strImgIrPath = strRunPath + "testIR.jpg";
            var strImgUvPath = strRunPath + "testUV.jpg";
            var carrImgPath = strImgPath.ToCharArray();

            var nRet = Dlls.SaveImage(carrImgPath);

            if (nRet != 0) return EngineStatus.FailedSavingImage;

            Image = StoreImageInternally(strImgPath);
          
            var res = SaveHeaderImage(strRunPath);

            ImageIr = StoreImageInternally(strImgIrPath);
            ImageUv = StoreImageInternally(strImgUvPath);

            return res != EngineStatus.HeaderImageSaved ? res : EngineStatus.ImageSaved;
        }

        private byte[] StoreImageInternally(string imagePath)
        {
            var file = imagePath;
            var bytes = File.ReadAllBytes(file);
            File.Delete(imagePath);
            return bytes;
        }

        #endregion
    }

    public class Response
    {
        public Dictionary<string,string> Result { get; set; }
        public EngineStatus Status { get; set; }
    }
}