﻿namespace iRent
{
    partial class CancomOTP
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtOTP = new MetroFramework.Controls.MetroTextBox();
            this.btnOK = new MetroFramework.Controls.MetroButton();
            this.btnResend = new MetroFramework.Controls.MetroButton();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.lblStatus = new MetroFramework.Controls.MetroLabel();
            this.SuspendLayout();
            // 
            // txtOTP
            // 
            this.txtOTP.FontSize = MetroFramework.MetroTextBoxSize.XL;
            this.txtOTP.Lines = new string[0];
            this.txtOTP.Location = new System.Drawing.Point(158, 77);
            this.txtOTP.MaxLength = 32767;
            this.txtOTP.Name = "txtOTP";
            this.txtOTP.PasswordChar = '\0';
            this.txtOTP.PromptText = "OTP";
            this.txtOTP.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtOTP.SelectedText = "";
            this.txtOTP.Size = new System.Drawing.Size(283, 30);
            this.txtOTP.TabIndex = 0;
            this.txtOTP.UseSelectable = true;
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnOK.Highlight = true;
            this.btnOK.Location = new System.Drawing.Point(419, 163);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(177, 80);
            this.btnOK.Style = MetroFramework.MetroColorStyle.Red;
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "&OK";
            this.btnOK.UseSelectable = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnResend
            // 
            this.btnResend.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnResend.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnResend.Highlight = true;
            this.btnResend.Location = new System.Drawing.Point(23, 163);
            this.btnResend.Name = "btnResend";
            this.btnResend.Size = new System.Drawing.Size(178, 80);
            this.btnResend.Style = MetroFramework.MetroColorStyle.Red;
            this.btnResend.TabIndex = 2;
            this.btnResend.Text = "&Resend";
            this.btnResend.UseSelectable = true;
            this.btnResend.Click += new System.EventHandler(this.btnResend_Click);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.XL;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel1.Location = new System.Drawing.Point(10, 10);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(573, 30);
            this.metroLabel1.TabIndex = 3;
            this.metroLabel1.Text = "Please enter the One-Time-Pin sent to your cellphone.";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblStatus.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblStatus.Location = new System.Drawing.Point(59, 130);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 0);
            this.lblStatus.TabIndex = 4;
            // 
            // CancomOTP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(619, 259);
            this.ControlBox = false;
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.btnResend);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.txtOTP);
            this.Name = "CancomOTP";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Style = MetroFramework.MetroColorStyle.Red;
            this.TopMost = true;
            this.Load += new System.EventHandler(this.CancomOTP_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroTextBox txtOTP;
        private MetroFramework.Controls.MetroButton btnOK;
        private MetroFramework.Controls.MetroButton btnResend;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel lblStatus;
    }
}
