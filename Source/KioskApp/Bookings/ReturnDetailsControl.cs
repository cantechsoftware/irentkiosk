﻿using System;
using System.Linq;
using System.Threading.Tasks;
using CanTech.Common.Extensions;
using Castle.Core;
using MetroFramework.Controls;
using iRent.Extensions;
using iRent.Framework;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;

namespace iRent.Bookings
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class ReturnDetailsControl : MetroUserControl
    {
        private readonly BranchApi _branchApi;

        public ReturnDetailsControl(BranchApi branchApi)
        {
            _branchApi = branchApi;
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            cmbDate.DisplayMember = "Text";
            cmbDate.ValueMember = "Date";
            cmbDate.DataSource = Enumerable.Range(1, 60)
                                           .Select(x => DateTime.Today.AddDays(x))
                                           .Select(x => new {Date = x, Text = x.ToString("ddd d MMM yyyy")})
                                           .ToList();            
            Task.Run(() => LoadProvinces());
        }
        
        private string DefaultProvince()
        {
            if (ContextData.ReturnBranch == null)
                return ContextData.Kiosk.Branch.Province;

            return ContextData.ReturnBranch.Province;
        }

        private void LoadProvinces()
        {
            try
            {
                var provinces = _branchApi.GetProvinces();
                cmbProvince.InvokeIfRequired(x =>
                    {
                        x.DataSource = provinces;
                        x.SelectedItem = DefaultProvince();
                    });
            }
            catch (Exception ex)
            {
                Logger.Log(this, "LoadProvinces", "Error loading provinces", ex);
                Errors.Notify("Error loading provinces", ex);
            }
        }

        public event EventHandler Next;
        public event EventHandler Back;

        private string ValidateInput()
        {
            if (cmbDate.SelectedValue == null)
                return "Please select a valid return date";

            if (cmbHour.SelectedItem == null || cmbMinute.SelectedItem == null)
                return "Please select a valid return time";

            if (cmbProvince.SelectedItem == null)
                return "Please select a return province";

            if (cmbBranch.SelectedItem == null)
                return "Please select a return branch";

            return null;
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            var error = ValidateInput();
            if (error.HasValue())
            {
                lblStatus.Text = error;
                return;
            }

            var branch = (BranchModel)cmbBranch.SelectedItem;
            ContextData.ReturnBranch = branch;
            ContextData.Booking.ReturnBranch = branch;
            var date = ((DateTime)cmbDate.SelectedValue);
            date = date.AddHours(int.Parse(cmbHour.Text)).AddMinutes(int.Parse(cmbMinute.Text));
            ContextData.VehicleReturnDate = date;
            ContextData.Booking.ReturnDate = date;

            if (Next != null)
                Next(this, e);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            if (Back != null)
                Back(this, e);
        }

        public void ToggleEnabled(bool enabled)
        {
            cmbDate.Enabled = enabled;
            cmbHour.Enabled = enabled;
            cmbMinute.Enabled = enabled;
            cmbProvince.Enabled = enabled;
            cmbBranch.Enabled = enabled;
        }

        private void cmbProvince_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbProvince.SelectedItem != null)
            {
                var province = (string) cmbProvince.SelectedItem;
                Task.Run(() => LoadBranches(province));
            }
        }

        private void LoadBranches(string province)
        {
            try
            {
                var branches = _branchApi.GetBranches(province);
                cmbBranch.InvokeIfRequired(o =>
                    {
                        if (cmbProvince.SelectedItem == province)
                        {
                            cmbBranch.DataSource = branches;
                            var branchId = InitialBranchId();
                            cmbBranch.SelectedItem = branches.FirstOrDefault(x => x.Id == branchId);
                        }
                    });
            }
            catch (Exception ex)
            {
                Logger.Log(this, "LoadBranches", "Error loading branches", ex);
                Errors.Notify("Error loading branches", ex);
            }
        }

        private int? InitialBranchId()
        {
            if (ContextData.ReturnBranch != null)
                return ContextData.ReturnBranch.Id;

            return ContextData.Kiosk.BranchId;
        }

        public void Setup(BookingModel booking, string title)
        {
            lblTitle.Text = title;
            ToggleEnabled(booking == null
                          || booking.PaymentMethod == null
                          || booking.PaymentMethod.CanChangeReturn);
            if (booking != null)
            {
                cmbDate.SelectedValue = booking.ReturnDate.Date;
                cmbHour.SelectedItem = booking.ReturnDate.Hour.ToString("00");
                cmbMinute.SelectedItem = booking.ReturnDate.Minute.ToString("00");
            }
            else
            {
                cmbDate.SelectedIndex = 1;
                cmbHour.SelectedItem = "12";
                cmbMinute.SelectedItem = "00";
            }
        }
    }
}