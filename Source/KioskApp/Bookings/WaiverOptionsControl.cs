﻿using System;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Castle.Core;
using MetroFramework;
using MetroFramework.Controls;
using SmartFormat;
using iRent.Extensions;
using iRent.Framework;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;

namespace iRent.Bookings
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class WaiverOptionsControl : MetroUserControl
    {
        private readonly BookingApi _bookingApi;

        public WaiverOptionsControl(BookingApi bookingApi)
        {
            _bookingApi = bookingApi;
            InitializeComponent();
        }

        public WaiverOptionModel SelectedOption { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            btnNext.Enabled = false;
            Task.Factory.StartNew(LoadOptions);
        }

        private void ListOptions(WaiverOptionModel[] options)
        {
            foreach (var v in options)
            {
                var button = new MetroButton();
                button.Click += button_Click;
                var lbl = new MetroLabel
                {
                    BackColor = Color.Transparent,
                    UseCustomBackColor = true,
                    Dock = DockStyle.Top,
                    Text = v.Name,
                    TextAlign = ContentAlignment.TopCenter,
                    FontSize = MetroLabelSize.XL,
                    FontWeight = MetroLabelWeight.Bold,
                    Margin = new Padding(10),
                    Height = 30
                };
                button.Controls.Add(lbl);
                button.Text = Smart.Format("{Description}\r\nCost R {Cost:0.00}, Excess R {Excess:0.00}", v);
                button.Tag = v;
                button.Height = pnlOptions.Height/3 - 10;
                button.Width = pnlOptions.Width - 20;
                button.Dock = DockStyle.Top;
                button.FontSize = MetroButtonSize.Medium;
                button.FontWeight = MetroButtonWeight.Regular;
                button.Enabled = CanChange;
                pnlOptions.Controls.Add(button);
            }
        }

        private void LoadOptions()
        {
            try
            {
                var options = _bookingApi.GetWaiverOptions(ContextData.Booking.BillingModelId);
                this.InvokeIfRequired(x => ListOptions(options));
            }
            catch (Exception ex)
            {
                Logger.Log(this, "LoadOptions", "Error loading waiver options", ex);
            }
        }

        private void button_Click(object sender, EventArgs e)
        {
            var button = (MetroButton) sender;
            SelectedOption = (WaiverOptionModel) button.Tag;
            lblStatus.Text = Smart.Format(
                "You have selected the following Waiver Option: {Name} (R {Cost:0.00}, Excess: R {Excess:0.00}).\r\nPlease click Next to continue",
                SelectedOption);
            btnNext.Enabled = true;
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (Next != null)
                Next(this, SelectedOption);
        }

        public event EventHandler<WaiverOptionModel> Next;

        public void Init(WaiverOptionModel waiverOption, bool canChange)
        {
            SelectedOption = waiverOption;
            if (waiverOption != null)
            {
                lblStatus.Text =
                    Smart.Format("You have selected {Name} (R {Cost:0.00}, Excess: R {Excess:0.00}) on reservation - would you like to change this option?",
                        waiverOption);

                if (!canChange)
                    lblStatus.Text = Smart.Format(
                        "You have selected {Name} (R {Cost:0.00}, Excess: R {Excess:0.00}) on reservation.\r\nPlease click Next to continue",
                        SelectedOption);
                btnNext.Enabled = true;
            }

            CanChange = canChange;
        }

        public bool CanChange { get; set; }
    }
}