﻿using System;
using System.Windows.Forms;
using SmartFormat;
using iRent2.Contracts.Models;

namespace iRent.Bookings
{
    public partial class AdditionalDriverRow : UserControl
    {
        public AdditionalDriverRow(DriverModel model)
        {
            InitializeComponent();
            Driver = model;
            RefreshDriver();
        }

        public DriverModel Driver { get; private set; }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (Edit != null)
                Edit(this, e);
        }

        public event EventHandler Edit;
        public event EventHandler Remove;

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (Remove != null)
                Remove(this, e);
        }

        public void RefreshDriver()
        {
            lblInfo.Text = Smart.Format("{FirstName} {Surname} - {IdNumber} ({DateOfBirth:yyyy/MM/dd})", Driver);
            btnEdit.Enabled = Driver.Id > 0;
        }
    }
}