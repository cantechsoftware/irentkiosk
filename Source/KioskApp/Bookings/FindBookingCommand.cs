﻿using System;
using System.Linq;
using CanTech.Common.Extensions;
using Castle.Core;
using iRent.Framework;
using iRent2.Contracts.Bookings;
using iRent2.Contracts.Enums;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;

namespace iRent.Bookings
{
    [CastleComponent]
    public class FindBookingCommand : BaseCommand
    {
        private readonly BookingApi _bookingApi;
        private readonly VehicleApi _vehicleApi;

        public FindBookingCommand(BookingApi bookingApi, VehicleApi vehicleApi)
        {
            _bookingApi = bookingApi;
            _vehicleApi = vehicleApi;
        }

        public string BookingNumber { get; set; }
        public string Valid { get; set; }
        public string Invalid { get; set; }

        public override void Invoke()
        {
            var booking = GetBooking();

            ContextData.SetBooking(booking);
            if (IsValid(booking))
            {
                if (booking.VehicleId.HasValue)
                    ContextData.Set(_vehicleApi.GetById(booking.VehicleId.Value));
                ContextData.VehicleReturnDate = booking.ReturnDate;
                Proceed(Valid);
            }
            else
                Proceed(Invalid);
        }

        private BookingModel GetBooking()
        {
            if (BookingNumber.HasValue())
            {
                var bookingId = BookingNumber.ToInt();
                var booking = _bookingApi.GetBooking(bookingId);
                return booking;
            }

            if (ContextData.Driver != null)
            {
                var bookings = _bookingApi.FindBookings(new SearchBookingsRequest
                {
                    DriverId = ContextData.Driver.Id,
                    Statuses = new[] {BookingStatus.VehicleAssigned, BookingStatus.Active}
                });

                return bookings.OrderBy(x => x.PickupDate).FirstOrDefault();
            }

            return null;
        }

        private bool IsValid(BookingModel booking)
        {
            if (booking == null)
                return false;

            if ((ContextData.Driver != null) && (booking.DriverId != ContextData.Driver.Id))
                return false;

            if (!booking.Status.In(BookingStatus.VehicleAssigned, BookingStatus.Active))
                return false;

            if (booking.PickupDate > DateTime.Now)
                return false;

            return true;
        }
    }
}