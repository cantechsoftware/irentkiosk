﻿using System;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Castle.Core;
using GalaSoft.MvvmLight.Messaging;
using MetroFramework.Controls;
using iRent.Classes;
using iRent.Extensions;
using iRent2.Contracts.Enums;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Services;
using CanTech.Common.Extensions;
using iRentKiosk.Core.Models;

namespace iRent.Bookings
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class AdditionalDriversControl : MetroUserControl
    {
        private readonly CountryService _countryService;
        private int _allowedDrivers;

        public AdditionalDriversControl(CountryService countryService)
        {
            _countryService = countryService;
            InitializeComponent();
            Messenger.Default.Register<NotificationMessage<ScannerMessageItem>>(
                this,
                up =>
                {
                    if (up.Content.msg.ToString() == "driver")
                        this.InvokeIfRequired(x => AddDriver(up.Content.Driver));
                });
        }

        protected DriverModel CurrentDriver { get; set; }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (Next != null)
                Next(this, pnlDrivers.Controls.OfType<AdditionalDriverRow>()
                                     .Select(x => x.Driver).ToArray());
        }

        public event EventHandler<DriverModel[]> Next;
        public event EventHandler Back;

        public void Setup(int numAllowed, DriverModel[] additionalDrivers)
        {
            _allowedDrivers = numAllowed;
            if (additionalDrivers != null)
                foreach(var drv in additionalDrivers)
                    Bind(drv);
            Reset();            
        }

        private void Reset()
        {
            lblStatus.Text = null;
            lblStatus.ForeColor = Color.Black;
            CurrentDriver = null;
            txtFirstName.Text = null;
            txtLastName.Text = null;
            txtIdNumber.Text = null;
            txtDateOfBirth.Text = null;
            btnAdd.Text = "&Add";
            ToggleEnabled(pnlDrivers.Controls.Count < _allowedDrivers);
            if (pnlDrivers.Controls.Count < _allowedDrivers)
                lblStatus.Text = "Scan licence or fill in details to add additional drivers.";
            else
                lblStatus.Text = "Maximum number of additional drivers reached";
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var err = ValidateInput();
            if (err != null)
            {
                lblStatus.Text = err;
                lblStatus.ForeColor = Color.Red;
                return;
            }

            var drv = CurrentDriver ??
                      new DriverModel {IdType = IdType.ID, IdCountry = _countryService.DefaultCode};
            drv.FirstName = txtFirstName.Text;
            drv.Surname = txtLastName.Text;
            drv.IdNumber = txtIdNumber.Text;
            drv.DateOfBirth = DateTime.ParseExact(txtDateOfBirth.Text, "dd/MM/yyyy", null);
            AddDriver(drv);
        }

        private void AddDriver(DriverModel drv)
        {            
            var row = pnlDrivers.Controls.OfType<AdditionalDriverRow>().FirstOrDefault(x => x.Driver == drv);
            if (row == null)
            {
                if (pnlDrivers.Controls.Count >= _allowedDrivers)
                {
                    lblStatus.Text = "You have reached the maximum number of drivers allowed";
                    lblStatus.ForeColor = Color.Red;
                    return;
                }
                
                if (drv.IdNumber == ContextData.DriverID.IdNumber)
                {
                    lblStatus.Text = "You are already the main driver for this rental.";
                    lblStatus.ForeColor = Color.Red;
                    return;
                }
                
                Bind(drv);
            }
            else
                row.RefreshDriver();
            Reset();            
        }

        private string ValidateInput()
        {
            if (txtIdNumber.Text.IsEmpty())
                return "Please enter the driver's ID number";
            if (txtFirstName.Text.IsEmpty())
                return "Please enter the driver's first name";
            if (txtLastName.Text.IsEmpty())
                return "Please enter the driver's last name";
            if (txtDateOfBirth.Text.IsEmpty())
                return "Please enter the driver's date of birth";
            DateTime dob;
            if (!DateTime.TryParseExact(txtDateOfBirth.Text, "dd/MM/yyyy",
                                        null, DateTimeStyles.AllowWhiteSpaces,
                                        out dob))
                return "Pleae enter a valid date of birth";

            if (dob > DateTime.Today.AddYears(-18))
                return "Driver must be at least 18 years old";
            return null;
        }

        private void Bind(DriverModel driver)
        {
            var row = new AdditionalDriverRow(driver) {Dock = DockStyle.Top, Width = pnlDrivers.Width};
            pnlDrivers.Controls.Add(row);
            row.Edit += (o, e) => Edit(driver);
            row.Remove += (o, e) => pnlDrivers.Controls.Remove(row);
        }

        private void Edit(DriverModel driver)
        {
            CurrentDriver = driver;
            txtFirstName.Text = driver.FirstName;
            txtLastName.Text = driver.Surname;
            txtIdNumber.Text = driver.IdNumber;
            txtDateOfBirth.Text = driver.DateOfBirth.Value.ToString("dd/MM/yyyy");
            btnAdd.Text = "&Save";
            ToggleEnabled(true);
        }

        private void ToggleEnabled(bool enabled)
        {
            txtFirstName.Enabled = enabled;
            txtLastName.Enabled = enabled;
            txtIdNumber.Enabled = enabled;
            txtDateOfBirth.Enabled = enabled;
            btnAdd.Enabled = enabled;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            if (Back != null)
                Back(this, e);
        }

        protected override void Dispose(bool disposing)
        {
            Messenger.Default.Unregister(this);
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}