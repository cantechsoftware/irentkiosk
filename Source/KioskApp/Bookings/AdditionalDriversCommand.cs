﻿using System;
using System.Windows.Forms;
using Castle.Core;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Models;
using iRentKiosk.Core.Services;

namespace iRent.Bookings
{
    [CastleComponent]
    public class AdditionalDriversCommand : ICommand
    {
        private readonly Func<AdditionalDriversControl> _factory;
        private MainForm _form;
        private readonly WorkflowService _workflowService;

        public AdditionalDriversCommand(Func<AdditionalDriversControl> factory,
                                        MainForm form,
                                        WorkflowService workflowService)
        {
            _factory = factory;
            _form = form;
            _workflowService = workflowService;
        }

        public string Next { get; set; }
        public string Back { get; set; }
        public Form MainForm
        {
            get { return _form; }
            set { _form = (MainForm)value; }
        }

        public void Invoke()
        {
            var control = _factory();
            var booking = ContextData.Booking;
            var numAllowed = 0;
            if (booking != null && booking.PaymentMethod != null)
                numAllowed = booking.PaymentMethod.AdditionalDrivers;
            control.Setup(numAllowed, booking.AdditionalDrivers);
            _form.AddToFormPanel(control);
            control.Next += OnNext;
            control.Back += OnBack;
        }

        private void OnBack(object sender, EventArgs e)
        {
            _workflowService.Execute(Back);
        }

        private void OnNext(object sender, DriverModel[] driverModels)
        {
            ContextData.Booking.AdditionalDrivers = driverModels;
            _workflowService.Execute(Next);
        }
    }
}