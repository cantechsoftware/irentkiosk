﻿using System;
using Castle.Core;
using iRent.Framework;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Services;

namespace iRent.Bookings
{
    [CastleComponent]
    public class WaiverOptionsCommand : BaseCommand
    {
        private readonly Func<WaiverOptionsControl> _factory;

        public WaiverOptionsCommand(Func<WaiverOptionsControl> factory)
        {
            _factory = factory;
        }

        public string Next { get; set; }

        public override void Invoke()
        {
            var control = _factory();
            var main = GetFormAs<MainForm>();
            main.AddToFormPanel(control);
            control.Init(ContextData.Booking.WaiverOption, ContextData.Booking.PaymentMethod.CanChangeWaiver);
            control.Next += OnNext;
        }

        private void OnNext(object sender, WaiverOptionModel e)
        {
            ContextData.Booking.WaiverOption = e;
            Proceed(Next);
        }
    }
}