﻿namespace iRent.Bookings
{
    partial class ReturnDetailsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.cmbDate = new MetroFramework.Controls.MetroComboBox();
            this.cmbBranch = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.lblTitle = new MetroFramework.Controls.MetroLabel();
            this.lblDate = new MetroFramework.Controls.MetroLabel();
            this.lblStatus = new MetroFramework.Controls.MetroLabel();
            this.btnBack = new MetroFramework.Controls.MetroButton();
            this.btnNext = new MetroFramework.Controls.MetroButton();
            this.lblTime = new MetroFramework.Controls.MetroLabel();
            this.cmbHour = new MetroFramework.Controls.MetroComboBox();
            this.cmbMinute = new MetroFramework.Controls.MetroComboBox();
            this.cmbProvince = new MetroFramework.Controls.MetroComboBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 201F));
            this.tableLayoutPanel1.Controls.Add(this.cmbDate, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.cmbBranch, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.metroLabel3, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.metroLabel2, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.lblTitle, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblDate, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblStatus, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.btnBack, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.btnNext, 3, 9);
            this.tableLayoutPanel1.Controls.Add(this.lblTime, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.cmbHour, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.cmbMinute, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.cmbProvince, 1, 5);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 10;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(591, 539);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // cmbDate
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.cmbDate, 2);
            this.cmbDate.FontSize = MetroFramework.MetroComboBoxSize.Tall;
            this.cmbDate.FormattingEnabled = true;
            this.cmbDate.ItemHeight = 29;
            this.cmbDate.Location = new System.Drawing.Point(203, 103);
            this.cmbDate.Name = "cmbDate";
            this.cmbDate.Size = new System.Drawing.Size(184, 35);
            this.cmbDate.TabIndex = 0;
            this.cmbDate.UseSelectable = true;
            // 
            // cmbBranch
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.cmbBranch, 2);
            this.cmbBranch.DisplayMember = "BranchName";
            this.cmbBranch.FontSize = MetroFramework.MetroComboBoxSize.Tall;
            this.cmbBranch.FormattingEnabled = true;
            this.cmbBranch.ItemHeight = 29;
            this.cmbBranch.Location = new System.Drawing.Point(203, 303);
            this.cmbBranch.Name = "cmbBranch";
            this.cmbBranch.Size = new System.Drawing.Size(184, 35);
            this.cmbBranch.TabIndex = 4;
            this.cmbBranch.UseSelectable = true;
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel3.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel3.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel3.Location = new System.Drawing.Point(3, 300);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(194, 50);
            this.metroLabel3.TabIndex = 15;
            this.metroLabel3.Text = "Return Branch";
            this.metroLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel2.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel2.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel2.Location = new System.Drawing.Point(3, 250);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(194, 50);
            this.metroLabel2.TabIndex = 13;
            this.metroLabel2.Text = "Return Province";
            this.metroLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // metroLabel1
            // 
            this.lblTitle.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.lblTitle, 4);
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTitle.FontSize = MetroFramework.MetroLabelSize.XL;
            this.lblTitle.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblTitle.Location = new System.Drawing.Point(3, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(585, 50);
            this.lblTitle.TabIndex = 3;
            this.lblTitle.Text = "Please confirm Return Date and Time";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDate.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblDate.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblDate.Location = new System.Drawing.Point(3, 100);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(194, 50);
            this.lblDate.TabIndex = 6;
            this.lblDate.Text = "Select Date";
            this.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.lblStatus, 4);
            this.lblStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblStatus.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblStatus.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblStatus.Location = new System.Drawing.Point(3, 389);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(585, 50);
            this.lblStatus.Style = MetroFramework.MetroColorStyle.Red;
            this.lblStatus.TabIndex = 8;
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnBack
            // 
            this.btnBack.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnBack.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnBack.Location = new System.Drawing.Point(3, 442);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(194, 94);
            this.btnBack.TabIndex = 9;
            this.btnBack.Text = "&Back";
            this.btnBack.UseSelectable = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnNext
            // 
            this.btnNext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNext.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnNext.Location = new System.Drawing.Point(393, 442);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(195, 94);
            this.btnNext.TabIndex = 10;
            this.btnNext.Text = "&Next";
            this.btnNext.UseSelectable = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTime.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblTime.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblTime.Location = new System.Drawing.Point(3, 150);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(194, 50);
            this.lblTime.TabIndex = 7;
            this.lblTime.Text = "Select Time";
            this.lblTime.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cmbHour
            // 
            this.cmbHour.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmbHour.FontSize = MetroFramework.MetroComboBoxSize.Tall;
            this.cmbHour.FontWeight = MetroFramework.MetroComboBoxWeight.Bold;
            this.cmbHour.FormattingEnabled = true;
            this.cmbHour.ItemHeight = 29;
            this.cmbHour.Items.AddRange(new object[] {
            "00",
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23"});
            this.cmbHour.Location = new System.Drawing.Point(203, 157);
            this.cmbHour.Name = "cmbHour";
            this.cmbHour.PromptText = "hour";
            this.cmbHour.Size = new System.Drawing.Size(89, 35);
            this.cmbHour.TabIndex = 1;
            this.cmbHour.UseSelectable = true;
            // 
            // cmbMinute
            // 
            this.cmbMinute.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmbMinute.FontSize = MetroFramework.MetroComboBoxSize.Tall;
            this.cmbMinute.FontWeight = MetroFramework.MetroComboBoxWeight.Bold;
            this.cmbMinute.FormattingEnabled = true;
            this.cmbMinute.ItemHeight = 29;
            this.cmbMinute.Items.AddRange(new object[] {
            "00",
            "10",
            "20",
            "30",
            "40",
            "50"});
            this.cmbMinute.Location = new System.Drawing.Point(298, 157);
            this.cmbMinute.Name = "cmbMinute";
            this.cmbMinute.PromptText = "min";
            this.cmbMinute.Size = new System.Drawing.Size(89, 35);
            this.cmbMinute.TabIndex = 2;
            this.cmbMinute.UseSelectable = true;
            // 
            // cmbProvince
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.cmbProvince, 2);
            this.cmbProvince.FontSize = MetroFramework.MetroComboBoxSize.Tall;
            this.cmbProvince.FormattingEnabled = true;
            this.cmbProvince.ItemHeight = 29;
            this.cmbProvince.Location = new System.Drawing.Point(203, 253);
            this.cmbProvince.Name = "cmbProvince";
            this.cmbProvince.Size = new System.Drawing.Size(184, 35);
            this.cmbProvince.TabIndex = 3;
            this.cmbProvince.UseSelectable = true;
            this.cmbProvince.SelectedIndexChanged += new System.EventHandler(this.cmbProvince_SelectedIndexChanged);
            // 
            // ReturnDetailsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "ReturnDetailsControl";
            this.Size = new System.Drawing.Size(591, 539);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MetroFramework.Controls.MetroLabel lblTitle;
        private MetroFramework.Controls.MetroLabel lblDate;
        private MetroFramework.Controls.MetroLabel lblTime;
        private MetroFramework.Controls.MetroLabel lblStatus;
        private MetroFramework.Controls.MetroButton btnBack;
        private MetroFramework.Controls.MetroButton btnNext;
        private MetroFramework.Controls.MetroComboBox cmbHour;
        private MetroFramework.Controls.MetroComboBox cmbMinute;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroComboBox cmbProvince;
        private MetroFramework.Controls.MetroComboBox cmbBranch;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroComboBox cmbDate;

    }
}
