﻿using GalaSoft.MvvmLight.Messaging;

namespace iRent.Bookings
{
    partial class AdditionalDriversControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnBack = new MetroFramework.Controls.MetroButton();
            this.lblStatus = new MetroFramework.Controls.MetroLabel();
            this.lblHeader = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.txtDateOfBirth = new MetroFramework.Controls.MetroTextBox();
            this.lblDateOfBirth = new MetroFramework.Controls.MetroLabel();
            this.btnAdd = new MetroFramework.Controls.MetroButton();
            this.txtLastName = new MetroFramework.Controls.MetroTextBox();
            this.txtFirstName = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.lblIdNumber = new MetroFramework.Controls.MetroLabel();
            this.txtIdNumber = new MetroFramework.Controls.MetroTextBox();
            this.pnlDrivers = new System.Windows.Forms.FlowLayoutPanel();
            this.btnNext = new MetroFramework.Controls.MetroButton();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.142858F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42.85714F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42.85714F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.142858F));
            this.tableLayoutPanel1.Controls.Add(this.btnBack, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblStatus, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblHeader, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnNext, 3, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(800, 528);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // btnBack
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.btnBack, 2);
            this.btnBack.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnBack.Location = new System.Drawing.Point(3, 430);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(194, 95);
            this.btnBack.TabIndex = 11;
            this.btnBack.Text = "&Back";
            this.btnBack.UseSelectable = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.lblStatus, 5);
            this.lblStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblStatus.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblStatus.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblStatus.Location = new System.Drawing.Point(3, 327);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(794, 100);
            this.lblStatus.TabIndex = 9;
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblHeader
            // 
            this.lblHeader.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.lblHeader, 3);
            this.lblHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblHeader.FontSize = MetroFramework.MetroLabelSize.XL;
            this.lblHeader.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblHeader.Location = new System.Drawing.Point(45, 0);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(708, 32);
            this.lblHeader.TabIndex = 4;
            this.lblHeader.Text = "Additional Drivers";
            this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel1.SetColumnSpan(this.tableLayoutPanel2, 3);
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 204F));
            this.tableLayoutPanel2.Controls.Add(this.txtDateOfBirth, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.lblDateOfBirth, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.btnAdd, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtLastName, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.txtFirstName, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.metroLabel2, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.metroLabel1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblIdNumber, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtIdNumber, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.pnlDrivers, 0, 4);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(45, 35);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 5;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(708, 289);
            this.tableLayoutPanel2.TabIndex = 10;
            // 
            // txtDateOfBirth
            // 
            this.txtDateOfBirth.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDateOfBirth.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtDateOfBirth.Lines = new string[0];
            this.txtDateOfBirth.Location = new System.Drawing.Point(255, 108);
            this.txtDateOfBirth.MaxLength = 32767;
            this.txtDateOfBirth.Name = "txtDateOfBirth";
            this.txtDateOfBirth.PasswordChar = '\0';
            this.txtDateOfBirth.PromptText = "dd/MM/yyyy";
            this.txtDateOfBirth.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtDateOfBirth.SelectedText = "";
            this.txtDateOfBirth.Size = new System.Drawing.Size(246, 29);
            this.txtDateOfBirth.TabIndex = 4;
            this.txtDateOfBirth.UseSelectable = true;
            // 
            // lblDateOfBirth
            // 
            this.lblDateOfBirth.AutoSize = true;
            this.lblDateOfBirth.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDateOfBirth.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblDateOfBirth.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblDateOfBirth.Location = new System.Drawing.Point(3, 105);
            this.lblDateOfBirth.Name = "lblDateOfBirth";
            this.lblDateOfBirth.Size = new System.Drawing.Size(246, 35);
            this.lblDateOfBirth.TabIndex = 10;
            this.lblDateOfBirth.Text = "Date of Birth:";
            this.lblDateOfBirth.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblDateOfBirth.UseCustomBackColor = true;
            // 
            // btnAdd
            // 
            this.btnAdd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAdd.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnAdd.Location = new System.Drawing.Point(507, 3);
            this.btnAdd.Name = "btnAdd";
            this.tableLayoutPanel2.SetRowSpan(this.btnAdd, 3);
            this.btnAdd.Size = new System.Drawing.Size(198, 99);
            this.btnAdd.TabIndex = 5;
            this.btnAdd.Text = "&Add";
            this.btnAdd.UseSelectable = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // txtLastName
            // 
            this.txtLastName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLastName.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtLastName.Lines = new string[0];
            this.txtLastName.Location = new System.Drawing.Point(255, 73);
            this.txtLastName.MaxLength = 32767;
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.PasswordChar = '\0';
            this.txtLastName.PromptText = "Driver Last Name";
            this.txtLastName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtLastName.SelectedText = "";
            this.txtLastName.Size = new System.Drawing.Size(246, 29);
            this.txtLastName.TabIndex = 3;
            this.txtLastName.UseSelectable = true;
            // 
            // txtFirstName
            // 
            this.txtFirstName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtFirstName.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtFirstName.Lines = new string[0];
            this.txtFirstName.Location = new System.Drawing.Point(255, 38);
            this.txtFirstName.MaxLength = 32767;
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.PasswordChar = '\0';
            this.txtFirstName.PromptText = "Driver First Name";
            this.txtFirstName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtFirstName.SelectedText = "";
            this.txtFirstName.Size = new System.Drawing.Size(246, 29);
            this.txtFirstName.TabIndex = 2;
            this.txtFirstName.UseSelectable = true;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel2.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel2.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel2.Location = new System.Drawing.Point(3, 70);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(246, 35);
            this.metroLabel2.TabIndex = 2;
            this.metroLabel2.Text = "Last Name:";
            this.metroLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.metroLabel2.UseCustomBackColor = true;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel1.Location = new System.Drawing.Point(3, 35);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(246, 35);
            this.metroLabel1.TabIndex = 1;
            this.metroLabel1.Text = "First Name:";
            this.metroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.metroLabel1.UseCustomBackColor = true;
            // 
            // lblIdNumber
            // 
            this.lblIdNumber.AutoSize = true;
            this.lblIdNumber.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblIdNumber.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblIdNumber.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblIdNumber.Location = new System.Drawing.Point(3, 0);
            this.lblIdNumber.Name = "lblIdNumber";
            this.lblIdNumber.Size = new System.Drawing.Size(246, 35);
            this.lblIdNumber.TabIndex = 0;
            this.lblIdNumber.Text = "ID Number:";
            this.lblIdNumber.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblIdNumber.UseCustomBackColor = true;
            // 
            // txtIdNumber
            // 
            this.txtIdNumber.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIdNumber.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtIdNumber.Lines = new string[0];
            this.txtIdNumber.Location = new System.Drawing.Point(255, 3);
            this.txtIdNumber.MaxLength = 32767;
            this.txtIdNumber.Name = "txtIdNumber";
            this.txtIdNumber.PasswordChar = '\0';
            this.txtIdNumber.PromptText = "Driver ID Number";
            this.txtIdNumber.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtIdNumber.SelectedText = "";
            this.txtIdNumber.Size = new System.Drawing.Size(246, 29);
            this.txtIdNumber.TabIndex = 1;
            this.txtIdNumber.UseSelectable = true;
            // 
            // pnlDrivers
            // 
            this.pnlDrivers.AutoScroll = true;
            this.tableLayoutPanel2.SetColumnSpan(this.pnlDrivers, 3);
            this.pnlDrivers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDrivers.Location = new System.Drawing.Point(3, 143);
            this.pnlDrivers.Name = "pnlDrivers";
            this.pnlDrivers.Size = new System.Drawing.Size(702, 178);
            this.pnlDrivers.TabIndex = 9;
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.SetColumnSpan(this.btnNext, 2);
            this.btnNext.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnNext.Location = new System.Drawing.Point(603, 430);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(194, 95);
            this.btnNext.TabIndex = 7;
            this.btnNext.Text = "&Next";
            this.btnNext.UseSelectable = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // AdditionalDriversControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "AdditionalDriversControl";
            this.Size = new System.Drawing.Size(800, 528);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MetroFramework.Controls.MetroLabel lblHeader;
        private MetroFramework.Controls.MetroButton btnNext;
        private MetroFramework.Controls.MetroLabel lblStatus;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private MetroFramework.Controls.MetroLabel lblIdNumber;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox txtLastName;
        private MetroFramework.Controls.MetroTextBox txtFirstName;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox txtIdNumber;
        private MetroFramework.Controls.MetroButton btnAdd;
        private MetroFramework.Controls.MetroButton btnBack;
        private MetroFramework.Controls.MetroLabel lblDateOfBirth;
        private System.Windows.Forms.FlowLayoutPanel pnlDrivers;
        private MetroFramework.Controls.MetroTextBox txtDateOfBirth;
    }
}
