﻿using System;
using Castle.Core;
using iRent.Framework;
using iRentKiosk.Core.Services;

namespace iRent.Bookings
{
    [CastleComponent]
    public class ReturnDetailsCommand : BaseCommand
    {
        private readonly Func<ReturnDetailsControl> _factory;

        public ReturnDetailsCommand(Func<ReturnDetailsControl> factory)
        {
            _factory = factory;
            Title = "Please Select Return Date & Time";
        }

        public string Next { get; set; }
        public string Back { get; set; }
        public string Title { get; set; }

        public override void Invoke()
        {
            var control = _factory();
            control.Setup(ContextData.Booking, ContextData.Format(Title));
            var main = GetFormAs<MainForm>();
            main.AddToFormPanel(control);
            control.Next += (o, e) => Proceed(Next);
            control.Back += (o, e) => Proceed(Back);
        }
    }
}