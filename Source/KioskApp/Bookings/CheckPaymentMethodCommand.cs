﻿using System.Windows.Forms;
using Castle.Core;
using iRentKiosk.Core.Models;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;

namespace iRent.Bookings
{
    [CastleComponent]
    public class CheckPaymentMethodCommand : ICommand
    {
        private readonly BookingApi _bookingApi;
        private readonly WorkflowService _workflowService;

        public CheckPaymentMethodCommand(WorkflowService workflowService, BookingApi bookingApi)
        {
            _workflowService = workflowService;
            _bookingApi = bookingApi;
            DefaultCode = "CC";
        }

        public string Next { get; set; }
        public string DefaultCode { get; set; }
        public Form MainForm { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }

        public void Invoke()
        {
            var booking = ContextData.Booking;
            if (booking.PaymentMethod == null)
                booking.PaymentMethod = _bookingApi.GetPaymentMethod(DefaultCode);
            _workflowService.Execute(Next);
        }
    }
}