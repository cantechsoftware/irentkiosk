﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace iRent.Extensions
{
    public static class ControlExtensions
    {
        public static void InvokeIfRequired<TControl>(this TControl c, Action<TControl> action)
            where TControl : Control
        {
            if (c.InvokeRequired)
            {
                c.Invoke(new Action(() => action(c)));
            }
            else
            {
                action(c);
            }
        }

        public static IEnumerable<Control> AllControls(this Control self)
        {
            foreach (Control ctrl in self.Controls)
            {
                yield return ctrl;
                foreach (var c in ctrl.AllControls())
                    yield return c;
            }
        }
    }
}