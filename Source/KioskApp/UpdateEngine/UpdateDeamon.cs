﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.IO;

namespace iRent.UpdateEngine
{
    public class UpdateDeamon
    {
        Thread _updateThread = new Thread(UpdateThreadExec);
        private static UpdateDeamon _updateDeamonInstance;

        private static void UpdateThreadExec(object o)
        {
            while (true)
            {
                bool UpdateRequired = false;
                //Contact WebService check for Update

                //Save the Next update Interval.
                //Sleep and wait for next update Interval.

                //Yes we should update.
                if (UpdateRequired)
                {
                    //Download the update package.
                    string FileName = UpdateDeamon.DownloadUpdateFile("","");
                    //Apply the Update. 
                    //Restart the Application.
                }
            }
        }

        private static string DownloadUpdateFile(string Uri, string AppVersion)
        {
            if (!Directory.Exists(".\\Updates"))
            {
                Directory.CreateDirectory(".\\Updates");
            }

            string outputFileName = ".\\Updates\\" + AppVersion + "_" + DateTime.Now.ToString("YYYYMMDD") + ".zip";
            using (WebClient client = new WebClient())
            {
                int retrycount = 0;

                while (retrycount < 3)
                {
                    try
                    {
                        client.DownloadFile(Uri, outputFileName);
                        break;
                    }
                    catch (Exception ex)
                    {
                        retrycount++;
                    }
                }
            }
            return outputFileName;
        }

        public static UpdateDeamon GetInstance()
        {
            if (UpdateDeamon._updateDeamonInstance == null)
            {
                UpdateDeamon._updateDeamonInstance = new UpdateDeamon();
            }

            return UpdateDeamon._updateDeamonInstance;
        }


        public void Start()
        {

        }

        public void Stop()
        {

        }
    }
}
