﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Castle.Core;
using MetroFramework;
using MetroFramework.Controls;
using iRent.Extensions;
using iRent.ViewModels;

namespace iRent
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class MainMenu : MetroUserControl
    {
        public MainMenu()
        {
            InitializeComponent();
        }

        public void SetStatus(string status)
        {
            this.InvokeIfRequired(x => lblStatus.Text = status);
        }

        public void SetOptions(MenuOption[] options)
        {
            pnlMenu.Controls.Clear();
            pnlMenu.RowCount = options.Length + 2;
            pnlMenu.RowStyles.Clear();
            pnlMenu.RowStyles.Add(new RowStyle(SizeType.AutoSize));
            var rowHeight = (float)Math.Min(500, pnlMenu.Height/4.1);
            for (var i = 0; i < options.Length; i++)
            {
                var opt = options[i];
                pnlMenu.RowStyles.Add(new RowStyle(SizeType.Absolute, rowHeight));
                var btn = new MetroButton();
                btn.Dock = DockStyle.Fill;
                btn.FontSize = MetroButtonSize.XL;
                btn.Highlight = true;
                btn.Location = new Point(3, 3 + 100*i);
                btn.Margin = new Padding(3, 5, 3, 3);
                btn.Name = "btn" + opt.Workflow;
                btn.Size = new Size(994, 100);
                btn.Style = MetroColorStyle.Red;
                btn.TabIndex = 11;
                btn.Text = opt.Text;
                btn.Theme = MetroThemeStyle.Dark;
                btn.UseSelectable = true;
                btn.Click += OnOptionButtonClick;
                btn.Tag = opt;
                pnlMenu.Controls.Add(btn, 0, i + 1);
            }
            pnlMenu.RowStyles.Add(new RowStyle(SizeType.AutoSize));
        }

        private void OnOptionButtonClick(object sender, EventArgs e)
        {
            var btn = (MetroButton) sender;
            if (OptionClicked != null)
                OptionClicked(this, (MenuOption) btn.Tag);
        }

        public event EventHandler<MenuOption> OptionClicked;
    }
}