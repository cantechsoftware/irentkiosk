﻿using System.Xml.Serialization;

namespace iRent.ViewModels
{
    [XmlType("Option")]
    public class MenuOption
    {
        [XmlAttribute("workflow")]
        public string Workflow { get; set; }

        [XmlText]
        public string Text { get; set; }
    }
}