﻿using System;
using CanTech.Common;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;

namespace iRent.ViewModels
{
    public class VehicleViewModel
    {
        public int VehicleID { get; set; }

        public string Description { get; set; }

        public string RegNumber { get; set; }

        public string VinNumber { get; set; }

        public string EngineNumber { get; set; }

        public string LicenseNo { get; set; }

        public string Mileage { get; set; }

        public string Make { get; set; }

        public string Model { get; set; }

        public string Year { get; set; }

        public string Colour { get; set; }

        public string Cellno { get; set; }

        public string RfidCard { get; set; }

        public string Password { get; set; }

        public string DisplayName { get; set; }

        public int VehicleGroupID { get; set; }

        public bool IsValid()
        {
            return true;
        }

        private void Bind(VehicleModel vehicle)
        {
            Cellno = vehicle.CellNo;
            Colour = vehicle.Colour;
            Description = String.Format("{0}-{1}", vehicle.Make, vehicle.Model);
            DisplayName = String.Format("{0}-{1}", vehicle.Make, vehicle.Model);
            EngineNumber = vehicle.EngineNo;
            LicenseNo = vehicle.LicenceNo;
            Make = vehicle.Make;
            Mileage = vehicle.Mileage.ToString("0");
            Model = vehicle.Model;
            Password = vehicle.Password;
            RegNumber = vehicle.RegNo;
            RfidCard = vehicle.Rfid;
            VehicleID = vehicle.Id;
            VinNumber = vehicle.VinNo;
            VehicleGroupID = vehicle.Group.Id;
            // this.Year = Vehicle.Year.ToString();
        }

        public void getVehiclesByid(int id)
        {
            if (id != 0)
            {
                var vehicle = IoC.Resolve<VehicleApi>().GetById(id);
                Bind(vehicle);
            }
        }
    }
}