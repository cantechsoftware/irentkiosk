﻿using System;
using System.Windows.Forms;
using Castle.Core;
using iRent.Framework;
using iRent.Util;
using SmartFormat;

namespace iRent.Completion
{
    [CastleComponent]
    public class PaymentCommand : BaseCommand
    {
        private readonly Func<PaymentControl> _factory;

        public PaymentCommand(Func<PaymentControl> factory)
        {
            _factory = factory;
        }

        public string Next { get; set; }
        public string Back { get; set; }
        public string PaymentFailed { get; set; }
        public string PaymentFailedMessage { get; set; }

        public override void Invoke()
        {
            var control = _factory();
            control.Dock = DockStyle.Fill;
            var main = GetFormAs<MainForm>();
            main.AddToFormPanel(control);
            control.Next += (o, e) => Proceed(Next);
            control.Back += (o, e) => Proceed(Back);
            control.PaymentFailed += (o, rsp) =>
                {
                    using (var frm = new MessageForm())
                    {
                        frm.Text = "Payment Failed";
                        frm.Message = PaymentFailedMessage.FormatSmart(rsp);
                        frm.ShowDialog();
                    }
                    Proceed(PaymentFailed);
                };
        }
    }
}