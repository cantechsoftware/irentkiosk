﻿namespace iRent.Completion
{
    partial class PaymentControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.cmdBack = new MetroFramework.Controls.MetroButton();
            this.lblStatus = new MetroFramework.Controls.MetroLabel();
            this.txtPay = new MetroFramework.Controls.MetroButton();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.txtCVV = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.txtCCNumber = new MetroFramework.Controls.MetroTextBox();
            this.txtAmount = new MetroFramework.Controls.MetroTextBox();
            this.btnNext = new MetroFramework.Controls.MetroButton();
            this.cmbYear = new MetroFramework.Controls.MetroComboBox();
            this.cmbMonth = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.metroLabel5, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.metroLabel4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.cmdBack, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.lblStatus, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.txtPay, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.metroLabel3, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.metroLabel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.metroLabel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtAmount, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnNext, 3, 10);
            this.tableLayoutPanel1.Controls.Add(this.cmbYear, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.cmbMonth, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.txtCCNumber, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.txtCVV, 1, 4);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 11;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(625, 551);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // cmdBack
            // 
            this.cmdBack.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdBack.FontSize = MetroFramework.MetroButtonSize.XL;
            this.cmdBack.Location = new System.Drawing.Point(3, 454);
            this.cmdBack.Name = "cmdBack";
            this.cmdBack.Size = new System.Drawing.Size(156, 94);
            this.cmdBack.TabIndex = 7;
            this.cmdBack.Text = "&Back";
            this.cmdBack.UseSelectable = true;
            this.cmdBack.Visible = false;
            this.cmdBack.Click += new System.EventHandler(this.cmdBack_Click);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.lblStatus, 4);
            this.lblStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblStatus.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblStatus.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblStatus.Location = new System.Drawing.Point(3, 425);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(619, 50);
            this.lblStatus.TabIndex = 18;
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtPay
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.txtPay, 2);
            this.txtPay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPay.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.txtPay.Location = new System.Drawing.Point(165, 353);
            this.txtPay.Name = "txtPay";
            this.txtPay.Size = new System.Drawing.Size(294, 69);
            this.txtPay.TabIndex = 5;
            this.txtPay.Text = "Make Payment";
            this.txtPay.UseSelectable = true;
            this.txtPay.Click += new System.EventHandler(this.txtPay_Click);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel3.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel3.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel3.Location = new System.Drawing.Point(3, 250);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(156, 50);
            this.metroLabel3.TabIndex = 16;
            this.metroLabel3.Text = "Expiry Date:";
            this.metroLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtCVV
            // 
            this.txtCVV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCVV.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtCVV.FontWeight = MetroFramework.MetroTextBoxWeight.Bold;
            this.txtCVV.Lines = new string[0];
            this.txtCVV.Location = new System.Drawing.Point(165, 203);
            this.txtCVV.MaxLength = 32767;
            this.txtCVV.Name = "txtCVV";
            this.txtCVV.PasswordChar = '\0';
            this.txtCVV.PromptText = "CVV";
            this.txtCVV.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCVV.SelectedText = "";
            this.txtCVV.Size = new System.Drawing.Size(144, 44);
            this.txtCVV.TabIndex = 2;
            this.txtCVV.UseSelectable = true;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.metroLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel2.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel2.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel2.Location = new System.Drawing.Point(3, 53);
            this.metroLabel2.Margin = new System.Windows.Forms.Padding(3);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(156, 44);
            this.metroLabel2.TabIndex = 13;
            this.metroLabel2.Text = "Amount:";
            this.metroLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.metroLabel1, 4);
            this.metroLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.XL;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel1.Location = new System.Drawing.Point(3, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(619, 50);
            this.metroLabel1.TabIndex = 12;
            this.metroLabel1.Text = "Payment";
            this.metroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtCCNumber
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.txtCCNumber, 2);
            this.txtCCNumber.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCCNumber.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtCCNumber.FontWeight = MetroFramework.MetroTextBoxWeight.Bold;
            this.txtCCNumber.Lines = new string[0];
            this.txtCCNumber.Location = new System.Drawing.Point(165, 153);
            this.txtCCNumber.MaxLength = 32767;
            this.txtCCNumber.Name = "txtCCNumber";
            this.txtCCNumber.PasswordChar = '\0';
            this.txtCCNumber.PromptText = "Credit Card No.";
            this.txtCCNumber.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCCNumber.SelectedText = "";
            this.txtCCNumber.Size = new System.Drawing.Size(294, 44);
            this.txtCCNumber.TabIndex = 1;
            this.txtCCNumber.UseSelectable = true;
            // 
            // txtAmount
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.txtAmount, 2);
            this.txtAmount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAmount.Enabled = false;
            this.txtAmount.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtAmount.FontWeight = MetroFramework.MetroTextBoxWeight.Bold;
            this.txtAmount.Lines = new string[0];
            this.txtAmount.Location = new System.Drawing.Point(165, 53);
            this.txtAmount.MaxLength = 32767;
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.PasswordChar = '\0';
            this.txtAmount.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtAmount.SelectedText = "";
            this.txtAmount.Size = new System.Drawing.Size(294, 44);
            this.txtAmount.TabIndex = 10;
            this.txtAmount.UseSelectable = true;
            // 
            // btnNext
            // 
            this.btnNext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNext.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnNext.Location = new System.Drawing.Point(465, 454);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(157, 94);
            this.btnNext.TabIndex = 6;
            this.btnNext.Text = "&Next";
            this.btnNext.UseSelectable = true;
            this.btnNext.Visible = false;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // cmbYear
            // 
            this.cmbYear.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmbYear.FontSize = MetroFramework.MetroComboBoxSize.Tall;
            this.cmbYear.FontWeight = MetroFramework.MetroComboBoxWeight.Bold;
            this.cmbYear.FormattingEnabled = true;
            this.cmbYear.ItemHeight = 29;
            this.cmbYear.Items.AddRange(new object[] {
            "2015",
            "2016",
            "2017",
            "2018",
            "2019",
            "2020"});
            this.cmbYear.Location = new System.Drawing.Point(190, 257);
            this.cmbYear.Name = "cmbYear";
            this.cmbYear.PromptText = "YYYY";
            this.cmbYear.Size = new System.Drawing.Size(94, 35);
            this.cmbYear.TabIndex = 3;
            this.cmbYear.UseSelectable = true;
            // 
            // cmbMonth
            // 
            this.cmbMonth.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmbMonth.FontSize = MetroFramework.MetroComboBoxSize.Tall;
            this.cmbMonth.FontWeight = MetroFramework.MetroComboBoxWeight.Bold;
            this.cmbMonth.FormattingEnabled = true;
            this.cmbMonth.ItemHeight = 29;
            this.cmbMonth.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"});
            this.cmbMonth.Location = new System.Drawing.Point(340, 257);
            this.cmbMonth.Name = "cmbMonth";
            this.cmbMonth.PromptText = "MM";
            this.cmbMonth.Size = new System.Drawing.Size(94, 35);
            this.cmbMonth.TabIndex = 4;
            this.cmbMonth.UseSelectable = true;
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel4.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel4.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel4.Location = new System.Drawing.Point(3, 150);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(156, 50);
            this.metroLabel4.TabIndex = 19;
            this.metroLabel4.Text = "Credit Card No:";
            this.metroLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel5.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel5.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel5.Location = new System.Drawing.Point(3, 200);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(156, 50);
            this.metroLabel5.TabIndex = 20;
            this.metroLabel5.Text = "CVV:";
            this.metroLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PaymentControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "PaymentControl";
            this.Size = new System.Drawing.Size(625, 551);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MetroFramework.Controls.MetroLabel lblStatus;
        private MetroFramework.Controls.MetroButton txtPay;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroTextBox txtCVV;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox txtCCNumber;
        private MetroFramework.Controls.MetroTextBox txtAmount;
        private MetroFramework.Controls.MetroButton btnNext;
        private MetroFramework.Controls.MetroComboBox cmbMonth;
        private MetroFramework.Controls.MetroComboBox cmbYear;
        private MetroFramework.Controls.MetroButton cmdBack;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel4;
    }
}
