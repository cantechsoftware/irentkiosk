﻿using System;
using Castle.Core;
using iRent.Framework;
using iRentKiosk.Core.Services;

namespace iRent.Completion
{
    [CastleComponent]
    public class SignatureCommand : BaseCommand
    {
        private readonly Func<SignatureControl> _factory;

        public SignatureCommand(Func<SignatureControl> factory)
        {
            _factory = factory;
        }

        public string Next { get; set; }

        public override void Invoke()
        {
            var control = _factory();
            var main = GetFormAs<MainForm>();
            main.AddToFormPanel(control);
            control.Next += (o, img) =>
                {
                    ContextData.Signature = img;
                    Proceed(Next);
                };
        }
    }
}