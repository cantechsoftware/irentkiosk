﻿using System;
using System.Drawing;
using System.Drawing.Printing;
using Castle.Core;
using MetroFramework.Controls;
using iRent.Extensions;

namespace iRent.Completion
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class TermsAndConditionsControl : MetroUserControl
    {
        private int _checkPrint;

        public TermsAndConditionsControl()
        {
            InitializeComponent();
        }

        public void Init(string terms)
        {
            try
            {
                txtTerms.Rtf = terms;
            }
            catch
            {
                txtTerms.Text = terms;
            }
        }

        private void printDocument_BeginPrint(object sender, PrintEventArgs e)
        {
            _checkPrint = 0;
        }

        private void printDocument_PrintPage(object sender, PrintPageEventArgs e)
        {
            // Print the content of RichTextBox. Store the last character printed.
            _checkPrint = txtTerms.Print(_checkPrint, txtTerms.TextLength, e);

            // Check for more pages
            if (_checkPrint < txtTerms.TextLength)
                e.HasMorePages = true;
            else
                e.HasMorePages = false;
        }

        public event EventHandler Next;

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (!chkAccept.Checked)
            {
                chkAccept.UseCustomForeColor = true;
                chkAccept.ForeColor = Color.Red;
                return;
            }

            if (Next != null)
                Next(this, e);
        }

        private void cmdPrint_Click(object sender, EventArgs e)
        {
            printDocument.Print();
        }
    }
}