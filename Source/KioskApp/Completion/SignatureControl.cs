﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Castle.Core;
using MetroFramework.Controls;

namespace iRent.Completion
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class SignatureControl : MetroUserControl
    {
        public SignatureControl()
        {
            InitializeComponent();
            Cursor.Current = Cursors.Default;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            //Reset the Signature Component and load with a WHITE picture
            signature.Clear(true);
            Cursor.Current = Cursors.Default;
        }

        public bool Save()
        {
            if (signature.Points == null)
            {
                lblStatus.Text = @"No signature detected. Please try again.";
                return false;
            }

            Cursor.Current = Cursors.WaitCursor;
            SignatureCaptureControl.SaveControlImage(signature, "Akhil");
            signature.Clear(false);
            Cursor.Current = Cursors.Default;
            return true;
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (signature.Points == null)
            {
                lblStatus.Text = @"No signature detected. Please try again.";
                return;
            }

            Cursor.Current = Cursors.WaitCursor;
            var img = signature.GetImage();
            signature.Clear(false);
            Cursor.Current = Cursors.Default;
            if (Next != null)
                Next(this, img);
        }

        public event EventHandler<Image> Next;
    }
}