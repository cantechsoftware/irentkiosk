﻿using System;
using System.Collections;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Castle.Core;
using MetroFramework.Controls;

namespace iRent.Completion
{
    public enum CaptureMode
    {
        Screen,
        Window
    }

    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class SignatureCaptureControl : MetroUserControl
    {
        private const int XPelsPerMeter = 0xb12;
        private const int YPelsPerMeter = 0xb12;
        private const int GPTR = 0x40;
        private const int SRCCOPY = 0x00CC0020;

        /// <summary> Position of the cursor relative to the start of the capture </summary>
        public static Point CursorPosition;

        private readonly string AppPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);
        private Bitmap BackGroundImage;
        private bool CaptureMouseCoordinates;
        private Graphics GraphicsHandle;
        private Point LastMouseCoordinates = new Point(0, 0);
        public ArrayList Points = new ArrayList();
        private Pen SignaturePen = new Pen(Color.Black);

        public SignatureCaptureControl()
        {
            InitializeComponent();
        }

        [DllImport("CoreDll.dll")]
        private static extern IntPtr GetFocus();

        [DllImport("CoreDll.dll")]
        private static extern IntPtr LocalAlloc(uint flags, uint cb);

        [DllImport("CoreDll.dll")]
        private static extern IntPtr LocalFree(IntPtr hMem);

        [DllImport("CoreDll.dll")]
        private static extern IntPtr CreateDIBSection(IntPtr hdc, BITMAPINFOHEADER hdr, uint colors, ref IntPtr pBits,
                                                      IntPtr hFile, uint offset);

        [DllImport("CoreDll.dll")]
        private static extern IntPtr CreateDIBSection(IntPtr hdc, IntPtr hdr, uint colors, ref IntPtr pBits,
                                                      IntPtr hFile, uint offset);

        [DllImport("CoreDll.dll")]
        private static extern IntPtr GetDC(IntPtr hWnd);

        [DllImport("CoreDll.dll")]
        private static extern void ReleaseDC(IntPtr hDC);

        [DllImport("CoreDll.dll")]
        private static extern void DeleteDC(IntPtr hDC);

        [DllImport("CoreDll.dll")]
        private static extern int BitBlt(IntPtr hdcDest, int nXDest, int nYDest, int nWidth, int nHeight, IntPtr hdcSrc,
                                         int nXSrc, int nYSrc, uint dwRop);

        [DllImport("CoreDll.dll")]
        private static extern IntPtr CreateCompatibleDC(IntPtr hdc);

        [DllImport("CoreDll.dll")]
        private static extern IntPtr SelectObject(IntPtr hdc, IntPtr hObj);

        [DllImport("CoreDll.dll")]
        private static extern void DeleteObject(IntPtr hObj);

        protected override void OnPaint(PaintEventArgs e)
        {
            LoadBackgroundImageIfInvalid();
            e.Graphics.DrawImage(BackGroundImage, 0, 0);
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);
            if (CaptureMouseCoordinates)
            {
                return;
            }
            CaptureMouseCoordinates = true;
            LastMouseCoordinates.X = e.X;
            LastMouseCoordinates.Y = e.Y;
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);
            CaptureMouseCoordinates = false;
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            if (!CaptureMouseCoordinates)
            {
                return;
            }
            var l = new LineToDraw();
            l.StartX = LastMouseCoordinates.X;
            l.StartY = LastMouseCoordinates.Y;
            l.EndX = e.X;
            l.EndY = e.Y;
            Points.Add(l);
            GraphicsHandle.DrawLine(SignaturePen, l.StartX + 1, l.StartY, l.EndX + 1, l.EndY);
            GraphicsHandle.DrawLine(SignaturePen, l.StartX, l.StartY + 1, l.EndX, l.EndY + 1);
            GraphicsHandle.DrawLine(SignaturePen, l.StartX + 1, l.StartY + 1, l.EndX + 1, l.EndY + 1);
            GraphicsHandle.DrawLine(SignaturePen, l.StartX, l.StartY, l.EndX, l.EndY);
            LastMouseCoordinates.X = l.EndX;
            LastMouseCoordinates.Y = l.EndY;
            Invalidate();
        }

        public void Save(string FileName)
        {
            try
            {
                SaveToFile(this, FileName);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception: "+ex.Message);
                // throw;
            }
        }

        public void Clear(bool ClearCapturedLines)
        {
            try
            {
                LoadBackgroundImage();
                Invalidate();
                if (ClearCapturedLines == false)
                {
                    return;
                }
                Points.Clear();
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void LoadBackgroundImageIfInvalid()
        {
            try
            {
                if (BackGroundImage == null || BackGroundImage.Width != Width || BackGroundImage.Height != Height)
                {
                    LoadBackgroundImage();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void LoadBackgroundImage()
        {
            try
            {
                BackGroundImage = new Bitmap(Width, Height);
                GraphicsHandle = Graphics.FromImage(BackGroundImage);
                GraphicsHandle.Clear(Color.White);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public byte[] ImageToByteArray(Image img)
        {
            var ms = new MemoryStream();
            img.Save(ms, ImageFormat.Gif);
            return ms.ToArray();
        }

        public Image ByteArrayToImage(byte[] data)
        {
            var ms = new MemoryStream(data);
            Image img = new Bitmap(ms);
            return img;
        }


        private void SaveToFile(Control ctl, string FileName)
        {
            IntPtr hDC;

            if (ctl != null)
            {
                ctl.Focus();
                var hReal = GetFocus();
                hDC = GetDC(hReal);
            }
            else
            {
                hDC = GetDC(IntPtr.Zero);
            }

            var hMemDC = CreateCompatibleDC(hDC);
            var bi = new BITMAPINFOHEADER();
            bi.biSize = (uint) Marshal.SizeOf(bi);
            bi.biBitCount = 16;
            bi.biClrUsed = 0;
            bi.biClrImportant = 0;
            bi.biCompression = 0;
            bi.biHeight = ctl != null ? ctl.Height : Screen.PrimaryScreen.Bounds.Height;
            bi.biWidth = ctl != null ? ctl.Width : Screen.PrimaryScreen.Bounds.Width;
            bi.biPlanes = 1;
            var cb = (bi.biHeight*bi.biWidth*bi.biBitCount/8);
            bi.biSizeImage = (uint) cb;
            bi.biXPelsPerMeter = XPelsPerMeter;
            bi.biYPelsPerMeter = YPelsPerMeter;
            var pBits = IntPtr.Zero;
            var pBI = LocalAlloc(GPTR, bi.biSize);
            Marshal.StructureToPtr(bi, pBI, false);
            var hBmp = CreateDIBSection(hDC, pBI, 0, ref pBits, IntPtr.Zero, 0);
            var biNew = (BITMAPINFOHEADER) Marshal.PtrToStructure(pBI, typeof (BITMAPINFOHEADER));
            var hOldBitmap = SelectObject(hMemDC, hBmp);
            var nRet = BitBlt(hMemDC, 0, 0, bi.biWidth, bi.biHeight, hDC, 0, 0, SRCCOPY);
            var RealBits = new byte[cb];
            Marshal.Copy(pBits, RealBits, 0, cb);
            var bfh = new BITMAPFILEHEADER();
            bfh.bfSize = (uint) cb + 0x36;
            bfh.bfType = 0x4d42;
            bfh.bfOffBits = 0x36;
            var HdrSize = 14;
            var header = new byte[HdrSize];
            BitConverter.GetBytes(bfh.bfType).CopyTo(header, 0);
            BitConverter.GetBytes(bfh.bfSize).CopyTo(header, 2);
            BitConverter.GetBytes(bfh.bfOffBits).CopyTo(header, 10);
            var data = new byte[cb + bfh.bfOffBits];


            header.CopyTo(data, 0);
            header = new byte[Marshal.SizeOf(bi)];
            var pHeader = LocalAlloc(GPTR, (uint) Marshal.SizeOf(bi));
            Marshal.StructureToPtr(biNew, pHeader, false);
            Marshal.Copy(pHeader, header, 0, Marshal.SizeOf(bi));
            LocalFree(pHeader);
            header.CopyTo(data, HdrSize);
            RealBits.CopyTo(data, (int) bfh.bfOffBits);
            var fs = new FileStream(FileName, FileMode.Create);
            fs.Write(data, 0, data.Length);


            var imm = ByteArrayToImage(data);
            imm.Save(Path.Combine(AppPath, "signature___________.bmp"), ImageFormat.Bmp);

            fs.Flush();
            fs.Close();
            data = null;
            DeleteObject(SelectObject(hMemDC, hOldBitmap));
            DeleteDC(hMemDC);
            ReleaseDC(hDC);
        }

        public void SetPenColor(Color penColour)
        {
            SignaturePen = new Pen(penColour);
        }

        public void LoadImage(String ImageFileName)
        {
            try
            {
                BackGroundImage = new Bitmap(ImageFileName);
                GraphicsHandle = Graphics.FromImage(BackGroundImage);
                Invalidate();
            }
            catch (Exception)
            {
                throw;
            }
        }


        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll")]
        private static extern IntPtr GetWindowRect(IntPtr hWnd, ref Rect rect);

        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        public static extern IntPtr GetDesktopWindow();

        /// <summary> Capture Active Window, Desktop, Window or Control by hWnd or .NET Contro/Form and save it to a specified file.  </summary>
        /// <param name="filename">
        ///     Filename.
        ///     <para>* If extension is omitted, it's calculated from the type of file</para>
        ///     <para>* If path is omitted, defaults to %TEMP%</para>
        ///     <para>* Use %NOW% to put a timestamp in the filename</para>
        /// </param>
        /// <param name="mode">Optional. The default value is CaptureMode.Window.</param>
        /// <param name="format">Optional file save mode.  Default is PNG</param>
        public static void CaptureAndSave(string filename, CaptureMode mode = CaptureMode.Window,
                                          ImageFormat format = null)
        {
            ImageSave(filename, format, Capture(mode));
        }

        /// <summary> Capture a specific window (or control) and save it to a specified file.  </summary>
        /// <param name="filename">
        ///     Filename.
        ///     <para>* If extension is omitted, it's calculated from the type of file</para>
        ///     <para>* If path is omitted, defaults to %TEMP%</para>
        ///     <para>* Use %NOW% to put a timestamp in the filename</para>
        /// </param>
        /// <param name="handle">hWnd (handle) of the window to capture</param>
        /// <param name="format">Optional file save mode.  Default is PNG</param>
        public static void CaptureAndSave(string filename, IntPtr handle, ImageFormat format = null)
        {
            ImageSave(filename, format, Capture(handle));
        }

        /// <summary> Capture a specific window (or control) and save it to a specified file.  </summary>
        /// <param name="filename">
        ///     Filename.
        ///     <para>* If extension is omitted, it's calculated from the type of file</para>
        ///     <para>* If path is omitted, defaults to %TEMP%</para>
        ///     <para>* Use %NOW% to put a timestamp in the filename</para>
        /// </param>
        /// <param name="c">Object to capture</param>
        /// <param name="format">Optional file save mode.  Default is PNG</param>
        public static void CaptureAndSave(string filename, Control c, ImageFormat format = null)
        {
            ImageSave(filename, format, Capture(c));
        }

        /// <summary> Capture the active window (default) or the desktop and return it as a bitmap </summary>
        /// <param name="mode">Optional. The default value is CaptureMode.Window.</param>
        public static Bitmap Capture(CaptureMode mode = CaptureMode.Window)
        {
            return Capture(mode == CaptureMode.Screen ? GetDesktopWindow() : GetForegroundWindow());
        }

        /// <summary> Capture a .NET Control, Form, UserControl, etc. </summary>
        /// <param name="c">Object to capture</param>
        /// <returns> Bitmap of control's area </returns>
        public static Bitmap Capture(Control c)
        {
            return Capture(c.Handle);
        }


        /// <summary> Capture a specific window and return it as a bitmap </summary>
        /// <param name="handle">hWnd (handle) of the window to capture</param>
        public static Bitmap Capture(IntPtr handle)
        {
            var rect = new Rect();
            GetWindowRect(handle, ref rect);
            rect.Left = 177;
            rect.Top = 55;
            rect.Right = 679;
            rect.Bottom = 229;
            var bounds = new Rectangle(rect.Left, rect.Top, rect.Right - rect.Left, rect.Bottom - rect.Top);
            //  CursorPosition = new Point(Cursor.Position.X - rect.Left, Cursor.Position.Y - rect.Top);
            CursorPosition = new Point(177 - rect.Left, 55 - rect.Top);
            var result = new Bitmap(bounds.Width, bounds.Height);
            using (var g = Graphics.FromImage(result))
                g.CopyFromScreen(new Point(bounds.Left, bounds.Top), Point.Empty, bounds.Size);

            return result;
        }


        /// <summary> Save an image to a specific file </summary>
        /// <param name="filename">
        ///     Filename.
        ///     <para>* If extension is omitted, it's calculated from the type of file</para>
        ///     <para>* If path is omitted, defaults to %TEMP%</para>
        ///     <para>* Use %NOW% to put a timestamp in the filename</para>
        /// </param>
        /// <param name="format">Optional file save mode.  Default is PNG</param>
        /// <param name="image">
        ///     Image to save.  Usually a BitMap, but can be any
        ///     Image.
        /// </param>
        private static void ImageSave(string filename, ImageFormat format, Image image)
        {
            format = format ?? ImageFormat.Png;
            if (!filename.Contains("."))
                filename = filename.Trim() + "." + format.ToString().ToLower();

            if (!filename.Contains(@"\"))
                filename = Path.Combine(Environment.GetEnvironmentVariable("TEMP") ?? @"C:\Temp", filename);

            filename = filename.Replace("%NOW%", DateTime.Now.ToString("yyyy-MM-dd@hh.mm.ss"));
            image.Save(filename, format);
        }


        public static void SaveControlImage(Control theControl, string filename)
        {
            if (!filename.Contains("."))
                filename = filename.Trim() + "." + "png";

            if (!filename.Contains(@"\"))
                filename = Path.Combine(Environment.GetEnvironmentVariable("TEMP") ?? @"C:\Temp", filename);

            var controlBitMap = new Bitmap(theControl.Width, theControl.Height);
            var g = Graphics.FromImage(controlBitMap);

            g.CopyFromScreen(new Point(theControl.Left, theControl.Top), Point.Empty, theControl.Size);
            // g.CopyFromScreen(PointToScreen(theControl.Location), new Point(0, 0), theControl.Size);

            // example of saving to the desktop
            filename = filename.Replace("%NOW%", DateTime.Now.ToString("yyyy-MM-dd@hh.mm.ss"));
            controlBitMap.Save(filename, ImageFormat.Png);
        }

        public Image GetImage()
        {
            var controlBitMap = new Bitmap(this.Width, this.Height);
            using (var g = Graphics.FromImage(controlBitMap))
                g.CopyFromScreen(new Point(this.Left, this.Top), Point.Empty, this.Size);

            return controlBitMap;
        }

        private struct BITMAPFILEHEADER
        {
            public uint bfOffBits;
            public ushort bfReserved1;
            public ushort bfReserved2;
            public uint bfSize;
            public ushort bfType;
        }

        private struct BITMAPINFOHEADER
        {
            public ushort biBitCount;
            public uint biClrImportant;
            public uint biClrUsed;
            public uint biCompression;
            public int biHeight;
            public ushort biPlanes;
            public uint biSize;
            public uint biSizeImage;
            public int biWidth;
            public int biXPelsPerMeter;
            public int biYPelsPerMeter;
        }

        private struct LineToDraw
        {
            public int EndX;
            public int EndY;
            public int StartX;
            public int StartY;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct Rect
        {
            public int Left;
            public int Top;
            public int Right;
            public int Bottom;
        }
    }
}