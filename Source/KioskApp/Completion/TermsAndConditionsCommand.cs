﻿using System;
using System.IO;
using System.Windows.Forms;
using Castle.Core;
using iRent.Framework;
using iRent.Util;

namespace iRent.Completion
{
    [CastleComponent]
    public class TermsAndConditionsCommand : BaseCommand
    {
        private readonly Func<TermsAndConditionsControl> _factory;

        public TermsAndConditionsCommand(Func<TermsAndConditionsControl> factory)
        {
            _factory = factory;
            Filename = "Documents/TandCs.rtf";
        }

        public string Filename { get; set; }
        public string Next { get; set; }

        public override void Invoke()
        {
            var control = _factory();
            control.Dock = DockStyle.Fill;
            var main = GetFormAs<MainForm>();
            main.AddToFormPanel(control);
            var terms = File.Exists(Filename) ? File.ReadAllText(Filename) : Filename + " not found.";
            control.Init(terms);
            control.Next += (o, e) => Proceed(Next);
        }
    }
}