﻿using System;
using Castle.Core;
using SmartFormat;
using iRent.Framework;
using iRent.Services;
using iRent.Util;

namespace iRent.Completion
{
    [CastleComponent]
    public class FinalizeCommand : BaseCommand
    {
        private readonly RfidService _rfidService;
        private readonly TransactionService _transactionService;

        public FinalizeCommand(TransactionService transactionService, RfidService rfidService)
        {
            _transactionService = transactionService;
            _rfidService = rfidService;
            NotifyDriver = true;
        }

        public string Next { get; set; }
        public bool NotifyDriver { get; set; }

        public override void Invoke()
        {
            try
            {                
                var rfid = _rfidService.GetCard(true);

                if (rfid == null || rfid == "Error")
                {
                    var trans = _transactionService.Create(null);
                    MessageForm.Show("Error Retrieving RFID Card",
                                     @"There was an error retrieving your RFID card.
Please contact the service desk for assistance.
Reference: {Id}.".FormatSmart(trans));
                }
                else
                    _transactionService.Activate(rfid, NotifyDriver);

                Proceed(Next);
            }
            catch (Exception ex)
            {
                Logger.Log("Transaction", "Save", "Error occurred saving transaction", ex);
                MessageForm.Show("Error Saving Transaction",
                                 @"There was an error processing the transaction. 
Please try again or contact the service desk for assistance.");
            }
        }
    }
}