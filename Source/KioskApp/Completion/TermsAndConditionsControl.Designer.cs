﻿namespace iRent.Completion
{
    partial class TermsAndConditionsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.cmdPrint = new MetroFramework.Controls.MetroButton();
            this.lblTerms = new MetroFramework.Controls.MetroLabel();
            this.btnNext = new MetroFramework.Controls.MetroButton();
            this.chkAccept = new MetroFramework.Controls.MetroCheckBox();
            this.txtTerms = new System.Windows.Forms.RichTextBox();
            this.printDocument = new System.Drawing.Printing.PrintDocument();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.cmdPrint, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblTerms, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnNext, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.chkAccept, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtTerms, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(603, 458);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // cmdPrint
            // 
            this.cmdPrint.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdPrint.FontSize = MetroFramework.MetroButtonSize.XL;
            this.cmdPrint.Location = new System.Drawing.Point(3, 361);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Size = new System.Drawing.Size(195, 94);
            this.cmdPrint.TabIndex = 9;
            this.cmdPrint.Text = "&Print";
            this.cmdPrint.UseSelectable = true;
            this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
            // 
            // lblTerms
            // 
            this.lblTerms.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.lblTerms, 3);
            this.lblTerms.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTerms.FontSize = MetroFramework.MetroLabelSize.XL;
            this.lblTerms.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblTerms.Location = new System.Drawing.Point(3, 0);
            this.lblTerms.Name = "lblTerms";
            this.lblTerms.Size = new System.Drawing.Size(597, 50);
            this.lblTerms.TabIndex = 3;
            this.lblTerms.Text = "Terms And Conditions";
            this.lblTerms.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnNext
            // 
            this.btnNext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNext.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnNext.Location = new System.Drawing.Point(404, 361);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(196, 94);
            this.btnNext.TabIndex = 6;
            this.btnNext.Text = "&Next";
            this.btnNext.UseSelectable = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // chkAccept
            // 
            this.chkAccept.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.chkAccept, 3);
            this.chkAccept.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkAccept.FontSize = MetroFramework.MetroCheckBoxSize.Tall;
            this.chkAccept.FontWeight = MetroFramework.MetroCheckBoxWeight.Bold;
            this.chkAccept.ForeColor = System.Drawing.SystemColors.ControlText;
            this.chkAccept.Location = new System.Drawing.Point(3, 311);
            this.chkAccept.Name = "chkAccept";
            this.chkAccept.Size = new System.Drawing.Size(597, 44);
            this.chkAccept.Style = MetroFramework.MetroColorStyle.Black;
            this.chkAccept.TabIndex = 7;
            this.chkAccept.Text = "I hereby accept all terms and conditions.";
            this.chkAccept.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkAccept.UseCustomForeColor = true;
            this.chkAccept.UseSelectable = true;
            // 
            // txtTerms
            // 
            this.txtTerms.BackColor = System.Drawing.SystemColors.Control;
            this.tableLayoutPanel1.SetColumnSpan(this.txtTerms, 3);
            this.txtTerms.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTerms.Location = new System.Drawing.Point(3, 53);
            this.txtTerms.Name = "txtTerms";
            this.txtTerms.Size = new System.Drawing.Size(597, 252);
            this.txtTerms.TabIndex = 8;
            this.txtTerms.Text = "";
            // 
            // printDocument
            // 
            this.printDocument.BeginPrint += new System.Drawing.Printing.PrintEventHandler(this.printDocument_BeginPrint);
            this.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument_PrintPage);
            // 
            // TermsAndConditionsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "TermsAndConditionsControl";
            this.Size = new System.Drawing.Size(603, 458);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MetroFramework.Controls.MetroLabel lblTerms;
        private MetroFramework.Controls.MetroButton btnNext;
        private MetroFramework.Controls.MetroCheckBox chkAccept;
        private System.Windows.Forms.RichTextBox txtTerms;
        private System.Drawing.Printing.PrintDocument printDocument;
        private MetroFramework.Controls.MetroButton cmdPrint;

    }
}
