﻿using System;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using Castle.Core;
using MetroFramework.Controls;
using iRent2.Contracts.Payments;
using iRentKiosk.Core.Services;

namespace iRent.Completion
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class PaymentControl : MetroUserControl
    {
        private readonly PaymentService _paymentService;
        private readonly BookingService _bookingService;

        public PaymentControl(PaymentService paymentService, BookingService bookingService)
        {
            _paymentService = paymentService;
            _bookingService = bookingService;

            InitializeComponent();

            txtCCNumber.Focus();
            cmbYear.DataSource = Enumerable.Range(0, 10).Select(x => DateTime.Today.Year + x).ToList();
            txtAmount.Text = ContextData.VehicleCost.ToString("C");
        }

        public event EventHandler Next;
        public event EventHandler Back;
        public event EventHandler<CreatePaymentResponse> PaymentFailed;

        private string ValidateInput()
        {
            if (txtCCNumber.Text.Trim() == "")
                return "Please enter your credit card number";

            if (!Regex.IsMatch(txtCCNumber.Text, @"^\d{16}$"))
                return "Please enter a valid credit card number";

            if (txtCVV.Text.Trim() == "")
                return "Please enter your CVV";

            if (cmbYear.Text == "" || cmbMonth.Text == "")
                return "Please enter the expiry date";

            var expiryDate = new DateTime(Convert.ToInt32(cmbYear.Text), Convert.ToInt32(cmbMonth.Text), 1);

            if (expiryDate < DateTime.Today)
                return "Card has expired.";

            return null;
        }

        private void txtPay_Click(object sender, EventArgs e)
        {
            var err = ValidateInput();
            if (err != null)
            {
                lblStatus.ForeColor = Color.Red;
                lblStatus.UseCustomForeColor = true;
                lblStatus.Text = err;
                return;
            }

            var booking = _bookingService.SaveBooking();

            var rsp = _paymentService.Submit(booking,
                                             ContextData.VehicleCost,
                                             txtCCNumber.Text, txtCVV.Text,
                                             int.Parse(cmbYear.Text),
                                             int.Parse(cmbMonth.Text));

            if (rsp.Success)
            {
                ContextData.PaymentKey = rsp.PaymentKey;
                lblStatus.ForeColor = Color.Black;
                lblStatus.Text = "Payment successful, click Next to continue.";
                txtPay.Visible = false;
                btnNext.Visible = true;
            }
            else
            {
                if (PaymentFailed != null)
                    PaymentFailed(this, rsp);
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (Next != null)
                Next(this, e);
        }

        private void cmdBack_Click(object sender, EventArgs e)
        {
            if (Back != null)
                Back(this, e);
        }
    }
}