﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iRent.Rental.UserControls
{
    public partial class ImageContainer : UserControl
    {
        public ImageContainer()
        {
            InitializeComponent();
            
        }

        public override string Text {
            get { return Label.Text; }
            set { Label.Text = value; }
        }

        public PictureBox Image
        {
            get { return ImageBox; }
            set { ImageBox = value; }
        }
       

    }
}
