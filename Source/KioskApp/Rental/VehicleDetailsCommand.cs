﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;
using Castle.Core;
using iRent.Extensions;
using iRent.Framework;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;

namespace iRent.Rental
{
    [CastleComponent]
    public class VehicleDetailsCommand : BaseCommand
    {
        private readonly Func<VehicleDetailsControl> _factory;
        private readonly BookingApi _bookingApi;
        private readonly VehicleApi _vehicleApi;

        public VehicleDetailsCommand(BookingApi bookingApi, Func<VehicleDetailsControl> factory, VehicleApi vehicleApi)
        {
            _bookingApi = bookingApi;
            _factory = factory;
            _vehicleApi = vehicleApi;
        }

        public string NotFound { get; set; }

        public override void Invoke()
        {
            var control = _factory();
            control.Title = "Your Vehicle";
            control.Message = "Please wait while we retrieve your vehicle details...";
            control.Dock = DockStyle.Fill;
            var main = GetFormAs<MainForm>();
            main.AddToFormPanel(control);
            var args = BusyForm.RunLongTask("Retrieving Vehicle Details...", RetrieveVehicle);
            if (args.Result != null)
            {
                var vehicle = (VehicleModel) args.Result;
                control.InvokeIfRequired(
                    x =>
                        x.Message =
                            $"Your vehicle is a {vehicle.Colour} {vehicle.Make} {vehicle.Model}" +
                            $"\r\nwith registration number {vehicle.RegNo}");
            }
            else
            {
                Proceed(NotFound);
            }
        }

        private void RetrieveVehicle(object sender, DoWorkEventArgs args)
        {
            for (var i = 0; i < 10; i++)
            {
                Thread.Sleep(2000);
                var vehicle = GetVehicle();
                if (vehicle != null)
                {
                    args.Result = vehicle;
                    return;
                }
            }
        }

        private VehicleModel GetVehicle()
        {
            try
            {
                if (ContextData.Vehicle != null)
                    return ContextData.Vehicle;

                if (ContextData.Booking != null)
                {
                    var booking = _bookingApi.GetBooking(ContextData.Booking.Id);
                    if (booking != null)
                    {
                        ContextData.SetBooking(booking);
                        if (ContextData.Booking.VehicleId.HasValue)
                            return _vehicleApi.GetById(ContextData.Booking.VehicleId.Value);
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.Log(this, "GetVehicle", "Error retrieving vehicle", ex);
                return null;
            }
        }
    }
}