﻿using System;
using System.Windows.Forms;
using Castle.Core;
using SmartFormat;
using iRent.Framework;
using iRentKiosk.Core.Services;

namespace iRent.Rental
{
    [CastleComponent]
    public class RentalTermsAndConditionsCommand : BaseCommand
    {
        private Func<RentalTermsAndConditionsControl> _factory;
        public string Next { get; set; }

        public RentalTermsAndConditionsCommand(Func<RentalTermsAndConditionsControl> Factory)
        {
            _factory = Factory;
        }

        public override void Invoke()
        {
            RentalTermsAndConditionsControl control = _factory();
            control.Dock = DockStyle.Fill;
            control.Next = OnNext;
            var main = GetFormAs<MainForm>();
            main.AddToFormPanel(control);
            
        }

        private void OnNext(object sender, object args)
        {
            Proceed(Next);
        }
    }
}
