﻿using System;
using System.Collections.Generic;
using iRentKiosk.Core.Models;
using iRentKiosk.Core.Services;
using System.Windows.Forms;
using iRent2.Contracts.Rental;
using MetroFramework.Controls;
using iRent2.Contracts.PayGate;
using Castle.Core;

namespace iRent.Rental
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class RentalShowBookingQuoteControl : MetroUserControl
    {
        public RentalShowBookingQuoteControl()
        {
            InitializeComponent();
        }

        public EventHandler<RegisterPaymentRequest> Next;
        public EventHandler Back;

        public void Bind(RentalQuoteBookingResponse quotedata)
        {
            RentalOptions options = ContextData.RentalOptions;
            txtVehicleGroup.Text = options.SelectedVehicle.VehicleModelInfo.Group.Name;
            txtVehicle.Text = options.SelectedVehicle.VehicleModelInfo.Make + " " + options.SelectedVehicle.VehicleModelInfo.Model + " " + options.SelectedVehicle.VehicleModelInfo.RegNo;
            txtFromDate.Text = options.FromDate.ToString();
            txtToDate.Text = options.ToDate.ToString();
            txtTotalCost.Text = quotedata.Total.ToString("R 0.00");
            txtIntiationFee.Text = quotedata.InitiationAmount.ToString("R 0.00");

            cmbPayMethod.ValueMember = "Key";
            cmbPayMethod.DisplayMember = "Value";
            GetPayTokensResponse blanktoken = new GetPayTokensResponse() { Token = ""};
            blanktoken.Id = -1;

            cmbPayMethod.Items.Clear();

            cmbPayMethod.Items.Add(new KeyValuePair<GetPayTokensResponse, string>(blanktoken, "[New Card]"));
            cmbPayMethod.SelectedIndex = 0;

            foreach (GetPayTokensResponse token in options.PaygateTokens)
            {
                string cleanedToken = ParseTokenNo(token.VaultData1);
                DateTime expiryDate = new DateTime(Convert.ToInt32(token.VaultData2.Substring(token.VaultData2.Length - 4)), Convert.ToInt32(token.VaultData2.Substring(0, 2)), 1,23,59,59);
                expiryDate.AddDays(DateTime.DaysInMonth(expiryDate.Year, expiryDate.Month) - 1);
                if (DateTime.Now > expiryDate)
                {
                    cleanedToken += " expired: " + token.VaultData2;
                }
                KeyValuePair<GetPayTokensResponse, string> keyval = new KeyValuePair<GetPayTokensResponse, string>(token,cleanedToken);
                cmbPayMethod.Items.Add(keyval);
            }
        }

        private string ParseTokenNo(string CardNo)
        {
            string CardIssuer = "";

            if (CardNo.StartsWith("4"))
            {
                CardIssuer = "[Visa]";
            }
            else if (CardNo.StartsWith("51") || CardNo.StartsWith("52") || CardNo.StartsWith("53") || CardNo.StartsWith("54") || CardNo.StartsWith("55"))
            {
                CardIssuer = "[MasterCard]";
            }
            else if (CardNo.StartsWith("56") || CardNo.StartsWith("67"))
            {
                CardIssuer = "[Maestro]";
            }
            else if (CardNo.StartsWith("36"))
            {
                CardIssuer = "[Diners Club]";
            }
            else if (CardNo.StartsWith("65"))
            {
                CardIssuer = "[Discover]";
            }
            else if (CardNo.StartsWith("35"))
            {
                CardIssuer = "[JCB]";
            }
            else if (CardNo.StartsWith("37"))
            {
                CardIssuer = "[American Express]";
            }

            return CardIssuer + " ending with **** " + CardNo.Substring(CardNo.Length - 4);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            RegisterPaymentRequest request = new RegisterPaymentRequest();
            RentalOptions options = ContextData.RentalOptions;
            request.Amount = options.QuoteData.Total;
            request.PayConfirmEmail = ContextData.Driver.Email;
            request.VaultToken = ((KeyValuePair<GetPayTokensResponse, string>)cmbPayMethod.SelectedItem).Key.Token;
            //request.Amount = q
            //Next(this,null);
            Next(this, request);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            Back(this, null);
        }
    }
}
