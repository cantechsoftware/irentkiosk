﻿using System;
using System.Windows.Forms;
using Castle.Core;
using iRent.Framework;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Models;
using iRent2.Contracts.Rental;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;

namespace iRent.Rental
{
    [CastleComponent]
    public class RentalStartCommand : BaseCommand
    {
        private Func<RentalStartControl> _factory;
        private RentalApi _rentalApi;

        public RentalStartCommand(Func<RentalStartControl> factory, RentalApi rentalApi)
        {
            _factory = factory;
            _rentalApi = rentalApi;
        }

        public string Next { get; set; }
        public string Back { get; set; }

        public override void Invoke()
        {
            var control = _factory();
            control.Dock = DockStyle.Fill;
            var main = GetFormAs<MainForm>();
            main.ToggleHomeButton(true);
            main.AddToFormPanel(control);
            control.Bind(ContextData.Kiosk);
            control.Next += OnNext;
            control.Back += OnBack;
        }

        private void OnBack(object sender, RentalVehicleGroupRequest contract)
        {
            Proceed(Back);
        }

        private void OnNext(object sender, RentalVehicleGroupRequest contract)
        {
            RentalVehicleGroupResponse[] contracts = _rentalApi.RentalVehicleClass(contract);

            RentalOptions options = new RentalOptions();
            options.FromDate = contract.FromDate;
            options.ToDate = contract.ToDate;
            options.VehicleGroups = contracts;
            //driver.IsRegistered = false;
            //driver = _driverApi.Save(driver);
            ContextData.Set(options);
            Proceed(Next);
        }
    }
}
