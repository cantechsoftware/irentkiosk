﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iRent2.Contracts.Models;
using Castle.Core;
using MetroFramework;
using MetroFramework.Controls;

namespace iRent.Rental
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class ListBookingControl : UserControl
    {
        public ListBookingControl()
        {
            InitializeComponent();
        }

        public event EventHandler<BookingModel> Next;
        public event EventHandler<BookingModel> Back;

        public void Bind(BookingModel[] bookings)
        {
            if (bookings != null && bookings.Length > 0)
            {
                foreach (BookingModel model in bookings)
                {
                    MetroButton button = new MetroButton();
                    button.FontSize = MetroButtonSize.XL;
                    button.Height = 124;
                    button.Width = 931;
                    button.Text = model.VehicleRegNo + " Picked up on: " + model.PickupDate.ToString("yyyy-MM-dd");
                    button.Tag = model;
                    button.Click += Booking_Click;
                    flowPanel.Controls.Add(button);
                }
            }
            else
            {
                MetroButton button = new MetroButton();
                button.FontSize = MetroButtonSize.XL;
                button.Height = 124;
                button.Width = 931;
                button.Text = "No Active Bookings found";
                button.TextAlign = ContentAlignment.MiddleCenter;
                button.Click += Back_Click;
                flowPanel.Controls.Add(button);
            }
        }

        private void Back_Click(object sender, EventArgs e)
        {
            MetroButton button = (MetroButton)sender;
            Back(this, null);
        }

        private void Booking_Click(object sender, EventArgs e)
        {
            MetroButton button = (MetroButton)sender;
            Next(this, (BookingModel)button.Tag);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            Back(this, null);
        }
    }
}
