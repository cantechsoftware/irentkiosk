﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Castle.Core;

namespace iRent.Rental
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class RentalSelectNationalityControl : UserControl
    {
        public EventHandler<object> IsCitizen;
        public EventHandler<object> NotCitizen;
        public EventHandler<object> Back;

        public RentalSelectNationalityControl()
        {
            InitializeComponent();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            IsCitizen(this, null);
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            NotCitizen(this, null);
        }

        private void metroButton3_Click(object sender, EventArgs e)
        {
            Back(this, null);
        }
    }
}
