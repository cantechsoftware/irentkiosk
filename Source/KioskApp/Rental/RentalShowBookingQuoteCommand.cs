﻿using System;
using System.Windows.Forms;
using Castle.Core;
using iRent.Framework;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Models;
using iRent2.Contracts.Rental;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;
using iRent2.Contracts.PayGate;
using System.Linq;
using iRent.Util;

namespace iRent.Rental
{
    [CastleComponent]
    public class RentalShowBookingQuoteCommand : BaseCommand
    {
        public string Next { get; set; }
        public string Back { get; set; }

        private Func<RentalShowBookingQuoteControl> _factory;

        private RentalApi _rentalApi;


        public RentalShowBookingQuoteCommand(Func<RentalShowBookingQuoteControl> factory, RentalApi RentalApi)
        {
            _rentalApi = RentalApi;
            _factory = factory;
        }

        public override void Invoke()
        {
            var control = _factory();
            control.Dock = DockStyle.Fill;
            control.Next += OnNext;
            control.Back += OnBack;
            var main = GetFormAs<MainForm>();
            main.AddToFormPanel(control);
            RentalOptions options = ContextData.RentalOptions;
            DriverModel driver = ContextData.Driver;

            RentalQuoteBookingRequest request = new RentalQuoteBookingRequest();
            request.FromDate = options.FromDate;
            request.ToDate = options.ToDate;
            request.VehicleId = options.SelectedVehicle.VehicleModelInfo.Id;
            request.WaverOption = options.SelectedWaiver.Id;
            request.RentalOptions = options.SelectedOptions.Select(x => x.Id).ToArray();
            request.DriverId = driver.Id;
            RentalQuoteBookingResponse response = _rentalApi.RentalQuoteBooking(request);

            GetPayTokensRequest tokenreq = new GetPayTokensRequest();
            tokenreq.DriverId = ContextData.Driver.Id;
            GetPayTokensResponse[] tokenresp = _rentalApi.RetrievePayTokens(tokenreq);
            options.PaygateTokens = tokenresp;

            options.QuoteData = response;
            control.Bind(response);
        }

        private void OnNext(object sender, RegisterPaymentRequest obj)
        {
            //Create web data object for Payment pages.
            ContextData.SetDynamicData("PayGateInit", obj);
            Proceed(Next);
        }

        private void OnBack(object sender, object obj)
        {
            Proceed(Back);
        }
    }
}
