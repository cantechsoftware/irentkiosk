﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MetroFramework;
using System.Windows.Forms;
using iRent2.Contracts.Rental;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Models;
using MetroFramework.Controls;
using Castle.Core;
using System.IO;

namespace iRent.Rental
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class RentalSelectVehicleControl : UserControl
    {
        public RentalSelectVehicleControl()
        {
            InitializeComponent();
        }

        private RentalSearchResponse[] _vehicles;

        private List<MetroTile> _activeTiles = new List<MetroTile>();

        public EventHandler<object> Next;
        public EventHandler<object> Back;

        public void Bind(RentalSearchResponse[] vehicles)
        {
            _vehicles = vehicles;

            foreach (MetroTile tile in _activeTiles)
            {
                if (tile != null)
                {
                    tile.Dispose();
                }
            }
            _activeTiles.Clear();

            foreach (RentalSearchResponse vehicle in _vehicles)
            {
                MetroTile tile = new MetroTile();
                tile.Text = "Make : "+vehicle.VehicleModelInfo.Make + "\r\nModel : " + vehicle.VehicleModelInfo.Model + "\r\nRegNo : "+  vehicle.VehicleModelInfo.RegNo;
                tile.TileTextFontSize = MetroTileTextSize.Medium;
                tile.TileTextFontWeight = MetroTileTextWeight.Bold;
                tile.Style = MetroColorStyle.Silver;
                tile.Dock = DockStyle.Fill;
                //using (var ms = new MemoryStream(ContextData.RentalOptions.VehicleGroups[0].Picture))
                //{
                //    tile.Image = Image.FromStream(ms);
                //}
                tile.Parent = tblPnlMain;
                tile.Tag = vehicle;
                tile.Click += SelectVehicle_Click;
                _activeTiles.Add(tile);
            }
        }

        private void SelectVehicle_Click(object Sender, EventArgs e)
        {
            MetroTile tile = (MetroTile)Sender;
            ContextData.RentalOptions.SelectedVehicle = (RentalSearchResponse)tile.Tag;
            Next(this, null);
        }


        //private void DynamicTile_Click(object sender, EventArgs e)
        //{
        //    MetroTile tile = (MetroTile)sender;
        //    //RentalClassContract contract = (RentalClassContract)tile.Tag;
        //    if (tile.Style == MetroColorStyle.Silver)
        //    {
        //        tile.Style = MetroColorStyle.Red;
        //    }
        //    else
        //    {
        //        tile.Style = MetroColorStyle.Silver;
        //    }
        //    RentalSearchResponse vehicle = (RentalSearchResponse)tile.Tag;
        //    Next(this, vehicle);
        //}

        private void btnNext_Click(object sender, EventArgs e)
        {
            //List<int> vehicleclasses = new List<int>();
            //foreach (MetroTile tile in _activeTiles)
            //{
            //    if (tile.Style == MetroFramework.MetroColorStyle.Red)
            //    {
            //        RentalVehicleGroupResponse contract = (RentalVehicleGroupResponse)tile.Tag;
            //        vehicleclasses.Add(contract.VehicleGroupID);
            //    }
            //}

            //RentalSearchRequest request = new RentalSearchRequest();
            //RentalOptions options = ContextData.RentalOptions;
            //request.FromDate = options.FromDate;
            //request.ToDate = options.ToDate;
            //request.VehicleClassList = vehicleclasses.ToArray();
            //request.BranchesList = new int[1] { ContextData.Kiosk.BranchId ?? 0 };

            //Next(this, request);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            Back(this, null);
        }
    }
}
