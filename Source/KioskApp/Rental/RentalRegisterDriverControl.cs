﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iRent2.Contracts.Models;
using Castle.Core;
using iRentKiosk.Core.Services.Api;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace iRent.Rental
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class RentalRegisterDriverControl : UserControl
    {
        DriverModel _driver;
        DriverApi _driverApi;
        private int resendcounter;

        public RentalRegisterDriverControl(DriverApi driverApi)
        {
            _driverApi = driverApi;
            InitializeComponent();
        }

        public event EventHandler<DriverModel> Next;
        public event EventHandler<DriverModel> Back;

        private void InitControls()
        {
            txtFirstName.Text = "";
            txtSurname.Text = "";
            txtEmail.Text = "";
            txtConfirmEmail.Text = "";
            txtCellNo.Text = "";
            txtWorkNo.Text = "";
            txtUnitNo.Text = "";
            txtComplex.Text = "";
            txtStreetNo.Text = "";
            txtStreetName.Text = "";
            txtCity.Text = "";
            txtProvince.Text = "";
            txtCountry.Text = "";
            txtPostalCode.Text = "";
            txtTown.Text = "";
            resendcounter = 0;
        }

        public void Bind(DriverModel driver)
        {
            _driver = driver;

            InitControls();
            lblError.Text = "Registration error information will apear here";

            txtIDNumber.Text = _driver.IdNumber;
            txtFirstName.Text = _driver.FirstName;
            txtSurname.Text = _driver.Surname;
            cmbTitle.SelectedIndex = -1;
            cmbTitle.SelectedIndex = cmbTitle.FindStringExact(_driver.Title);

            txtEmail.Text = _driver.Email;
            txtConfirmEmail.Text = _driver.Email;
            txtCellNo.Text = _driver.CellNo;
            if (_driver.Values != null)
            {
                if (_driver.Values.ContainsKey("WorkNo"))
                {
                    txtWorkNo.Text = _driver.Values["WorkNo"]?.ToString() ?? "";
                }
            }
            txtUnitNo.Text = _driver.PostalAddress?.HouseNo;
            txtComplex.Text = _driver.PostalAddress?.Complex;
            txtStreetNo.Text = _driver.PostalAddress?.StreetNo;
            txtStreetName.Text = _driver.PostalAddress?.StreetName;
            txtCity.Text = _driver.PostalAddress?.City;
            txtTown.Text = _driver.PostalAddress?.Suburb;
            txtProvince.Text = _driver.PostalAddress?.State;
            txtCountry.Text = _driver.PostalAddress?.Country;
            txtPostalCode.Text = _driver.PostalAddress?.PostCode;
        }

        private void SetError(string errortext)
        {
            lblError.ForeColor = Color.Red;
            lblError.Text = errortext;
            lblError.Visible = true;
            lblError.BringToFront();
        }

        private bool ValidateData()
        {
            if (cmbTitle.SelectedIndex == -1)
            {
                SetError("Please select a Title.");
                return false;
            }
            try
            {
                var m = new MailAddress(txtEmail.Text);
                //return true;
            }
            catch (FormatException)
            {
                SetError("Email is not in valid format.");
                return false;
            }

            if(txtPin.Text == txtConfirmPin.Text)
            {
                //string pswdregex = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$";
                //if (Regex.IsMatch(txtPin.Text, pswdregex))
                //    SetError("Password fields must match or requirements not met, \r\nat least 8 characters are required with one capital and one number.");
                //return false;
            }
            else if(txtPin.Text != txtConfirmPin.Text)
            {
                SetError("Password fields do not match.");
                return false;
            }
            else if (txtEmail.Text != txtConfirmEmail.Text)
            {
                SetError("Email fields must match.");
                return false;
            }
            else if (string.IsNullOrEmpty(txtIDNumber.Text.Trim()))
            {
                SetError("ID Number cannot be blank");
                return false;
            }
            else if (string.IsNullOrEmpty(txtFirstName.Text.Trim()))
            {
                SetError("First Name cannot be blank");
                return false;
            }
            else if (string.IsNullOrEmpty(txtSurname.Text.Trim()))
            {
                SetError("Surname cannot be blank");
                return false;
            }
            else if (string.IsNullOrEmpty(txtEmail.Text.Trim()))
            {
                SetError("Email cannot be blank");
                return false;
            }
            else if (string.IsNullOrEmpty(txtWorkNo.Text.Trim()))
            {
                SetError("Work Number cannot be blank");
                return false;
            }
            else if (string.IsNullOrEmpty(txtCellNo.Text.Trim()))
            {
                SetError("Cell Number cannot be blank");
                return false;
            }
            else if (string.IsNullOrEmpty(txtStreetName.Text.Trim()))
            {
                SetError("Street Name cannot be blank");
                return false;
            }
            else if (string.IsNullOrEmpty(txtStreetNo.Text.Trim()))
            {
                SetError("Street No cannot be blank");
                return false;
            }
            else if (string.IsNullOrEmpty(txtTown.Text.Trim()))
            {
                SetError("Suburb cannot be blank");
                return false;
            }
            else if (string.IsNullOrEmpty(txtCity.Text.Trim()))
            {
                SetError("City cannot be blank");
                return false;
            }
            else if (string.IsNullOrEmpty(txtProvince.Text.Trim()))
            {
                SetError("Province cannot be blank");
                return false;
            }
            else if (string.IsNullOrEmpty(txtCountry.Text.Trim()))
            {
                SetError("Country cannot be blank");
                return false;
            }
            else if (string.IsNullOrEmpty(txtPostalCode.Text.Trim()))
            {
                SetError("PostalCode cannot be blank");
                return false;
            }
            else if (!(txtEmail.Text.Contains('@') && txtEmail.Text.Contains('.')))
            {
                SetError("Email must be a valid email address");
                return false;
            }

            lblHeader.Text = "We need some personal details before you can continue";
            lblHeader.ForeColor = Color.Black;
            return true;
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (ValidateData())
            {
                _driver.Title = cmbTitle.Text;
                _driver.FirstName = txtFirstName.Text;
                _driver.Surname = txtSurname.Text;
                _driver.Email = txtEmail.Text;
                _driver.CellNo = txtCellNo.Text;
                _driver["WorkNo"] = txtWorkNo.Text;

                if (_driver.Values.ContainsKey("CustomerPin"))
                {
                    _driver.Values["CustomerPin"] = txtPin.Text;
                }
                else
                {
                    _driver.Values.Add("CustomerPin", txtPin.Text);
                }

                if (_driver.PostalAddress == null)
                {
                    _driver.PostalAddress = new AddressModel();
                }
                _driver.PostalAddress.HouseNo = txtUnitNo.Text;
                _driver.PostalAddress.Complex = txtComplex.Text;
                _driver.PostalAddress.StreetNo = txtStreetNo.Text;
                _driver.PostalAddress.StreetName = txtStreetName.Text;
                _driver.PostalAddress.Suburb = txtTown.Text;
                _driver.PostalAddress.City = txtCity.Text;
                _driver.PostalAddress.State = txtProvince.Text;
                _driver.PostalAddress.Country = txtCountry.Text;
                _driver.PostalAddress.PostCode = txtPostalCode.Text;
                Next(this, _driver);

            }
        }

        private void btnOTP_Click(object sender, EventArgs e)
        {
            if(btnOTP.Text.Contains("verify"))
            {
                //checkotp api call
                if(txtOTP.Text.Length == 4)
                {
                    bool res = _driverApi.CheckOTP(txtCellNo.Text, txtOTP.Text);  
                    if (res)
                    {
                        btnNext.Visible = true;
                        btnOTP.Visible = false;
                        txtOTP.Visible = false;
                    }
                }
            }
            else
            {
                //send otp cal
                if (txtCellNo.Text != "" && txtCellNo.Text.Length > 9)
                {
                    string res = _driverApi.SendOTP(txtCellNo.Text);
                    txtOTP.PromptText = res;
                    btnOTP.Text = "Resend OTP";
                    resendcounter++;
                }
                if(resendcounter > 4)
                {
                    lblOTP.Visible = true;
                    btnOTP.Visible = false;
                    txtOTP.Visible = false;
                }
            }
        }

        private void txtOTP_KeyUp(object sender, KeyEventArgs e)
        {
            if(txtOTP.Text.Length == 4)
            {
                btnOTP.Text = "Click to verify";
            }
            else
            {
                btnOTP.Text = "Resend OTP";
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            Back(this, null);
        }

        private void btnShowPassword_Click(object sender, EventArgs e)
        {
            if (txtPin.PasswordChar != '*')
                txtPin.PasswordChar = '*';
            else
                txtPin.PasswordChar = '\0';
            if (txtConfirmPin.PasswordChar != '*')
                txtConfirmPin.PasswordChar = '*';
            else
                txtConfirmPin.PasswordChar = '\0';
        }
    }
}
