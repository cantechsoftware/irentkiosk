﻿using System;
using System.Linq;
using CanTech.Common.Extensions;
using Castle.Core;
using MetroFramework.Controls;
using iRent2.Contracts.Models;
using SmartFormat;

namespace iRent.Rental
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class RentalSummaryControl : MetroUserControl
    {
        public RentalSummaryControl()
        {
            InitializeComponent();
        }

        public event EventHandler Issue;
        //public event EventHandler Next;
        public EventHandler<object> Next;

        private void btnNext_Click(object sender, EventArgs e)
        {
            Next(this, null);
            //if (Issue != null)
            //    Issue(this, e);
        }

        public void Init(string intro, string status, BookingModel booking)
        {
            txtVehicleGroup.Text = booking.VehicleRegNo;
            txtDrivers.Text = booking.AdditionalDrivers.Select(x => x.FirstName + " " + x.Surname).Join(", ");
            if (booking.AdditionalDrivers.Length == 0)
                txtDrivers.Text = "none";
            txtWaiver.Text = booking.WaiverOption.Name;
            txtReturnBranch.Text = booking.ReturnBranch.BranchName;
            txtReturnDateTime.Text = booking.ReturnDate.ToString("ddd d MMM yyyy, HH:mm");
            lblIntroText.Text = intro;
            lblStatus.Text = status;
            txtKilometres.Text = "R {0:0.00} / km, {1} free kms / day".FormatSmart(booking.RatePerKm, booking.FreeKms ?? 0);
        }
    }
}