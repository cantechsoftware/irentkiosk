﻿namespace iRent.Rental
{
    partial class RentalDamageInspectionControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblHeader = new MetroFramework.Controls.MetroLabel();
            this.metroPanel3 = new MetroFramework.Controls.MetroPanel();
            this.btnPrint = new MetroFramework.Controls.MetroButton();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.btnNext = new MetroFramework.Controls.MetroButton();
            this.pnlThumbImages = new MetroFramework.Controls.MetroPanel();
            this.flowThumbs = new System.Windows.Forms.FlowLayoutPanel();
            this.pnlLargeImage = new MetroFramework.Controls.MetroPanel();
            this.picLarge = new System.Windows.Forms.PictureBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.printDocument = new System.Drawing.Printing.PrintDocument();
            this.pnlNoImages = new MetroFramework.Controls.MetroPanel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.btnBack = new MetroFramework.Controls.MetroButton();
            this.metroPanel3.SuspendLayout();
            this.pnlThumbImages.SuspendLayout();
            this.pnlLargeImage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLarge)).BeginInit();
            this.pnlNoImages.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblHeader
            // 
            this.lblHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblHeader.FontSize = MetroFramework.MetroLabelSize.XL;
            this.lblHeader.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblHeader.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblHeader.Location = new System.Drawing.Point(0, 0);
            this.lblHeader.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(1126, 62);
            this.lblHeader.TabIndex = 15;
            this.lblHeader.Text = "We need you to review damages assigned to this vehicle";
            this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // metroPanel3
            // 
            this.metroPanel3.Controls.Add(this.btnBack);
            this.metroPanel3.Controls.Add(this.btnPrint);
            this.metroPanel3.Controls.Add(this.metroButton1);
            this.metroPanel3.Controls.Add(this.btnNext);
            this.metroPanel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.metroPanel3.HorizontalScrollbarBarColor = true;
            this.metroPanel3.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel3.HorizontalScrollbarSize = 10;
            this.metroPanel3.Location = new System.Drawing.Point(0, 543);
            this.metroPanel3.Name = "metroPanel3";
            this.metroPanel3.Size = new System.Drawing.Size(1126, 122);
            this.metroPanel3.TabIndex = 17;
            this.metroPanel3.VerticalScrollbarBarColor = true;
            this.metroPanel3.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel3.VerticalScrollbarSize = 10;
            // 
            // btnPrint
            // 
            this.btnPrint.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnPrint.Location = new System.Drawing.Point(40, 4);
            this.btnPrint.Margin = new System.Windows.Forms.Padding(4);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(259, 115);
            this.btnPrint.TabIndex = 18;
            this.btnPrint.Text = "&Print";
            this.btnPrint.UseSelectable = true;
            this.btnPrint.Visible = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // metroButton1
            // 
            this.metroButton1.FontSize = MetroFramework.MetroButtonSize.XL;
            this.metroButton1.Location = new System.Drawing.Point(806, 7);
            this.metroButton1.Margin = new System.Windows.Forms.Padding(4);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(259, 115);
            this.metroButton1.TabIndex = 17;
            this.metroButton1.Text = "No I do not accept";
            this.metroButton1.UseSelectable = true;
            this.metroButton1.Visible = false;
            this.metroButton1.Click += new System.EventHandler(this.metroButton1_Click);
            // 
            // btnNext
            // 
            this.btnNext.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnNext.Location = new System.Drawing.Point(430, 7);
            this.btnNext.Margin = new System.Windows.Forms.Padding(4);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(259, 99);
            this.btnNext.TabIndex = 16;
            this.btnNext.Text = "&Continue";
            this.btnNext.UseSelectable = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // pnlThumbImages
            // 
            this.pnlThumbImages.Controls.Add(this.flowThumbs);
            this.pnlThumbImages.HorizontalScrollbarBarColor = true;
            this.pnlThumbImages.HorizontalScrollbarHighlightOnWheel = false;
            this.pnlThumbImages.HorizontalScrollbarSize = 10;
            this.pnlThumbImages.Location = new System.Drawing.Point(40, 65);
            this.pnlThumbImages.Name = "pnlThumbImages";
            this.pnlThumbImages.Size = new System.Drawing.Size(1038, 427);
            this.pnlThumbImages.TabIndex = 18;
            this.pnlThumbImages.VerticalScrollbarBarColor = true;
            this.pnlThumbImages.VerticalScrollbarHighlightOnWheel = false;
            this.pnlThumbImages.VerticalScrollbarSize = 10;
            // 
            // flowThumbs
            // 
            this.flowThumbs.AutoScroll = true;
            this.flowThumbs.BackColor = System.Drawing.Color.White;
            this.flowThumbs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowThumbs.Location = new System.Drawing.Point(0, 0);
            this.flowThumbs.Name = "flowThumbs";
            this.flowThumbs.Size = new System.Drawing.Size(1038, 427);
            this.flowThumbs.TabIndex = 2;
            // 
            // pnlLargeImage
            // 
            this.pnlLargeImage.Controls.Add(this.picLarge);
            this.pnlLargeImage.Controls.Add(this.metroLabel1);
            this.pnlLargeImage.HorizontalScrollbarBarColor = true;
            this.pnlLargeImage.HorizontalScrollbarHighlightOnWheel = false;
            this.pnlLargeImage.HorizontalScrollbarSize = 10;
            this.pnlLargeImage.Location = new System.Drawing.Point(14, 94);
            this.pnlLargeImage.Name = "pnlLargeImage";
            this.pnlLargeImage.Size = new System.Drawing.Size(1038, 427);
            this.pnlLargeImage.TabIndex = 19;
            this.pnlLargeImage.VerticalScrollbarBarColor = true;
            this.pnlLargeImage.VerticalScrollbarHighlightOnWheel = false;
            this.pnlLargeImage.VerticalScrollbarSize = 10;
            // 
            // picLarge
            // 
            this.picLarge.BackColor = System.Drawing.Color.Transparent;
            this.picLarge.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picLarge.Location = new System.Drawing.Point(0, 0);
            this.picLarge.Name = "picLarge";
            this.picLarge.Size = new System.Drawing.Size(1038, 387);
            this.picLarge.TabIndex = 17;
            this.picLarge.TabStop = false;
            this.picLarge.Click += new System.EventHandler(this.picLarge_Click);
            // 
            // metroLabel1
            // 
            this.metroLabel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.XL;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.metroLabel1.Location = new System.Drawing.Point(0, 387);
            this.metroLabel1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(1038, 40);
            this.metroLabel1.TabIndex = 16;
            this.metroLabel1.Text = "Hi we need you to review damages assigned to this vehicle";
            this.metroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroLabel1.Visible = false;
            // 
            // pnlNoImages
            // 
            this.pnlNoImages.Controls.Add(this.metroLabel2);
            this.pnlNoImages.HorizontalScrollbarBarColor = true;
            this.pnlNoImages.HorizontalScrollbarHighlightOnWheel = false;
            this.pnlNoImages.HorizontalScrollbarSize = 10;
            this.pnlNoImages.Location = new System.Drawing.Point(27, 79);
            this.pnlNoImages.Name = "pnlNoImages";
            this.pnlNoImages.Size = new System.Drawing.Size(1071, 427);
            this.pnlNoImages.TabIndex = 20;
            this.pnlNoImages.VerticalScrollbarBarColor = true;
            this.pnlNoImages.VerticalScrollbarHighlightOnWheel = false;
            this.pnlNoImages.VerticalScrollbarSize = 10;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.FontSize = MetroFramework.MetroLabelSize.XL;
            this.metroLabel2.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel2.Location = new System.Drawing.Point(218, 177);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(590, 31);
            this.metroLabel2.TabIndex = 2;
            this.metroLabel2.Text = "The vehicle assigned to your booking has no damages";
            // 
            // btnBack
            // 
            this.btnBack.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnBack.Location = new System.Drawing.Point(53, 7);
            this.btnBack.Margin = new System.Windows.Forms.Padding(4);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(259, 99);
            this.btnBack.TabIndex = 19;
            this.btnBack.Text = "&Back";
            this.btnBack.UseSelectable = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // RentalDamageInspectionControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlThumbImages);
            this.Controls.Add(this.pnlNoImages);
            this.Controls.Add(this.pnlLargeImage);
            this.Controls.Add(this.metroPanel3);
            this.Controls.Add(this.lblHeader);
            this.Name = "RentalDamageInspectionControl";
            this.Size = new System.Drawing.Size(1126, 665);
            this.metroPanel3.ResumeLayout(false);
            this.pnlThumbImages.ResumeLayout(false);
            this.pnlLargeImage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picLarge)).EndInit();
            this.pnlNoImages.ResumeLayout(false);
            this.pnlNoImages.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroLabel lblHeader;
        private MetroFramework.Controls.MetroPanel metroPanel3;
        private MetroFramework.Controls.MetroButton btnNext;
        private MetroFramework.Controls.MetroButton metroButton1;
        private MetroFramework.Controls.MetroPanel pnlThumbImages;
        private MetroFramework.Controls.MetroPanel pnlLargeImage;
        private System.Windows.Forms.PictureBox picLarge;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private System.Windows.Forms.FlowLayoutPanel flowThumbs;
        private MetroFramework.Controls.MetroButton btnPrint;
        private System.Drawing.Printing.PrintDocument printDocument;
        private MetroFramework.Controls.MetroPanel pnlNoImages;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroButton btnBack;
    }
}
