﻿using System;
using System.Collections.Generic;
using iRentKiosk.Core.Models;
using iRentKiosk.Core.Services;
using System.Windows.Forms;
using iRent2.Contracts.Rental;
using MetroFramework.Controls;
using Castle.Core;
using System.IO;

namespace iRent.Rental
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class RentalSelectVehicleGroupControl : MetroUserControl
    {
        private RentalVehicleGroupResponse[] _vehicleClasses;

        private List<MetroTile> _activeTiles = new List<MetroTile>();

        public EventHandler<RentalSearchRequest> Next;
        public EventHandler<RentalSearchRequest> Back;

        public RentalSelectVehicleGroupControl()
        {
            InitializeComponent();
        }

        private void UpdateControls()
        {

        }

        private string FormattedAmount(decimal? _amount)
        {
            return _amount == null ? "null" : string.Format("{0:C}", _amount.Value);
        }


        public void Bind(RentalVehicleGroupResponse[] vehicleClasses)
        {
            _vehicleClasses = vehicleClasses;
            foreach (MetroTile tile in _activeTiles)
            {
                if (tile != null)
                {
                    tile.Dispose();
                }
            }
            _activeTiles.Clear();
            bool vehiclesavailable = false;
            btnNext.Enabled = true;
            tblPnlMain.ColumnCount = 4;
            tblPnlMain.RowCount = 2;
            tblPnlMain.Visible = true;
            pnlNoCars.Visible = false;

            foreach (RentalVehicleGroupResponse contract in _vehicleClasses)
            {
                if (!contract.IsDeleted)
                {
                    vehiclesavailable = true;
                    //Create controls for each corresponding contract.
                    MetroTile tile = new MetroTile();
                    tile.Text = "Group " + contract.Name + "\r\n" + FormattedAmount(contract.Cost);
                    tile.TileTextFontSize = MetroFramework.MetroTileTextSize.Medium;
                    tile.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold;
                    tile.Style = MetroFramework.MetroColorStyle.Silver;
                    tile.Dock = DockStyle.Fill;
                    //tile.Image = contract.Im
                    tile.Parent = tblPnlMain;
                    tile.Tag = contract;
                    tile.Size = new System.Drawing.Size(1024, 768);
                    tile.AutoSize = true;
                    tile.Click += DynamicTile_Click;



                    PictureBox picture = new PictureBox();
                    if (contract.Picture != null)
                    {
                        using (var ms = new MemoryStream(contract.Picture))
                        {
                            picture.Image = System.Drawing.Image.FromStream(ms);
                        }
                    }
                    //picture.Location = new Point(0x2b, 0x11);
                    picture.Name = "picture" + contract.Name;
                    //picture.Size = new Size(120, 0x5b);
                    picture.SizeMode = PictureBoxSizeMode.StretchImage;
                    picture.TabIndex = 0;
                    picture.TabStop = false;
                    picture.Click += DynamicTile_Click;
                    picture.SizeMode = PictureBoxSizeMode.StretchImage;
                    System.Drawing.Size size = new System.Drawing.Size();
                    size.Width = tile.Width;
                    size.Height = tile.Height - 50;
                    picture.Size = size;

                    tile.Controls.Add(picture);
                    _activeTiles.Add(tile);
                }
            }
            if(!vehiclesavailable)
            {
                tblPnlMain.Visible = false;
                pnlNoCars.Visible = true;
                btnNext.Enabled = false;
            }
        }

        private void DynamicTile_Click(object sender, EventArgs e)
        {
            //foreach (MetroTile tiles in _activeTiles)
            //{
            //    tiles.Style = MetroFramework.MetroColorStyle.Silver;
            //}

            PictureBox pic = new PictureBox();
            MetroTile tile = new MetroTile();
            if (sender.GetType().Name == "PictureBox")
            {
                pic = (PictureBox)sender;
                tile = (MetroTile)pic.Parent;
            }
            if (sender.GetType().Name == "MetroTile")
            {
                tile = (MetroTile)sender;
            }

            RentalVehicleGroupResponse contract = (RentalVehicleGroupResponse)tile.Tag;

            RentalSearchRequest request = new RentalSearchRequest();
            RentalOptions options = ContextData.RentalOptions;
            request.FromDate = options.FromDate;
            request.ToDate = options.ToDate;
            List<int> vehicleclasses = new List<int>();
            vehicleclasses.Add(contract.VehicleGroupID);
            request.VehicleClassList = vehicleclasses.ToArray();
            request.BranchesList = new int[1] { ContextData.Kiosk.BranchId ?? 0 };

            Next(this, request);
            //if (tile.Style == MetroFramework.MetroColorStyle.Silver)
            //{
            //    tile.Style = MetroFramework.MetroColorStyle.Red;
            //}
            //this.Refresh();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            //List<int> vehicleclasses = new List<int>();
            //foreach (MetroTile tile in _activeTiles)
            //{
            //    if (tile.Style == MetroFramework.MetroColorStyle.Red)
            //    {
            //        RentalVehicleGroupResponse contract = (RentalVehicleGroupResponse)tile.Tag;
            //        vehicleclasses.Add(contract.VehicleGroupID);
            //    }
            //}

            //if (vehicleclasses.Count > 0)
            //{
            //    RentalSearchRequest request = new RentalSearchRequest();
            //    RentalOptions options = ContextData.RentalOptions;
            //    request.FromDate = options.FromDate;
            //    request.ToDate = options.ToDate;
            //    request.VehicleClassList = vehicleclasses.ToArray();
            //    request.BranchesList = new int[1] { ContextData.Kiosk.BranchId ?? 0 };

            //    Next(this, request);
            //}
            //else
            //{
            //    return;
            //}
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            Back(this, null);
        }
    }
}
