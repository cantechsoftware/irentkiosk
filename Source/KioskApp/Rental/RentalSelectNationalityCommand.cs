﻿using System;
using System.Windows.Forms;
using Castle.Core;
using SmartFormat;
using iRent.Framework;
using iRentKiosk.Core.Services;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Services.Api;

namespace iRent.Rental
{
    [CastleComponent]
    public class RentalSelectNationalityCommand : BaseCommand
    {
        public string IsCitizen { get; set; }
        public string NotCitizen { get; set; }
        public string Back { get; set; }

        private Func<RentalSelectNationalityControl> _factory;

        public RentalSelectNationalityCommand(Func<RentalSelectNationalityControl> factory, RentalApi RentalApi)
        {
            _factory = factory;
        }

        public override void Invoke()
        {
            var control = _factory();
            control.Dock = DockStyle.None;
            control.IsCitizen += OnIsCitizen;
            control.NotCitizen += OnNotCitizen;
            control.Back += OnBack;
            var main = GetFormAs<MainForm>();
            main.ToggleHomeButton(true);
            main.AddToFormPanel(control);
        }

        private void OnBack(object sender, object obj)
        {
            //Create web data object for Payment pages.
            //dynamic webdata = 

            Proceed(Back);
        }

        private void OnNotCitizen(object sender, object obj)
        {
            //Create web data object for Payment pages.
            //dynamic webdata = 

            Proceed(NotCitizen);
        }


        private void OnIsCitizen(object sender, object obj)
        {
            //Create web data object for Payment pages.
            //dynamic webdata = 

            Proceed(IsCitizen);
        }

    }
}
