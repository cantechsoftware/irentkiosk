﻿using System;
using System.Windows.Forms;
using Castle.Core;
using SmartFormat;
using iRent.Framework;
using iRentKiosk.Core.Services;

namespace iRent.Rental
{
    [CastleComponent]
    public class BookingQuoteCommand : BaseCommand
    {
        private readonly Func<BookingQuoteControl> _factory;

        public BookingQuoteCommand(Func<BookingQuoteControl> factory)
        {
            _factory = factory;
            Intro = "Please confirm your booking details:";
            Status = "Please click Next to continue.";
        }

        public string Intro { get; set; }
        public string Status { get; set; }
        public string Next { get; set; }
        public string Back { get; set; }

        public override void Invoke()
        {
            var control = _factory();
            control.Dock = DockStyle.Fill;
            var main = GetFormAs<MainForm>();
            main.AddToFormPanel(control);
            var quote = BookingApi.GetQuote(ContextData.Booking);
            control.Setup(Intro.FormatSmart(ContextData.Instance),
                          Status.FormatSmart(ContextData.Instance),
                          quote);
            control.Next += (o, e) =>
                {
                    ContextData.VehicleCost = quote.Total;
                    Proceed(Next);
                };
            control.Back += (o, e) => Proceed(Back);
        }
    }
}