﻿using System;
using System.Windows.Forms;
using Castle.Core;
using MetroFramework;
using MetroFramework.Controls;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Services;
using SmartFormat;

namespace iRent.Rental
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class BookingQuoteControl : MetroUserControl
    {
        public BookingQuoteControl()
        {
            InitializeComponent();
        }

        public event EventHandler Next;
        public event EventHandler Back;

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (Next != null)
                Next(this, e);
        }

        private void cmdBack_Click(object sender, EventArgs e)
        {
            if (Back != null)
                Back(this, e);
        }

        public void Setup(string intro, string status, QuoteModel quote)
        {
            lblIntroText.Text = intro;
            lblStatus.Text = status;
            if (ContextData.VehicleGroup != null)
                AddRow("Vehicle Group:", ContextData.VehicleGroup.Name);
            if (ContextData.VehicleName != null)
                AddRow("Vehicle:", ContextData.VehicleName);
            if (ContextData.VehicleReturnDate.HasValue)
                AddRow("Return Date:", ContextData.VehicleReturnDate.Value.ToString("ddd d MMM yyyy HH:mm"));
            if (ContextData.ReturnBranch != null)
                AddRow("Return To:", ContextData.ReturnBranch.BranchName);

            AddRow("Kilometers:",
                "R {0:0.00} / km, {1} free kms / day".FormatSmart(ContextData.Booking.RatePerKm,
                    ContextData.Booking.FreeKms ?? 0));

            foreach (var line in quote.Lines)
            {
                AddRow(line.Description, line.Amount.ToString("C"));
            }

            AddLine();
            AddRow("Total Ex VAT:", quote.TotalExVat.ToString("C"));
            AddRow("VAT:", quote.Vat.ToString("C"));
            AddLine();
            AddRow("Total:", quote.Total.ToString("C"));
        }

        private void AddLine()
        {
            var pnl = new Panel {BorderStyle = BorderStyle.FixedSingle, Dock = DockStyle.Top, Height = 1};
            pnlSummary.Controls.Add(pnl);
            pnl.BringToFront();
        }

        private Panel AddRow(string description, string value)
        {
            var pnl = new Panel
                {
                    Dock = DockStyle.Top,
                    Height = 45,
                    Padding = new Padding(5)
                };

            var lbl = new MetroLabel
                {
                    Text = description,
                    Height = 40,
                    Margin = new Padding(10),
                    AutoSize = false,
                    Dock = DockStyle.Fill,
                    FontSize = MetroLabelSize.Tall
                };

            pnl.Controls.Add(lbl);
            pnl.Controls.Add(new MetroTextBox
                {
                    ReadOnly = true,
                    Text = value,
                    Dock = DockStyle.Right,
                    Width = 300,
                    AutoSize = false,
                    FontSize = MetroTextBoxSize.Tall,
                    FontWeight = MetroTextBoxWeight.Bold,
                    Margin = new Padding(0, 0, 25, 0)
                });
            pnlSummary.Controls.Add(pnl);
            pnl.BringToFront();

            return pnl;
        }
    }
}