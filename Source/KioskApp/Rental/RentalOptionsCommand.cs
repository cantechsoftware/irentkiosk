﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iRent.Framework;
using iRentKiosk.Core.Services.Api;
using Castle.Core;
using System.Windows.Forms;
using iRent2.Contracts.Models;
using iRent2.Contracts.Finance;
using iRent2.Contracts.AdoModels;
using iRentKiosk.Core.Services;
using iRent2.Contracts.Enums;
using RentalOptionModel = iRent2.Contracts.AdoModels.RentalOptionModel;

namespace iRent.Rental
{
    [CastleComponent]
    public class RentalOptionsCommand : BaseCommand
    {
        public RentalOptionsCommand(Func<RentalOptionsControl> Factory, RentalApi RentalApi)
        {
            _factory = Factory;
            _rentalApi = RentalApi;
        }


        public string Next { get; set; }
        public string Back { get; set; }

        private Func<RentalOptionsControl> _factory;

        private RentalApi _rentalApi;


        public override void Invoke()
        {
            var control = _factory();
            control.Dock = DockStyle.Fill;
            control.Next = OnNext;
            control.Back = OnBack;

            RentalOptionModel[] options = _rentalApi.GetRentalOptions();
            WaiverOptionModel[] waivers = _rentalApi.GetWaiverOptions();


            foreach(RentalOptionModel opt in options)
            {
                FeeCostingRequest req = new FeeCostingRequest
                {
                    VehicleGroupId = ContextData.RentalOptions.SelectedVehicle.VehicleModelInfo.Group.Id,
                    FeeCode = FinCodes.RentalOptionCharge,
                    FromDate = ContextData.RentalOptions.SelectedVehicle.FromDate,
                    ToDate = ContextData.RentalOptions.SelectedVehicle.ToDate,
                    SubFeeCode = opt.SubFeeCode
                };

                FeeCostingResult feeres = _rentalApi.GetFeeCosting(req);
                opt.Charge = (decimal)feeres.Total;
                decimal unitcost = (decimal)feeres.UnitCost;
                opt.Name = opt.Name + " (" + FormattedAmount(unitcost) + "p/h)";

            }

            foreach (WaiverOptionModel waive in waivers)
            {
                FeeCostingRequest req = new FeeCostingRequest
                {
                    VehicleGroupId = ContextData.RentalOptions.SelectedVehicle.VehicleModelInfo.Group.Id,
                    FeeCode = FinCodes.InsuranceWaiverCharge,
                    FromDate = ContextData.RentalOptions.SelectedVehicle.FromDate,
                    ToDate = ContextData.RentalOptions.SelectedVehicle.ToDate,
                    SubFeeCode = waive.SubFeeCode
                };

                FeeCostingResult feeres = _rentalApi.GetFeeCosting(req);
                waive.Excess = (decimal)feeres.Total;
                decimal unitcost = (decimal)feeres.UnitCost;
                waive.Name = waive.Name + " (" + FormattedAmount(unitcost) + "p/h)";
            }

            ContextData.RentalOptions.Options = options;
            ContextData.RentalOptions.WaiverOptions = waivers;

            control.Next += OnNext;
            control.Back += OnBack;
            var main = GetFormAs<MainForm>();
            main.AddToFormPanel(control);
            control.Bind();
        }

        private string FormattedAmount(decimal? _amount)
        {
            return _amount == null ? "null" : string.Format("{0:C}", _amount.Value);
        }


        private void OnBack(object sender, object obj)
        {
            //Create web data object for Payment pages.
            //dynamic webdata = 

            Proceed(Back);
        }

        private void OnNext(object sender, object obj)
        {
            //Create web data object for Payment pages.
            //dynamic webdata = 

            Proceed(Next);
        }
    }
}
