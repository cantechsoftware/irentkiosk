﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.Core;
using System.Windows.Forms;
using iRent2.Contracts.Rental;
using iRent2.Contracts.Models;
using iRent.Rental.UserControls;
using System.IO;
using System.Drawing.Drawing2D;
using System.Resources;
using System.Configuration;
using TheArtOfDev.HtmlRenderer.PdfSharp;
using PdfSharp.Pdf;

namespace iRent.Rental
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class RentalDamageInspectionControl : UserControl
    {
        public RentalDamageInspectionControl()
        {
            InitializeComponent();

        }

        public event EventHandler<RentalInspectionRequest> Next;
        public event EventHandler<RentalInspectionRequest> Back;
        private RentalInspectionRequest _request;
        private VehicleModel _vehicle;

        private List<ImageContainer> Images = new List<ImageContainer>();

        public void Bind(RentalInspectionRequest request, VehicleModel vehicle)
        {
            _request = request;
            _vehicle = vehicle;
            pnlThumbImages.Dock = DockStyle.Fill;
            pnlLargeImage.Dock = DockStyle.Fill;
            pnlNoImages.Dock = DockStyle.Fill;
            pnlThumbImages.BringToFront();
            BuildThumImages(request);
            if (Back == null)
                btnBack.Visible = false;
            else
                btnBack.Visible = true;
        }

        private string BuildDamagesHtml(string damageshtml)
        {
            string ret = damageshtml;
            ret = ret.Replace("<regno>", _vehicle.RegNo);
            ret = ret.Replace("<documentdate>", _vehicle.RegNo);
            ret = ret.Replace("<drivername>", _vehicle.RegNo);
            ret = ret.Replace("<driveremail>", _vehicle.RegNo);
            ret = ret.Replace("<bookingno>", _vehicle.RegNo);
            ret = ret.Replace("<fromdate>", _vehicle.RegNo);
            ret = ret.Replace("<todate>", _vehicle.RegNo);
            string lineitems = "";
            string imagename = "";
            int imgcnt = 0;
            foreach (Control c in flowThumbs.Controls)
            {
                if (c.GetType() == typeof(ImageContainer))
                {
                    ImageContainer imagecontainer = c as ImageContainer;
                    foreach (Control ic in imagecontainer.Controls)
                    {
                        if (ic.GetType() == typeof(PictureBox))
                        {
                            PictureBox pic = ic as PictureBox;
                            Image i = pic.Image;
                            imagename = _request.BookingID.ToString() + imgcnt.ToString();
                            imagename = @"C:\cefsharp\" + imagename+".jpeg";
                            i.Save(imagename);
                            lineitems += "<tr class='details'><td><img src = '" + imagename + "' alt = 'Smiley face' height = '42' width = '42' ></td>";
                            imgcnt++;
                        }

                        if (ic.GetType() == typeof(MetroFramework.Controls.MetroLabel))
                        {
                            string data = ic.Text;
                            lineitems += "<td>" + data +" </td></tr> ";
                        }
                    }
                }
            }
            ret = ret.Replace("<lineitems>", lineitems);
            return ret;
        }

        public void BuildThumImages(RentalInspectionRequest request)
        {
            foreach (Control c in flowThumbs.Controls)
            {
                flowThumbs.Controls.Remove(c);
            }

            if (request.Damages != null && request.Damages.Length > 0)
            {
                pnlThumbImages.Visible = true;
                pnlLargeImage.Visible = true;
                pnlNoImages.Visible = false;

                foreach (DamageModel damage in request.Damages)
                {
                    ImageContainer container = new ImageContainer();
                    container.Text = damage.Location + " - " + damage.DamageType;
                    PictureBox pic = container.Image;
                    pic.SizeMode = PictureBoxSizeMode.Zoom;
                    if (damage.Image != null)
                    {
                        MemoryStream memstream = new MemoryStream(damage.Image.Data);
                        Image img = Image.FromStream(memstream);
                        pic.Image = img;
                        pic.Tag = damage;
                    }
                    pic.Click += picThumb_Click;
                    flowThumbs.Controls.Add(container);
                }
            }
            else
            {
                pnlThumbImages.Visible = false;
                pnlLargeImage.Visible = false;
                pnlNoImages.Visible = true;
                ImageContainer container = new ImageContainer();
                container.Width = 500;
                container.Text = "The vehicle assigned to you has no damages.";
                Font f = new Font("Times New Roman", 20.0f);
                container.Font = f;
                flowThumbs.Controls.Add(container);
            }
        }


        public void BuildLargeImage()
        {

        }

        private void picThumb_Click(object sender, EventArgs e)
        {
            PictureBox srcpic = (PictureBox)sender;
            picLarge.Image = srcpic.Image;
            picLarge.SizeMode = PictureBoxSizeMode.Zoom;
            pnlLargeImage.BringToFront();
        }

        private void picLarge_Click(object sender, EventArgs e)
        {
            //hide this panel.
            pnlThumbImages.BringToFront();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            //go go
            _request.Status = iRent2.Contracts.Enums.RentalInspectionStatus.Accepted;
            Next(this, _request);
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            //no go 
            _request.Status = iRent2.Contracts.Enums.RentalInspectionStatus.NotAccepted;
            Next(this, _request);
        }

        private static Image ResizeImage(Image image, Size size,
            bool preserveAspectRatio = true)
        {
            int newWidth;
            int newHeight;
            if (preserveAspectRatio)
            {
                int originalWidth = image.Width;
                int originalHeight = image.Height;
                float percentWidth = (float)size.Width / (float)originalWidth;
                float percentHeight = (float)size.Height / (float)originalHeight;
                float percent = percentHeight < percentWidth ? percentHeight : percentWidth;
                newWidth = (int)(originalWidth * percent);
                newHeight = (int)(originalHeight * percent);
            }
            else
            {
                newWidth = size.Width;
                newHeight = size.Height;
            }
            Image newImage = new Bitmap(newWidth, newHeight);
            using (Graphics graphicsHandle = Graphics.FromImage(newImage))
            {
                graphicsHandle.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphicsHandle.DrawImage(image, 0, 0, newWidth, newHeight);
            }
            return newImage;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {

            PdfGenerateConfig cnf = new PdfGenerateConfig();
            cnf.PageSize = PdfSharp.PageSize.A4;
            string damageshtml = File.ReadAllText(@"C:\cefsharp\CliQ_Damages.html");
            damageshtml = BuildDamagesHtml(damageshtml);
            PdfDocument pdf = PdfGenerator.GeneratePdf(damageshtml, cnf);
            pdf.Save(@"C:\cefsharp\test.pdf");
            //pdf.
            //TheArtOfDev.HtmlRenderer.WinForms.HtmlPanel htmlPanel = new TheArtOfDev.HtmlRenderer.WinForms.HtmlPanel();

            //htmlPanel.Text = damageshtml;
            //htmlPanel.Dock = DockStyle.Fill;
            //flowThumbs.Controls.Add(htmlPanel);
            //Controls.Add(htmlPanel);

            //axAcroPDF1.LoadFile(@"C:\cefsharp\test.pdf");
            //axAcroPDF1.Refresh();
            return;


            int pageleft = 10;
            int pagetop = 10;
            int imgwidth = 150;
            int imgheight = 150;
            int txtwidth = 855;
            int txtheight = 15;
            int spacer = 15;
            
            string heading = $"List of damages recorded for {_vehicle.RegNo}.\r\nReport addional damages to support@cancom.co.za";
            //string grid = "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890";

            System.Drawing.Printing.PrintDocument pd = new System.Drawing.Printing.PrintDocument();
            pd.PrintPage += (sender1, args) =>
            {
                //object O = Properties.Resources.ResourceManager.GetObject("Canfleetrental_Black");
                Image headerimg = Properties.Resources.CliQ_Logo___Black___Transparent___Embedded;// (Image)O;
                //Image headerimg = Properties.Resources.;// (Image)O;

                Point hiloc = new Point(pageleft + 50, pagetop);
                Size hisize = new Size(512, 100);
                headerimg = ResizeImage(headerimg, hisize);
                Size hirecsize = new Size(headerimg.Width, headerimg.Height);
                Rectangle hiim = new Rectangle(hiloc, hirecsize);//  args.MarginBounds);
                args.Graphics.DrawImage(headerimg, hiim);
                pagetop = pagetop + headerimg.Height + spacer;

                PointF hlocf = new PointF(pageleft + 15, pagetop);
                SizeF hsizef = new SizeF(txtwidth, txtheight + 40);
                RectangleF hm = new RectangleF(hlocf, hsizef);
                args.Graphics.DrawString(heading, new Font("Arial", 5, FontStyle.Regular), Brushes.Black, hm);
                pagetop = pagetop + txtheight + 40 + spacer;



                int imgcnt = 0;
                int imgleft = pageleft;
                int imgtop = pagetop;
                int maximgheight = 0;
                int maximgleft = 0;
                foreach (Control c in flowThumbs.Controls)
                {
                    if (c.GetType() == typeof(ImageContainer))
                    {
                        ImageContainer imagecontainer = c as ImageContainer;
                        foreach (Control ic in imagecontainer.Controls)
                        {
                            if (ic.GetType() == typeof(PictureBox))
                            {
                                PictureBox pic = ic as PictureBox;
                                Image i = pic.Image;
                                Point loc = new Point(imgleft, imgtop);
                                Size size = new Size(imgwidth, imgheight);
                                i = ResizeImage(i, size);
                                Size recsize = new Size(i.Width, i.Height);
                                Rectangle im = new Rectangle(loc, recsize);
                                args.Graphics.DrawImage(i, im);
                                //pagetop = pagetop + i.Height + spacer;
                                if (i.Height > maximgheight)
                                {
                                    maximgheight = i.Height;
                                }
                                if (i.Width > maximgleft)
                                {
                                    maximgleft = i.Width;
                                }
                            }
                            if (ic.GetType() == typeof(MetroFramework.Controls.MetroLabel))
                            {
                                PointF locf = new PointF(imgleft, imgtop + maximgheight + spacer);
                                SizeF sizef = new SizeF(txtwidth, txtheight);
                                RectangleF tm = new RectangleF(locf, sizef);
                                string data = ic.Text;
                                args.Graphics.DrawString(data, new Font("Arial", 5, FontStyle.Regular), Brushes.Black, tm);
                                //pagetop = pagetop + txtheight + spacer;
                            }
                        }
                    }
                    imgcnt++;
                    if(imgcnt > 2)
                    {
                        imgcnt = 0;
                        pagetop = pagetop + maximgheight + spacer + spacer;
                        imgleft = pageleft;
                        imgtop = pagetop;
                        maximgheight = 0;
                        maximgleft = 0;
                    }
                    else
                    {
                        imgleft = imgleft + maximgleft + spacer;
                    }
                }
            };
            pd.Print();
            //printDocument.Print();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            Back(this, null);
        }
    }
}

