﻿using System;
using System.Windows.Forms;
using Castle.Core;
using SmartFormat;
using iRent.Framework;
using iRentKiosk.Core.Services;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Services.Api;

namespace iRent.Rental
{
    [CastleComponent]
    public class RentalRegisterDriverCommand : BaseCommand
    {
        private readonly Func<RentalRegisterDriverControl> _factory;
        private DriverApi _driverApi;

        public string Next { get; set; }
        public string Back { get; set; }
        public string IssueCard { get; set; }

        public RentalRegisterDriverCommand(Func<RentalRegisterDriverControl> Factory, DriverApi DriverApi)
        {
            _factory = Factory;
            _driverApi = DriverApi;
        }

        public override void Invoke()
        {
            var control = _factory();
            control.Dock = DockStyle.Fill;
            var main = GetFormAs<MainForm>();
            main.AddToFormPanel(control);
            control.Bind(ContextData.Driver);
            control.Next += OnNext;
            control.Back += OnBack;
        }

        private void OnNext(object sender, DriverModel driver)
        {
            driver.OTPSuccess = true;
            _driverApi.Save(driver);
            Proceed(Next);
        }

        private void OnBack(object sender, DriverModel driver)
        {
            Proceed(Back);
        }
    }
}
