﻿using System;
using System.Windows.Forms;
using Castle.Core;
using iRent.Framework;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Models;
using iRent2.Contracts.Rental;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;

namespace iRent.Rental
{
    [CastleComponent]
    public class ConfirmPaymentCommand : BaseCommand
    {
        public string Next { get; set; }
        public string MessageContext { get; set; }

        private Func<ConfirmPaymentControl> _factory;
        BookingApi _bookingApi;

        public ConfirmPaymentCommand(Func<ConfirmPaymentControl> Factory, BookingApi bookingApi)
        {
            _factory = Factory;
            _bookingApi = bookingApi;
        }

        public override void Invoke()
        {
            BookingModel response = _bookingApi.GetBooking(ContextData.RentalOptions.BookingInfo.BookingID);
            bool payed = response.Payed;
            var control = _factory();
            control.Dock = DockStyle.Fill;
            control.Next = OnNext;
            control.Bind(ContextData.DynamicData[MessageContext], payed);
            var main = GetFormAs<MainForm>();
            main.AddToFormPanel(control);
        }

        public void OnNext(object sender, object data)
        {
            Proceed(Next);
        }
    }
}
