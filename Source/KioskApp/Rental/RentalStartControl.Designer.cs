﻿namespace iRent.Rental
{
    partial class RentalStartControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlStart = new MetroFramework.Controls.MetroPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn1Day = new MetroFramework.Controls.MetroButton();
            this.btnHalfDay = new MetroFramework.Controls.MetroButton();
            this.btnMoreTime = new MetroFramework.Controls.MetroButton();
            this.metroPanel12 = new MetroFramework.Controls.MetroPanel();
            this.cmbToMonth = new MetroFramework.Controls.MetroComboBox();
            this.lblToMonth = new System.Windows.Forms.Label();
            this.lblToMonthName = new MetroFramework.Controls.MetroLabel();
            this.btnToYearUp = new MetroFramework.Controls.MetroButton();
            this.btnToYearDown = new MetroFramework.Controls.MetroButton();
            this.cmbToYear = new MetroFramework.Controls.MetroComboBox();
            this.lblToYear = new System.Windows.Forms.Label();
            this.metroPanel11 = new MetroFramework.Controls.MetroPanel();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.btnToMonthDown = new MetroFramework.Controls.MetroButton();
            this.btnToMonthUp = new MetroFramework.Controls.MetroButton();
            this.btnToMinUp = new MetroFramework.Controls.MetroButton();
            this.btnToMinDown = new MetroFramework.Controls.MetroButton();
            this.cmbFromYear = new MetroFramework.Controls.MetroComboBox();
            this.cmbFromMonth = new MetroFramework.Controls.MetroComboBox();
            this.cmbFromDay = new MetroFramework.Controls.MetroComboBox();
            this.cmbFromHour = new MetroFramework.Controls.MetroComboBox();
            this.cmbFromMins = new MetroFramework.Controls.MetroComboBox();
            this.btnFromTimeDown = new MetroFramework.Controls.MetroButton();
            this.btnFromMinDown = new MetroFramework.Controls.MetroButton();
            this.btnFromTimeUp = new MetroFramework.Controls.MetroButton();
            this.btnFromMinUp = new MetroFramework.Controls.MetroButton();
            this.btnFromDayDown = new MetroFramework.Controls.MetroButton();
            this.btnFromDayUp = new MetroFramework.Controls.MetroButton();
            this.btnFromMonthDown = new MetroFramework.Controls.MetroButton();
            this.btnFromMonthUp = new MetroFramework.Controls.MetroButton();
            this.btnFromYearDown = new MetroFramework.Controls.MetroButton();
            this.btnFromYearUp = new MetroFramework.Controls.MetroButton();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.lblHeader = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
            this.lblWarning = new MetroFramework.Controls.MetroLabel();
            this.btnNext = new MetroFramework.Controls.MetroButton();
            this.btnBack = new MetroFramework.Controls.MetroButton();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.metroPanel5 = new MetroFramework.Controls.MetroPanel();
            this.lblReturnDate = new MetroFramework.Controls.MetroLabel();
            this.btnToTimeDown = new MetroFramework.Controls.MetroButton();
            this.btnToTimeUp = new MetroFramework.Controls.MetroButton();
            this.btnToDayDown = new MetroFramework.Controls.MetroButton();
            this.btnToDayUp = new MetroFramework.Controls.MetroButton();
            this.metroPanel10 = new MetroFramework.Controls.MetroPanel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.lblToTime = new System.Windows.Forms.Label();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.cmbToHour = new MetroFramework.Controls.MetroComboBox();
            this.cmbToMins = new MetroFramework.Controls.MetroComboBox();
            this.metroPanel13 = new MetroFramework.Controls.MetroPanel();
            this.lblToDay = new System.Windows.Forms.Label();
            this.lblToDayName = new MetroFramework.Controls.MetroLabel();
            this.cmbToDay = new MetroFramework.Controls.MetroComboBox();
            this.lblToDateText = new MetroFramework.Controls.MetroLabel();
            this.lblFromDateText = new MetroFramework.Controls.MetroLabel();
            this.metroPanel2 = new MetroFramework.Controls.MetroPanel();
            this.metroPanel9 = new MetroFramework.Controls.MetroPanel();
            this.lblFromTime = new System.Windows.Forms.Label();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroPanel8 = new MetroFramework.Controls.MetroPanel();
            this.lblFromYear = new System.Windows.Forms.Label();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroPanel7 = new MetroFramework.Controls.MetroPanel();
            this.lblFromMonth = new System.Windows.Forms.Label();
            this.lblFromMonthName = new MetroFramework.Controls.MetroLabel();
            this.metroPanel6 = new MetroFramework.Controls.MetroPanel();
            this.lblFromDay = new System.Windows.Forms.Label();
            this.lblFromDayName = new MetroFramework.Controls.MetroLabel();
            this.pnlStart.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.metroPanel12.SuspendLayout();
            this.metroPanel11.SuspendLayout();
            this.metroPanel1.SuspendLayout();
            this.tableLayoutPanelMain.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.metroPanel5.SuspendLayout();
            this.metroPanel10.SuspendLayout();
            this.metroPanel13.SuspendLayout();
            this.metroPanel2.SuspendLayout();
            this.metroPanel9.SuspendLayout();
            this.metroPanel8.SuspendLayout();
            this.metroPanel7.SuspendLayout();
            this.metroPanel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlStart
            // 
            this.pnlStart.BackColor = System.Drawing.SystemColors.Control;
            this.pnlStart.Controls.Add(this.flowLayoutPanel1);
            this.pnlStart.HorizontalScrollbarBarColor = true;
            this.pnlStart.HorizontalScrollbarHighlightOnWheel = false;
            this.pnlStart.HorizontalScrollbarSize = 10;
            this.pnlStart.Location = new System.Drawing.Point(1015, 202);
            this.pnlStart.Name = "pnlStart";
            this.pnlStart.Size = new System.Drawing.Size(950, 409);
            this.pnlStart.TabIndex = 5;
            this.pnlStart.VerticalScrollbarBarColor = true;
            this.pnlStart.VerticalScrollbarHighlightOnWheel = false;
            this.pnlStart.VerticalScrollbarSize = 10;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel1.Controls.Add(this.btn1Day);
            this.flowLayoutPanel1.Controls.Add(this.btnHalfDay);
            this.flowLayoutPanel1.Controls.Add(this.btnMoreTime);
            this.flowLayoutPanel1.Controls.Add(this.metroPanel12);
            this.flowLayoutPanel1.Controls.Add(this.btnToYearUp);
            this.flowLayoutPanel1.Controls.Add(this.btnToYearDown);
            this.flowLayoutPanel1.Controls.Add(this.cmbToYear);
            this.flowLayoutPanel1.Controls.Add(this.lblToYear);
            this.flowLayoutPanel1.Controls.Add(this.metroPanel11);
            this.flowLayoutPanel1.Controls.Add(this.btnToMonthDown);
            this.flowLayoutPanel1.Controls.Add(this.btnToMonthUp);
            this.flowLayoutPanel1.Controls.Add(this.btnToMinUp);
            this.flowLayoutPanel1.Controls.Add(this.btnToMinDown);
            this.flowLayoutPanel1.Controls.Add(this.cmbFromYear);
            this.flowLayoutPanel1.Controls.Add(this.cmbFromMonth);
            this.flowLayoutPanel1.Controls.Add(this.cmbFromDay);
            this.flowLayoutPanel1.Controls.Add(this.cmbFromHour);
            this.flowLayoutPanel1.Controls.Add(this.cmbFromMins);
            this.flowLayoutPanel1.Controls.Add(this.btnFromTimeDown);
            this.flowLayoutPanel1.Controls.Add(this.btnFromMinDown);
            this.flowLayoutPanel1.Controls.Add(this.btnFromTimeUp);
            this.flowLayoutPanel1.Controls.Add(this.btnFromMinUp);
            this.flowLayoutPanel1.Controls.Add(this.btnFromDayDown);
            this.flowLayoutPanel1.Controls.Add(this.btnFromDayUp);
            this.flowLayoutPanel1.Controls.Add(this.btnFromMonthDown);
            this.flowLayoutPanel1.Controls.Add(this.btnFromMonthUp);
            this.flowLayoutPanel1.Controls.Add(this.btnFromYearDown);
            this.flowLayoutPanel1.Controls.Add(this.btnFromYearUp);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(950, 409);
            this.flowLayoutPanel1.TabIndex = 2;
            // 
            // btn1Day
            // 
            this.btn1Day.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btn1Day.Location = new System.Drawing.Point(4, 4);
            this.btn1Day.Margin = new System.Windows.Forms.Padding(4);
            this.btn1Day.Name = "btn1Day";
            this.btn1Day.Size = new System.Drawing.Size(275, 115);
            this.btn1Day.TabIndex = 6;
            this.btn1Day.Text = "For 1 Hour";
            this.btn1Day.UseSelectable = true;
            this.btn1Day.Click += new System.EventHandler(this.btn1Day_Click);
            // 
            // btnHalfDay
            // 
            this.btnHalfDay.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnHalfDay.Location = new System.Drawing.Point(287, 4);
            this.btnHalfDay.Margin = new System.Windows.Forms.Padding(4);
            this.btnHalfDay.Name = "btnHalfDay";
            this.btnHalfDay.Size = new System.Drawing.Size(259, 115);
            this.btnHalfDay.TabIndex = 7;
            this.btnHalfDay.Text = "For 2 Hours";
            this.btnHalfDay.UseSelectable = true;
            this.btnHalfDay.Click += new System.EventHandler(this.btnHalfDay_Click);
            // 
            // btnMoreTime
            // 
            this.btnMoreTime.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnMoreTime.Location = new System.Drawing.Point(554, 4);
            this.btnMoreTime.Margin = new System.Windows.Forms.Padding(4);
            this.btnMoreTime.Name = "btnMoreTime";
            this.btnMoreTime.Size = new System.Drawing.Size(259, 115);
            this.btnMoreTime.TabIndex = 9;
            this.btnMoreTime.Text = "More Options";
            this.btnMoreTime.UseSelectable = true;
            this.btnMoreTime.Click += new System.EventHandler(this.btnMoreTime_Click);
            // 
            // metroPanel12
            // 
            this.metroPanel12.Controls.Add(this.cmbToMonth);
            this.metroPanel12.Controls.Add(this.lblToMonth);
            this.metroPanel12.Controls.Add(this.lblToMonthName);
            this.metroPanel12.HorizontalScrollbarBarColor = true;
            this.metroPanel12.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel12.HorizontalScrollbarSize = 10;
            this.metroPanel12.Location = new System.Drawing.Point(3, 126);
            this.metroPanel12.Name = "metroPanel12";
            this.metroPanel12.Size = new System.Drawing.Size(150, 150);
            this.metroPanel12.TabIndex = 3;
            this.metroPanel12.VerticalScrollbarBarColor = true;
            this.metroPanel12.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel12.VerticalScrollbarSize = 10;
            // 
            // cmbToMonth
            // 
            this.cmbToMonth.FontSize = MetroFramework.MetroComboBoxSize.XL;
            this.cmbToMonth.FormattingEnabled = true;
            this.cmbToMonth.ItemHeight = 41;
            this.cmbToMonth.Items.AddRange(new object[] {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "Jule",
            "August",
            "September",
            "October",
            "November",
            "December"});
            this.cmbToMonth.Location = new System.Drawing.Point(17, 74);
            this.cmbToMonth.Name = "cmbToMonth";
            this.cmbToMonth.Size = new System.Drawing.Size(121, 47);
            this.cmbToMonth.TabIndex = 26;
            this.cmbToMonth.UseSelectable = true;
            this.cmbToMonth.Visible = false;
            this.cmbToMonth.SelectionChangeCommitted += new System.EventHandler(this.cmbToMonth_SelectionChangeCommitted);
            // 
            // lblToMonth
            // 
            this.lblToMonth.AutoSize = true;
            this.lblToMonth.BackColor = System.Drawing.Color.Transparent;
            this.lblToMonth.Font = new System.Drawing.Font("MS Reference Sans Serif", 32F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblToMonth.Location = new System.Drawing.Point(26, 74);
            this.lblToMonth.Name = "lblToMonth";
            this.lblToMonth.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblToMonth.Size = new System.Drawing.Size(99, 67);
            this.lblToMonth.TabIndex = 21;
            this.lblToMonth.Text = "00";
            this.lblToMonth.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblToMonthName
            // 
            this.lblToMonthName.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblToMonthName.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblToMonthName.Location = new System.Drawing.Point(17, 20);
            this.lblToMonthName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblToMonthName.Name = "lblToMonthName";
            this.lblToMonthName.Size = new System.Drawing.Size(97, 30);
            this.lblToMonthName.TabIndex = 20;
            this.lblToMonthName.Text = "Month";
            this.lblToMonthName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnToYearUp
            // 
            this.btnToYearUp.BackgroundImage = global::iRent.Properties.Resources.TopArrow_fw;
            this.btnToYearUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnToYearUp.Location = new System.Drawing.Point(159, 126);
            this.btnToYearUp.Name = "btnToYearUp";
            this.btnToYearUp.Size = new System.Drawing.Size(160, 32);
            this.btnToYearUp.TabIndex = 12;
            this.btnToYearUp.UseSelectable = true;
            this.btnToYearUp.Visible = false;
            this.btnToYearUp.Click += new System.EventHandler(this.btnToYearUp_Click);
            // 
            // btnToYearDown
            // 
            this.btnToYearDown.BackgroundImage = global::iRent.Properties.Resources.BottomArrow_fw;
            this.btnToYearDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnToYearDown.Location = new System.Drawing.Point(325, 126);
            this.btnToYearDown.Name = "btnToYearDown";
            this.btnToYearDown.Size = new System.Drawing.Size(160, 32);
            this.btnToYearDown.TabIndex = 15;
            this.btnToYearDown.UseSelectable = true;
            this.btnToYearDown.Visible = false;
            this.btnToYearDown.Click += new System.EventHandler(this.btnToYearDown_Click);
            // 
            // cmbToYear
            // 
            this.cmbToYear.FontSize = MetroFramework.MetroComboBoxSize.XL;
            this.cmbToYear.FormattingEnabled = true;
            this.cmbToYear.ItemHeight = 41;
            this.cmbToYear.Location = new System.Drawing.Point(491, 126);
            this.cmbToYear.Name = "cmbToYear";
            this.cmbToYear.Size = new System.Drawing.Size(137, 47);
            this.cmbToYear.TabIndex = 27;
            this.cmbToYear.UseSelectable = true;
            this.cmbToYear.Visible = false;
            this.cmbToYear.SelectionChangeCommitted += new System.EventHandler(this.cmbToYear_SelectionChangeCommitted);
            // 
            // lblToYear
            // 
            this.lblToYear.AutoSize = true;
            this.lblToYear.BackColor = System.Drawing.Color.Transparent;
            this.lblToYear.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblToYear.Font = new System.Drawing.Font("MS Reference Sans Serif", 32F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblToYear.Location = new System.Drawing.Point(634, 123);
            this.lblToYear.Name = "lblToYear";
            this.lblToYear.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblToYear.Size = new System.Drawing.Size(169, 67);
            this.lblToYear.TabIndex = 21;
            this.lblToYear.Text = "9999";
            this.lblToYear.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // metroPanel11
            // 
            this.metroPanel11.Controls.Add(this.metroLabel8);
            this.metroPanel11.HorizontalScrollbarBarColor = true;
            this.metroPanel11.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel11.HorizontalScrollbarSize = 10;
            this.metroPanel11.Location = new System.Drawing.Point(3, 282);
            this.metroPanel11.Name = "metroPanel11";
            this.metroPanel11.Size = new System.Drawing.Size(165, 144);
            this.metroPanel11.TabIndex = 4;
            this.metroPanel11.VerticalScrollbarBarColor = true;
            this.metroPanel11.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel11.VerticalScrollbarSize = 10;
            // 
            // metroLabel8
            // 
            this.metroLabel8.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel8.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel8.Location = new System.Drawing.Point(10, 14);
            this.metroLabel8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(139, 30);
            this.metroLabel8.TabIndex = 20;
            this.metroLabel8.Text = "Year";
            this.metroLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnToMonthDown
            // 
            this.btnToMonthDown.BackgroundImage = global::iRent.Properties.Resources.BottomArrow_fw;
            this.btnToMonthDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnToMonthDown.Location = new System.Drawing.Point(174, 282);
            this.btnToMonthDown.Name = "btnToMonthDown";
            this.btnToMonthDown.Size = new System.Drawing.Size(150, 32);
            this.btnToMonthDown.TabIndex = 14;
            this.btnToMonthDown.UseSelectable = true;
            this.btnToMonthDown.Visible = false;
            this.btnToMonthDown.Click += new System.EventHandler(this.btnToMonthDown_Click);
            // 
            // btnToMonthUp
            // 
            this.btnToMonthUp.BackgroundImage = global::iRent.Properties.Resources.TopArrow_fw;
            this.btnToMonthUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnToMonthUp.Location = new System.Drawing.Point(330, 282);
            this.btnToMonthUp.Name = "btnToMonthUp";
            this.btnToMonthUp.Size = new System.Drawing.Size(149, 32);
            this.btnToMonthUp.TabIndex = 11;
            this.btnToMonthUp.UseSelectable = true;
            this.btnToMonthUp.Visible = false;
            this.btnToMonthUp.Click += new System.EventHandler(this.btnToMonthUp_Click);
            // 
            // btnToMinUp
            // 
            this.btnToMinUp.BackgroundImage = global::iRent.Properties.Resources.TopArrow_fw;
            this.btnToMinUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnToMinUp.Location = new System.Drawing.Point(485, 282);
            this.btnToMinUp.Name = "btnToMinUp";
            this.btnToMinUp.Size = new System.Drawing.Size(62, 35);
            this.btnToMinUp.TabIndex = 18;
            this.btnToMinUp.UseSelectable = true;
            this.btnToMinUp.Visible = false;
            this.btnToMinUp.Click += new System.EventHandler(this.btnToMinUp_Click);
            // 
            // btnToMinDown
            // 
            this.btnToMinDown.BackgroundImage = global::iRent.Properties.Resources.BottomArrow_fw;
            this.btnToMinDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnToMinDown.Location = new System.Drawing.Point(553, 282);
            this.btnToMinDown.Name = "btnToMinDown";
            this.btnToMinDown.Size = new System.Drawing.Size(62, 35);
            this.btnToMinDown.TabIndex = 17;
            this.btnToMinDown.UseSelectable = true;
            this.btnToMinDown.Visible = false;
            this.btnToMinDown.Click += new System.EventHandler(this.btnToMinDown_Click);
            // 
            // cmbFromYear
            // 
            this.cmbFromYear.FontSize = MetroFramework.MetroComboBoxSize.XL;
            this.cmbFromYear.FormattingEnabled = true;
            this.cmbFromYear.ItemHeight = 41;
            this.cmbFromYear.Location = new System.Drawing.Point(621, 282);
            this.cmbFromYear.Name = "cmbFromYear";
            this.cmbFromYear.Size = new System.Drawing.Size(121, 47);
            this.cmbFromYear.TabIndex = 22;
            this.cmbFromYear.UseSelectable = true;
            this.cmbFromYear.Visible = false;
            this.cmbFromYear.SelectedIndexChanged += new System.EventHandler(this.cmbFromYear_SelectedIndexChanged);
            // 
            // cmbFromMonth
            // 
            this.cmbFromMonth.FontSize = MetroFramework.MetroComboBoxSize.XL;
            this.cmbFromMonth.FormattingEnabled = true;
            this.cmbFromMonth.ItemHeight = 41;
            this.cmbFromMonth.Items.AddRange(new object[] {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"});
            this.cmbFromMonth.Location = new System.Drawing.Point(748, 282);
            this.cmbFromMonth.Name = "cmbFromMonth";
            this.cmbFromMonth.Size = new System.Drawing.Size(121, 47);
            this.cmbFromMonth.TabIndex = 23;
            this.cmbFromMonth.UseSelectable = true;
            this.cmbFromMonth.Visible = false;
            this.cmbFromMonth.SelectedIndexChanged += new System.EventHandler(this.cmbFromMonth_SelectedIndexChanged);
            // 
            // cmbFromDay
            // 
            this.cmbFromDay.FontSize = MetroFramework.MetroComboBoxSize.XL;
            this.cmbFromDay.FormattingEnabled = true;
            this.cmbFromDay.ItemHeight = 41;
            this.cmbFromDay.Location = new System.Drawing.Point(3, 432);
            this.cmbFromDay.Name = "cmbFromDay";
            this.cmbFromDay.Size = new System.Drawing.Size(121, 47);
            this.cmbFromDay.TabIndex = 24;
            this.cmbFromDay.UseSelectable = true;
            this.cmbFromDay.Visible = false;
            this.cmbFromDay.SelectedIndexChanged += new System.EventHandler(this.cmbFromDay_SelectedIndexChanged);
            // 
            // cmbFromHour
            // 
            this.cmbFromHour.FontSize = MetroFramework.MetroComboBoxSize.XL;
            this.cmbFromHour.FormattingEnabled = true;
            this.cmbFromHour.ItemHeight = 41;
            this.cmbFromHour.Location = new System.Drawing.Point(130, 432);
            this.cmbFromHour.Name = "cmbFromHour";
            this.cmbFromHour.Size = new System.Drawing.Size(75, 47);
            this.cmbFromHour.TabIndex = 22;
            this.cmbFromHour.UseSelectable = true;
            this.cmbFromHour.Visible = false;
            // 
            // cmbFromMins
            // 
            this.cmbFromMins.FontSize = MetroFramework.MetroComboBoxSize.XL;
            this.cmbFromMins.FormattingEnabled = true;
            this.cmbFromMins.ItemHeight = 41;
            this.cmbFromMins.Location = new System.Drawing.Point(211, 432);
            this.cmbFromMins.Name = "cmbFromMins";
            this.cmbFromMins.Size = new System.Drawing.Size(74, 47);
            this.cmbFromMins.TabIndex = 23;
            this.cmbFromMins.UseSelectable = true;
            this.cmbFromMins.Visible = false;
            // 
            // btnFromTimeDown
            // 
            this.btnFromTimeDown.BackgroundImage = global::iRent.Properties.Resources.BottomArrow_fw;
            this.btnFromTimeDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFromTimeDown.Location = new System.Drawing.Point(291, 432);
            this.btnFromTimeDown.Name = "btnFromTimeDown";
            this.btnFromTimeDown.Size = new System.Drawing.Size(60, 23);
            this.btnFromTimeDown.TabIndex = 16;
            this.btnFromTimeDown.UseSelectable = true;
            this.btnFromTimeDown.Visible = false;
            this.btnFromTimeDown.Click += new System.EventHandler(this.btnFromTimeDown_Click);
            // 
            // btnFromMinDown
            // 
            this.btnFromMinDown.BackgroundImage = global::iRent.Properties.Resources.BottomArrow_fw;
            this.btnFromMinDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFromMinDown.Location = new System.Drawing.Point(357, 432);
            this.btnFromMinDown.Name = "btnFromMinDown";
            this.btnFromMinDown.Size = new System.Drawing.Size(64, 23);
            this.btnFromMinDown.TabIndex = 17;
            this.btnFromMinDown.UseSelectable = true;
            this.btnFromMinDown.Visible = false;
            this.btnFromMinDown.Click += new System.EventHandler(this.btnFromMinDown_Click);
            // 
            // btnFromTimeUp
            // 
            this.btnFromTimeUp.BackgroundImage = global::iRent.Properties.Resources.TopArrow_fw;
            this.btnFromTimeUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFromTimeUp.Location = new System.Drawing.Point(427, 432);
            this.btnFromTimeUp.Name = "btnFromTimeUp";
            this.btnFromTimeUp.Size = new System.Drawing.Size(60, 23);
            this.btnFromTimeUp.TabIndex = 13;
            this.btnFromTimeUp.UseSelectable = true;
            this.btnFromTimeUp.Visible = false;
            this.btnFromTimeUp.Click += new System.EventHandler(this.btnFromTimeUp_Click);
            // 
            // btnFromMinUp
            // 
            this.btnFromMinUp.BackgroundImage = global::iRent.Properties.Resources.TopArrow_fw;
            this.btnFromMinUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFromMinUp.Location = new System.Drawing.Point(493, 432);
            this.btnFromMinUp.Name = "btnFromMinUp";
            this.btnFromMinUp.Size = new System.Drawing.Size(64, 23);
            this.btnFromMinUp.TabIndex = 18;
            this.btnFromMinUp.UseSelectable = true;
            this.btnFromMinUp.Visible = false;
            this.btnFromMinUp.Click += new System.EventHandler(this.btnFromMinUp_Click);
            // 
            // btnFromDayDown
            // 
            this.btnFromDayDown.BackgroundImage = global::iRent.Properties.Resources.BottomArrow_fw;
            this.btnFromDayDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFromDayDown.Location = new System.Drawing.Point(563, 432);
            this.btnFromDayDown.Name = "btnFromDayDown";
            this.btnFromDayDown.Size = new System.Drawing.Size(146, 23);
            this.btnFromDayDown.TabIndex = 10;
            this.btnFromDayDown.UseSelectable = true;
            this.btnFromDayDown.Visible = false;
            this.btnFromDayDown.Click += new System.EventHandler(this.btnFromDayDown_Click);
            // 
            // btnFromDayUp
            // 
            this.btnFromDayUp.BackgroundImage = global::iRent.Properties.Resources.TopArrow_fw;
            this.btnFromDayUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFromDayUp.Location = new System.Drawing.Point(715, 432);
            this.btnFromDayUp.Name = "btnFromDayUp";
            this.btnFromDayUp.Size = new System.Drawing.Size(142, 23);
            this.btnFromDayUp.TabIndex = 6;
            this.btnFromDayUp.UseSelectable = true;
            this.btnFromDayUp.Visible = false;
            this.btnFromDayUp.Click += new System.EventHandler(this.btnFromDayUp_Click);
            // 
            // btnFromMonthDown
            // 
            this.btnFromMonthDown.BackgroundImage = global::iRent.Properties.Resources.BottomArrow_fw;
            this.btnFromMonthDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFromMonthDown.Location = new System.Drawing.Point(3, 485);
            this.btnFromMonthDown.Name = "btnFromMonthDown";
            this.btnFromMonthDown.Size = new System.Drawing.Size(149, 23);
            this.btnFromMonthDown.TabIndex = 14;
            this.btnFromMonthDown.UseSelectable = true;
            this.btnFromMonthDown.Visible = false;
            this.btnFromMonthDown.Click += new System.EventHandler(this.btnFromMonthDown_Click);
            // 
            // btnFromMonthUp
            // 
            this.btnFromMonthUp.BackgroundImage = global::iRent.Properties.Resources.TopArrow_fw;
            this.btnFromMonthUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFromMonthUp.Location = new System.Drawing.Point(158, 485);
            this.btnFromMonthUp.Name = "btnFromMonthUp";
            this.btnFromMonthUp.Size = new System.Drawing.Size(149, 23);
            this.btnFromMonthUp.TabIndex = 11;
            this.btnFromMonthUp.UseSelectable = true;
            this.btnFromMonthUp.Visible = false;
            this.btnFromMonthUp.Click += new System.EventHandler(this.btnFromMonthUp_Click);
            // 
            // btnFromYearDown
            // 
            this.btnFromYearDown.BackgroundImage = global::iRent.Properties.Resources.BottomArrow_fw;
            this.btnFromYearDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFromYearDown.Location = new System.Drawing.Point(313, 485);
            this.btnFromYearDown.Name = "btnFromYearDown";
            this.btnFromYearDown.Size = new System.Drawing.Size(161, 23);
            this.btnFromYearDown.TabIndex = 15;
            this.btnFromYearDown.UseSelectable = true;
            this.btnFromYearDown.Visible = false;
            this.btnFromYearDown.Click += new System.EventHandler(this.btnFromYearDown_Click);
            // 
            // btnFromYearUp
            // 
            this.btnFromYearUp.BackgroundImage = global::iRent.Properties.Resources.TopArrow_fw;
            this.btnFromYearUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFromYearUp.Location = new System.Drawing.Point(480, 485);
            this.btnFromYearUp.Name = "btnFromYearUp";
            this.btnFromYearUp.Size = new System.Drawing.Size(165, 23);
            this.btnFromYearUp.TabIndex = 12;
            this.btnFromYearUp.UseSelectable = true;
            this.btnFromYearUp.Visible = false;
            this.btnFromYearUp.Click += new System.EventHandler(this.btnFromYearUp_Click);
            // 
            // metroPanel1
            // 
            this.metroPanel1.Controls.Add(this.lblHeader);
            this.metroPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(0, 0);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(1032, 100);
            this.metroPanel1.TabIndex = 4;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // lblHeader
            // 
            this.lblHeader.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblHeader.FontSize = MetroFramework.MetroLabelSize.XL;
            this.lblHeader.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblHeader.Location = new System.Drawing.Point(0, 38);
            this.lblHeader.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(1032, 62);
            this.lblHeader.TabIndex = 13;
            this.lblHeader.Text = "How long would you like the vehicle for ?";
            this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanelMain
            // 
            this.tableLayoutPanelMain.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanelMain.ColumnCount = 3;
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 267F));
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelMain.Controls.Add(this.lblWarning, 0, 1);
            this.tableLayoutPanelMain.Controls.Add(this.btnNext, 2, 2);
            this.tableLayoutPanelMain.Controls.Add(this.btnBack, 0, 2);
            this.tableLayoutPanelMain.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanelMain.Location = new System.Drawing.Point(18, 136);
            this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
            this.tableLayoutPanelMain.RowCount = 3;
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 600F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 94F));
            this.tableLayoutPanelMain.Size = new System.Drawing.Size(982, 747);
            this.tableLayoutPanelMain.TabIndex = 2;
            // 
            // lblWarning
            // 
            this.lblWarning.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblWarning.AutoSize = true;
            this.tableLayoutPanelMain.SetColumnSpan(this.lblWarning, 3);
            this.lblWarning.FontSize = MetroFramework.MetroLabelSize.XL;
            this.lblWarning.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lblWarning.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblWarning.Location = new System.Drawing.Point(356, 622);
            this.lblWarning.Name = "lblWarning";
            this.lblWarning.Size = new System.Drawing.Size(270, 31);
            this.lblWarning.TabIndex = 10;
            this.lblWarning.Text = "* Minimum 1 hour rental.";
            this.lblWarning.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnNext
            // 
            this.btnNext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNext.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnNext.Location = new System.Drawing.Point(628, 657);
            this.btnNext.Margin = new System.Windows.Forms.Padding(4);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(350, 86);
            this.btnNext.TabIndex = 7;
            this.btnNext.Text = "&Continue";
            this.btnNext.UseSelectable = true;
            this.btnNext.Visible = false;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnBack
            // 
            this.btnBack.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnBack.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnBack.Location = new System.Drawing.Point(4, 657);
            this.btnBack.Margin = new System.Windows.Forms.Padding(4);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(349, 86);
            this.btnBack.TabIndex = 9;
            this.btnBack.Text = "&Back";
            this.btnBack.UseSelectable = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanelMain.SetColumnSpan(this.tableLayoutPanel3, 3);
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.metroPanel5, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.lblToDateText, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.lblFromDateText, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.metroPanel2, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 4;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 328F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(976, 594);
            this.tableLayoutPanel3.TabIndex = 1;
            this.tableLayoutPanel3.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel3_Paint);
            // 
            // metroPanel5
            // 
            this.metroPanel5.Controls.Add(this.lblReturnDate);
            this.metroPanel5.Controls.Add(this.btnToTimeDown);
            this.metroPanel5.Controls.Add(this.btnToTimeUp);
            this.metroPanel5.Controls.Add(this.btnToDayDown);
            this.metroPanel5.Controls.Add(this.btnToDayUp);
            this.metroPanel5.Controls.Add(this.metroPanel10);
            this.metroPanel5.Controls.Add(this.metroPanel13);
            this.metroPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel5.HorizontalScrollbarBarColor = true;
            this.metroPanel5.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel5.HorizontalScrollbarSize = 10;
            this.metroPanel5.Location = new System.Drawing.Point(3, 233);
            this.metroPanel5.Name = "metroPanel5";
            this.metroPanel5.Size = new System.Drawing.Size(970, 358);
            this.metroPanel5.TabIndex = 18;
            this.metroPanel5.VerticalScrollbarBarColor = true;
            this.metroPanel5.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel5.VerticalScrollbarSize = 10;
            // 
            // lblReturnDate
            // 
            this.lblReturnDate.AutoSize = true;
            this.lblReturnDate.FontSize = MetroFramework.MetroLabelSize.XL;
            this.lblReturnDate.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblReturnDate.Location = new System.Drawing.Point(167, 278);
            this.lblReturnDate.Name = "lblReturnDate";
            this.lblReturnDate.Size = new System.Drawing.Size(341, 31);
            this.lblReturnDate.TabIndex = 18;
            this.lblReturnDate.Text = "Your rental return date will be:";
            // 
            // btnToTimeDown
            // 
            this.btnToTimeDown.BackgroundImage = global::iRent.Properties.Resources.BottomArrow_fw;
            this.btnToTimeDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnToTimeDown.Location = new System.Drawing.Point(661, 199);
            this.btnToTimeDown.Name = "btnToTimeDown";
            this.btnToTimeDown.Size = new System.Drawing.Size(150, 35);
            this.btnToTimeDown.TabIndex = 16;
            this.btnToTimeDown.UseSelectable = true;
            this.btnToTimeDown.Click += new System.EventHandler(this.btnToTimeDown_Click);
            // 
            // btnToTimeUp
            // 
            this.btnToTimeUp.BackgroundImage = global::iRent.Properties.Resources.TopArrow_fw;
            this.btnToTimeUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnToTimeUp.Location = new System.Drawing.Point(661, 14);
            this.btnToTimeUp.Name = "btnToTimeUp";
            this.btnToTimeUp.Size = new System.Drawing.Size(150, 35);
            this.btnToTimeUp.TabIndex = 13;
            this.btnToTimeUp.UseSelectable = true;
            this.btnToTimeUp.Click += new System.EventHandler(this.btnToTimeUp_Click);
            // 
            // btnToDayDown
            // 
            this.btnToDayDown.BackgroundImage = global::iRent.Properties.Resources.BottomArrow_fw;
            this.btnToDayDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnToDayDown.Location = new System.Drawing.Point(434, 199);
            this.btnToDayDown.Name = "btnToDayDown";
            this.btnToDayDown.Size = new System.Drawing.Size(150, 35);
            this.btnToDayDown.TabIndex = 10;
            this.btnToDayDown.UseSelectable = true;
            this.btnToDayDown.Click += new System.EventHandler(this.btnToDayDown_Click);
            // 
            // btnToDayUp
            // 
            this.btnToDayUp.BackgroundImage = global::iRent.Properties.Resources.TopArrow_fw;
            this.btnToDayUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnToDayUp.Location = new System.Drawing.Point(434, 14);
            this.btnToDayUp.Name = "btnToDayUp";
            this.btnToDayUp.Size = new System.Drawing.Size(150, 35);
            this.btnToDayUp.TabIndex = 6;
            this.btnToDayUp.UseSelectable = true;
            this.btnToDayUp.Click += new System.EventHandler(this.btnToDayUp_Click);
            // 
            // metroPanel10
            // 
            this.metroPanel10.Controls.Add(this.metroLabel1);
            this.metroPanel10.Controls.Add(this.lblToTime);
            this.metroPanel10.Controls.Add(this.metroLabel7);
            this.metroPanel10.Controls.Add(this.cmbToHour);
            this.metroPanel10.Controls.Add(this.cmbToMins);
            this.metroPanel10.HorizontalScrollbarBarColor = true;
            this.metroPanel10.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel10.HorizontalScrollbarSize = 10;
            this.metroPanel10.Location = new System.Drawing.Point(650, 54);
            this.metroPanel10.Name = "metroPanel10";
            this.metroPanel10.Size = new System.Drawing.Size(178, 139);
            this.metroPanel10.TabIndex = 5;
            this.metroPanel10.VerticalScrollbarBarColor = true;
            this.metroPanel10.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel10.VerticalScrollbarSize = 10;
            // 
            // metroLabel1
            // 
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel1.Location = new System.Drawing.Point(4, 14);
            this.metroLabel1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(75, 30);
            this.metroLabel1.TabIndex = 30;
            this.metroLabel1.Text = "Hour";
            this.metroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblToTime
            // 
            this.lblToTime.AutoSize = true;
            this.lblToTime.BackColor = System.Drawing.Color.Transparent;
            this.lblToTime.Font = new System.Drawing.Font("MS Reference Sans Serif", 26F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblToTime.Location = new System.Drawing.Point(12, 59);
            this.lblToTime.Name = "lblToTime";
            this.lblToTime.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblToTime.Size = new System.Drawing.Size(161, 55);
            this.lblToTime.TabIndex = 21;
            this.lblToTime.Text = "00:00";
            this.lblToTime.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // metroLabel7
            // 
            this.metroLabel7.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel7.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel7.Location = new System.Drawing.Point(99, 14);
            this.metroLabel7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(62, 30);
            this.metroLabel7.TabIndex = 20;
            this.metroLabel7.Text = "Min";
            this.metroLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.metroLabel7.Visible = false;
            // 
            // cmbToHour
            // 
            this.cmbToHour.FontSize = MetroFramework.MetroComboBoxSize.XL;
            this.cmbToHour.FormattingEnabled = true;
            this.cmbToHour.ItemHeight = 41;
            this.cmbToHour.Location = new System.Drawing.Point(4, 77);
            this.cmbToHour.Name = "cmbToHour";
            this.cmbToHour.Size = new System.Drawing.Size(81, 47);
            this.cmbToHour.TabIndex = 28;
            this.cmbToHour.UseSelectable = true;
            this.cmbToHour.Visible = false;
            this.cmbToHour.SelectionChangeCommitted += new System.EventHandler(this.cmbToHour_SelectionChangeCommitted);
            // 
            // cmbToMins
            // 
            this.cmbToMins.FontSize = MetroFramework.MetroComboBoxSize.XL;
            this.cmbToMins.FormattingEnabled = true;
            this.cmbToMins.ItemHeight = 41;
            this.cmbToMins.Location = new System.Drawing.Point(95, 77);
            this.cmbToMins.Name = "cmbToMins";
            this.cmbToMins.Size = new System.Drawing.Size(80, 47);
            this.cmbToMins.TabIndex = 29;
            this.cmbToMins.UseSelectable = true;
            this.cmbToMins.Visible = false;
            this.cmbToMins.SelectionChangeCommitted += new System.EventHandler(this.cmbToMins_SelectionChangeCommitted);
            // 
            // metroPanel13
            // 
            this.metroPanel13.Controls.Add(this.lblToDay);
            this.metroPanel13.Controls.Add(this.lblToDayName);
            this.metroPanel13.Controls.Add(this.cmbToDay);
            this.metroPanel13.HorizontalScrollbarBarColor = true;
            this.metroPanel13.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel13.HorizontalScrollbarSize = 10;
            this.metroPanel13.Location = new System.Drawing.Point(434, 54);
            this.metroPanel13.Name = "metroPanel13";
            this.metroPanel13.Size = new System.Drawing.Size(150, 139);
            this.metroPanel13.TabIndex = 2;
            this.metroPanel13.VerticalScrollbarBarColor = true;
            this.metroPanel13.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel13.VerticalScrollbarSize = 10;
            // 
            // lblToDay
            // 
            this.lblToDay.AutoSize = true;
            this.lblToDay.BackColor = System.Drawing.Color.Transparent;
            this.lblToDay.Font = new System.Drawing.Font("MS Reference Sans Serif", 32F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblToDay.Location = new System.Drawing.Point(30, 49);
            this.lblToDay.Name = "lblToDay";
            this.lblToDay.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblToDay.Size = new System.Drawing.Size(99, 67);
            this.lblToDay.TabIndex = 19;
            this.lblToDay.Text = "00";
            this.lblToDay.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblToDayName
            // 
            this.lblToDayName.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblToDayName.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblToDayName.Location = new System.Drawing.Point(8, 14);
            this.lblToDayName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblToDayName.Name = "lblToDayName";
            this.lblToDayName.Size = new System.Drawing.Size(121, 30);
            this.lblToDayName.TabIndex = 18;
            this.lblToDayName.Text = "Day";
            this.lblToDayName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cmbToDay
            // 
            this.cmbToDay.FontSize = MetroFramework.MetroComboBoxSize.XL;
            this.cmbToDay.FormattingEnabled = true;
            this.cmbToDay.ItemHeight = 41;
            this.cmbToDay.Location = new System.Drawing.Point(8, 68);
            this.cmbToDay.Name = "cmbToDay";
            this.cmbToDay.Size = new System.Drawing.Size(121, 47);
            this.cmbToDay.TabIndex = 25;
            this.cmbToDay.UseSelectable = true;
            this.cmbToDay.Visible = false;
            this.cmbToDay.SelectionChangeCommitted += new System.EventHandler(this.cmbToDay_SelectionChangeCommitted);
            // 
            // lblToDateText
            // 
            this.lblToDateText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblToDateText.FontSize = MetroFramework.MetroLabelSize.XL;
            this.lblToDateText.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblToDateText.Location = new System.Drawing.Point(4, 190);
            this.lblToDateText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblToDateText.Name = "lblToDateText";
            this.lblToDateText.Size = new System.Drawing.Size(968, 40);
            this.lblToDateText.TabIndex = 16;
            this.lblToDateText.Text = "How many days or hours do you wish to rent ?";
            this.lblToDateText.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFromDateText
            // 
            this.lblFromDateText.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblFromDateText.FontSize = MetroFramework.MetroLabelSize.XL;
            this.lblFromDateText.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblFromDateText.Location = new System.Drawing.Point(4, 0);
            this.lblFromDateText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFromDateText.Name = "lblFromDateText";
            this.lblFromDateText.Size = new System.Drawing.Size(968, 30);
            this.lblFromDateText.TabIndex = 14;
            this.lblFromDateText.Text = "From what date do you wish to rent?";
            this.lblFromDateText.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblFromDateText.Visible = false;
            // 
            // metroPanel2
            // 
            this.metroPanel2.Controls.Add(this.metroPanel9);
            this.metroPanel2.Controls.Add(this.metroPanel8);
            this.metroPanel2.Controls.Add(this.metroPanel7);
            this.metroPanel2.Controls.Add(this.metroPanel6);
            this.metroPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel2.HorizontalScrollbarBarColor = true;
            this.metroPanel2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel2.HorizontalScrollbarSize = 10;
            this.metroPanel2.Location = new System.Drawing.Point(3, 33);
            this.metroPanel2.Name = "metroPanel2";
            this.metroPanel2.Size = new System.Drawing.Size(970, 154);
            this.metroPanel2.TabIndex = 17;
            this.metroPanel2.VerticalScrollbarBarColor = true;
            this.metroPanel2.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel2.VerticalScrollbarSize = 10;
            // 
            // metroPanel9
            // 
            this.metroPanel9.Controls.Add(this.lblFromTime);
            this.metroPanel9.Controls.Add(this.metroLabel6);
            this.metroPanel9.HorizontalScrollbarBarColor = true;
            this.metroPanel9.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel9.HorizontalScrollbarSize = 10;
            this.metroPanel9.Location = new System.Drawing.Point(649, 12);
            this.metroPanel9.Name = "metroPanel9";
            this.metroPanel9.Size = new System.Drawing.Size(175, 125);
            this.metroPanel9.TabIndex = 5;
            this.metroPanel9.VerticalScrollbarBarColor = true;
            this.metroPanel9.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel9.VerticalScrollbarSize = 10;
            // 
            // lblFromTime
            // 
            this.lblFromTime.AutoSize = true;
            this.lblFromTime.BackColor = System.Drawing.Color.Transparent;
            this.lblFromTime.Font = new System.Drawing.Font("MS Reference Sans Serif", 25.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFromTime.Location = new System.Drawing.Point(3, 47);
            this.lblFromTime.Name = "lblFromTime";
            this.lblFromTime.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblFromTime.Size = new System.Drawing.Size(156, 53);
            this.lblFromTime.TabIndex = 21;
            this.lblFromTime.Text = "00:00";
            this.lblFromTime.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // metroLabel6
            // 
            this.metroLabel6.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel6.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel6.Location = new System.Drawing.Point(8, 8);
            this.metroLabel6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(170, 24);
            this.metroLabel6.TabIndex = 20;
            this.metroLabel6.Text = "Time";
            this.metroLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // metroPanel8
            // 
            this.metroPanel8.Controls.Add(this.lblFromYear);
            this.metroPanel8.Controls.Add(this.metroLabel5);
            this.metroPanel8.HorizontalScrollbarBarColor = true;
            this.metroPanel8.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel8.HorizontalScrollbarSize = 10;
            this.metroPanel8.Location = new System.Drawing.Point(11, 12);
            this.metroPanel8.Name = "metroPanel8";
            this.metroPanel8.Size = new System.Drawing.Size(172, 125);
            this.metroPanel8.TabIndex = 4;
            this.metroPanel8.VerticalScrollbarBarColor = true;
            this.metroPanel8.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel8.VerticalScrollbarSize = 10;
            // 
            // lblFromYear
            // 
            this.lblFromYear.AutoSize = true;
            this.lblFromYear.BackColor = System.Drawing.Color.Transparent;
            this.lblFromYear.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblFromYear.Font = new System.Drawing.Font("MS Reference Sans Serif", 32F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFromYear.Location = new System.Drawing.Point(3, 47);
            this.lblFromYear.Name = "lblFromYear";
            this.lblFromYear.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblFromYear.Size = new System.Drawing.Size(169, 67);
            this.lblFromYear.TabIndex = 21;
            this.lblFromYear.Text = "9999";
            this.lblFromYear.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // metroLabel5
            // 
            this.metroLabel5.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel5.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel5.Location = new System.Drawing.Point(9, 8);
            this.metroLabel5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(144, 30);
            this.metroLabel5.TabIndex = 20;
            this.metroLabel5.Text = "Year";
            this.metroLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // metroPanel7
            // 
            this.metroPanel7.Controls.Add(this.lblFromMonth);
            this.metroPanel7.Controls.Add(this.lblFromMonthName);
            this.metroPanel7.HorizontalScrollbarBarColor = true;
            this.metroPanel7.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel7.HorizontalScrollbarSize = 10;
            this.metroPanel7.Location = new System.Drawing.Point(230, 12);
            this.metroPanel7.Name = "metroPanel7";
            this.metroPanel7.Size = new System.Drawing.Size(152, 125);
            this.metroPanel7.TabIndex = 3;
            this.metroPanel7.VerticalScrollbarBarColor = true;
            this.metroPanel7.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel7.VerticalScrollbarSize = 10;
            // 
            // lblFromMonth
            // 
            this.lblFromMonth.AutoSize = true;
            this.lblFromMonth.BackColor = System.Drawing.Color.Transparent;
            this.lblFromMonth.Font = new System.Drawing.Font("MS Reference Sans Serif", 32F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFromMonth.Location = new System.Drawing.Point(27, 47);
            this.lblFromMonth.Name = "lblFromMonth";
            this.lblFromMonth.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblFromMonth.Size = new System.Drawing.Size(99, 67);
            this.lblFromMonth.TabIndex = 21;
            this.lblFromMonth.Text = "00";
            this.lblFromMonth.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblFromMonthName
            // 
            this.lblFromMonthName.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblFromMonthName.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblFromMonthName.Location = new System.Drawing.Point(15, 8);
            this.lblFromMonthName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFromMonthName.Name = "lblFromMonthName";
            this.lblFromMonthName.Size = new System.Drawing.Size(97, 30);
            this.lblFromMonthName.TabIndex = 20;
            this.lblFromMonthName.Text = "Month";
            this.lblFromMonthName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // metroPanel6
            // 
            this.metroPanel6.Controls.Add(this.lblFromDay);
            this.metroPanel6.Controls.Add(this.lblFromDayName);
            this.metroPanel6.HorizontalScrollbarBarColor = true;
            this.metroPanel6.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel6.HorizontalScrollbarSize = 10;
            this.metroPanel6.Location = new System.Drawing.Point(433, 12);
            this.metroPanel6.Name = "metroPanel6";
            this.metroPanel6.Size = new System.Drawing.Size(150, 125);
            this.metroPanel6.TabIndex = 2;
            this.metroPanel6.VerticalScrollbarBarColor = true;
            this.metroPanel6.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel6.VerticalScrollbarSize = 10;
            // 
            // lblFromDay
            // 
            this.lblFromDay.AutoSize = true;
            this.lblFromDay.BackColor = System.Drawing.Color.Transparent;
            this.lblFromDay.Font = new System.Drawing.Font("MS Reference Sans Serif", 32F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFromDay.Location = new System.Drawing.Point(30, 47);
            this.lblFromDay.Name = "lblFromDay";
            this.lblFromDay.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblFromDay.Size = new System.Drawing.Size(99, 67);
            this.lblFromDay.TabIndex = 19;
            this.lblFromDay.Text = "00";
            this.lblFromDay.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblFromDayName
            // 
            this.lblFromDayName.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblFromDayName.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblFromDayName.Location = new System.Drawing.Point(4, 8);
            this.lblFromDayName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFromDayName.Name = "lblFromDayName";
            this.lblFromDayName.Size = new System.Drawing.Size(125, 30);
            this.lblFromDayName.TabIndex = 18;
            this.lblFromDayName.Text = "Day";
            this.lblFromDayName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // RentalStartControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlStart);
            this.Controls.Add(this.tableLayoutPanelMain);
            this.Controls.Add(this.metroPanel1);
            this.Name = "RentalStartControl";
            this.Size = new System.Drawing.Size(1032, 900);
            this.pnlStart.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.metroPanel12.ResumeLayout(false);
            this.metroPanel12.PerformLayout();
            this.metroPanel11.ResumeLayout(false);
            this.metroPanel1.ResumeLayout(false);
            this.tableLayoutPanelMain.ResumeLayout(false);
            this.tableLayoutPanelMain.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.metroPanel5.ResumeLayout(false);
            this.metroPanel5.PerformLayout();
            this.metroPanel10.ResumeLayout(false);
            this.metroPanel10.PerformLayout();
            this.metroPanel13.ResumeLayout(false);
            this.metroPanel13.PerformLayout();
            this.metroPanel2.ResumeLayout(false);
            this.metroPanel9.ResumeLayout(false);
            this.metroPanel9.PerformLayout();
            this.metroPanel8.ResumeLayout(false);
            this.metroPanel8.PerformLayout();
            this.metroPanel7.ResumeLayout(false);
            this.metroPanel7.PerformLayout();
            this.metroPanel6.ResumeLayout(false);
            this.metroPanel6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroLabel lblHeader;
        private MetroFramework.Controls.MetroPanel pnlStart;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private MetroFramework.Controls.MetroButton btn1Day;
        private MetroFramework.Controls.MetroButton btnMoreTime;
        private MetroFramework.Controls.MetroButton btnHalfDay;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain;
        private MetroFramework.Controls.MetroButton btnBack;
        private MetroFramework.Controls.MetroButton btnNext;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private MetroFramework.Controls.MetroPanel metroPanel5;
        private MetroFramework.Controls.MetroButton btnToMinUp;
        private MetroFramework.Controls.MetroButton btnToMinDown;
        private MetroFramework.Controls.MetroButton btnToTimeDown;
        private MetroFramework.Controls.MetroButton btnToYearDown;
        private MetroFramework.Controls.MetroButton btnToMonthDown;
        private MetroFramework.Controls.MetroButton btnToTimeUp;
        private MetroFramework.Controls.MetroButton btnToYearUp;
        private MetroFramework.Controls.MetroButton btnToMonthUp;
        private MetroFramework.Controls.MetroButton btnToDayDown;
        private MetroFramework.Controls.MetroButton btnToDayUp;
        private MetroFramework.Controls.MetroPanel metroPanel10;
        private MetroFramework.Controls.MetroComboBox cmbToMins;
        private MetroFramework.Controls.MetroComboBox cmbToHour;
        private System.Windows.Forms.Label lblToTime;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroPanel metroPanel11;
        private MetroFramework.Controls.MetroComboBox cmbToYear;
        private System.Windows.Forms.Label lblToYear;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroPanel metroPanel12;
        private MetroFramework.Controls.MetroComboBox cmbToMonth;
        private System.Windows.Forms.Label lblToMonth;
        private MetroFramework.Controls.MetroLabel lblToMonthName;
        private MetroFramework.Controls.MetroPanel metroPanel13;
        private MetroFramework.Controls.MetroComboBox cmbToDay;
        private System.Windows.Forms.Label lblToDay;
        private MetroFramework.Controls.MetroLabel lblToDayName;
        private MetroFramework.Controls.MetroLabel lblToDateText;
        private MetroFramework.Controls.MetroLabel lblFromDateText;
        private MetroFramework.Controls.MetroPanel metroPanel2;
        private MetroFramework.Controls.MetroButton btnFromMinUp;
        private MetroFramework.Controls.MetroButton btnFromMinDown;
        private MetroFramework.Controls.MetroButton btnFromTimeDown;
        private MetroFramework.Controls.MetroButton btnFromYearDown;
        private MetroFramework.Controls.MetroButton btnFromMonthDown;
        private MetroFramework.Controls.MetroButton btnFromTimeUp;
        private MetroFramework.Controls.MetroButton btnFromYearUp;
        private MetroFramework.Controls.MetroButton btnFromMonthUp;
        private MetroFramework.Controls.MetroButton btnFromDayDown;
        private MetroFramework.Controls.MetroButton btnFromDayUp;
        private MetroFramework.Controls.MetroPanel metroPanel9;
        private MetroFramework.Controls.MetroComboBox cmbFromMins;
        private MetroFramework.Controls.MetroComboBox cmbFromHour;
        private System.Windows.Forms.Label lblFromTime;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroPanel metroPanel8;
        private MetroFramework.Controls.MetroComboBox cmbFromYear;
        private System.Windows.Forms.Label lblFromYear;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroPanel metroPanel7;
        private MetroFramework.Controls.MetroComboBox cmbFromMonth;
        private System.Windows.Forms.Label lblFromMonth;
        private MetroFramework.Controls.MetroLabel lblFromMonthName;
        private MetroFramework.Controls.MetroPanel metroPanel6;
        private MetroFramework.Controls.MetroComboBox cmbFromDay;
        private System.Windows.Forms.Label lblFromDay;
        private MetroFramework.Controls.MetroLabel lblFromDayName;
        private MetroFramework.Controls.MetroLabel lblWarning;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel lblReturnDate;
    }
}
