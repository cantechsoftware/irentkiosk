﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Castle.Core;

namespace iRent.Rental
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class RentalRentalDamageDisagreeControl : UserControl
    {
        public EventHandler<object> Next;
        public EventHandler<object> Back;

        public RentalRentalDamageDisagreeControl()
        {
            InitializeComponent();
            
        }

        public void Bind(string bookingno)
        {
            lblBkno.Text = lblBkno.Text.Replace("bknno", bookingno);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (!chkAddDamages.Checked)
            {
                Next(this, false);
            }
            else
            {
                Next(this, true);
            }
            
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            Back(null, null);
        }
    }
}
