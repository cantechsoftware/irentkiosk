﻿using System;
using System.Windows.Forms;
using Castle.Core;
using SmartFormat;
using iRent.Framework;
using iRentKiosk.Core.Services;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Services.Api;
using iRentKiosk.Core.Models;

namespace iRent.Rental
{
    [CastleComponent]
    public class RentalDamageDisagreeCommand : BaseCommand
    {
        public string Next { get; set; }
        public string Back { get; set; }

        Func<RentalRentalDamageDisagreeControl> _factory;
        private RentalApi _rentalApi;
        private int _bookingid { get; set; }

        public RentalDamageDisagreeCommand(Func<RentalRentalDamageDisagreeControl> Factory, RentalApi RentalAPI)
        {
            _factory = Factory;
            _rentalApi = RentalAPI;
        }

        public override void Invoke()
        {
            int bookingid = 0;
            RentalOptions options = ContextData.RentalOptions;
            if (options == null || options?.BookingInfo == null)
            {
                bookingid = ContextData.Booking.Id;
            }
            else
            {
                bookingid = options.BookingInfo.BookingID;
            }
            BookingModel booking = BookingApi.GetBooking(bookingid);
            _bookingid = bookingid;
            string bookingno = booking.BookingNo;

            var control = _factory();
            control.Dock = DockStyle.Fill;
            control.Next = OnNext;
            control.Back = OnBack;
            control.Bind(bookingno);
            var main = GetFormAs<MainForm>();
            main.AddToFormPanel(control);
        }
        public void OnBack(object sender, object data)
        {
            Proceed(Back);
        }

        public void OnNext(object sender, object data)
        {
            bool inform = (bool)data;
            if (inform)
            {
                _rentalApi.KioskDamageNotify(_bookingid.ToString());
            }
            Proceed(Next);
        }
    }
}
