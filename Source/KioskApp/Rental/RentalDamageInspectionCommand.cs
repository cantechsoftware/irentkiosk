﻿using System;
using Castle.Core;
using iRent.Framework;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Services;
using iRent2.Contracts.Rental;
using iRentKiosk.Core.Services.Api;
using System.Windows.Forms;
using iRentKiosk.Core.Models;
using iRent2.Contracts.PayGate;
using iRent2.Contracts.Bookings;
using iRent2.Contracts.Enums;
namespace iRent.Rental
{
    [CastleComponent]
    public class RentalDamageInspectionCommand : BaseCommand
    {
        private VehicleApi _vehApi;
        private Func<RentalDamageInspectionControl> _factory;
        private RentalApi _rentalApi;
        private DamagesApi _damagesApi;

        public RentalDamageInspectionCommand(Func<RentalDamageInspectionControl> Factory, RentalApi RentalApi, VehicleApi vehicleApi, DamagesApi damagesApi)
        {
            _vehApi = vehicleApi;
            _factory = Factory;
            _rentalApi = RentalApi;
            _damagesApi = damagesApi;
        }

        public string Accept { get; set; }
        public string Decline { get; set; }
        public string Back { get; set; }

        public override void Invoke()
        {
            DriverModel d = ContextData.Driver;

            int bookingid = 0;
            RentalOptions options = ContextData.RentalOptions;
            if (options == null || options?.BookingInfo == null)
            {
                bookingid = ContextData.Booking.Id;
            }
            else
            {
                bookingid = options.BookingInfo.BookingID;
            }

            //Get Booking
            BookingModel booking = BookingApi.GetBooking(bookingid);
            VehicleModel vehicle = _vehApi.GetById(booking.VehicleId ?? 0);
            DamageModel[] damages = _damagesApi.GetDamages(vehicle.Id);

            if (damages.Length <= 0)
            {
                Proceed(Accept);
            }

            ContextData.Vehicle = vehicle;

            RentalInspectionRequest request = new RentalInspectionRequest();
            request.BookingID = booking.Id;
            request.VehicleID = vehicle.Id;
            request.InspectionDate = DateTime.Now;
            request.Damages = damages;
            ContextData.SetRentalInspection(request);

            var control = _factory();
            control.Dock = DockStyle.Fill;
            var main = GetFormAs<MainForm>();
            main.AddToFormPanel(control);
            control.Bind(ContextData.RentalInspection, ContextData.Vehicle);
            control.Next += OnNext;
            if(Back != null && Back !="")
                control.Back += OnBack;
        }

        public void OnBack(object Sender, RentalInspectionRequest request)
        {
            Proceed(Back);
        }

        public void OnNext(object Sender, RentalInspectionRequest request)
        {
            //Send inspection data
            if (request.Status == RentalInspectionStatus.Accepted)
            {
                request.Damages = new DamageModel[0];
                RentalInspectionResponse response = _rentalApi.RentalInspection(request);
                Proceed(Accept);
            }
            else
            {
                request.Damages = new DamageModel[0];
                RentalInspectionResponse response = _rentalApi.RentalInspection(request);
                Proceed(Decline);
            }
            
        }
    }
}
