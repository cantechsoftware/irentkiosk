﻿using System;
using System.Windows.Forms;
using Castle.Core;
using iRent.Framework;
using iRentKiosk.Core.Services;


namespace iRent.Rental
{
    [CastleComponent]
    public class RentalSummaryCommand : BaseCommand
    {
        private readonly Func<RentalSummaryControl> _factory;

        public RentalSummaryCommand(Func<RentalSummaryControl> factory)
        {
            _factory = factory;
            Intro = "Thank you {DriverName} for using the CliQ system:";
            Status = "Your rental details are as follows.";
        }

        public string Intro { get; set; }
        public string Status { get; set; }
        public string Next { get; set; }

        public override void Invoke()
        {
            var control = _factory();
            control.Dock = DockStyle.Fill;
            var main = GetFormAs<MainForm>();
            
            ContextData.SetBooking(BookingApi.GetBooking(ContextData.DynamicData["PayGateInit"].BookingId));
            main.AddToFormPanel(control);
            control.Init(ContextData.Format(Intro), ContextData.Format(Status), ContextData.Booking);
            control.Next += (o, e) => Proceed(Next);
        }
    }
}