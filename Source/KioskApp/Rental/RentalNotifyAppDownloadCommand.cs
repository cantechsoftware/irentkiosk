﻿using System;
using System.Windows.Forms;
using Castle.Core;
using SmartFormat;
using iRent.Framework;
using iRentKiosk.Core.Services;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Services.Api;

namespace iRent.Rental
{
    [CastleComponent]
    public class RentalNotifyAppDownloadCommand : BaseCommand
    {
        public string Next { get; set; }

        Func<RentalNotifyAppDownloadControl> _factory;

        public RentalNotifyAppDownloadCommand(Func<RentalNotifyAppDownloadControl> Factory)
        {
            _factory = Factory;
        }

        public override void Invoke()
        {
            var control = _factory();
            control.Dock = DockStyle.Fill;
            control.Next = OnNext;
            var main = GetFormAs<MainForm>();
            main.AddToFormPanel(control);
        }

        public void OnNext(object sender, object data)
        {
            Proceed(Next);
        }
    }
}
