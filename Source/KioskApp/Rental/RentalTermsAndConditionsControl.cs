﻿using System;
using System.Collections.Generic;
using iRentKiosk.Core.Models;
using iRentKiosk.Core.Services;
using System.Windows.Forms;
using iRent2.Contracts.Rental;
using MetroFramework.Controls;
using Castle.Core;

namespace iRent.Rental
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class RentalTermsAndConditionsControl : MetroUserControl
    {
        public EventHandler<object> Next;

        public RentalTermsAndConditionsControl()
        {
            InitializeComponent();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            Next(this, null);
        }
    }
}
