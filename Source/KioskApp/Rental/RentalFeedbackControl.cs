﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Castle.Core;
using iRent2.Contracts.Rental;
using iRent2.Contracts.Enums;

namespace iRent.Rental
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class RentalFeedbackControl : UserControl
    {
        public EventHandler<object> Next;
        public EventHandler<object> Back;
        public string _bookingno { get; set; }
        public int _bookingid { get; set; }

        public RentalFeedbackControl()
        {
            InitializeComponent();
            
        }

        public void Bind(string bookingno, int bookingid)
        {
            //lblBkno.Text = lblBkno.Text.Replace("bknno", bookingno);
            _bookingno = bookingno;
            _bookingid = bookingid;
        }

        private CustomerFeedbackRequest[] SaveFeedback()
        {
            CustomerFeedbackRequest[] ret = new CustomerFeedbackRequest[3];

            var checkedClean = pnlIsClean.Controls.OfType<RadioButton>()
                                      .FirstOrDefault(r => r.Checked);

            CustomerFeedbackRequest clean = new CustomerFeedbackRequest();
            clean.BookingId = _bookingid;
            if (checkedClean.Text == "Yes")
            {
                clean.Answer = true;
            }
            else
            {
                clean.Answer = false;
            }
            clean.ClientVersion = "V1.0";
            clean.FeedbackType = CustFeedbackType.Question;
            clean.Question = "Is the vehicel clean?";
            clean.QuestionIdentifier = 1;
            clean.IsKiosk = true;
            ret[0] = clean;


            var checkedRate = pnlRating.Controls.OfType<RadioButton>()
                          .FirstOrDefault(r => r.Checked);

            CustomerFeedbackRequest rate = new CustomerFeedbackRequest();
            rate.BookingId = _bookingid;
            rate.ClientVersion = "V1.0";
            rate.FeedbackType = CustFeedbackType.Rating;
            rate.Question = "How would you rate the overall experience";
            rate.QuestionIdentifier = 2;
            rate.IsKiosk = true;
            if (checkedRate.Text == "1 Star")
            {
                rate.Rating = 1;
            }
            else if (checkedRate.Text == "2 Stars")
            {
                rate.Rating = 2;
            }
            else if (checkedRate.Text == "3 Stars")
            {
                rate.Rating = 3;
            }
            else if (checkedRate.Text == "4 Stars")
            {
                rate.Rating = 4;
            }
            else if (checkedRate.Text == "5 Stars")
            {
                rate.Rating = 5;
            }
            ret[1] = rate;

            CustomerFeedbackRequest comment = new CustomerFeedbackRequest();
            comment.BookingId = _bookingid;
            comment.ClientVersion = "V1.0";
            comment.FeedbackType = CustFeedbackType.Comment;
            comment.Question = "Comments";
            comment.QuestionIdentifier = 3;
            comment.Comment = txtComments.Text;
            comment.IsKiosk = true;
            ret[2] = comment;
            return ret;
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            CustomerFeedbackRequest[] feedback = SaveFeedback();
            Next(this, feedback);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            Back(this, null);
        }
    }
}
