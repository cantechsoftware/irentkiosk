﻿namespace iRent.Rental
{
    partial class RentalFeedbackControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.metroPanel3 = new MetroFramework.Controls.MetroPanel();
            this.btnNext = new MetroFramework.Controls.MetroButton();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.txtComments = new MetroFramework.Controls.MetroTextBox();
            this.pnlRating = new MetroFramework.Controls.MetroPanel();
            this.radRating5 = new MetroFramework.Controls.MetroRadioButton();
            this.radRating4 = new MetroFramework.Controls.MetroRadioButton();
            this.radRating3 = new MetroFramework.Controls.MetroRadioButton();
            this.radRating2 = new MetroFramework.Controls.MetroRadioButton();
            this.radRating1 = new MetroFramework.Controls.MetroRadioButton();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.pnlIsClean = new MetroFramework.Controls.MetroPanel();
            this.radNo = new MetroFramework.Controls.MetroRadioButton();
            this.radYes = new MetroFramework.Controls.MetroRadioButton();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.lblHeader = new MetroFramework.Controls.MetroLabel();
            this.metroStyleExtender1 = new MetroFramework.Components.MetroStyleExtender(this.components);
            this.metroStyleExtender2 = new MetroFramework.Components.MetroStyleExtender(this.components);
            this.metroStyleExtender3 = new MetroFramework.Components.MetroStyleExtender(this.components);
            this.btnBack = new MetroFramework.Controls.MetroButton();
            this.metroPanel3.SuspendLayout();
            this.metroPanel1.SuspendLayout();
            this.pnlRating.SuspendLayout();
            this.pnlIsClean.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroPanel3
            // 
            this.metroPanel3.Controls.Add(this.btnBack);
            this.metroPanel3.Controls.Add(this.btnNext);
            this.metroPanel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.metroPanel3.HorizontalScrollbarBarColor = true;
            this.metroPanel3.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel3.HorizontalScrollbarSize = 10;
            this.metroPanel3.Location = new System.Drawing.Point(0, 456);
            this.metroPanel3.Name = "metroPanel3";
            this.metroPanel3.Size = new System.Drawing.Size(1005, 122);
            this.metroPanel3.TabIndex = 17;
            this.metroPanel3.VerticalScrollbarBarColor = true;
            this.metroPanel3.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel3.VerticalScrollbarSize = 10;
            // 
            // btnNext
            // 
            this.btnNext.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnNext.Location = new System.Drawing.Point(673, 3);
            this.btnNext.Margin = new System.Windows.Forms.Padding(4);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(259, 115);
            this.btnNext.TabIndex = 5;
            this.btnNext.Text = "&Continue";
            this.btnNext.UseSelectable = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // metroPanel1
            // 
            this.metroPanel1.Controls.Add(this.txtComments);
            this.metroPanel1.Controls.Add(this.pnlRating);
            this.metroPanel1.Controls.Add(this.metroLabel2);
            this.metroPanel1.Controls.Add(this.pnlIsClean);
            this.metroPanel1.Controls.Add(this.metroLabel1);
            this.metroPanel1.Controls.Add(this.lblHeader);
            this.metroPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(0, 0);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(1005, 456);
            this.metroPanel1.TabIndex = 18;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // txtComments
            // 
            this.txtComments.Lines = new string[0];
            this.txtComments.Location = new System.Drawing.Point(182, 267);
            this.txtComments.MaxLength = 250;
            this.txtComments.Multiline = true;
            this.txtComments.Name = "txtComments";
            this.txtComments.PasswordChar = '\0';
            this.txtComments.PromptText = "Comments";
            this.txtComments.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtComments.SelectedText = "";
            this.txtComments.Size = new System.Drawing.Size(647, 162);
            this.txtComments.TabIndex = 23;
            this.txtComments.UseSelectable = true;
            // 
            // pnlRating
            // 
            this.pnlRating.Controls.Add(this.radRating5);
            this.pnlRating.Controls.Add(this.radRating4);
            this.pnlRating.Controls.Add(this.radRating3);
            this.pnlRating.Controls.Add(this.radRating2);
            this.pnlRating.Controls.Add(this.radRating1);
            this.pnlRating.HorizontalScrollbarBarColor = true;
            this.pnlRating.HorizontalScrollbarHighlightOnWheel = false;
            this.pnlRating.HorizontalScrollbarSize = 10;
            this.pnlRating.Location = new System.Drawing.Point(182, 215);
            this.pnlRating.Name = "pnlRating";
            this.pnlRating.Size = new System.Drawing.Size(647, 46);
            this.pnlRating.TabIndex = 22;
            this.pnlRating.VerticalScrollbarBarColor = true;
            this.pnlRating.VerticalScrollbarHighlightOnWheel = false;
            this.pnlRating.VerticalScrollbarSize = 10;
            // 
            // radRating5
            // 
            this.radRating5.AutoSize = true;
            this.radRating5.Checked = true;
            this.radRating5.FontSize = MetroFramework.MetroCheckBoxSize.Medium;
            this.radRating5.FontWeight = MetroFramework.MetroCheckBoxWeight.Bold;
            this.radRating5.Location = new System.Drawing.Point(476, 12);
            this.radRating5.Name = "radRating5";
            this.radRating5.Size = new System.Drawing.Size(73, 20);
            this.radRating5.TabIndex = 24;
            this.radRating5.TabStop = true;
            this.radRating5.Text = "5 Stars";
            this.radRating5.UseSelectable = true;
            // 
            // radRating4
            // 
            this.radRating4.AutoSize = true;
            this.radRating4.FontSize = MetroFramework.MetroCheckBoxSize.Medium;
            this.radRating4.FontWeight = MetroFramework.MetroCheckBoxWeight.Bold;
            this.radRating4.Location = new System.Drawing.Point(387, 12);
            this.radRating4.Name = "radRating4";
            this.radRating4.Size = new System.Drawing.Size(73, 20);
            this.radRating4.TabIndex = 23;
            this.radRating4.Text = "4 Stars";
            this.radRating4.UseSelectable = true;
            // 
            // radRating3
            // 
            this.radRating3.AutoSize = true;
            this.radRating3.FontSize = MetroFramework.MetroCheckBoxSize.Medium;
            this.radRating3.FontWeight = MetroFramework.MetroCheckBoxWeight.Bold;
            this.radRating3.Location = new System.Drawing.Point(298, 12);
            this.radRating3.Name = "radRating3";
            this.radRating3.Size = new System.Drawing.Size(73, 20);
            this.radRating3.TabIndex = 22;
            this.radRating3.Text = "3 Stars";
            this.radRating3.UseSelectable = true;
            // 
            // radRating2
            // 
            this.radRating2.AutoSize = true;
            this.radRating2.FontSize = MetroFramework.MetroCheckBoxSize.Medium;
            this.radRating2.FontWeight = MetroFramework.MetroCheckBoxWeight.Bold;
            this.radRating2.Location = new System.Drawing.Point(209, 12);
            this.radRating2.Name = "radRating2";
            this.radRating2.Size = new System.Drawing.Size(73, 20);
            this.radRating2.TabIndex = 21;
            this.radRating2.Text = "2 Stars";
            this.radRating2.UseSelectable = true;
            // 
            // radRating1
            // 
            this.radRating1.AutoSize = true;
            this.radRating1.FontSize = MetroFramework.MetroCheckBoxSize.Medium;
            this.radRating1.FontWeight = MetroFramework.MetroCheckBoxWeight.Bold;
            this.radRating1.Location = new System.Drawing.Point(120, 12);
            this.radRating1.Name = "radRating1";
            this.radRating1.Size = new System.Drawing.Size(66, 20);
            this.radRating1.TabIndex = 20;
            this.radRating1.Text = "1 Star";
            this.radRating1.UseSelectable = true;
            // 
            // metroLabel2
            // 
            this.metroLabel2.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel2.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.metroLabel2.Location = new System.Drawing.Point(182, 169);
            this.metroLabel2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(647, 43);
            this.metroLabel2.TabIndex = 21;
            this.metroLabel2.Text = "How would you rate the over all experience ?";
            this.metroLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlIsClean
            // 
            this.pnlIsClean.Controls.Add(this.radNo);
            this.pnlIsClean.Controls.Add(this.radYes);
            this.pnlIsClean.HorizontalScrollbarBarColor = true;
            this.pnlIsClean.HorizontalScrollbarHighlightOnWheel = false;
            this.pnlIsClean.HorizontalScrollbarSize = 10;
            this.pnlIsClean.Location = new System.Drawing.Point(645, 120);
            this.pnlIsClean.Name = "pnlIsClean";
            this.pnlIsClean.Size = new System.Drawing.Size(146, 46);
            this.pnlIsClean.TabIndex = 20;
            this.pnlIsClean.VerticalScrollbarBarColor = true;
            this.pnlIsClean.VerticalScrollbarHighlightOnWheel = false;
            this.pnlIsClean.VerticalScrollbarSize = 10;
            // 
            // radNo
            // 
            this.radNo.AutoSize = true;
            this.radNo.FontSize = MetroFramework.MetroCheckBoxSize.Medium;
            this.radNo.FontWeight = MetroFramework.MetroCheckBoxWeight.Bold;
            this.radNo.Location = new System.Drawing.Point(81, 12);
            this.radNo.Name = "radNo";
            this.radNo.Size = new System.Drawing.Size(46, 20);
            this.radNo.TabIndex = 20;
            this.radNo.Text = "No";
            this.radNo.UseSelectable = true;
            // 
            // radYes
            // 
            this.radYes.AutoSize = true;
            this.radYes.Checked = true;
            this.radYes.FontSize = MetroFramework.MetroCheckBoxSize.Medium;
            this.radYes.FontWeight = MetroFramework.MetroCheckBoxWeight.Bold;
            this.radYes.Location = new System.Drawing.Point(13, 12);
            this.radYes.Name = "radYes";
            this.radYes.Size = new System.Drawing.Size(48, 20);
            this.radYes.TabIndex = 19;
            this.radYes.TabStop = true;
            this.radYes.Text = "Yes";
            this.radYes.UseSelectable = true;
            // 
            // metroLabel1
            // 
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.metroLabel1.Location = new System.Drawing.Point(182, 119);
            this.metroLabel1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(467, 50);
            this.metroLabel1.TabIndex = 19;
            this.metroLabel1.Text = "Is the vehicle clean ?";
            this.metroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblHeader
            // 
            this.lblHeader.FontSize = MetroFramework.MetroLabelSize.XL;
            this.lblHeader.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblHeader.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblHeader.Location = new System.Drawing.Point(132, 55);
            this.lblHeader.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(800, 62);
            this.lblHeader.TabIndex = 16;
            this.lblHeader.Text = "Customer Feedback";
            this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnBack
            // 
            this.btnBack.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnBack.Location = new System.Drawing.Point(109, 0);
            this.btnBack.Margin = new System.Windows.Forms.Padding(4);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(259, 115);
            this.btnBack.TabIndex = 6;
            this.btnBack.Text = "&Back";
            this.btnBack.UseSelectable = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // RentalFeedbackControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.metroPanel1);
            this.Controls.Add(this.metroPanel3);
            this.Name = "RentalFeedbackControl";
            this.Size = new System.Drawing.Size(1005, 578);
            this.metroPanel3.ResumeLayout(false);
            this.metroPanel1.ResumeLayout(false);
            this.pnlRating.ResumeLayout(false);
            this.pnlRating.PerformLayout();
            this.pnlIsClean.ResumeLayout(false);
            this.pnlIsClean.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroPanel metroPanel3;
        private MetroFramework.Controls.MetroButton btnNext;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroLabel lblHeader;
        private MetroFramework.Components.MetroStyleExtender metroStyleExtender1;
        private MetroFramework.Controls.MetroTextBox txtComments;
        private MetroFramework.Controls.MetroPanel pnlRating;
        private MetroFramework.Controls.MetroRadioButton radRating5;
        private MetroFramework.Controls.MetroRadioButton radRating4;
        private MetroFramework.Controls.MetroRadioButton radRating3;
        private MetroFramework.Controls.MetroRadioButton radRating2;
        private MetroFramework.Controls.MetroRadioButton radRating1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroPanel pnlIsClean;
        private MetroFramework.Controls.MetroRadioButton radNo;
        private MetroFramework.Controls.MetroRadioButton radYes;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Components.MetroStyleExtender metroStyleExtender2;
        private MetroFramework.Components.MetroStyleExtender metroStyleExtender3;
        private MetroFramework.Controls.MetroButton btnBack;
    }
}
