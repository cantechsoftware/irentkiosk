﻿using System;
using Castle.Core;
using iRent.Framework;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Services;
using iRent2.Contracts.Rental;
using iRentKiosk.Core.Services.Api;
using iRentKiosk.Core.Models;
using iRent2.Contracts.PayGate;
using System.Linq;

namespace iRent.Rental
{
    [CastleComponent]
    public class RentalReissueCardCommand : BaseCommand
    {
        RentalApi _rentalApi;
        PayGateApi _payGateApi;

        public string Next { get; set; }
        public string OutputDataContext { get; set; }

        public RentalReissueCardCommand(RentalApi rentalApi, PayGateApi payGateApi)
        {
            _rentalApi = rentalApi;
            _payGateApi = payGateApi;
        }

        public override void Invoke()
        {
            ChargeReIssueCardRequest request = new ChargeReIssueCardRequest();
            RentalOptions options = ContextData.RentalOptions;

            request.DriverId = ContextData.Driver.Id;
            //NB! check
            ChargeReIssueCardResponse response = _rentalApi.CardReissueCharge(request);
            if(response == null)
            {
                //what aare we supposed to here ? nothing ?
                
            }
            RegisterPaymentRequest paygatedata = new RegisterPaymentRequest();
            if (ContextData.DynamicData.ContainsKey("PayGateInit"))
            {
                paygatedata = ContextData.DynamicData["PayGateInit"];
            }
            paygatedata.Amount = response.Amount;
            //NB! check
            paygatedata.BookingId = null;
            paygatedata.PayConfirmEmail = ContextData.Driver.Email;
            paygatedata.ReturnUrl = "";
            paygatedata.InvoiceId = response.InvoiceId;
            paygatedata.VaultToken = "";
            ContextData.SetDynamicData(OutputDataContext, paygatedata);
            Proceed(Next);
        }
    }
}
