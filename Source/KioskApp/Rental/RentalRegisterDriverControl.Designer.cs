﻿namespace iRent.Rental
{
    partial class RentalRegisterDriverControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RentalRegisterDriverControl));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblError = new MetroFramework.Controls.MetroLabel();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.btnShowPassword = new MetroFramework.Controls.MetroButton();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.txtTown = new MetroFramework.Controls.MetroTextBox();
            this.txtWorkNo = new MetroFramework.Controls.MetroTextBox();
            this.cmbTitle = new MetroFramework.Controls.MetroComboBox();
            this.lblOTP = new MetroFramework.Controls.MetroLabel();
            this.txtOTP = new MetroFramework.Controls.MetroTextBox();
            this.btnOTP = new MetroFramework.Controls.MetroButton();
            this.txtConfirmPin = new MetroFramework.Controls.MetroTextBox();
            this.txtPin = new MetroFramework.Controls.MetroTextBox();
            this.txtIDNumber = new MetroFramework.Controls.MetroTextBox();
            this.txtSurname = new MetroFramework.Controls.MetroTextBox();
            this.txtPostalCode = new MetroFramework.Controls.MetroTextBox();
            this.txtCountry = new MetroFramework.Controls.MetroTextBox();
            this.txtProvince = new MetroFramework.Controls.MetroTextBox();
            this.txtCity = new MetroFramework.Controls.MetroTextBox();
            this.txtStreetName = new MetroFramework.Controls.MetroTextBox();
            this.txtStreetNo = new MetroFramework.Controls.MetroTextBox();
            this.txtComplex = new MetroFramework.Controls.MetroTextBox();
            this.txtUnitNo = new MetroFramework.Controls.MetroTextBox();
            this.lblHeader2 = new MetroFramework.Controls.MetroLabel();
            this.txtFirstName = new MetroFramework.Controls.MetroTextBox();
            this.txtConfirmEmail = new MetroFramework.Controls.MetroTextBox();
            this.txtCellNo = new MetroFramework.Controls.MetroTextBox();
            this.txtEmail = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroPanel3 = new MetroFramework.Controls.MetroPanel();
            this.btnBack = new MetroFramework.Controls.MetroButton();
            this.btnNext = new MetroFramework.Controls.MetroButton();
            this.lblHeader = new MetroFramework.Controls.MetroLabel();
            this.metroStyleExtender1 = new MetroFramework.Components.MetroStyleExtender(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            this.metroPanel1.SuspendLayout();
            this.metroPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.lblError, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.metroPanel1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 62);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 93F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(984, 592);
            this.tableLayoutPanel1.TabIndex = 17;
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.BackColor = System.Drawing.SystemColors.Control;
            this.lblError.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblError.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblError.ForeColor = System.Drawing.Color.Red;
            this.lblError.Location = new System.Drawing.Point(3, 550);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(978, 42);
            this.lblError.TabIndex = 33;
            this.lblError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblError.Visible = false;
            this.lblError.WrapToLine = true;
            // 
            // metroPanel1
            // 
            this.metroPanel1.Controls.Add(this.btnShowPassword);
            this.metroPanel1.Controls.Add(this.metroLabel2);
            this.metroPanel1.Controls.Add(this.txtTown);
            this.metroPanel1.Controls.Add(this.txtWorkNo);
            this.metroPanel1.Controls.Add(this.cmbTitle);
            this.metroPanel1.Controls.Add(this.lblOTP);
            this.metroPanel1.Controls.Add(this.txtOTP);
            this.metroPanel1.Controls.Add(this.btnOTP);
            this.metroPanel1.Controls.Add(this.txtConfirmPin);
            this.metroPanel1.Controls.Add(this.txtPin);
            this.metroPanel1.Controls.Add(this.txtIDNumber);
            this.metroPanel1.Controls.Add(this.txtSurname);
            this.metroPanel1.Controls.Add(this.txtPostalCode);
            this.metroPanel1.Controls.Add(this.txtCountry);
            this.metroPanel1.Controls.Add(this.txtProvince);
            this.metroPanel1.Controls.Add(this.txtCity);
            this.metroPanel1.Controls.Add(this.txtStreetName);
            this.metroPanel1.Controls.Add(this.txtStreetNo);
            this.metroPanel1.Controls.Add(this.txtComplex);
            this.metroPanel1.Controls.Add(this.txtUnitNo);
            this.metroPanel1.Controls.Add(this.lblHeader2);
            this.metroPanel1.Controls.Add(this.txtFirstName);
            this.metroPanel1.Controls.Add(this.txtConfirmEmail);
            this.metroPanel1.Controls.Add(this.txtCellNo);
            this.metroPanel1.Controls.Add(this.txtEmail);
            this.metroPanel1.Controls.Add(this.metroLabel1);
            this.metroPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(3, 3);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(978, 544);
            this.metroPanel1.TabIndex = 15;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // btnShowPassword
            // 
            this.btnShowPassword.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btnShowPassword.Location = new System.Drawing.Point(678, 231);
            this.btnShowPassword.Name = "btnShowPassword";
            this.btnShowPassword.Size = new System.Drawing.Size(169, 37);
            this.btnShowPassword.TabIndex = 11;
            this.btnShowPassword.Text = "Show Password";
            this.btnShowPassword.UseSelectable = true;
            this.btnShowPassword.Click += new System.EventHandler(this.btnShowPassword_Click);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel2.Location = new System.Drawing.Point(82, 272);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(665, 20);
            this.metroLabel2.TabIndex = 32;
            this.metroLabel2.Text = "A secure password should contain at least eight (8) alpha numeric characters with" +
    " one capital.";
            this.metroLabel2.Visible = false;
            this.metroLabel2.WrapToLine = true;
            // 
            // txtTown
            // 
            this.txtTown.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtTown.Icon = global::iRent.Properties.Resources.icon_star;
            this.txtTown.IconRight = true;
            this.txtTown.Lines = new string[0];
            this.txtTown.Location = new System.Drawing.Point(14, 382);
            this.txtTown.Margin = new System.Windows.Forms.Padding(4);
            this.txtTown.MaxLength = 32767;
            this.txtTown.Name = "txtTown";
            this.txtTown.PasswordChar = '\0';
            this.txtTown.PromptText = "Suburb";
            this.txtTown.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtTown.SelectedText = "";
            this.txtTown.Size = new System.Drawing.Size(327, 37);
            this.txtTown.TabIndex = 16;
            this.txtTown.UseSelectable = true;
            // 
            // txtWorkNo
            // 
            this.txtWorkNo.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtWorkNo.Icon = ((System.Drawing.Image)(resources.GetObject("txtWorkNo.Icon")));
            this.txtWorkNo.IconRight = true;
            this.txtWorkNo.Lines = new string[0];
            this.txtWorkNo.Location = new System.Drawing.Point(18, 185);
            this.txtWorkNo.Margin = new System.Windows.Forms.Padding(4);
            this.txtWorkNo.MaxLength = 32767;
            this.txtWorkNo.Name = "txtWorkNo";
            this.txtWorkNo.PasswordChar = '\0';
            this.txtWorkNo.PromptText = "Work Number";
            this.txtWorkNo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtWorkNo.SelectedText = "";
            this.txtWorkNo.Size = new System.Drawing.Size(327, 37);
            this.txtWorkNo.TabIndex = 8;
            this.txtWorkNo.UseSelectable = true;
            // 
            // cmbTitle
            // 
            this.cmbTitle.BackColor = System.Drawing.SystemColors.Control;
            this.cmbTitle.FontSize = MetroFramework.MetroComboBoxSize.Tall;
            this.cmbTitle.ItemHeight = 29;
            this.cmbTitle.Items.AddRange(new object[] {
            "Mr",
            "Mrs",
            "Miss",
            "Ms",
            "Dr"});
            this.cmbTitle.Location = new System.Drawing.Point(18, 52);
            this.cmbTitle.Margin = new System.Windows.Forms.Padding(4);
            this.cmbTitle.Name = "cmbTitle";
            this.cmbTitle.PromptText = "Title";
            this.cmbTitle.Size = new System.Drawing.Size(325, 35);
            this.cmbTitle.Style = MetroFramework.MetroColorStyle.Blue;
            this.cmbTitle.TabIndex = 0;
            this.cmbTitle.UseCustomBackColor = true;
            this.cmbTitle.UseSelectable = true;
            // 
            // lblOTP
            // 
            this.lblOTP.AutoSize = true;
            this.lblOTP.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblOTP.Location = new System.Drawing.Point(366, 488);
            this.lblOTP.Name = "lblOTP";
            this.lblOTP.Size = new System.Drawing.Size(341, 20);
            this.lblOTP.TabIndex = 28;
            this.lblOTP.Text = "Unable to send the OTP, please contact support.";
            this.lblOTP.Visible = false;
            this.lblOTP.WrapToLine = true;
            // 
            // txtOTP
            // 
            this.txtOTP.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtOTP.Icon = global::iRent.Properties.Resources.icon_star;
            this.txtOTP.IconRight = true;
            this.txtOTP.Lines = new string[0];
            this.txtOTP.Location = new System.Drawing.Point(691, 471);
            this.txtOTP.MaxLength = 32767;
            this.txtOTP.Name = "txtOTP";
            this.txtOTP.PasswordChar = '\0';
            this.txtOTP.PromptText = "Enter OTP";
            this.txtOTP.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtOTP.SelectedText = "";
            this.txtOTP.Size = new System.Drawing.Size(179, 37);
            this.txtOTP.TabIndex = 23;
            this.txtOTP.UseSelectable = true;
            this.txtOTP.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtOTP_KeyUp);
            // 
            // btnOTP
            // 
            this.btnOTP.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btnOTP.Location = new System.Drawing.Point(387, 471);
            this.btnOTP.Name = "btnOTP";
            this.btnOTP.Size = new System.Drawing.Size(259, 52);
            this.btnOTP.TabIndex = 22;
            this.btnOTP.Text = "Request OTP";
            this.btnOTP.UseSelectable = true;
            this.btnOTP.Click += new System.EventHandler(this.btnOTP_Click);
            // 
            // txtConfirmPin
            // 
            this.txtConfirmPin.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtConfirmPin.Icon = global::iRent.Properties.Resources.icon_star;
            this.txtConfirmPin.IconRight = true;
            this.txtConfirmPin.Lines = new string[0];
            this.txtConfirmPin.Location = new System.Drawing.Point(369, 231);
            this.txtConfirmPin.Margin = new System.Windows.Forms.Padding(4);
            this.txtConfirmPin.MaxLength = 10;
            this.txtConfirmPin.Name = "txtConfirmPin";
            this.txtConfirmPin.PasswordChar = '*';
            this.txtConfirmPin.PromptText = "Confirm Password";
            this.txtConfirmPin.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtConfirmPin.SelectedText = "";
            this.txtConfirmPin.Size = new System.Drawing.Size(253, 37);
            this.txtConfirmPin.TabIndex = 10;
            this.txtConfirmPin.UseSelectable = true;
            // 
            // txtPin
            // 
            this.txtPin.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtPin.Icon = global::iRent.Properties.Resources.icon_star;
            this.txtPin.IconRight = true;
            this.txtPin.Lines = new string[0];
            this.txtPin.Location = new System.Drawing.Point(80, 231);
            this.txtPin.Margin = new System.Windows.Forms.Padding(4);
            this.txtPin.MaxLength = 10;
            this.txtPin.Name = "txtPin";
            this.txtPin.PasswordChar = '*';
            this.txtPin.PromptText = "Password";
            this.txtPin.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtPin.SelectedText = "";
            this.txtPin.Size = new System.Drawing.Size(265, 37);
            this.txtPin.TabIndex = 9;
            this.txtPin.UseSelectable = true;
            // 
            // txtIDNumber
            // 
            this.txtIDNumber.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtIDNumber.Icon = global::iRent.Properties.Resources.icon_star;
            this.txtIDNumber.IconRight = true;
            this.txtIDNumber.Lines = new string[0];
            this.txtIDNumber.Location = new System.Drawing.Point(369, 50);
            this.txtIDNumber.Margin = new System.Windows.Forms.Padding(4);
            this.txtIDNumber.MaxLength = 32767;
            this.txtIDNumber.Name = "txtIDNumber";
            this.txtIDNumber.PasswordChar = '\0';
            this.txtIDNumber.PromptText = "ID Number";
            this.txtIDNumber.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtIDNumber.SelectedText = "";
            this.txtIDNumber.Size = new System.Drawing.Size(327, 37);
            this.txtIDNumber.TabIndex = 1;
            this.txtIDNumber.UseSelectable = true;
            // 
            // txtSurname
            // 
            this.txtSurname.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtSurname.Icon = global::iRent.Properties.Resources.icon_star;
            this.txtSurname.IconRight = true;
            this.txtSurname.Lines = new string[0];
            this.txtSurname.Location = new System.Drawing.Point(369, 95);
            this.txtSurname.Margin = new System.Windows.Forms.Padding(4);
            this.txtSurname.MaxLength = 32767;
            this.txtSurname.Name = "txtSurname";
            this.txtSurname.PasswordChar = '\0';
            this.txtSurname.PromptText = "Surname";
            this.txtSurname.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtSurname.SelectedText = "";
            this.txtSurname.Size = new System.Drawing.Size(327, 37);
            this.txtSurname.TabIndex = 5;
            this.txtSurname.UseSelectable = true;
            // 
            // txtPostalCode
            // 
            this.txtPostalCode.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtPostalCode.Icon = global::iRent.Properties.Resources.icon_star;
            this.txtPostalCode.IconRight = true;
            this.txtPostalCode.Lines = new string[0];
            this.txtPostalCode.Location = new System.Drawing.Point(715, 382);
            this.txtPostalCode.Margin = new System.Windows.Forms.Padding(4);
            this.txtPostalCode.MaxLength = 32767;
            this.txtPostalCode.Name = "txtPostalCode";
            this.txtPostalCode.PasswordChar = '\0';
            this.txtPostalCode.PromptText = "Postal Code";
            this.txtPostalCode.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtPostalCode.SelectedText = "";
            this.txtPostalCode.Size = new System.Drawing.Size(174, 37);
            this.txtPostalCode.TabIndex = 18;
            this.txtPostalCode.UseSelectable = true;
            // 
            // txtCountry
            // 
            this.txtCountry.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtCountry.Icon = global::iRent.Properties.Resources.icon_star;
            this.txtCountry.IconRight = true;
            this.txtCountry.Lines = new string[0];
            this.txtCountry.Location = new System.Drawing.Point(461, 427);
            this.txtCountry.Margin = new System.Windows.Forms.Padding(4);
            this.txtCountry.MaxLength = 32767;
            this.txtCountry.Name = "txtCountry";
            this.txtCountry.PasswordChar = '\0';
            this.txtCountry.PromptText = "Country";
            this.txtCountry.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCountry.SelectedText = "";
            this.txtCountry.Size = new System.Drawing.Size(428, 37);
            this.txtCountry.TabIndex = 20;
            this.txtCountry.UseSelectable = true;
            // 
            // txtProvince
            // 
            this.txtProvince.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtProvince.Icon = global::iRent.Properties.Resources.icon_star;
            this.txtProvince.IconRight = true;
            this.txtProvince.Lines = new string[0];
            this.txtProvince.Location = new System.Drawing.Point(14, 427);
            this.txtProvince.Margin = new System.Windows.Forms.Padding(4);
            this.txtProvince.MaxLength = 32767;
            this.txtProvince.Name = "txtProvince";
            this.txtProvince.PasswordChar = '\0';
            this.txtProvince.PromptText = "Province/State";
            this.txtProvince.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtProvince.SelectedText = "";
            this.txtProvince.Size = new System.Drawing.Size(428, 37);
            this.txtProvince.TabIndex = 19;
            this.txtProvince.UseSelectable = true;
            // 
            // txtCity
            // 
            this.txtCity.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtCity.Icon = global::iRent.Properties.Resources.icon_star;
            this.txtCity.IconRight = true;
            this.txtCity.Lines = new string[0];
            this.txtCity.Location = new System.Drawing.Point(367, 382);
            this.txtCity.Margin = new System.Windows.Forms.Padding(4);
            this.txtCity.MaxLength = 32767;
            this.txtCity.Name = "txtCity";
            this.txtCity.PasswordChar = '\0';
            this.txtCity.PromptText = "City";
            this.txtCity.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCity.SelectedText = "";
            this.txtCity.Size = new System.Drawing.Size(327, 37);
            this.txtCity.TabIndex = 17;
            this.txtCity.UseSelectable = true;
            // 
            // txtStreetName
            // 
            this.txtStreetName.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtStreetName.Icon = global::iRent.Properties.Resources.icon_star;
            this.txtStreetName.IconRight = true;
            this.txtStreetName.Lines = new string[0];
            this.txtStreetName.Location = new System.Drawing.Point(601, 337);
            this.txtStreetName.Margin = new System.Windows.Forms.Padding(4);
            this.txtStreetName.MaxLength = 32767;
            this.txtStreetName.Name = "txtStreetName";
            this.txtStreetName.PasswordChar = '\0';
            this.txtStreetName.PromptText = "Street Name";
            this.txtStreetName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtStreetName.SelectedText = "";
            this.txtStreetName.Size = new System.Drawing.Size(288, 37);
            this.txtStreetName.TabIndex = 15;
            this.txtStreetName.UseSelectable = true;
            // 
            // txtStreetNo
            // 
            this.txtStreetNo.BackColor = System.Drawing.SystemColors.Control;
            this.txtStreetNo.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtStreetNo.Icon = global::iRent.Properties.Resources.icon_star;
            this.txtStreetNo.IconRight = true;
            this.txtStreetNo.Lines = new string[0];
            this.txtStreetNo.Location = new System.Drawing.Point(461, 337);
            this.txtStreetNo.Margin = new System.Windows.Forms.Padding(4);
            this.txtStreetNo.MaxLength = 32767;
            this.txtStreetNo.Name = "txtStreetNo";
            this.txtStreetNo.PasswordChar = '\0';
            this.txtStreetNo.PromptText = "Str No";
            this.txtStreetNo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtStreetNo.SelectedText = "";
            this.txtStreetNo.Size = new System.Drawing.Size(118, 37);
            this.txtStreetNo.TabIndex = 14;
            this.txtStreetNo.UseSelectable = true;
            // 
            // txtComplex
            // 
            this.txtComplex.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtComplex.IconRight = true;
            this.txtComplex.Lines = new string[0];
            this.txtComplex.Location = new System.Drawing.Point(154, 337);
            this.txtComplex.Margin = new System.Windows.Forms.Padding(4);
            this.txtComplex.MaxLength = 32767;
            this.txtComplex.Name = "txtComplex";
            this.txtComplex.PasswordChar = '\0';
            this.txtComplex.PromptText = "Complex Name";
            this.txtComplex.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtComplex.SelectedText = "";
            this.txtComplex.Size = new System.Drawing.Size(288, 37);
            this.txtComplex.TabIndex = 13;
            this.txtComplex.UseSelectable = true;
            // 
            // txtUnitNo
            // 
            this.txtUnitNo.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtUnitNo.IconRight = true;
            this.txtUnitNo.Lines = new string[0];
            this.txtUnitNo.Location = new System.Drawing.Point(14, 337);
            this.txtUnitNo.Margin = new System.Windows.Forms.Padding(4);
            this.txtUnitNo.MaxLength = 32767;
            this.txtUnitNo.Name = "txtUnitNo";
            this.txtUnitNo.PasswordChar = '\0';
            this.txtUnitNo.PromptText = "Unit No";
            this.txtUnitNo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtUnitNo.SelectedText = "";
            this.txtUnitNo.Size = new System.Drawing.Size(118, 37);
            this.txtUnitNo.TabIndex = 12;
            this.txtUnitNo.UseSelectable = true;
            // 
            // lblHeader2
            // 
            this.lblHeader2.AutoSize = true;
            this.lblHeader2.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblHeader2.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lblHeader2.Location = new System.Drawing.Point(18, 305);
            this.lblHeader2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHeader2.Name = "lblHeader2";
            this.lblHeader2.Size = new System.Drawing.Size(137, 25);
            this.lblHeader2.TabIndex = 27;
            this.lblHeader2.Text = "Billing Address";
            // 
            // txtFirstName
            // 
            this.txtFirstName.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtFirstName.Icon = global::iRent.Properties.Resources.icon_star;
            this.txtFirstName.IconRight = true;
            this.txtFirstName.Lines = new string[0];
            this.txtFirstName.Location = new System.Drawing.Point(16, 95);
            this.txtFirstName.Margin = new System.Windows.Forms.Padding(4);
            this.txtFirstName.MaxLength = 32767;
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.PasswordChar = '\0';
            this.txtFirstName.PromptText = "First Name";
            this.txtFirstName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtFirstName.SelectedText = "";
            this.txtFirstName.Size = new System.Drawing.Size(327, 37);
            this.txtFirstName.TabIndex = 2;
            this.txtFirstName.UseSelectable = true;
            // 
            // txtConfirmEmail
            // 
            this.txtConfirmEmail.BackColor = System.Drawing.Color.Red;
            this.txtConfirmEmail.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtConfirmEmail.Icon = ((System.Drawing.Image)(resources.GetObject("txtConfirmEmail.Icon")));
            this.txtConfirmEmail.IconRight = true;
            this.txtConfirmEmail.Lines = new string[0];
            this.txtConfirmEmail.Location = new System.Drawing.Point(369, 140);
            this.txtConfirmEmail.Margin = new System.Windows.Forms.Padding(4);
            this.txtConfirmEmail.MaxLength = 32767;
            this.txtConfirmEmail.Name = "txtConfirmEmail";
            this.txtConfirmEmail.PasswordChar = '\0';
            this.txtConfirmEmail.PromptText = "Confirm Email";
            this.txtConfirmEmail.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtConfirmEmail.SelectedText = "";
            this.txtConfirmEmail.Size = new System.Drawing.Size(327, 37);
            this.txtConfirmEmail.TabIndex = 7;
            this.txtConfirmEmail.UseSelectable = true;
            // 
            // txtCellNo
            // 
            this.txtCellNo.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtCellNo.Icon = global::iRent.Properties.Resources.icon_star;
            this.txtCellNo.IconRight = true;
            this.txtCellNo.Lines = new string[0];
            this.txtCellNo.Location = new System.Drawing.Point(15, 471);
            this.txtCellNo.Margin = new System.Windows.Forms.Padding(4);
            this.txtCellNo.MaxLength = 32767;
            this.txtCellNo.Name = "txtCellNo";
            this.txtCellNo.PasswordChar = '\0';
            this.txtCellNo.PromptText = "Cell Number";
            this.txtCellNo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCellNo.SelectedText = "";
            this.txtCellNo.Size = new System.Drawing.Size(327, 37);
            this.txtCellNo.TabIndex = 21;
            this.txtCellNo.UseSelectable = true;
            // 
            // txtEmail
            // 
            this.txtEmail.BackColor = System.Drawing.Color.Red;
            this.txtEmail.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtEmail.Icon = ((System.Drawing.Image)(resources.GetObject("txtEmail.Icon")));
            this.txtEmail.IconRight = true;
            this.txtEmail.Lines = new string[0];
            this.txtEmail.Location = new System.Drawing.Point(16, 140);
            this.txtEmail.Margin = new System.Windows.Forms.Padding(4);
            this.txtEmail.MaxLength = 32767;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.PasswordChar = '\0';
            this.txtEmail.PromptText = "Email";
            this.txtEmail.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtEmail.SelectedText = "";
            this.txtEmail.Size = new System.Drawing.Size(327, 37);
            this.txtEmail.TabIndex = 6;
            this.txtEmail.UseSelectable = true;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel1.Location = new System.Drawing.Point(16, 11);
            this.metroLabel1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(139, 25);
            this.metroLabel1.TabIndex = 23;
            this.metroLabel1.Text = "Contact Details";
            // 
            // metroPanel3
            // 
            this.metroPanel3.Controls.Add(this.btnBack);
            this.metroPanel3.Controls.Add(this.btnNext);
            this.metroPanel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.metroPanel3.HorizontalScrollbarBarColor = true;
            this.metroPanel3.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel3.HorizontalScrollbarSize = 10;
            this.metroPanel3.Location = new System.Drawing.Point(0, 654);
            this.metroPanel3.Name = "metroPanel3";
            this.metroPanel3.Size = new System.Drawing.Size(984, 116);
            this.metroPanel3.TabIndex = 16;
            this.metroPanel3.VerticalScrollbarBarColor = true;
            this.metroPanel3.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel3.VerticalScrollbarSize = 10;
            // 
            // btnBack
            // 
            this.btnBack.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnBack.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnBack.Location = new System.Drawing.Point(0, 0);
            this.btnBack.Margin = new System.Windows.Forms.Padding(4);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(259, 116);
            this.btnBack.TabIndex = 24;
            this.btnBack.Text = "&Back";
            this.btnBack.UseSelectable = true;
            this.btnBack.Visible = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnNext
            // 
            this.btnNext.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnNext.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnNext.Location = new System.Drawing.Point(725, 0);
            this.btnNext.Margin = new System.Windows.Forms.Padding(4);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(259, 116);
            this.btnNext.TabIndex = 25;
            this.btnNext.Text = "&Continue";
            this.btnNext.UseSelectable = true;
            this.btnNext.Visible = false;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // lblHeader
            // 
            this.lblHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblHeader.FontSize = MetroFramework.MetroLabelSize.XL;
            this.lblHeader.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblHeader.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblHeader.Location = new System.Drawing.Point(0, 0);
            this.lblHeader.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(984, 62);
            this.lblHeader.TabIndex = 14;
            this.lblHeader.Text = "We need some personal details before you can continue";
            this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // RentalRegisterDriverControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.metroPanel3);
            this.Controls.Add(this.lblHeader);
            this.Name = "RentalRegisterDriverControl";
            this.Size = new System.Drawing.Size(984, 770);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel1.PerformLayout();
            this.metroPanel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private MetroFramework.Controls.MetroLabel lblHeader;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroTextBox txtFirstName;
        private MetroFramework.Controls.MetroTextBox txtConfirmEmail;
        private MetroFramework.Controls.MetroTextBox txtCellNo;
        private MetroFramework.Controls.MetroTextBox txtEmail;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel lblHeader2;
        private MetroFramework.Controls.MetroTextBox txtUnitNo;
        private MetroFramework.Controls.MetroTextBox txtComplex;
        private MetroFramework.Controls.MetroTextBox txtCountry;
        private MetroFramework.Controls.MetroTextBox txtProvince;
        private MetroFramework.Controls.MetroTextBox txtCity;
        private MetroFramework.Controls.MetroTextBox txtStreetName;
        private MetroFramework.Controls.MetroTextBox txtStreetNo;
        private MetroFramework.Controls.MetroTextBox txtPostalCode;
        private MetroFramework.Controls.MetroTextBox txtSurname;
        private MetroFramework.Controls.MetroPanel metroPanel3;
        private MetroFramework.Controls.MetroButton btnNext;
        private MetroFramework.Controls.MetroTextBox txtConfirmPin;
        private MetroFramework.Controls.MetroTextBox txtPin;
        private MetroFramework.Controls.MetroTextBox txtIDNumber;
        private MetroFramework.Controls.MetroButton btnOTP;
        private MetroFramework.Components.MetroStyleExtender metroStyleExtender1;
        private MetroFramework.Controls.MetroTextBox txtOTP;
        private MetroFramework.Controls.MetroLabel lblOTP;
        private MetroFramework.Controls.MetroComboBox cmbTitle;
        private MetroFramework.Controls.MetroTextBox txtWorkNo;
        private MetroFramework.Controls.MetroTextBox txtTown;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel lblError;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MetroFramework.Controls.MetroButton btnBack;
        private MetroFramework.Controls.MetroButton btnShowPassword;
    }
}
