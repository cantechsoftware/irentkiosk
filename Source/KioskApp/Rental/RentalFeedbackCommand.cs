﻿using System;
using System.Windows.Forms;
using Castle.Core;
using SmartFormat;
using iRent.Framework;
using iRentKiosk.Core.Services;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Services.Api;
using iRentKiosk.Core.Models;
using iRent2.Contracts.Rental;

namespace iRent.Rental
{
    [CastleComponent]
    public class RentalFeedbackCommand : BaseCommand
    {
        public string Next { get; set; }
        public string Back { get; set; }

        Func<RentalFeedbackControl> _factory;
        private RentalApi _rentalApi;

        public RentalFeedbackCommand(Func<RentalFeedbackControl> Factory, RentalApi RentalAPI)
        {
            _factory = Factory;
            _rentalApi = RentalAPI;
        }

        public override void Invoke()
        {
            int bookingid = 0;
            RentalOptions options = ContextData.RentalOptions;
            if (options == null || options?.BookingInfo == null)
            {
                bookingid = ContextData.Booking.Id;
            }
            else
            {
                bookingid = options.BookingInfo.BookingID;
            }
            BookingModel booking = BookingApi.GetBooking(bookingid);
            string bookingno = booking.BookingNo;

            var control = _factory();
            control.Dock = DockStyle.Fill;
            control.Next = OnNext;
            control.Back = OnBack;
            control.Bind(bookingno, bookingid);
            var main = GetFormAs<MainForm>();
            main.AddToFormPanel(control);
        }
        public void OnBack(object sender, object data)
        {
            Proceed(Back);
        }

        public void OnNext(object sender, object data)
        {
            // call the curomer feedback api at this point
            CustomerFeedbackRequest[] cust = (CustomerFeedbackRequest[])data;
            for (int i = 0; i < 3; i++)
            {
                CustomerFeedbackRequest send = cust[i];
                _rentalApi.CustomerFeedback(send);
            }
            Proceed(Next);
        }
    }
}
