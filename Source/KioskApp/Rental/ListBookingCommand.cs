﻿using System;
using Castle.Core;
using iRent.Framework;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;
using iRentKiosk.Core.Models;
using iRent2.Contracts.Bookings;
using iRent2.Contracts.Enums;

namespace iRent.Rental
{
    [CastleComponent]
    public class ListBookingCommand : BaseCommand
    {
        BookingApi _bookingApi;
        Func<ListBookingControl> _factory;

        public string Next { get; set; }
        public string Completed { get; set; }

        public ListBookingCommand(Func<ListBookingControl> Factory, BookingApi BookingApi)
        {
            _bookingApi = BookingApi;
            _factory = Factory;
        }

        public override void Invoke()
        {
            SearchBookingsRequest request = new SearchBookingsRequest();
            request.DriverId = ContextData.Driver.Id;
            request.Statuses = new BookingStatus[2] { BookingStatus.VehicleAssigned, BookingStatus.Active };
            BookingModel[] bookings = _bookingApi.FindBookings(request);
            var control = _factory();
            var main = GetFormAs<MainForm>();
            main.AddToFormPanel(control);
            control.Bind(bookings);
            control.Next += OnNext;
        }



        private void OnNext(object sender, BookingModel booking)
        {
            ContextData.SetBooking(booking);
            Proceed(Next);
        }
    }
}
