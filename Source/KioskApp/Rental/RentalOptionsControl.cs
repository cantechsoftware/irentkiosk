﻿using System;
using System.Collections.Generic;
using iRentKiosk.Core.Models;
using iRentKiosk.Core.Services;
using System.Windows.Forms;
using iRent2.Contracts.Rental;
using MetroFramework.Controls;
using Castle.Core;
using iRent2.Contracts.Models;
using iRent2.Contracts.AdoModels;
using MetroFramework;
using RentalOptionModel = iRent2.Contracts.AdoModels.RentalOptionModel;

namespace iRent.Rental
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class RentalOptionsControl : MetroUserControl
    {
        public RentalOptionsControl()
        {
            InitializeComponent();
        }

        public EventHandler<object> Next;
        public EventHandler<object> Back;

        private List<MetroTile> _activeOptions = new List<MetroTile>();

        private List<MetroTile> _waiverTiles = new List<MetroTile>();
        private List<MetroTile> _optionTiles = new List<MetroTile>();
        private Dictionary<string, string> _selections = new Dictionary<string, string>();

        public void Bind()
        {
            _selections.Clear();
            RentalOptions rentalOptions = ContextData.RentalOptions;
            PopulateRentalOptions(rentalOptions.Options);
            PopulateWaiverOptions(rentalOptions.WaiverOptions);
            UpdateSelections();
        }

        private string FormattedAmount(decimal? _amount)
        {
            return _amount == null ? "null" : string.Format("{0:C}", _amount.Value);
        }

        private void PopulateRentalOptions(RentalOptionModel[] options)
        {
            foreach (RentalOptionModel model in options)
            {
                MetroTile tile = new MetroTile();
                tile.Text = model.Name+ "\r\n" + FormattedAmount(model.Charge);
                tile.TileTextFontSize = MetroTileTextSize.Medium;
                tile.TileTextFontWeight = MetroTileTextWeight.Bold;
                tile.Style = MetroColorStyle.Silver;
                tile.Dock = DockStyle.Fill;
                tile.Parent = tblOptions;
                tile.Tag = model;
                tile.TextAlign = System.Drawing.ContentAlignment.TopLeft;
                tile.Click += SelectOption_Click;
                _optionTiles.Add(tile);
            }
        }
        private void UpdateSelections()
        {
            txtSelections.Clear();
            if (_selections != null && _selections.Count > 0)
            {
                foreach (KeyValuePair<string, string> entry in _selections)
                {
                    txtSelections.AppendText(entry.Value);
                    txtSelections.AppendText(Environment.NewLine);
                }
            }
        }

        private void PopulateWaiverOptions(WaiverOptionModel[] waivers)
        {
            foreach (WaiverOptionModel waiver in waivers)
            {
                MetroTile tile = new MetroTile();
                tile.Text = waiver.Name + "\r\n" + FormattedAmount(waiver.Excess);
                tile.TileTextFontSize = MetroTileTextSize.Medium;
                tile.TileTextFontWeight = MetroTileTextWeight.Bold;
                tile.Style = MetroColorStyle.Silver;
                tile.Dock = DockStyle.Fill;
                tile.Parent = tblWaiver;
                tile.Tag = waiver;
                tile.TextAlign = System.Drawing.ContentAlignment.TopLeft;
                tile.Click += SelectWaiver_Click;

                _waiverTiles.Add(tile);
            }
            MetroTile firsttile = new MetroTile();
            firsttile = _waiverTiles[0];
            firsttile.Style = MetroColorStyle.Red;
            _selections.Add("Waiver", ((WaiverOptionModel)firsttile.Tag).Description);
        }

        private void SelectWaiver_Click(object Sender, EventArgs e)
        {
            MetroTile tile = (MetroTile)Sender;
            if(_selections.ContainsKey("Waiver"))
            {
                _selections["Waiver"] = ((WaiverOptionModel)tile.Tag).Description;
            }
            else
            {
                _selections.Add("Waiver", ((WaiverOptionModel)tile.Tag).Description);
            }

            if (tile.Style == MetroColorStyle.Silver)
            {
                tile.Style = MetroColorStyle.Red;
            }
            else
            {
                tile.Style = MetroColorStyle.Silver;
            }

            // check if the selecte otpion is selected
            foreach (MetroTile otile in _waiverTiles)
            {
                if (otile != tile)
                {
                    otile.Style = MetroColorStyle.Silver;
                }

                if (otile == tile)
                {
                    otile.Style = tile.Style;
                }
            }
            bool isslected = false;
            foreach (MetroTile otile in _waiverTiles)
            {
                if(otile.Style == MetroColorStyle.Red)
                {
                    isslected = true;
                }
            }
            if(!isslected)
            {
                foreach (MetroTile otile in _waiverTiles)
                {
                    if(otile == tile)
                    {
                        otile.Style = MetroColorStyle.Red;
                        tile.Style = MetroColorStyle.Red;
                    }
                }
            }
            UpdateSelections();
            Refresh();
        }

        private void SelectOption_Click(object Sender, EventArgs e)
        {
            MetroTile tile = (MetroTile)Sender;
            if (tile.Style == MetroColorStyle.Silver)
            {
                tile.Style = MetroColorStyle.Red;
            }
            else
            {
                tile.Style = MetroColorStyle.Silver;
            }

            if (_selections.ContainsKey(((RentalOptionModel)tile.Tag).Name))
            {
                if (tile.Style == MetroColorStyle.Silver)
                {
                    _selections.Remove(((RentalOptionModel)tile.Tag).Name);
                }
            }
            else
            {
                if (tile.Style == MetroColorStyle.Red)
                {
                    _selections.Add(((RentalOptionModel)tile.Tag).Name, ((RentalOptionModel)tile.Tag).Description);
                }
            }
            UpdateSelections();
            Refresh();
        }

        private void metroPanel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            List<RentalOptionModel> options = new List<RentalOptionModel>();
            foreach (MetroTile optiontile in _optionTiles)
            {
                if (optiontile.Style == MetroColorStyle.Red)
                {
                    options.Add((RentalOptionModel)optiontile.Tag);
                }
            }
            WaiverOptionModel waiver = null;
            foreach (MetroTile waivertile in _waiverTiles)
            {
                if (waivertile.Style == MetroColorStyle.Red)
                {
                    waiver = (WaiverOptionModel)waivertile.Tag;
                }
            }

            ContextData.RentalOptions.SelectedWaiver = waiver;
            ContextData.RentalOptions.SelectedOptions = options.ToArray();
            Next(this, null);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            Back(this, null);
        }
    }
}
