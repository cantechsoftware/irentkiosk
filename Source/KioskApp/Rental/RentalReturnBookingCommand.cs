﻿using System;
using Castle.Core;
using iRent.Framework;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;
using iRent2.Contracts.Rental;

namespace iRent.Rental
{
    [CastleComponent]
    public class RentalReturnBookingCommand : BaseCommand
    {
        public RentalReturnBookingCommand(RentalApi rentalApi)
        {
            _rentalApi = rentalApi;
        }

        private RentalApi _rentalApi;

        public string Next { get; set; }

        public override void Invoke()
        {
            RentalReturnRequest request = new RentalReturnRequest();
            request.BookingID = ContextData.Booking.Id;
            request.ReturnDate = DateTime.Now;
            _rentalApi.RentalReturn(request);
            Proceed(Next);
        }
    }
}
