﻿using System;
using Castle.Core;
using iRent.Framework;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;

namespace iRent.Rental
{
    [CastleComponent]
    public class CreateBookingCommand : BaseCommand
    {
        private readonly BookingApi _bookingApi;

        public CreateBookingCommand(BookingApi bookingApi)
        {
            _bookingApi = bookingApi;
        }

        public string Next { get; set; }

        public override void Invoke()
        {
            var booking = new BookingModel
                {
                    PickupBranch = ContextData.Kiosk.Branch,
                    PickupDate = DateTime.Now,
                    DriverId = ContextData.Driver.Id,
                    VehicleGroup = ContextData.VehicleGroup
                };

            booking = _bookingApi.Save(booking);
            if (booking == null)
                return;
            ContextData.SetBooking(booking);
            Proceed(Next);
        }
    }
}