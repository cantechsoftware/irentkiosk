﻿namespace iRent.Rental
{
    partial class RentalShowBookingQuoteControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.lblHeader = new MetroFramework.Controls.MetroLabel();
            this.metroPanel3 = new MetroFramework.Controls.MetroPanel();
            this.btnBack = new MetroFramework.Controls.MetroButton();
            this.btnNext = new MetroFramework.Controls.MetroButton();
            this.metroPanel2 = new MetroFramework.Controls.MetroPanel();
            this.txtIntiationFee = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroPanel4 = new MetroFramework.Controls.MetroPanel();
            this.cmbPayMethod = new MetroFramework.Controls.MetroComboBox();
            this.txtTotalCost = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.txtToDate = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.txtFromDate = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.txtVehicle = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.txtVehicleGroup = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroPanel1.SuspendLayout();
            this.metroPanel3.SuspendLayout();
            this.metroPanel2.SuspendLayout();
            this.metroPanel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroPanel1
            // 
            this.metroPanel1.Controls.Add(this.lblHeader);
            this.metroPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(0, 0);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(1065, 92);
            this.metroPanel1.TabIndex = 5;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // lblHeader
            // 
            this.lblHeader.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblHeader.FontSize = MetroFramework.MetroLabelSize.XL;
            this.lblHeader.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblHeader.Location = new System.Drawing.Point(0, 30);
            this.lblHeader.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(1065, 62);
            this.lblHeader.TabIndex = 13;
            this.lblHeader.Text = "Please confirm your booking information";
            this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // metroPanel3
            // 
            this.metroPanel3.Controls.Add(this.btnBack);
            this.metroPanel3.Controls.Add(this.btnNext);
            this.metroPanel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.metroPanel3.HorizontalScrollbarBarColor = true;
            this.metroPanel3.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel3.HorizontalScrollbarSize = 10;
            this.metroPanel3.Location = new System.Drawing.Point(0, 612);
            this.metroPanel3.Name = "metroPanel3";
            this.metroPanel3.Size = new System.Drawing.Size(1065, 122);
            this.metroPanel3.TabIndex = 6;
            this.metroPanel3.VerticalScrollbarBarColor = true;
            this.metroPanel3.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel3.VerticalScrollbarSize = 10;
            // 
            // btnBack
            // 
            this.btnBack.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnBack.Location = new System.Drawing.Point(710, 0);
            this.btnBack.Margin = new System.Windows.Forms.Padding(4);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(259, 115);
            this.btnBack.TabIndex = 6;
            this.btnBack.Text = "Change Options";
            this.btnBack.UseSelectable = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnNext
            // 
            this.btnNext.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnNext.Location = new System.Drawing.Point(422, 0);
            this.btnNext.Margin = new System.Windows.Forms.Padding(4);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(259, 115);
            this.btnNext.TabIndex = 5;
            this.btnNext.Text = "Proceed to Payment";
            this.btnNext.UseSelectable = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // metroPanel2
            // 
            this.metroPanel2.Controls.Add(this.txtIntiationFee);
            this.metroPanel2.Controls.Add(this.metroLabel7);
            this.metroPanel2.Controls.Add(this.metroLabel5);
            this.metroPanel2.Controls.Add(this.metroPanel4);
            this.metroPanel2.Controls.Add(this.txtTotalCost);
            this.metroPanel2.Controls.Add(this.metroLabel6);
            this.metroPanel2.Controls.Add(this.txtToDate);
            this.metroPanel2.Controls.Add(this.metroLabel4);
            this.metroPanel2.Controls.Add(this.txtFromDate);
            this.metroPanel2.Controls.Add(this.metroLabel3);
            this.metroPanel2.Controls.Add(this.txtVehicle);
            this.metroPanel2.Controls.Add(this.metroLabel2);
            this.metroPanel2.Controls.Add(this.txtVehicleGroup);
            this.metroPanel2.Controls.Add(this.metroLabel1);
            this.metroPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel2.HorizontalScrollbarBarColor = true;
            this.metroPanel2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel2.HorizontalScrollbarSize = 10;
            this.metroPanel2.Location = new System.Drawing.Point(0, 92);
            this.metroPanel2.Name = "metroPanel2";
            this.metroPanel2.Size = new System.Drawing.Size(1065, 520);
            this.metroPanel2.TabIndex = 7;
            this.metroPanel2.VerticalScrollbarBarColor = true;
            this.metroPanel2.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel2.VerticalScrollbarSize = 10;
            // 
            // txtIntiationFee
            // 
            this.txtIntiationFee.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtIntiationFee.FontWeight = MetroFramework.MetroTextBoxWeight.Bold;
            this.txtIntiationFee.Lines = new string[0];
            this.txtIntiationFee.Location = new System.Drawing.Point(339, 333);
            this.txtIntiationFee.Margin = new System.Windows.Forms.Padding(4);
            this.txtIntiationFee.MaxLength = 32767;
            this.txtIntiationFee.Name = "txtIntiationFee";
            this.txtIntiationFee.PasswordChar = '\0';
            this.txtIntiationFee.ReadOnly = true;
            this.txtIntiationFee.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtIntiationFee.SelectedText = "";
            this.txtIntiationFee.Size = new System.Drawing.Size(525, 54);
            this.txtIntiationFee.TabIndex = 46;
            this.txtIntiationFee.UseSelectable = true;
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel7.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel7.Location = new System.Drawing.Point(181, 349);
            this.metroLabel7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(123, 25);
            this.metroLabel7.TabIndex = 45;
            this.metroLabel7.Text = "Booking Fee";
            this.metroLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel5.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel5.Location = new System.Drawing.Point(115, 402);
            this.metroLabel5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(165, 25);
            this.metroLabel5.TabIndex = 44;
            this.metroLabel5.Text = "Payment Method";
            this.metroLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // metroPanel4
            // 
            this.metroPanel4.Controls.Add(this.cmbPayMethod);
            this.metroPanel4.HorizontalScrollbarBarColor = true;
            this.metroPanel4.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel4.HorizontalScrollbarSize = 10;
            this.metroPanel4.Location = new System.Drawing.Point(339, 394);
            this.metroPanel4.Name = "metroPanel4";
            this.metroPanel4.Size = new System.Drawing.Size(525, 54);
            this.metroPanel4.TabIndex = 43;
            this.metroPanel4.VerticalScrollbarBarColor = true;
            this.metroPanel4.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel4.VerticalScrollbarSize = 10;
            // 
            // cmbPayMethod
            // 
            this.cmbPayMethod.BackColor = System.Drawing.SystemColors.Control;
            this.cmbPayMethod.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbPayMethod.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbPayMethod.FontSize = MetroFramework.MetroComboBoxSize.Tall;
            this.cmbPayMethod.ItemHeight = 29;
            this.cmbPayMethod.Items.AddRange(new object[] {
            "[Add New Card...]"});
            this.cmbPayMethod.Location = new System.Drawing.Point(0, 0);
            this.cmbPayMethod.Margin = new System.Windows.Forms.Padding(4);
            this.cmbPayMethod.Name = "cmbPayMethod";
            this.cmbPayMethod.Size = new System.Drawing.Size(525, 35);
            this.cmbPayMethod.TabIndex = 3;
            this.cmbPayMethod.UseCustomBackColor = true;
            this.cmbPayMethod.UseCustomForeColor = true;
            this.cmbPayMethod.UseSelectable = true;
            // 
            // txtTotalCost
            // 
            this.txtTotalCost.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtTotalCost.FontWeight = MetroFramework.MetroTextBoxWeight.Bold;
            this.txtTotalCost.Lines = new string[0];
            this.txtTotalCost.Location = new System.Drawing.Point(339, 273);
            this.txtTotalCost.Margin = new System.Windows.Forms.Padding(4);
            this.txtTotalCost.MaxLength = 32767;
            this.txtTotalCost.Name = "txtTotalCost";
            this.txtTotalCost.PasswordChar = '\0';
            this.txtTotalCost.ReadOnly = true;
            this.txtTotalCost.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtTotalCost.SelectedText = "";
            this.txtTotalCost.Size = new System.Drawing.Size(525, 54);
            this.txtTotalCost.TabIndex = 42;
            this.txtTotalCost.UseSelectable = true;
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel6.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel6.Location = new System.Drawing.Point(181, 289);
            this.metroLabel6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(99, 25);
            this.metroLabel6.TabIndex = 41;
            this.metroLabel6.Text = "Total Cost";
            this.metroLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtToDate
            // 
            this.txtToDate.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtToDate.FontWeight = MetroFramework.MetroTextBoxWeight.Bold;
            this.txtToDate.Lines = new string[0];
            this.txtToDate.Location = new System.Drawing.Point(339, 211);
            this.txtToDate.Margin = new System.Windows.Forms.Padding(4);
            this.txtToDate.MaxLength = 32767;
            this.txtToDate.Name = "txtToDate";
            this.txtToDate.PasswordChar = '\0';
            this.txtToDate.ReadOnly = true;
            this.txtToDate.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtToDate.SelectedText = "";
            this.txtToDate.Size = new System.Drawing.Size(525, 54);
            this.txtToDate.TabIndex = 38;
            this.txtToDate.UseSelectable = true;
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel4.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel4.Location = new System.Drawing.Point(201, 227);
            this.metroLabel4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(79, 25);
            this.metroLabel4.TabIndex = 37;
            this.metroLabel4.Text = "To Date";
            this.metroLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFromDate
            // 
            this.txtFromDate.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtFromDate.FontWeight = MetroFramework.MetroTextBoxWeight.Bold;
            this.txtFromDate.Lines = new string[0];
            this.txtFromDate.Location = new System.Drawing.Point(339, 149);
            this.txtFromDate.Margin = new System.Windows.Forms.Padding(4);
            this.txtFromDate.MaxLength = 32767;
            this.txtFromDate.Name = "txtFromDate";
            this.txtFromDate.PasswordChar = '\0';
            this.txtFromDate.ReadOnly = true;
            this.txtFromDate.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtFromDate.SelectedText = "";
            this.txtFromDate.Size = new System.Drawing.Size(525, 54);
            this.txtFromDate.TabIndex = 36;
            this.txtFromDate.UseSelectable = true;
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel3.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel3.Location = new System.Drawing.Point(175, 165);
            this.metroLabel3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(105, 25);
            this.metroLabel3.TabIndex = 35;
            this.metroLabel3.Text = "From Date";
            this.metroLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtVehicle
            // 
            this.txtVehicle.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtVehicle.FontWeight = MetroFramework.MetroTextBoxWeight.Bold;
            this.txtVehicle.Lines = new string[0];
            this.txtVehicle.Location = new System.Drawing.Point(339, 87);
            this.txtVehicle.Margin = new System.Windows.Forms.Padding(4);
            this.txtVehicle.MaxLength = 32767;
            this.txtVehicle.Name = "txtVehicle";
            this.txtVehicle.PasswordChar = '\0';
            this.txtVehicle.ReadOnly = true;
            this.txtVehicle.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtVehicle.SelectedText = "";
            this.txtVehicle.Size = new System.Drawing.Size(525, 54);
            this.txtVehicle.TabIndex = 34;
            this.txtVehicle.UseSelectable = true;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel2.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel2.Location = new System.Drawing.Point(206, 103);
            this.metroLabel2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(74, 25);
            this.metroLabel2.TabIndex = 33;
            this.metroLabel2.Text = "Vehicle";
            this.metroLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtVehicleGroup
            // 
            this.txtVehicleGroup.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtVehicleGroup.FontWeight = MetroFramework.MetroTextBoxWeight.Bold;
            this.txtVehicleGroup.Lines = new string[0];
            this.txtVehicleGroup.Location = new System.Drawing.Point(339, 25);
            this.txtVehicleGroup.Margin = new System.Windows.Forms.Padding(4);
            this.txtVehicleGroup.MaxLength = 32767;
            this.txtVehicleGroup.Name = "txtVehicleGroup";
            this.txtVehicleGroup.PasswordChar = '\0';
            this.txtVehicleGroup.ReadOnly = true;
            this.txtVehicleGroup.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtVehicleGroup.SelectedText = "";
            this.txtVehicleGroup.Size = new System.Drawing.Size(525, 54);
            this.txtVehicleGroup.TabIndex = 32;
            this.txtVehicleGroup.UseSelectable = true;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel1.Location = new System.Drawing.Point(143, 41);
            this.metroLabel1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(137, 25);
            this.metroLabel1.TabIndex = 27;
            this.metroLabel1.Text = "Vehicle Group";
            this.metroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // RentalShowBookingQuoteControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.metroPanel2);
            this.Controls.Add(this.metroPanel3);
            this.Controls.Add(this.metroPanel1);
            this.Name = "RentalShowBookingQuoteControl";
            this.Size = new System.Drawing.Size(1065, 734);
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel3.ResumeLayout(false);
            this.metroPanel2.ResumeLayout(false);
            this.metroPanel2.PerformLayout();
            this.metroPanel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroLabel lblHeader;
        private MetroFramework.Controls.MetroPanel metroPanel3;
        private MetroFramework.Controls.MetroButton btnNext;
        private MetroFramework.Controls.MetroPanel metroPanel2;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox txtTotalCost;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroTextBox txtToDate;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroTextBox txtFromDate;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroTextBox txtVehicle;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox txtVehicleGroup;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroPanel metroPanel4;
        private MetroFramework.Controls.MetroComboBox cmbPayMethod;
        private MetroFramework.Controls.MetroButton btnBack;
        private MetroFramework.Controls.MetroTextBox txtIntiationFee;
        private MetroFramework.Controls.MetroLabel metroLabel7;
    }
}
