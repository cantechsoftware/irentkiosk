﻿using System;
using Castle.Core;
using iRent.Framework;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Services;
using iRent2.Contracts.Rental;
using iRentKiosk.Core.Services.Api;
using iRentKiosk.Core.Models;
using iRent2.Contracts.PayGate;
using System.Linq;

namespace iRent.Rental
{
    [CastleComponent]
    public class RentalCreateBookingCommand : BaseCommand
    {
        RentalApi _rentalApi;
        PayGateApi _payGateApi;

        public string Next { get; set; }
        public string OutputDataContext { get; set; }

        public RentalCreateBookingCommand(RentalApi rentalApi, PayGateApi payGateApi)
        {
            _rentalApi = rentalApi;
            _payGateApi = payGateApi;
        }

        public override void Invoke()
        {
            RentalBookingRequest request = new RentalBookingRequest();
            RentalOptions options = ContextData.RentalOptions;

            request.DriverID = ContextData.Driver.Id;
            request.FromDate = options.FromDate;
            request.ToDate = options.ToDate;
            request.VehicleID = options.SelectedVehicle.VehicleModelInfo.Id;
            request.RentalOptions = options.Options.Select(x => x.Id).ToArray();
            request.WaiverOptionID = options.SelectedWaiver?.Id ?? 0;
            request.PickupBranchID = ContextData.Kiosk.BranchId ?? 0;
            request.ReturnBranchID = ContextData.Kiosk.BranchId ?? 0;
            //NB! check
            RentalBookingResponse response = _rentalApi.RentalBooking(request);
            if(response == null)
            {
                //what aare we supposed to here ? nothing ?
            }
            options.BookingInfo = response;

            RegisterPaymentRequest paygatedata = new RegisterPaymentRequest();
            if (ContextData.DynamicData.ContainsKey("PayGateInit"))
            {
                paygatedata = ContextData.DynamicData["PayGateInit"];
            }
            paygatedata.Amount = options.QuoteData.Total;
            //NB! check
            paygatedata.BookingId = options.BookingInfo.BookingID;
            paygatedata.PayConfirmEmail = ContextData.Driver.Email;
            paygatedata.ReturnUrl = "";
            paygatedata.InvoiceId = options.BookingInfo.InvoiceID;
            ContextData.SetDynamicData(OutputDataContext, paygatedata);
            Proceed(Next);
        }
    }
}
