﻿namespace iRent.Rental
{
    partial class BookingQuoteControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {           
            this.btnNext = new MetroFramework.Controls.MetroButton();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.cmdBack = new MetroFramework.Controls.MetroButton();
            this.lblStatus = new MetroFramework.Controls.MetroLabel();
            this.lblIntroText = new MetroFramework.Controls.MetroLabel();
            this.pnlSummary = new MetroFramework.Controls.MetroPanel();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnNext
            // 
            this.btnNext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNext.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnNext.Location = new System.Drawing.Point(542, 583);
            this.btnNext.Margin = new System.Windows.Forms.Padding(4);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(263, 115);
            this.btnNext.TabIndex = 13;
            this.btnNext.Text = "&Continue";
            this.btnNext.UseSelectable = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 267F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.cmdBack, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblStatus, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblIntroText, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnNext, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.pnlSummary, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 123F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(809, 702);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // cmdBack
            // 
            this.cmdBack.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdBack.FontSize = MetroFramework.MetroButtonSize.XL;
            this.cmdBack.Location = new System.Drawing.Point(4, 583);
            this.cmdBack.Margin = new System.Windows.Forms.Padding(4);
            this.cmdBack.Name = "cmdBack";
            this.cmdBack.Size = new System.Drawing.Size(263, 115);
            this.cmdBack.TabIndex = 28;
            this.cmdBack.Text = "&Back";
            this.cmdBack.UseSelectable = true;
            this.cmdBack.Click += new System.EventHandler(this.cmdBack_Click);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.lblStatus, 3);
            this.lblStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblStatus.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblStatus.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblStatus.Location = new System.Drawing.Point(4, 49);
            this.lblStatus.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(801, 49);
            this.lblStatus.TabIndex = 27;
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblIntroText
            // 
            this.lblIntroText.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.lblIntroText, 3);
            this.lblIntroText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblIntroText.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblIntroText.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblIntroText.Location = new System.Drawing.Point(4, 0);
            this.lblIntroText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblIntroText.Name = "lblIntroText";
            this.lblIntroText.Size = new System.Drawing.Size(801, 49);
            this.lblIntroText.TabIndex = 26;
            this.lblIntroText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlSummary
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.pnlSummary, 3);
            this.pnlSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSummary.HorizontalScrollbarBarColor = true;
            this.pnlSummary.HorizontalScrollbarHighlightOnWheel = false;
            this.pnlSummary.HorizontalScrollbarSize = 10;
            this.pnlSummary.Location = new System.Drawing.Point(3, 101);
            this.pnlSummary.Name = "pnlSummary";
            this.pnlSummary.Size = new System.Drawing.Size(803, 475);
            this.pnlSummary.TabIndex = 29;
            this.pnlSummary.VerticalScrollbarBarColor = true;
            this.pnlSummary.VerticalScrollbarHighlightOnWheel = false;
            this.pnlSummary.VerticalScrollbarSize = 10;
            // 
            // BookingQuoteControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "BookingQuoteControl";
            this.Size = new System.Drawing.Size(809, 702);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private MetroFramework.Controls.MetroButton btnNext;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MetroFramework.Controls.MetroLabel lblStatus;
        private MetroFramework.Controls.MetroLabel lblIntroText;
        private MetroFramework.Controls.MetroButton cmdBack;
        private MetroFramework.Controls.MetroPanel pnlSummary;
    }
}
