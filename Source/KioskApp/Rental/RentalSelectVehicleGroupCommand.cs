﻿using System;
using System.Collections.Generic;
using Castle.Core;
using iRent.Framework;
using System.Windows.Forms;
using iRent2.Contracts.Models;
using iRent2.Contracts.PayGate;
using iRentKiosk.Core.Models;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;
using iRent2.Contracts.Rental;

namespace iRent.Rental
{
    [CastleComponent]
    public class RentalSelectVehicleGroupCommand : BaseCommand
    {
        public string Next { get; set; }
        public string Back { get; set; }

        public string OutputDataContext { get; set; }

        private Func<RentalSelectVehicleGroupControl> _factory;
        private RentalApi _rentalApi;


        public RentalSelectVehicleGroupCommand(Func<RentalSelectVehicleGroupControl> factory, RentalApi RentalApi)
        {
            _rentalApi = RentalApi;
            _factory = factory;
        }

        public override void Invoke()
        {
            var control = _factory();
            control.Dock = DockStyle.Fill;
            control.Next += OnNext;
            control.Back += OnBack;
            var main = GetFormAs<MainForm>();
            main.AddToFormPanel(control);

            RentalSearchRequest request = new RentalSearchRequest();
            KioskModel _kiosk = ContextData.Kiosk;
            RentalOptions options = ContextData.RentalOptions;
            
            for (int i = 0; i < options.VehicleGroups.Length; i++)
            {
                request.VehicleClassList = new int[1] { options.VehicleGroups[i].VehicleGroupID };
                request.BranchesList = new int[1] { _kiosk.BranchId ?? 0 } ;
                request.FromDate = options.FromDate;
                request.ToDate = options.ToDate;
                RentalSearchResponse[] response = _rentalApi.RentalSearch(request);
                if(response.Length > 0)
                {
                    options.VehicleGroups[i].IsDeleted = false;
                }
                else
                {
                    options.VehicleGroups[i].IsDeleted = true;
                }
            }
            control.Bind(options.VehicleGroups);
        }

        private void OnBack(object sender, RentalSearchRequest request)
        {
            Proceed(Back);
        }

        private void OnNext(object sender, RentalSearchRequest request)
        {
            //Create web data object for Payment pages.
            RentalSearchResponse[] response = _rentalApi.RentalSearch(request);
            RentalOptions options = ContextData.RentalOptions;
            response[0].FromDate = request.FromDate;
            response[0].ToDate = request.ToDate;
            options.Vehicles = response;
            ContextData.RentalOptions.SelectedVehicle = response[0];
            Proceed(Next);
        }

    }
}
