﻿namespace iRent.Rental
{
    partial class RentalSummaryControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlSummary = new MetroFramework.Controls.MetroPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.txtKilometres = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.txtReturnBranch = new MetroFramework.Controls.MetroTextBox();
            this.txtReturnDateTime = new MetroFramework.Controls.MetroTextBox();
            this.txtWaiver = new MetroFramework.Controls.MetroTextBox();
            this.txtDrivers = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.lblIntroText = new MetroFramework.Controls.MetroLabel();
            this.lblStatus = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.txtVehicleGroup = new MetroFramework.Controls.MetroTextBox();
            this.btnNext = new MetroFramework.Controls.MetroButton();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pnlSummary.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlSummary
            // 
            this.pnlSummary.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel1.SetColumnSpan(this.pnlSummary, 3);
            this.pnlSummary.Controls.Add(this.tableLayoutPanel2);
            this.pnlSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSummary.HorizontalScrollbarBarColor = true;
            this.pnlSummary.HorizontalScrollbarHighlightOnWheel = false;
            this.pnlSummary.HorizontalScrollbarSize = 12;
            this.pnlSummary.Location = new System.Drawing.Point(4, 61);
            this.pnlSummary.Margin = new System.Windows.Forms.Padding(4);
            this.pnlSummary.Name = "pnlSummary";
            this.pnlSummary.Size = new System.Drawing.Size(801, 513);
            this.pnlSummary.TabIndex = 11;
            this.pnlSummary.VerticalScrollbarBarColor = true;
            this.pnlSummary.VerticalScrollbarHighlightOnWheel = false;
            this.pnlSummary.VerticalScrollbarSize = 13;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.66667F));
            this.tableLayoutPanel2.Controls.Add(this.txtKilometres, 1, 7);
            this.tableLayoutPanel2.Controls.Add(this.metroLabel6, 0, 7);
            this.tableLayoutPanel2.Controls.Add(this.txtReturnBranch, 1, 6);
            this.tableLayoutPanel2.Controls.Add(this.txtReturnDateTime, 1, 5);
            this.tableLayoutPanel2.Controls.Add(this.txtWaiver, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.txtDrivers, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.metroLabel5, 0, 6);
            this.tableLayoutPanel2.Controls.Add(this.metroLabel4, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.metroLabel3, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.metroLabel2, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.lblIntroText, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblStatus, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.metroLabel1, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.txtVehicleGroup, 1, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 9;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(799, 511);
            this.tableLayoutPanel2.TabIndex = 14;
            // 
            // txtKilometres
            // 
            this.txtKilometres.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtKilometres.FontSize = MetroFramework.MetroTextBoxSize.XL;
            this.txtKilometres.FontWeight = MetroFramework.MetroTextBoxWeight.Bold;
            this.txtKilometres.Lines = new string[0];
            this.txtKilometres.Location = new System.Drawing.Point(270, 438);
            this.txtKilometres.Margin = new System.Windows.Forms.Padding(4);
            this.txtKilometres.MaxLength = 32767;
            this.txtKilometres.Name = "txtKilometres";
            this.txtKilometres.PasswordChar = '\0';
            this.txtKilometres.ReadOnly = true;
            this.txtKilometres.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtKilometres.SelectedText = "";
            this.txtKilometres.Size = new System.Drawing.Size(525, 44);
            this.txtKilometres.TabIndex = 37;
            this.txtKilometres.UseSelectable = true;
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel6.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel6.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel6.Location = new System.Drawing.Point(4, 434);
            this.metroLabel6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(258, 52);
            this.metroLabel6.TabIndex = 36;
            this.metroLabel6.Text = "Kilometres";
            this.metroLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtReturnBranch
            // 
            this.txtReturnBranch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtReturnBranch.FontSize = MetroFramework.MetroTextBoxSize.XL;
            this.txtReturnBranch.FontWeight = MetroFramework.MetroTextBoxWeight.Bold;
            this.txtReturnBranch.Lines = new string[0];
            this.txtReturnBranch.Location = new System.Drawing.Point(270, 376);
            this.txtReturnBranch.Margin = new System.Windows.Forms.Padding(4);
            this.txtReturnBranch.MaxLength = 32767;
            this.txtReturnBranch.Name = "txtReturnBranch";
            this.txtReturnBranch.PasswordChar = '\0';
            this.txtReturnBranch.ReadOnly = true;
            this.txtReturnBranch.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtReturnBranch.SelectedText = "";
            this.txtReturnBranch.Size = new System.Drawing.Size(525, 54);
            this.txtReturnBranch.TabIndex = 35;
            this.txtReturnBranch.UseSelectable = true;
            // 
            // txtReturnDateTime
            // 
            this.txtReturnDateTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtReturnDateTime.FontSize = MetroFramework.MetroTextBoxSize.XL;
            this.txtReturnDateTime.FontWeight = MetroFramework.MetroTextBoxWeight.Bold;
            this.txtReturnDateTime.Lines = new string[0];
            this.txtReturnDateTime.Location = new System.Drawing.Point(270, 314);
            this.txtReturnDateTime.Margin = new System.Windows.Forms.Padding(4);
            this.txtReturnDateTime.MaxLength = 32767;
            this.txtReturnDateTime.Name = "txtReturnDateTime";
            this.txtReturnDateTime.PasswordChar = '\0';
            this.txtReturnDateTime.ReadOnly = true;
            this.txtReturnDateTime.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtReturnDateTime.SelectedText = "";
            this.txtReturnDateTime.Size = new System.Drawing.Size(525, 54);
            this.txtReturnDateTime.TabIndex = 34;
            this.txtReturnDateTime.UseSelectable = true;
            // 
            // txtWaiver
            // 
            this.txtWaiver.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtWaiver.FontSize = MetroFramework.MetroTextBoxSize.XL;
            this.txtWaiver.FontWeight = MetroFramework.MetroTextBoxWeight.Bold;
            this.txtWaiver.Lines = new string[0];
            this.txtWaiver.Location = new System.Drawing.Point(270, 252);
            this.txtWaiver.Margin = new System.Windows.Forms.Padding(4);
            this.txtWaiver.MaxLength = 32767;
            this.txtWaiver.Name = "txtWaiver";
            this.txtWaiver.PasswordChar = '\0';
            this.txtWaiver.ReadOnly = true;
            this.txtWaiver.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtWaiver.SelectedText = "";
            this.txtWaiver.Size = new System.Drawing.Size(525, 54);
            this.txtWaiver.TabIndex = 33;
            this.txtWaiver.UseSelectable = true;
            // 
            // txtDrivers
            // 
            this.txtDrivers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDrivers.FontSize = MetroFramework.MetroTextBoxSize.XL;
            this.txtDrivers.FontWeight = MetroFramework.MetroTextBoxWeight.Bold;
            this.txtDrivers.Lines = new string[0];
            this.txtDrivers.Location = new System.Drawing.Point(270, 190);
            this.txtDrivers.Margin = new System.Windows.Forms.Padding(4);
            this.txtDrivers.MaxLength = 32767;
            this.txtDrivers.Name = "txtDrivers";
            this.txtDrivers.PasswordChar = '\0';
            this.txtDrivers.ReadOnly = true;
            this.txtDrivers.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtDrivers.SelectedText = "";
            this.txtDrivers.Size = new System.Drawing.Size(525, 54);
            this.txtDrivers.TabIndex = 32;
            this.txtDrivers.UseSelectable = true;
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel5.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel5.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel5.Location = new System.Drawing.Point(4, 372);
            this.metroLabel5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(258, 62);
            this.metroLabel5.TabIndex = 30;
            this.metroLabel5.Text = "Return Branch";
            this.metroLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel4.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel4.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel4.Location = new System.Drawing.Point(4, 310);
            this.metroLabel4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(258, 62);
            this.metroLabel4.TabIndex = 29;
            this.metroLabel4.Text = "Return Date && Time";
            this.metroLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel3.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel3.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel3.ForeColor = System.Drawing.Color.Black;
            this.metroLabel3.Location = new System.Drawing.Point(4, 248);
            this.metroLabel3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(258, 62);
            this.metroLabel3.TabIndex = 28;
            this.metroLabel3.Text = "Waiver Option";
            this.metroLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel2.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel2.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel2.Location = new System.Drawing.Point(4, 186);
            this.metroLabel2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(258, 62);
            this.metroLabel2.TabIndex = 27;
            this.metroLabel2.Text = "Additional Drivers";
            this.metroLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblIntroText
            // 
            this.lblIntroText.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.lblIntroText, 2);
            this.lblIntroText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblIntroText.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblIntroText.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblIntroText.Location = new System.Drawing.Point(4, 0);
            this.lblIntroText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblIntroText.Name = "lblIntroText";
            this.lblIntroText.Size = new System.Drawing.Size(791, 62);
            this.lblIntroText.TabIndex = 25;
            this.lblIntroText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.lblStatus, 2);
            this.lblStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblStatus.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblStatus.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblStatus.Location = new System.Drawing.Point(4, 62);
            this.lblStatus.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(791, 62);
            this.lblStatus.TabIndex = 24;
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel1.Location = new System.Drawing.Point(4, 124);
            this.metroLabel1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(258, 62);
            this.metroLabel1.TabIndex = 26;
            this.metroLabel1.Text = "Reg No";
            this.metroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtVehicleGroup
            // 
            this.txtVehicleGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVehicleGroup.FontSize = MetroFramework.MetroTextBoxSize.XL;
            this.txtVehicleGroup.FontWeight = MetroFramework.MetroTextBoxWeight.Bold;
            this.txtVehicleGroup.Lines = new string[0];
            this.txtVehicleGroup.Location = new System.Drawing.Point(270, 128);
            this.txtVehicleGroup.Margin = new System.Windows.Forms.Padding(4);
            this.txtVehicleGroup.MaxLength = 32767;
            this.txtVehicleGroup.Name = "txtVehicleGroup";
            this.txtVehicleGroup.PasswordChar = '\0';
            this.txtVehicleGroup.ReadOnly = true;
            this.txtVehicleGroup.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtVehicleGroup.SelectedText = "";
            this.txtVehicleGroup.Size = new System.Drawing.Size(525, 54);
            this.txtVehicleGroup.TabIndex = 31;
            this.txtVehicleGroup.UseSelectable = true;
            // 
            // btnNext
            // 
            this.btnNext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNext.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnNext.Location = new System.Drawing.Point(275, 582);
            this.btnNext.Margin = new System.Windows.Forms.Padding(4);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(259, 116);
            this.btnNext.TabIndex = 13;
            this.btnNext.Text = "&Continue";
            this.btnNext.UseSelectable = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 267F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.pnlSummary, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnNext, 1, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 123F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(809, 702);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // RentalSummaryControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "RentalSummaryControl";
            this.Size = new System.Drawing.Size(809, 702);
            this.pnlSummary.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroButton btnNext;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private MetroFramework.Controls.MetroLabel lblIntroText;
        private MetroFramework.Controls.MetroLabel lblStatus;
        private MetroFramework.Controls.MetroTextBox txtReturnBranch;
        private MetroFramework.Controls.MetroTextBox txtReturnDateTime;
        private MetroFramework.Controls.MetroTextBox txtWaiver;
        private MetroFramework.Controls.MetroTextBox txtDrivers;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox txtVehicleGroup;
        private MetroFramework.Controls.MetroPanel pnlSummary;
        private MetroFramework.Controls.MetroTextBox txtKilometres;
        private MetroFramework.Controls.MetroLabel metroLabel6;
    }
}
