﻿using System;
using System.Windows.Forms;
using Castle.Core;

namespace iRent.Rental
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class ConfirmPaymentControl : UserControl
    {
        public EventHandler<object> Next;

        public ConfirmPaymentControl()
        {
            InitializeComponent();
        }

        delegate void SetTextDelegate(string data, bool payed);

        public void Bind(string Data, bool Payed)
        {
            if (this.InvokeRequired)
            {
                SetTextDelegate c = new SetTextDelegate(Bind);
                this.Invoke(c, new object[] { Data, Payed });
            }
            else
            {
                lblStatus.Text = Data;
            }
            if(Payed)
            {
                btnNext.Enabled = true;
            }
            else
            {
                btnNext.Enabled = false;
                lblStatus.Text = lblStatus.Text + " Please click on Home and retry your rental.";
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            Next(this, null);
        }
    }
}
