﻿using System;
using System.Windows.Forms;
using MetroFramework;
using MetroFramework.Controls;
using iRent2.Contracts.Models;
using iRent2.Contracts.Rental;
using Castle.Core;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;
using System.Linq;
using System.Drawing;
using System.Threading;


namespace iRent.Rental
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class RentalStartControl : MetroUserControl
    {

        enum Months
        {
            January, February, March, April, May, June, July, August, September, October, November, December
        }


        public RentalStartControl()
        {
            InitializeComponent();
        }

        private DateTime _fromDate = DateTime.Now;
        private DateTime _toDate = DateTime.Now;
        private KioskModel _kiosk;

        public void InitControls()
        {
            //pnlStart.Dock = DockStyle.Fill;
            tableLayoutPanelMain.Dock = DockStyle.Fill;
            _fromDate = DateTime.Now.AddMinutes(5);
            _toDate = DateTime.Now.AddMinutes(65);
            _toDate = RoundUp(_toDate, TimeSpan.FromMinutes(5));
            _fromDate = RoundUp(_fromDate, TimeSpan.FromMinutes(5));
            //pnlTimeSelect.BringToFront();
            pnlStart.Visible = false;
            UpdateControls();
            UpdateMonthDays();
            SetRentalTimes();
        }

        private DateTime RoundUp(DateTime dt, TimeSpan d)
        {
            return new DateTime((dt.Ticks + d.Ticks - 1) / d.Ticks * d.Ticks, dt.Kind);
        }

        private void SetRentalTimes()
        {
            cmbFromHour.Items.Clear();
            cmbToHour.Items.Clear();
            cmbFromHour.Items.AddRange(Enumerable.Range(1, 23).Select(i => (object)i).ToArray());
            cmbToHour.Items.AddRange(Enumerable.Range(1, 23).Select(i => (object)i).ToArray());
            cmbFromMins.Items.AddRange(Enumerable.Repeat(0, ((55 - 0) / 5) + 1).Select((tr, ti) => tr + (5 * ti)).Select(i => (object)i).ToArray());
            cmbToMins.Items.AddRange(Enumerable.Repeat(0, ((55 - 0) / 5) + 1).Select((tr, ti) => tr + (5 * ti)).Select(i => (object)i).ToArray());

            cmbToDay.Items.AddRange(Enumerable.Range(1, 23).Select(i => (object)i).ToArray());

            int fromhour = _fromDate.Hour;
            int tohour = _toDate.Hour;
            int frommins = RoundUp(_fromDate, TimeSpan.FromMinutes(5)).Minute;
            int tomins = RoundUp(_toDate, TimeSpan.FromMinutes(5)).Minute;

            cmbFromHour.SelectedIndex = fromhour - 1;
            cmbToHour.SelectedIndex = tohour - 1;

            frommins = cmbFromMins.Items.IndexOf(frommins);
            tomins = cmbToMins.Items.IndexOf(tomins);

            cmbToMins.SelectedIndex = tomins;
            cmbFromMins.SelectedIndex = frommins;
        }

        private void SetRentalMonths()
        {
            cmbFromMonth.Items.Clear();
            cmbToMonth.Items.Clear();

            foreach (var item in Enum.GetValues(typeof(Months)))
            {
                cmbFromMonth.Items.Add(item);
                cmbToMonth.Items.Add(item);
            }

            int frommonth = _fromDate.Month;
            int tomonth = _toDate.Month;
            cmbFromMonth.SelectedIndex = frommonth - 1;
            cmbToMonth.SelectedIndex = tomonth - 1;
        }

        private void SetRentalYears()
        {
            int fromyear = _fromDate.Year;
            int[] toyears = Enumerable.Range(fromyear, 5).ToArray();
            int[] fromyears = Enumerable.Range(fromyear, 5).ToArray();
            int fromcuryear = _fromDate.Year;
            int tocuryear = _toDate.Year;
            cmbFromYear.Items.Clear();
            cmbFromYear.Items.AddRange(Enumerable.Range(fromyear, 5).Select(i => (object)i).ToArray());
            cmbToYear.Items.Clear();
            cmbToYear.Items.AddRange(Enumerable.Range(fromyear, 5).Select(i => (object)i).ToArray());

            if (fromyears.Contains(fromcuryear))
            {
                cmbFromYear.SelectedItem = fromcuryear;
            }
            else
            {
                cmbFromYear.SelectedIndex = 0;
            }

            if (toyears.Contains(tocuryear))
            {
                cmbToYear.SelectedItem = tocuryear;
            }
            else
            {
                cmbToYear.SelectedIndex = 0;
            }
        }

        private void UpdateMonthDays()
        {
            //int fromyear = _fromDate.Year;
            //int frommonth = _fromDate.Month;
            //int[] fromdays = Enumerable.Range(1, DateTime.DaysInMonth(fromyear, frommonth)).ToArray();
            //int fromcurday = _fromDate.Day;
            //cmbFromDay.Items.Clear();
            //cmbFromDay.Items.AddRange(Enumerable.Range(1, DateTime.DaysInMonth(fromyear, frommonth)).Select(i => (object)i).ToArray());
            //if (fromdays.Contains(fromcurday))
            //{
            //    cmbFromDay.SelectedItem = fromcurday;
            //}
            //else
            //{
            //    cmbFromDay.SelectedIndex = 0;
            //}

            int toyear = _toDate.Year;
            int tomonth = _toDate.Month;
            int[] todays = Enumerable.Range(1, DateTime.DaysInMonth(toyear, tomonth)).ToArray();
            int tocurday = _toDate.Day;
            cmbToDay.Items.Clear();
            cmbToDay.Items.AddRange(Enumerable.Range(1, DateTime.DaysInMonth(toyear, tomonth)).Select(i => (object)i).ToArray());
            if (todays.Contains(tocurday))
            {
                cmbToDay.SelectedItem = tocurday;
            }
            else
            {
                cmbToDay.SelectedIndex = 0;
            }

        }

        public void UpdateControls()
        {
            lblFromDay.Text = _fromDate.Day.ToString("00");
            lblFromDayName.Text = _fromDate.DayOfWeek.ToString();
            lblFromMonth.Text = _fromDate.Month.ToString("00");
            lblFromMonthName.Text = _fromDate.ToString("MMMM");
            lblFromYear.Text = _fromDate.Year.ToString("0000");
            lblFromTime.Text = _fromDate.ToString("HH:mm");

            lblToDay.Text = _toDate.Day.ToString("00");
            lblToDayName.Text = _toDate.DayOfWeek.ToString();
            lblToMonth.Text = _toDate.Month.ToString("00");
            lblToMonthName.Text = _toDate.ToString("MMMM");
            lblToYear.Text = _toDate.Year.ToString("0000");
            lblToTime.Text = _toDate.ToString("HH:mm");
            SetNextReady();
        }

        public event EventHandler<RentalVehicleGroupRequest> Next;
        public event EventHandler<RentalVehicleGroupRequest> Back;

        public void Bind(KioskModel model)
        {
            _kiosk = model;


            InitControls();
        }

        private void tableLayoutPanel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btn1Day_Click(object sender, EventArgs e)
        {
            _fromDate = DateTime.Now;
            //_toDate = DateTime.Now.AddHours(1);
            _fromDate = RoundUp(_fromDate, TimeSpan.FromMinutes(5));
            
            btnToTimeUp_Click(sender, e);
            _toDate = RoundUp(_toDate, TimeSpan.FromMinutes(5));

            
            UpdateControls();
            UpdateMonthDays();
            SetRentalYears();
            SetRentalMonths();
            //pnlTimeSelect.BringToFront();
            btnNext_Click(sender, e);
        }

        private void btnHalfDay_Click(object sender, EventArgs e)
        {
            _fromDate = DateTime.Now;
            //_toDate = DateTime.Now.AddHours(1);
            _fromDate = RoundUp(_fromDate, TimeSpan.FromMinutes(5));

            btnToTimeUp_Click(sender, e);
            Thread.Sleep(500);
            btnToTimeUp_Click(sender, e);
            _toDate = RoundUp(_toDate, TimeSpan.FromMinutes(5));

            
            UpdateControls();
            UpdateMonthDays();
            SetRentalYears();
            SetRentalMonths();
            //pnlTimeSelect.BringToFront();
            btnNext_Click(sender, e);
        }

        private void btnMoreTime_Click(object sender, EventArgs e)
        {
            _fromDate = DateTime.Now;
            _toDate = DateTime.Now.AddDays(1);

            btnToDayUp_Click(sender, e);
            //pnlTimeSelect.BringToFront();
            UpdateControls();
            UpdateMonthDays();
            SetRentalYears();
            SetRentalMonths();
        }

        private void btnFromDayUp_Click(object sender, EventArgs e)
        {
            if (cmbFromDay.SelectedIndex + 1 <= cmbFromDay.Items.Count - 1)
            {
                cmbFromDay.SelectedIndex = cmbFromDay.SelectedIndex + 1;
                _fromDate = _fromDate.AddDays(1);
            }
            UpdateControls();
        }

        private void btnFromDayDown_Click(object sender, EventArgs e)
        {
            if (cmbFromDay.SelectedIndex - 1 >= 0)
            {
                cmbFromDay.SelectedIndex = cmbFromDay.SelectedIndex - 1;
                _fromDate = _fromDate.AddDays(-1);
            }
            UpdateControls();
        }

        private void btnFromMonthUp_Click(object sender, EventArgs e)
        {
            if (cmbFromMonth.SelectedIndex + 1 <= cmbFromMonth.Items.Count - 1)
            {
                cmbFromMonth.SelectedIndex = cmbFromMonth.SelectedIndex + 1;
                _fromDate = _fromDate.AddMonths(1);
            }
            UpdateControls();
        }

        private void btnFromMonthDown_Click(object sender, EventArgs e)
        {
            if (cmbFromMonth.SelectedIndex - 1 >= 0)
            {
                cmbFromMonth.SelectedIndex = cmbFromMonth.SelectedIndex - 1;
                _fromDate = _fromDate.AddMonths(-1);
            }
            UpdateControls();
        }

        private void btnFromYearUp_Click(object sender, EventArgs e)
        {
            if (cmbFromYear.SelectedIndex + 1 <= cmbFromYear.Items.Count - 1)
            {
                cmbFromYear.SelectedIndex = cmbFromYear.SelectedIndex + 1;
                _fromDate = _fromDate.AddYears(1);

            }
            UpdateControls();
        }

        private void btnFromYearDown_Click(object sender, EventArgs e)
        {
            if (cmbFromYear.SelectedIndex - 1 >= 0)
            {
                cmbFromYear.SelectedIndex = cmbFromYear.SelectedIndex - 1;
                _fromDate = _fromDate.AddYears(-1);
            }
            UpdateControls();
        }

        private void btnFromTimeUp_Click(object sender, EventArgs e)
        {
            if (cmbFromHour.SelectedIndex + 1 <= cmbFromHour.Items.Count - 1)
            {
                cmbFromHour.SelectedIndex = cmbFromHour.SelectedIndex + 1;
                _fromDate = _fromDate.AddHours(1);
            }
            UpdateControls();
        }

        private void btnFromTimeDown_Click(object sender, EventArgs e)
        {
            if (cmbFromHour.SelectedIndex - 1 >= 0)
            {
                cmbFromHour.SelectedIndex = cmbFromHour.SelectedIndex - 1;
                _fromDate = _fromDate.AddHours(-1);
            }
            UpdateControls();
        }

        private void btnToDayUp_Click(object sender, EventArgs e)
        {
            if (cmbToDay.SelectedIndex + 1 <= cmbToDay.Items.Count - 1)
            {
                cmbToDay.SelectedIndex = cmbToDay.SelectedIndex + 1;
                _toDate = _toDate.AddDays(1);
            }
            UpdateControls();
        }

        private void btnToDayDown_Click(object sender, EventArgs e)
        {
            if (cmbToDay.SelectedIndex - 1 >= 0)
            {
                cmbToDay.SelectedIndex = cmbToDay.SelectedIndex - 1;
                _toDate = _toDate.AddDays(-1);
            }
            UpdateControls();
        }

        private void btnToMonthUp_Click(object sender, EventArgs e)
        {
            if (cmbToMonth.SelectedIndex + 1 <= cmbToMonth.Items.Count - 1)
            {
                cmbToMonth.SelectedIndex = cmbToMonth.SelectedIndex + 1;
                _toDate = _toDate.AddMonths(1);
            }
            UpdateControls();
        }

        private void btnToMonthDown_Click(object sender, EventArgs e)
        {
            if (cmbToMonth.SelectedIndex - 1 >= 0)
            {
                cmbToMonth.SelectedIndex = cmbToMonth.SelectedIndex - 1;
                _toDate = _toDate.AddMonths(-1);
            }
            UpdateControls();
        }

        private void btnToYearUp_Click(object sender, EventArgs e)
        {
            if (cmbToYear.SelectedIndex + 1 <= cmbToYear.Items.Count - 1)
            {
                cmbToYear.SelectedIndex = cmbToYear.SelectedIndex + 1;
                _toDate = _toDate.AddYears(1);
            }
            UpdateControls();
        }

        private void btnToYearDown_Click(object sender, EventArgs e)
        {
            if (cmbToYear.SelectedIndex - 1 >= 0)
            {
                cmbToYear.SelectedIndex = cmbToYear.SelectedIndex - 1;
                _toDate = _toDate.AddYears(-1);
            }
            UpdateControls();
        }

        private void btnToTimeUp_Click(object sender, EventArgs e)
        {
            if (cmbToHour.SelectedIndex + 1 <= cmbToHour.Items.Count - 1)
            {
                cmbToHour.SelectedIndex = cmbToHour.SelectedIndex + 1;
                _toDate = _toDate.AddHours(1);
            }
            UpdateControls();
        }

        private void btnToTimeDown_Click(object sender, EventArgs e)
        {
            if (cmbToHour.SelectedIndex - 1 >= 0)
            {
                cmbToHour.SelectedIndex = cmbToHour.SelectedIndex - 1;
                _toDate = _toDate.AddHours(-1);
            }
            UpdateControls();
        }

        private void btnFromMinUp_Click(object sender, EventArgs e)
        {
            if (cmbFromMins.SelectedIndex + 1 <= cmbFromMins.Items.Count - 1)
            {
                cmbFromMins.SelectedIndex = cmbFromMins.SelectedIndex + 1;
                _fromDate = _fromDate.AddMinutes(5);
            }
            UpdateControls();
        }

        private void btnFromMinDown_Click(object sender, EventArgs e)
        {
            if (cmbFromMins.SelectedIndex - 1 >= 0)
            {
                cmbFromMins.SelectedIndex = cmbFromMins.SelectedIndex - 1;
                _fromDate = _fromDate.AddMinutes(-5);
            }
            UpdateControls();
        }

        private void btnToMinUp_Click(object sender, EventArgs e)
        {
            if (cmbToMins.SelectedIndex + 1 <= cmbToMins.Items.Count - 1)
            {
                cmbToMins.SelectedIndex = cmbToMins.SelectedIndex + 1;
                _toDate = _toDate.AddMinutes(5);
            }
            UpdateControls();
        }

        private void btnToMinDown_Click(object sender, EventArgs e)
        {
            if (cmbToMins.SelectedIndex - 1 >= 0)
            {
                cmbToMins.SelectedIndex = cmbToMins.SelectedIndex - 1;
                _toDate = _toDate.AddMinutes(-5);
            }
            UpdateControls();
        }

        private void SetNextReady()
        {
            lblReturnDate.Text = "Return Date: "+_toDate.ToString("yyyy MMMM dd HH:mm");
            double hours = (_toDate - _fromDate).TotalHours;
            if (hours >= 1)
            {
                btnNext.Visible = true;
            }
            else
            {
                btnNext.Visible = false;
                //lblWarning.Text = "* One (1) hour rental minimum.";
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            //Create request object
            RentalVehicleGroupRequest request = new RentalVehicleGroupRequest();
            request.FromDate = _fromDate;
            request.ToDate = _toDate;
            request.Branches = new int[1] { _kiosk.BranchId ?? 0 };
            Next(this, request);
            //Send to api go to next workflow section
        }

        private void cmbFromDay_SelectedIndexChanged(object sender, EventArgs e)
        {
            int day = Convert.ToInt32(cmbFromDay.Items[cmbFromDay.SelectedIndex]);
            DateTime fromDate = new DateTime(_fromDate.Year, _fromDate.Month, day, _fromDate.Hour, _fromDate.Minute, 0);
            _fromDate = fromDate;
            UpdateControls();
        }

        private void cmbFromMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Months mnth;
            Enum.TryParse(cmbFromMonth.Items[cmbFromMonth.SelectedIndex].ToString(), out Months mnth);
            int month = Convert.ToInt32(mnth) + 1;
            if (DateTime.DaysInMonth(_fromDate.Year, month) >= _fromDate.Day)
            {
                DateTime fromDate = new DateTime(_fromDate.Year, month, _fromDate.Day, _fromDate.Hour, _fromDate.Minute, 0);
                _fromDate = fromDate;
            }
            else
            {
                DateTime fromDate = new DateTime(_fromDate.Year, month, DateTime.DaysInMonth(_fromDate.Year, month), _fromDate.Hour, _fromDate.Minute, 0);
                _fromDate = fromDate;
            }
            UpdateControls();
            UpdateMonthDays();
        }

        private void cmbFromYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            int year = Convert.ToInt32(cmbFromYear.Items[cmbFromYear.SelectedIndex]);
            DateTime fromDate = new DateTime(year, _fromDate.Month, _fromDate.Day, _fromDate.Hour, _fromDate.Minute, 0);
            _fromDate = fromDate;
            UpdateControls();
            UpdateMonthDays();
        }

        private void cmbToHour_SelectionChangeCommitted(object sender, EventArgs e)
        {
            int hour = Convert.ToInt32(cmbToHour.Items[cmbToHour.SelectedIndex]);
            DateTime toDate = new DateTime(_toDate.Year, _toDate.Month, _toDate.Day, hour, _toDate.Minute, 0);
            _toDate = toDate;
            UpdateControls();
        }

        private void cmbToMins_SelectionChangeCommitted(object sender, EventArgs e)
        {
            int mins = Convert.ToInt32(cmbToMins.Items[cmbToMins.SelectedIndex]);
            DateTime toDate = new DateTime(_toDate.Year, _toDate.Month, _toDate.Day, _toDate.Hour, mins, 0);
            _toDate = toDate;
            UpdateControls();
        }

        private void cmbToYear_SelectionChangeCommitted(object sender, EventArgs e)
        {
            int year = Convert.ToInt32(cmbToYear.Items[cmbToYear.SelectedIndex]);
            DateTime toDate = new DateTime(year, _toDate.Month, _toDate.Day, _toDate.Hour, _toDate.Minute, 0);
            _toDate = toDate;
            UpdateControls();
            UpdateMonthDays();
        }

        private void cmbToMonth_SelectionChangeCommitted(object sender, EventArgs e)
        {
            Enum.TryParse(cmbToMonth.Items[cmbToMonth.SelectedIndex].ToString(), out Months mnth);
            int month = Convert.ToInt32(mnth) + 1;
            if (DateTime.DaysInMonth(_toDate.Year, month) >= _toDate.Day)
            {
                DateTime toDate = new DateTime(_toDate.Year, month, _toDate.Day, _toDate.Hour, _toDate.Minute, 0);
                _toDate = toDate;
            }
            else
            {
                DateTime toDate = new DateTime(_toDate.Year, month, DateTime.DaysInMonth(_toDate.Year, month), _toDate.Hour, _toDate.Minute, 0);
                _toDate = toDate;
            }
            UpdateControls();
            UpdateMonthDays();
        }

        private void cmbToDay_SelectionChangeCommitted(object sender, EventArgs e)
        {
            int day = Convert.ToInt32(cmbToDay.Items[cmbToDay.SelectedIndex]);
            DateTime toDate = new DateTime(_toDate.Year, _toDate.Month, day, _toDate.Hour, _toDate.Minute, 0);
            _toDate = toDate;
            UpdateControls();

        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            Back(this, null);
        }
    }
}

