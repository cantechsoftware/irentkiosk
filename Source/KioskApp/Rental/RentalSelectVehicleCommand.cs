﻿using System;
using System.Windows.Forms;
using Castle.Core;
using SmartFormat;
using iRent.Framework;
using iRentKiosk.Core.Services;
using iRent2.Contracts.Models;
using iRent2.Contracts.Rental;
using iRentKiosk.Core.Models;
using iRentKiosk.Core.Services.Api;


namespace iRent.Rental
{
    [CastleComponent]
    public class RentalSelectVehicleCommand : BaseCommand
    {
        public string Next { get; set; }
        public string Back { get; set; }

        private Func<RentalSelectVehicleControl> _factory;
        private RentalApi _rentalAPI;

        public RentalSelectVehicleCommand(Func<RentalSelectVehicleControl> factory, RentalApi RentalApi)
        {
            _factory = factory;
            _rentalAPI = RentalApi;
        }

        public override void Invoke()
        {
            var control = _factory();
            control.Dock = DockStyle.Fill;
            control.Next += OnNext;
            control.Back += OnBack;
            var main = GetFormAs<MainForm>();
            main.AddToFormPanel(control);
            RentalOptions options = ContextData.RentalOptions;
            control.Bind(options.Vehicles);
        }

        private void OnBack(object sender, object obj)
        {
            //Create web data object for Payment pages.
            //dynamic webdata = 

            Proceed(Back);
        }

        private void OnNext(object sender, object obj)
        {
            //Create web data object for Payment pages.
            //dynamic webdata = 

            Proceed(Next);
        }
    }
}
