﻿namespace iRent
{
    partial class ExistingCardControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblHeader = new MetroFramework.Controls.MetroLabel();
            this.lblMessage = new MetroFramework.Controls.MetroLabel();
            this.btnReturnCard = new MetroFramework.Controls.MetroButton();
            this.btnTerminatePrevious = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // lblHeader
            // 
            this.lblHeader.AutoSize = true;
            this.lblHeader.FontSize = MetroFramework.MetroLabelSize.XL;
            this.lblHeader.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblHeader.Location = new System.Drawing.Point(10, 10);
            this.lblHeader.Name = "lblPrevCard";
            this.lblHeader.Size = new System.Drawing.Size(150, 30);
            this.lblHeader.TabIndex = 3;
            this.lblHeader.Text = "Existing Card";
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Location = new System.Drawing.Point(10, 60);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(626, 19);
            this.lblMessage.TabIndex = 3;
            this.lblMessage.Text = "Please return your previous card. Or if you have lost your card you need to termi" +
                                   "nate your previous rental";
            // 
            // btnReturnCard
            // 
            this.btnReturnCard.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnReturnCard.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnReturnCard.Highlight = true;
            this.btnReturnCard.Location = new System.Drawing.Point(10, 120);
            this.btnReturnCard.Name = "btnReturnCard";
            this.btnReturnCard.Size = new System.Drawing.Size(177, 80);
            this.btnReturnCard.Style = MetroFramework.MetroColorStyle.Red;
            this.btnReturnCard.TabIndex = 1;
            this.btnReturnCard.Text = "&Return Card";
            this.btnReturnCard.UseSelectable = true;
            this.btnReturnCard.Click += new System.EventHandler(this.btnReturnCard_Click);
            // 
            // btnTerminatePrevious
            // 
            this.btnTerminatePrevious.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnTerminatePrevious.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnTerminatePrevious.Highlight = true;
            this.btnTerminatePrevious.Location = new System.Drawing.Point(340, 120);
            this.btnTerminatePrevious.Name = "btnTerminatePrevious";
            this.btnTerminatePrevious.Size = new System.Drawing.Size(300, 80);
            this.btnTerminatePrevious.Style = MetroFramework.MetroColorStyle.Red;
            this.btnTerminatePrevious.TabIndex = 1;
            this.btnTerminatePrevious.Text = "&Terminate Previous Card";
            this.btnTerminatePrevious.UseSelectable = true;
            this.btnTerminatePrevious.Click += new System.EventHandler(this.btnTerminatePrevious_Click);
            // 
            // ExistingCard
            // 
            this.ClientSize = new System.Drawing.Size(806, 384);
            this.Controls.Add(this.lblHeader);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.btnReturnCard);
            this.Controls.Add(this.btnTerminatePrevious);
            this.Name = "ExistingCardControl";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel lblHeader;
        private MetroFramework.Controls.MetroLabel lblMessage;
        private MetroFramework.Controls.MetroButton btnReturnCard;
        private MetroFramework.Controls.MetroButton btnTerminatePrevious;
    }
}