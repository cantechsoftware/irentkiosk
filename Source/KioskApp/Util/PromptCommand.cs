﻿using System;
using System.Windows.Forms;
using Castle.Core;
using iRent.Classes;
using iRentKiosk.Core.Models;
using iRentKiosk.Core.Services;

namespace iRent.Util
{
    [CastleComponent]
    public class PromptCommand : ICommand
    {
        private readonly Func<PromptControl> _factory;
        private MainForm _form;
        private readonly WorkflowService _workflowService;

        public PromptCommand(WorkflowService workflowService, MainForm form, Func<PromptControl> factory)
        {
            _workflowService = workflowService;
            _form = form;
            _factory = factory;
        }

        public Form MainForm
        {
            get { return _form; }
            set { _form = (MainForm)value; }
        }

        public string Title { get; set; }
        public string Message { get; set; }
        public string Prompt { get; set; }

        public string OK { get; set; }
        public string Cancel { get; set; }
        public string ContextKey { get; set; }

        public void Invoke()
        {
            var control = _factory();
            control.Title = Title;
            control.Message = Message;
            control.Prompt = Prompt;
            control.OK += OnOk;
            control.Cancel += OnCancel;
            _form.AddToFormPanel(control);
        }

        private void OnCancel(object sender, EventArgs e)
        {
            _workflowService.Execute(Cancel);
        }

        private void OnOk(object sender, string value)
        {
            ContextData.Values[ContextKey] = value;
            _workflowService.Execute(OK);
        }
    }
}