﻿using System;
using Castle.Core;
using MetroFramework.Controls;

namespace iRent.Util
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class ConfirmControl : MetroUserControl
    {
        public ConfirmControl()
        {
            InitializeComponent();
        }

        public bool ShowYes
        {
            get { return btnYes.Enabled; }
            set { btnYes.Enabled = value; }
        }

        public bool ShowNo
        {
            get { return btnNo.Enabled; }
            set { btnNo.Enabled = value; }
        }

public string Title
        {
            get { return lblTitle.Text; }
            set { lblTitle.Text = value; }
        }

        public string Message
        {
            get { return lblMessage.Text; }
            set { lblMessage.Text = value; }
        }

        public event EventHandler Yes;
        public event EventHandler No;

        private void btnYes_Click(object sender, EventArgs e)
        {
            if (Yes != null)
                Yes(this, e);
        }

        private void btnNo_Click(object sender, EventArgs e)
        {
            if (No != null)
                No(this, e);
        }
    }
}