﻿using CanTech.Common.Extensions;
using MetroFramework.Forms;
using System;

namespace iRent.Util
{
    public partial class ConfirmMessageForm : MetroForm
    {


        public ConfirmMessageForm()
        {
            InitializeComponent();
        }

        public string Message
        {
            get { return lblMessage.Text; }
            set { lblMessage.Text = value; }
        }

        public static System.Windows.Forms.DialogResult Show(string title, string message, string buttonYesText = null, 
                                                            string buttonNoText = null, bool stopTimer = true, int waitTime = 15)
        {
            if (buttonYesText.IsEmpty())
                buttonYesText = "&Yes";
            using (var frm = new ConfirmMessageForm())
            {
                frm.timProgress.Visible = false;
                if (!stopTimer)
                {
                    frm.timProgress.Style = MetroFramework.MetroColorStyle.Pink;
                    frm.timProgress.Visible = true;
                    frm.timProgress.Maximum = waitTime;
                    frm.timProgress.Minimum = 0;
                    frm.timProgress.Value = waitTime;

                    System.Windows.Forms.Timer t = new System.Windows.Forms.Timer();
                    t.Interval = 1000;
                    t.Start();
                    t.Tick += (s, arg) =>
                    {
                        frm.timProgress.Increment(-1);
                        if (waitTime == 0)
                        {
                            frm.DialogResult = System.Windows.Forms.DialogResult.Yes;
                            //frm.Close();
                        }
                        waitTime--;
                    };
                }
                
                frm.btnYes.Text = buttonYesText;
                frm.btnNo.Text = buttonNoText;
                frm.Text = title;
                frm.Message = message;
                frm.BringToFront();
                frm.ShowDialog();
                return frm.DialogResult;
            }
        }

        private void btnNo_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.No;
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Yes;
        }
    }
}