﻿using CanTech.Common.Extensions;
using MetroFramework.Forms;
using System;

namespace iRent.Util
{
    public partial class MessageForm : MetroForm
    {


        public MessageForm()
        {
            InitializeComponent();
        }

        public string Message
        {
            get { return lblMessage.Text; }
            set { lblMessage.Text = value; }
        }

        public static void Show(string title, string message, string buttonText = null, bool stopTimer = true)
        {
            if (buttonText.IsEmpty())
                buttonText = "&Close";
            using (var frm = new MessageForm())
            {
                if(!stopTimer)
                {
                    System.Windows.Forms.Timer t = new System.Windows.Forms.Timer();
                    t.Interval = 1000;
                    t.Start();
                    int seconds = 5;
                    t.Tick += (s, arg) =>
                    {
                        if (seconds == 0)
                        {
                            frm.Close();
                        }
                        seconds--;
                    };
                }
                frm.btnClose.Text = buttonText;
                frm.Text = title;
                frm.Message = message;
                frm.ShowDialog();
            }
        }
    }
}