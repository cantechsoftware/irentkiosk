﻿using Castle.Core;
using iRent.Framework;
using iRentKiosk.Core.Services;

namespace iRent.Util
{
    [CastleComponent]
    public class MessageCommand : BaseCommand
    {
        public string Next { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public string Button { get; set; }

        public override void Invoke()
        {
            MessageForm.Show(ContextData.Format(Title), ContextData.Format(Message), Button);
            Proceed(Next);
        }
    }
}