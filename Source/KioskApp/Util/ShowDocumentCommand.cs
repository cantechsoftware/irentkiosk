﻿using System;
using System.IO;
using System.Windows.Forms;
using Castle.Core;
using SmartFormat;
using iRent.Framework;
using iRentKiosk.Core.Services;

namespace iRent.Util
{
    [CastleComponent]
    public class ShowDocumentCommand : BaseCommand
    {
        private readonly Func<ShowDocumentControl> _factory;

        public ShowDocumentCommand(Func<ShowDocumentControl> factory)
        {
            _factory = factory;
            Filename = "DamagesDisclaimer.rtf";
            Title = "Disclaimer";
        }

        public string Filename { get; set; }
        public string Title { get; set; }
        public string Next { get; set; }

        public override void Invoke()
        {
            var control = _factory();
            control.Dock = DockStyle.Fill;
            var main = GetFormAs<MainForm>();
            main.AddToFormPanel(control);
            var terms = File.Exists(Filename) 
                ? File.ReadAllText(Filename) 
                : Filename + " not found.";
            control.Init(Title.FormatSmart(ContextData.Instance), terms);
            control.Next += (o, e) => Proceed(Next);
        }
    }
}