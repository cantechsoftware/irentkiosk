﻿using System;
using System.Windows.Forms;
using Castle.Core;
using iRentKiosk.Core.Models;
using iRentKiosk.Core.Services;

namespace iRent.Util
{
    [CastleComponent]
    public class ConfirmCommand : ICommand
    {
        private readonly Func<ConfirmControl> _factory;
        private MainForm _form;
        private readonly WorkflowService _workflowService;

        public ConfirmCommand(WorkflowService workflowService, MainForm form, Func<ConfirmControl> factory)
        {
            _workflowService = workflowService;
            _form = form;
            _factory = factory;
        }

        public Form MainForm
        {
            get { return _form; }
            set { _form = (MainForm)value; }
        }

        public string Title { get; set; }
        public string Message { get; set; }

        public string Yes { get; set; }
        public string No { get; set; }
        public bool ShowYes { get; set; }
        public bool ShowNo { get; set; }
        

        public void Invoke()
        {
            var control = _factory();
            control.Title = Title;
            control.Message = Message;
            control.Yes += OnYes;
            control.No += OnNo;
            control.ShowYes = ShowYes;
            control.ShowNo = ShowNo;
            _form.AddToFormPanel(control);
        }

        private void OnNo(object sender, EventArgs e)
        {
            _workflowService.Execute(No);
        }

        private void OnYes(object sender, EventArgs e)
        {
            _workflowService.Execute(Yes);
        }
    }
}