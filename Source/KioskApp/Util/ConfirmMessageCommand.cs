﻿using Castle.Core;
using iRent.Framework;
using iRentKiosk.Core.Services;

namespace iRent.Util
{
    [CastleComponent]
    public class ConfimrmMessageCommand : BaseCommand
    {
        public string Next { get; set; }
        public string Back { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public string Button { get; set; }

        public override void Invoke()
        {
            if (ConfirmMessageForm.Show(ContextData.Format(Title), ContextData.Format(Message), Button) == System.Windows.Forms.DialogResult.No)
            {
                Proceed(Next);
            }
            else
            {
                Proceed(Back);
            }
        }
    }
}