﻿using Castle.Core;
using iRent.Framework;
using iRent.Services;

namespace iRent.Util
{
    [CastleComponent]
    public class TestRfidCommand : BaseCommand
    {
        private readonly RfidService _rfidService;

        public TestRfidCommand(RfidService rfidService)
        {
            _rfidService = rfidService;
        }

        public override void Invoke()
        {
            var rfid = _rfidService.ReadAndReturn();
        }
    }
}