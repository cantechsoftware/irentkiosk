﻿using Castle.Core;
using Castle.Core.Internal;
using iRent.Framework;
using iRent.Services;

namespace iRent.Util
{
    [CastleComponent]
    public class EvalCommand : BaseCommand
    {
        public string Expression { get; set; }

        private readonly EvalService _evalService;

        public EvalCommand(EvalService evalService)
        {
            _evalService = evalService;
        }

        public override void Invoke()
        {
            var next = GetNext();
            
            Proceed(next);
        }

        private string GetNext()
        {
            var result = _evalService.Evaluate(Expression).ToString();

            foreach (var attr in Config.Attributes())
            {
                if (attr.Name.LocalName.EqualsText(result))
                    return attr.Value;
            }

            foreach (var elt in Config.Elements())
                if (elt.Name.LocalName.EqualsText(result))
                    return elt.Value;

            return result;
        }
    }
}