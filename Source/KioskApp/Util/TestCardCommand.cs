﻿using Castle.Core;
using iRent.Framework;
using iRent.Services;

namespace iRent.Util
{
    [CastleComponent]
    public class TestCardCommand : BaseCommand
    {
        private readonly RfidService _rfidService;

        public TestCardCommand(RfidService rfidService)
        {
            _rfidService = rfidService;
        }

        public string Next { get; set; }

        public override void Invoke()
        {
            var rfid = _rfidService.GetCard(true);
            MessageForm.Show("RFID", $"RFID is: {rfid}");
            Proceed(Next);
        }
    }
}