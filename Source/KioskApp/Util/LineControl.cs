﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace iRent.Util
{
    public partial class LineControl : UserControl
    {
        private Point _startPoint;
        private Point _endPoint;
        private float _penWidth = 1.0f;
        private Color _lineColor = Color.Black;
        
        public event PropertyChangedEventHandler PropertyChanged;

        public LineControl()
        {
            InitializeComponent();
            _startPoint = new Point(0, 0);
            _endPoint = new Point(100, 0);
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x20;
                return cp;
            }
        }

        [Browsable(true), Category("Layout"), Description("Start point of the line in control coordinates.")]
        public Point StartPoint
        {
            get { return _startPoint; }
            set
            {
                if (value != _startPoint)
                {
                    _startPoint = value;
                    OnPropertyChanged("StartPoint");
                    RecalcSize();
                }
            }
        }

        [Browsable(true), Category("Layout"), Description("End point of the line in control coordinates.")]
        public Point EndPoint
        {
            get { return _endPoint; }
            set
            {
                if (value != _endPoint)
                {
                    _endPoint = value;
                    OnPropertyChanged("EndPoint");
                    RecalcSize();
                }
            }
        }

        [Category("Appearance"), Description("Color of the line drawn."), DefaultValue(typeof(Color), "Black")]
        public Color LineColor
        {
            get { return _lineColor; }
            set
            {
                if (value != _lineColor)
                {
                    _lineColor = value;
                    OnPropertyChanged("LineColor");
                    InvokeInvalidate();
                }
            }
        }

        [Category("Appearance"), Description("Width of the line drawn."), DefaultValue(1.0f)]
        public float LineWidth
        {
            get { return _penWidth; }
            set
            {
                if (value != _penWidth)
                {
                    _penWidth = value;
                    OnPropertyChanged("LineWidth");
                    InvokeInvalidate();
                }
            }
        }

        protected override void OnResize(EventArgs e)
        {
            InvokeInvalidate();
            base.OnResize(e);
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;

            using (Pen p = new Pen(_lineColor, _penWidth))
            {
                e.Graphics.DrawLine(p, _startPoint, _endPoint);
            }

        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            //base.OnPaintBackground(e);
            this.CreateGraphics().DrawRectangle(new Pen(Color.Transparent, 1), new Rectangle(0, 0, this.Size.Width, this.Size.Height));
        }

        private void RecalcSize()
        {
            InvokeInvalidate();

            base.OnResize(EventArgs.Empty);
        }

        private void InvokeInvalidate()
        {
            if (!IsHandleCreated)
                return;

            Rectangle rect = new Rectangle(Location, Size);

            try
            {
                this.Invoke((MethodInvoker)delegate { Parent.Invalidate(rect, true); });
            }
            catch { }
        }
        
    }
}
