﻿using System;
using Castle.Core;
using MetroFramework.Controls;
using CanTech.Common.Extensions;

namespace iRent.Util
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class PromptControl : MetroUserControl
    {
        public PromptControl()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            txtValue.Focus();
        }

        public string Title
        {
            get { return lblTitle.Text; }
            set { lblTitle.Text = value; }
        }

        public string Message
        {
            get { return lblMessage.Text; }
            set { lblMessage.Text = value; }
        }

        public string Prompt
        {
            get { return txtValue.PromptText; }
            set { txtValue.PromptText = value; }
        }

        public event EventHandler<string> OK;
        public event EventHandler Cancel;

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (txtValue.Text.IsEmpty())
                return;

            if (OK != null)
                OK(this, txtValue.Text);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (Cancel != null)
                Cancel(this, e);
        }
    }
}