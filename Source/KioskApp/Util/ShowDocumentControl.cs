﻿using System;
using System.Drawing.Printing;
using Castle.Core;
using MetroFramework.Controls;
using iRent.Extensions;

namespace iRent.Util
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class ShowDocumentControl : MetroUserControl
    {
        private int _checkPrint;

        public ShowDocumentControl()
        {
            InitializeComponent();
        }

        public void Init(string title, string terms)
        {
            lblTitle.Text = title;
            try
            {
                txtDisclaimer.Rtf = terms;
            }
            catch
            {
                txtDisclaimer.Text = terms;
            }
        }

        private void printDocument_BeginPrint(object sender, PrintEventArgs e)
        {
            _checkPrint = 0;
        }

        private void printDocument_PrintPage(object sender, PrintPageEventArgs e)
        {
            // Print the content of RichTextBox. Store the last character printed.
            _checkPrint = txtDisclaimer.Print(_checkPrint, txtDisclaimer.TextLength, e);

            // Check for more pages
            if (_checkPrint < txtDisclaimer.TextLength)
                e.HasMorePages = true;
            else
                e.HasMorePages = false;
        }

        public event EventHandler Next;

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (Next != null)
                Next(this, e);
        }

        private void cmdPrint_Click(object sender, EventArgs e)
        {
            printDocument.Print();
        }
    }
}