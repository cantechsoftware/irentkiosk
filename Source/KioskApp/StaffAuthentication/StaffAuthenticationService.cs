﻿using System;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using CanTech.Common.Extensions;
using Castle.Core;
using iRent.Framework;
using iRent2.Contracts;
using iRent2.Contracts.Drivers;
using iRentKiosk.Core.Services.Api;

namespace iRent.StaffAuthentication
{
    [CastleComponent]
    public class StaffAuthenticationService
    {
        private readonly DriverApi _driverApi;
        private readonly StaffAuthenticationMethod _method;

        public StaffAuthenticationService(DriverApi driverApi, StaffAuthenticationMethod method)
        {
           _driverApi = driverApi;
            _method = method;
        }

        public bool CheckAuthentication()
        {
            var staffNumber = _method.GetStaffNumber();
            var password = _method.GetPassword();

            if (staffNumber.IsEmpty())
                return OnError("Employee number is required.");
            if(password.IsEmpty())
                return OnError("Password is required.");

            try
            {
                var cert = _driverApi.GetCertificate();
                var pwd = EncryptPassword(password,cert);
                var rsp = _driverApi.StaffNumberAuthentication(new StaffAuthenticationRequest { Staffnumber = staffNumber, Password = pwd});
                return rsp.StatusCode == ApiStatusCode.Success;
            }
           catch (Exception ex)
            {
                Logger.Log(this, "StaffNumberAuthentication", "Error Authenticating Employee Number and password.", ex);
                return OnError("Unknown Employee number or invalid password.");
            }
        }

        private string EncryptPassword(string password, X509Certificate2 cert)
        {
            var str = $"{DateTime.UtcNow:yyyyMMddHHmmss},{password}";
            var buffer = Encoding.UTF8.GetBytes(str);
            var rsa = (RSACryptoServiceProvider)cert.PublicKey.Key;
            var retBuff = rsa.Encrypt(buffer, true);

            return Convert.ToBase64String(retBuff);
        }

        private bool OnError(string msg)
        {
            _method.DisplayError(msg);
            return false;
        }
    }
}