﻿using System.Drawing;
using System.Windows.Forms;
using iRent.Forms;
using iRent.Util;
using iRentKiosk.Core.Services;
using MetroFramework.Controls;

namespace iRent.StaffAuthentication
{
    public class StaffAuthenticationMethod 
    {
        public string GetStaffNumber()
        {
            MessageForm.Show(ContextData.Format("Authentication required"), ContextData.Format("Authentication required by Bidvest \ncar rental representative."), "OK");

            var txt = new MetroTextBox { Size = new Size(100, 30) };
            txt.WordWrap = true;
            using (var frm = new InputForm(txt))
            {
                frm.Text = "Car Rental Employee Number:";
                if (frm.ShowDialog() == DialogResult.OK)
                    return txt.Text;
            }
            return null;
        }

        public string GetPassword()
        {
            var txt = new MetroTextBox { Size = new Size(100, 30), PasswordChar = '*'};
            using (var frm = new InputForm(txt))
            {
                frm.Text = "Enter your password:";
                if (frm.ShowDialog() == DialogResult.OK)
                    return txt.Text;
            }
            return null;
        }

        public string DisplayError(string error)
        {
            MessageForm.Show("Car Rental Employee Authentication Error", error, "Back");
            return null;
        }
    }
}