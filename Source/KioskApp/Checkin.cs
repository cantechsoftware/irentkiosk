﻿using System;
using System.Windows.Forms;
using Castle.Core;
using System.Configuration;

namespace iRent
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class Checkin : UserControl
    {
        public Checkin()
        {
            InitializeComponent();
            txtRfid.Visible = false;
#if DEBUG
            txtRfid.KeyUp += txtRfid_KeyUp;
            metroLabel1.DoubleClick += metroLabel1_DoubleClick;
#endif
            if (ConfigurationManager.AppSettings["RFIDType"] == "TapAndGo")
                metroLabel1.Text = "Please place RFID card on Tap & Go Reader";
            else
                metroLabel1.Text = "Please insert your RFID card.";
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            ParentForm.Close();
        }

        private void metroLabel1_DoubleClick(object sender, EventArgs e)
        {
            txtRfid.Visible = true;
            txtRfid.BringToFront();
        }

        public string Rfid
        {
            get { return txtRfid.Text; }
        }

        private void txtRfid_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ParentForm.DialogResult = DialogResult.Yes;
                ParentForm.Close();
            }
            else if (e.KeyCode == Keys.Escape)
            {
                txtRfid.Text = "";
                txtRfid.Visible = false;
            }
        }
    }
}