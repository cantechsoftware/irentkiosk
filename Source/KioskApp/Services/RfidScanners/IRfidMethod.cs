﻿namespace iRent.Services.RfidScanners
{
    public interface IRfidMethod
    {
        string GetCard();
        string ReturnCard();
        string ReadAndReturn();
    }

    public class ReturnCardResponse
    {
        public string Rfid { get; set; }
        public bool CardReturned { get; set; }
    }
}