﻿using System.Drawing;
using System.Windows.Forms;
using iRent.Forms;
using MetroFramework.Controls;
using MetroFramework.Forms;

namespace iRent.Services.RfidScanners
{
    public class ManualRfidMethod : IRfidMethod
    {
        public bool TapAndGo { get; set; }

        public string GetCard()
        {
            var txt = new MetroTextBox {Size = new Size(100, 30)};
            using (var frm = new InputForm(txt))
            {
                frm.Text = "Enter RFID for testing:";
                if (frm.ShowDialog() == DialogResult.OK)
                    return txt.Text;
            }
            return null;
        }

        public string ReturnCard()
        {
            var txt = new MetroTextBox {Size = new Size(100, 30)};
            using (var frm = new InputForm(txt))
            {
                frm.Text = "Return RFID for testing:";
                if (frm.ShowDialog() == DialogResult.OK)
                    return txt.Text;
            }
            return null;
        }

        public string ReadAndReturn()
        {
            var checkin = new Checkin();

            var task = new MetroTaskWindow(10, checkin);
            task.TopMost = true;
            if (task.ShowDialog() == DialogResult.Yes)
                return checkin.Rfid;

            return null;
        }
    }
}