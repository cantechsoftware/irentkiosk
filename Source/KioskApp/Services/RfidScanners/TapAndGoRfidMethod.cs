﻿using System;
using Castle.Core;
using iRent.Classes;
using MetroFramework.Forms;

namespace iRent.Services.RfidScanners
{
    public class TapAndGoRfidMethod : IRfidMethod, IInitializable, IDisposable
    {
        private readonly TapAndGo _tapAndGo = new TapAndGo();

        public void Dispose()
        {
            //_tapAndGo.CloseComm();
        }

        public void Initialize()
        {
            //_tapAndGo.OpenComm();
        }

        public string GetCard()
        {
            ShowPrompt();
            return _tapAndGo.CardActivate();
        }

        public string ReturnCard()
        {
            ShowPrompt();
            string retval = _tapAndGo.CardActivate();
            return retval;
        }

        public string ReadAndReturn()
        {
            ShowPrompt();
            string retval = _tapAndGo.CardActivate();
            return retval;
        }

        private void ShowPrompt()
        {
            var checkin = new Checkin();
            var task = new MetroTaskWindow(10, checkin);
            task.TopMost = true;
            task.ShowDialog();
        }
    }
}