﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using CanTech.Common.Extensions;
using Castle.Core;
using iRent.Classes;
using iRent.Extensions;
using iRent.Framework;
using MetroFramework.Forms;

namespace iRent.Services.RfidScanners
{
    public class ScannerRfidMethod : IRfidMethod, IInitializable, IDisposable
    {
        private readonly CancomRFID _rfidController;

        public ScannerRfidMethod(uint baudrate = 38400)
        {
            _rfidController = new CancomRFID(baudrate);
        }

        public void Dispose()
        {
            _rfidController.CloseComm();
        }

        public void Initialize()
        {
            _rfidController.OpenComm();
        }

        public string GetCard()
        {
            _rfidController.Initialize();

            var rfidMsg = ActivateCardRetry();
            if (rfidMsg.HasValue() && rfidMsg != "Error")
                _rfidController.MoveCardToFront();

            return rfidMsg;
        }

        public string ReturnCard()
        {
            _rfidController.Initialize();

            _rfidController.AllCardInBtn();

            var checkin = new Checkin();
            var task = new MetroTaskWindow(10, checkin);
            task.TopMost = true;
            task.ShowDialog();

            var rfidMsg = _rfidController.CardActivateBtn();
            _rfidController.MoveCardToRecycleBin();

            return rfidMsg;
        }

        public string ReadAndReturn()
        {
            _rfidController.Initialize();

            _rfidController.AllCardInBtn();

            string rfidMsg = null;
            var checkin = new Checkin();

            var task = new MetroTaskWindow(10, checkin);
            Task.Run(() =>
            {
                while (rfidMsg == null)
                {
                    Thread.Sleep(100);
                    var rfid = _rfidController.CardActivateBtn();
                    if (rfid.HasValue())
                    {
                        task.InvokeIfRequired(x =>
                        {
                            x.CancelTimer = true;
                            x.Close();
                            x.Dispose();
                            rfidMsg = rfid;
                        });
                        return;
                    }
                }
            });

            _rfidController.MoveCardToFront();
            return rfidMsg;
        }

        private string ActivateCardRetry()
        {
            for (var i = 0; i < 5; i++)
            {
                try
                {
                    _rfidController.MoveCardToRFBtn();
                    var rfidMsg = _rfidController.CardActivate();
                    if (rfidMsg.HasValue() && rfidMsg != "Error")
                        return rfidMsg;
                }
                catch (Exception ex)
                {
                    Logger.Log(this, "ActivateCardRetry", "Error activating card", ex);
                    if (i == 4)
                    {
                        Errors.Notify("Error activating card", ex);
                        return null;
                    }
                }
            }
            return null;
        }
    }
}