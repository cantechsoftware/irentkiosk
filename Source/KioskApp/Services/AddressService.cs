﻿using System;
using Castle.Core;

namespace iRent.Services
{
    [CastleComponent]
    public class AddressService
    {
        public string BuildPostalAddress(iRent2.Contracts.Models.AddressModel postal)
        {
            var addr = String.IsNullOrEmpty(postal.HouseNo) ? "" : postal.HouseNo;
            addr += String.IsNullOrEmpty(postal.Complex) ? "" : postal.Complex;
            addr += String.IsNullOrEmpty(postal.StreetNo) ? "" : postal.StreetNo;
            addr += String.IsNullOrEmpty(postal.StreetName) ? "" : postal.StreetName;
            addr += String.IsNullOrEmpty(postal.PostCode) ? "" : postal.PostCode;
            addr += String.IsNullOrEmpty(postal.Suburb) ? "" : postal.Suburb;
            addr += String.IsNullOrEmpty(postal.City) ? "" : postal.City;
            addr += String.IsNullOrEmpty(postal.State) ? "" : postal.State;
            addr += String.IsNullOrEmpty(postal.Country) ? "" : postal.Country;
            return addr;
        }
    }
}