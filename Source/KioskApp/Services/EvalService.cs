﻿using System;
using System.Collections.Concurrent;
using Castle.Core;
using ExpressionEvaluator;
using iRentKiosk.Core.Services;

namespace iRent.Services
{
    [CastleComponent]
    public class EvalService
    {
        private readonly ConcurrentDictionary<string, Func<ContextData.InstanceWrapper, object>> _cache
            = new ConcurrentDictionary<string, Func<ContextData.InstanceWrapper, object>>();

        public object Evaluate(string expression)
        {
            var expr = _cache.GetOrAdd(expression,
                x => new CompiledExpression(x).ScopeCompile<ContextData.InstanceWrapper>());

            return expr(ContextData.Instance);
        }
    }
}