﻿using System;
using System.Windows.Forms;
using CanTech.Common.Extensions;
using Castle.Core;
using Castle.MicroKernel;
using iRent.Framework;
using iRent.Services.RfidScanners;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;
using MetroFramework;

namespace iRent.Services
{
    [CastleComponent]
    public class RfidService
    {
        private readonly MainForm _mainForm;
        private readonly IRfidMethod _method;
        private readonly VehicleApi _vehicleApi;

        public RfidService(MainForm mainForm, IKernel kernel, VehicleApi vehicleApi, string method = "Scanner")
        {
            _mainForm = mainForm;
            _vehicleApi = vehicleApi;
            _method = kernel.Resolve<IRfidMethod>(method + "RfidMethod");
        }

        public bool ResetDriverRfid { get; set; }

        public string GetCard(bool checkExisting)
        {
            if (ContextData.PreviousRfid.HasValue())
                return ContextData.PreviousRfid;

            var driver = ContextData.Driver;
            if (checkExisting && driver.RfId.HasValue())
                ExistingRfid(driver.RfId);

            return _method.GetCard();
        }

        public void ExistingRfid(string rfid)
        {
            var dr = MetroMessageBox.Show(
                _mainForm,
                "You have not checked in your previous vehicle. Would you like to check in now?",
                "RFID CARD", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
                ReturnCard();
        }

        public bool ReturnCard()
        {
            var rfid = _method.ReturnCard();

            if (rfid.IsEmpty() || rfid == "Error")
                return false;

            try
            {
                _vehicleApi.ReturnVehicle(rfid, ResetDriverRfid);
                if (ContextData.Driver != null)
                    ContextData.Driver.RfId = null;
                return true;
            }
            catch (Exception ex)
            {
                var dr = MetroMessageBox.Show(
                    _mainForm,
                    ex.Message, "Vehicle Returned"
                    , MessageBoxButtons.OK);
                Logger.Log(this, "ReturnVehicle", "Error returning vehicle", ex);
                //Errors.Notify("Error returning vehicle", ex);

                return false;
            }
        }

        public string ReadAndReturn()
        {
            return _method.ReadAndReturn();
        }

       }
}