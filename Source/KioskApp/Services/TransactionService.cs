﻿using System;
using Castle.Core;
using iRent2.Contracts.Enums;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;

namespace iRent.Services
{
    [CastleComponent]
    public class TransactionService
    {
        private readonly BookingApi _bookingApi;
        private readonly TransactionApi _transactionApi;

        public TransactionService(TransactionApi transactionApi, BookingApi bookingApi)
        {
            _transactionApi = transactionApi;
            _bookingApi = bookingApi;
        }

        private void ActivateBooking(BookingModel booking, string rfid)
        {
            _bookingApi.Activate(booking, rfid, ContextData.PaymentKey);
        }

        public TransactionModel Create(string rfidNo)
        {
            var trans = new TransactionModel
                {
                    Direction = DirectionType.Out,
                    Vehicle = ContextData.Vehicle,
                    VehicleGroup = ContextData.VehicleGroup,
                    DueBack = new DueBackModel
                        {
                            Date = ContextData.VehicleReturnDate
                        },
                    PaymentKey = ContextData.PaymentKey,
                    Driver = ContextData.Driver,
                    TotalCost = ContextData.VehicleCost,
                    DateTime = DateTime.Now,
                    RfIdNo = rfidNo
                };
            if (ContextData.VehicleReturnDate.HasValue || ContextData.ReturnBranch != null)
            {
                trans.DueBack = new DueBackModel
                    {
                        BranchId = ContextData.ReturnBranch.Id,
                        Date = ContextData.VehicleReturnDate
                    };
            }
            if (ContextData.Booking != null)
                trans.BookingId = ContextData.Booking.Id;

            return _transactionApi.Update(trans);
        }

        public void Activate(string rfid, bool notifyDriver)
        {
            if (ContextData.Booking != null)
                ActivateBooking(ContextData.Booking, rfid);
            else
            {
                var trans = Create(rfid);
                _transactionApi.Activate(trans.Id, notifyDriver);
            }
        }
    }
}