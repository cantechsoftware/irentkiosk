﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using Castle.Core;
using Microsoft.Reporting.WinForms;
using iRent.Classes;

namespace iRent.Services
{
   [CastleComponent]
    public class PrintService
    {
       public IList<Stream> _mStreams;
       public int _mCurrentPageIndex;

       private void print(printds ds)
       {
           var reportviewer = new ReportViewer();
           reportviewer.ProcessingMode = ProcessingMode.Local;
           var ii = new List<printds>();
           ii.Add(ds);
           reportviewer.LocalReport.DataSources.Add(new ReportDataSource("ds", ii));
           var report = new LocalReport();
           report.DataSources.Add(new ReportDataSource("ds", ii));
           Export(report);

           var p = Application.StartupPath + "\\";
           var report_path = p + "VMS_CarReport.rdlc";

           reportviewer.LocalReport.ReportPath = report_path;

           reportviewer.RefreshReport();

           var pdf = reportviewer.LocalReport.Render("PDF");
           File.WriteAllBytes(p + "VMS_CarReport.pdf", pdf);
       }

       public void Print()
       {
           //set as the printer name
           const string printerName =
               "Microsoft Office Document Image Writer";
           if (_mStreams == null || _mStreams.Count == 0)
               return;
           var printDoc = new PrintDocument();
           printDoc.PrinterSettings.PrinterName = printerName;
           if (!printDoc.PrinterSettings.IsValid)
           {
               var msg = String.Format(
                   "Can't find printer \"{0}\".", printerName);
               MessageBox.Show(msg, "Print Error");
               return;
           }
           printDoc.PrintPage += PrintPage;
           printDoc.Print();
       }

       // Handler for PrintPageEvents
       private void PrintPage(object sender, PrintPageEventArgs ev)
       {
           var pageImage = new
               Metafile(_mStreams[_mCurrentPageIndex]);
           ev.Graphics.DrawImage(pageImage, ev.PageBounds);
           _mCurrentPageIndex++;
           ev.HasMorePages = (_mCurrentPageIndex < _mStreams.Count);
       }

       private Stream CreateStream(string name, string fileNameExtension, Encoding encoding, string mimeType,
                                   bool willSeek)
       {
           Stream stream = new FileStream(@"..\..\" + name +
                                          "." + fileNameExtension, FileMode.Create);
           _mStreams.Add(stream);
           return stream;
       }

       // Export the given report as an EMF (Enhanced Metafile) file.
       private void Export(LocalReport report)
       {
           var deviceInfo =
               "<DeviceInfo>" +
               "  <OutputFormat>EMF</OutputFormat>" +
               "  <PageWidth>8.5in</PageWidth>" +
               "  <PageHeight>11in</PageHeight>" +
               "  <MarginTop>0.25in</MarginTop>" +
               "  <MarginLeft>0.25in</MarginLeft>" +
               "  <MarginRight>0.25in</MarginRight>" +
               "  <MarginBottom>0.25in</MarginBottom>" +
               "</DeviceInfo>";
           Warning[] warnings;
           _mStreams = new List<Stream>();
           report.Render("Image", deviceInfo, CreateStream,
                         out warnings);
           foreach (var stream in _mStreams)
               stream.Position = 0;
       }
    }
}