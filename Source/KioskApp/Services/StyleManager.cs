﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using CanTech.Common.Extensions;
using Castle.Core;
using iRentKiosk.Core.Models;
using iRentKiosk.Core.Util;
using log4net;
using System.Windows.Forms;

namespace iRentKiosk.Core.Services
{
    [CastleComponent]
    public class StyleManager
    {
        private string _path;
        private XElement _styledefinition;



        public StyleManager(string path)
        {
            _path = path;
            _path = Path.GetFullPath(path);
            using (FileStream fs = File.Open(_path, FileMode.Open, FileAccess.Read))
            {
                _styledefinition = XElement.Load(fs);
                fs.Close();
            }
        }

        public void ApplyStyle(Form form)
        {
            Control c = form;
            //Nested Loop of all controls. If the control matches a style mapping then set the controls style properties.


        }


    }
}
