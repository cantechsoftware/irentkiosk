﻿using System.Collections.Generic;
using System.Text;
using Castle.Core;
using GalaSoft.MvvmLight.Messaging;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Models;
using iRentKiosk.Core.Services.Api;
using iRentKiosk.Core.Util;

namespace iRent.Services
{
    [CastleComponent]
    public class LicenceScanner
    {
        private readonly DriverApi _driverApi;

        public LicenceScanner(DriverApi driverApi)
        {
            _driverApi = driverApi;
        }

        public bool Process(byte[] raw)
        {
            if (DecodeLicence(raw))
                return true;

            if (DecodeTempLicence(raw))
                return true;

            return false;
        }

        public bool DecodeLicence(byte[] raw)
        {
            if (raw.Length > 715)
            {
                var driver = _driverApi.DecryptAndSaveLicense(raw);
                var msg = new ScannerMessageItem
                {
                    msg = "driver",
                    Driver = driver
                };
                Messenger.Default.Send(new NotificationMessage<ScannerMessageItem>(msg, "Scanner"));
                return true;
            }

            return false;
        }

        public bool DecodeTempLicence(byte[] raw)
        {
            var data = Encoding.ASCII.GetString(raw).Trim();

            var parts = data.Split('%');
            if (parts.Length < 15)
                return false;

            var id = new IdInfo(parts[7]);
            var driver = _driverApi.GetByIdNo(id) ?? new DriverModel
                         {
                             IdNumber = id.IdNumber,
                             IdCountry = id.Country,
                             IdType = id.Type
                         };

            driver.Licence.CertificateNumber = parts[5];
            driver.Licence.ValidFrom = parts[14].ToDate("yyyy-MM-dd");
            driver.Licence.ValidUntil = parts[13].Extract(@"\d{4}-\d{2}-\d{2}$").ToDate("yyyy-MM-dd");
            var cls = parts[9].Split('/');
            if (cls.Length > 2)
            {
                driver.Licence.Classes = new List<DriverLicenceClassModel>
                {
                    new DriverLicenceClassModel
                    {
                        FirstIssueDate = cls[1].ToDate("yyyy-MM-dd"),
                        VehicleCode = cls[0],
                        VehicleRestriction = cls[2]
                    }
                };
            }

            var np = parts[8].Split(new[] {' '}, 2);
            driver.Initials = np[0];
            driver.Surname = np[1];

            driver = _driverApi.Save(driver);
            var msg = new ScannerMessageItem
            {
                msg = "driver",
                Driver = driver
            };
            Messenger.Default.Send(new NotificationMessage<ScannerMessageItem>(msg, "Scanner"));

            return true;
        }
    }
}