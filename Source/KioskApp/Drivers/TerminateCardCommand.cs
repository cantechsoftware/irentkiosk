﻿using Castle.Core;
using iRent.Framework;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;

namespace iRent.Drivers
{
    [CastleComponent]
    public class TerminateCardCommand : BaseCommand
    {
        private readonly DriverApi _driverApi;

        public TerminateCardCommand(DriverApi driverApi)
        {
            _driverApi = driverApi;
        }

        public string Next { get; set; }

        public override void Invoke()
        {
            var driver = _driverApi.Terminate(ContextData.Driver.Id);
            ContextData.SetDriver(driver);

            Proceed(Next);
        }
    }
}