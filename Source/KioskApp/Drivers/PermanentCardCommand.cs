﻿using System;
using System.Windows.Forms;
using CanTech.Common.Extensions;
using Castle.Core;
using iRent.Framework;
using iRent.Services;
using iRent.Util;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;

namespace iRent.Drivers
{
    [CastleComponent]
    public class PermanentCardCommand : BaseCommand
    {
        private readonly Func<ConfirmControl> _factory;
        private readonly RfidService _rfidService;
        private readonly VehicleApi _vehicleApi;

        public PermanentCardCommand(Func<ConfirmControl> factory,
            VehicleApi vehicleApi,
            RfidService rfidService)
        {
            _factory = factory;
            _vehicleApi = vehicleApi;
            _rfidService = rfidService;
        }

        public string Next { get; set; }
        public string InUse { get; set; }
        public string InvalidRfid { get; set; }

        public override void Invoke()
        {
            if (ContextData.Driver.PermanentRfid.IsEmpty())
            {
                Proceed(Next);
                return;
            }

            var control = _factory();
            control.Title = "Permanent Card";
            control.Message = "Do you want to use your permanent card?";
            control.Dock = DockStyle.Fill;
            var main = GetFormAs<MainForm>();
            main.AddToFormPanel(control);
            control.Yes += OnYes;
            control.No += (o, e) => Proceed(Next);
        }

        private void OnYes(object sender, EventArgs e)
        {
            var rfid = _rfidService.ReadAndReturn();

            if (rfid != ContextData.Driver.PermanentRfid)
            {
                Proceed(InvalidRfid);
                return;
            }

            var veh = _vehicleApi.GetByRfid(rfid);
            if (veh != null)
            {
                Proceed(InUse);
                return;
            }

            ContextData.PreviousRfid = rfid;
            Proceed(Next);
        }
    }
}