﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using CanTech.Common.Extensions;
using Castle.Core;
using GalaSoft.MvvmLight.Messaging;
using MetroFramework.Controls;
using iRent.Classes;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Models;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;
using iRent.Extensions;

namespace iRent.Drivers
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class ScanDriverControl : MetroUserControl
    {
        private readonly DataApi _dataApi;
        private readonly DriverApi _driverApi;

        public ScanDriverControl(DataApi dataApi, DriverApi driverApi)
        {
            _dataApi = dataApi;
            _driverApi = driverApi;
            InitializeComponent();

            Messenger.Default.Register<NotificationMessage<ScannerMessageItem>>(
                this,
                up =>
                    {
                        if (up.Content.msg.ToString() == "driver")
                        {
                            this.InvokeIfRequired(x => UpdateForm(up.Content.Driver));
                            Thread.Sleep(5000);
                            this.InvokeIfRequired(x => Process(up.Content.Driver));
                        }
                    });
        }

        private void Login_Load(object sender, EventArgs e)
        {
#if DEBUG
            txtIDNo.ReadOnly = false;
            txtIDNo.KeyUp += txtIDNo_KeyUp;
#endif
        }

        private void txtIDNo_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
                Prepopulate(txtIDNo.Text);
        }

        public void ClearControls()
        {
            Messenger.Default.Register<NotificationMessage<ScannerMessageItem>>(
                this,
                up =>
                {
                    if (up.Content.msg.ToString() == "driver")
                    {
                        UpdateForm(up.Content.Driver);
                        Thread.Sleep(5000);
                        Process(up.Content.Driver);
                    }
                });
            txtBDate.Text = "";
            txtDriverRes.Text = "";
            txtGender.Text = "";
            txtIDNo.Text = "";
            txtIniSurname.Text = "";
            txtIssueDate.Text = "";
            txtIssueNo.Text = "";
            txtLicNo.Text = "";
            txtValidDate.Text = "";
            txtVehicleCodes.Text = "";
            pictureBox1.Image = null;
            lblStatus.Text = "";
        }

        private void Process(DriverModel driver)
        {
            if (Next != null)
                Next(this, driver);
        }

        public Image ByteArrayToImage(byte[] byteArrayIn)
        {
            var ms = new MemoryStream(byteArrayIn);
            var returnImage = Image.FromStream(ms);
            return returnImage;
        }

        protected override void Dispose(bool disposing)
        {
            Messenger.Default.Unregister(this);
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void UpdateForm(DriverModel driver)
        {
            txtBDate.Text = $"{driver.DateOfBirth:yyyy-MM-dd}";
            txtDriverRes.Text = driver.Licence?.DriverRestrictions?.Join(",");
            txtGender.Text = driver.Gender.ToString();
            txtIDNo.Text = driver.IdNumber;
            ContextData.SetDriver(driver);
            txtIniSurname.Text = $"{driver.Initials} {driver.Surname}";
            txtIssueDate.Text = $"{driver.Licence.Classes.Select(x => x.FirstIssueDate).FirstOrDefault():yyyy-MM-dd}";
            txtIssueNo.Text = driver.Licence.IssueNo;
            txtLicNo.Text = driver.Licence.CertificateNumber;
            txtValidDate.Text = $"{driver.Licence.ValidFrom:yyyy-MM-dd} - {driver.Licence.ValidUntil:yyyy-MM-dd}";
            txtVehicleCodes.Text = driver.Licence?.Classes?.Select(x => x.VehicleCode).Join(",");
            if (driver.Picture != null)
                LoadImage(driver.Picture);
        }

        private void LoadImage(BlobModel doc)
        {
            var buff = _dataApi.Download(doc.Id);
            try
            {
                var img = ByteArrayToImage(buff);
                pictureBox1.Image = img;
            }
            catch
            {
                pictureBox1.Image = null;
            }
        }

        private void Prepopulate(string idNumber)
        {
            var driver = _driverApi.GetByIdNo(new IdInfo(idNumber));
            if (driver == null)
                return;
            UpdateForm(driver);
            btnNext.Enabled = true;
            btnNext.Visible = true;
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            Process(ContextData.Driver);
        }

        public event EventHandler<DriverModel> Next;
    }
}