﻿using System;
using Castle.Core;
using iRent.Framework;
using iRent.Util;
using iRentKiosk.Core.Services;

namespace iRent.Drivers
{
    [CastleComponent]
    public class CheckLicenceCommand : BaseCommand
    {
        public string Message { get; set; } = "Your Drivers License has expired\r\nPlease contact the helpdesk";
        public string Title { get; set; } = "Licence Expired";
        public string Button { get; set; } = "OK";
        public string Next { get; set; }
        public string Expired { get; set; }

        public override void Invoke()
        {
            if (ContextData.Driver?.Licence?.ValidUntil < DateTime.Today)
            {
                MessageForm.Show(ContextData.Format(Title), ContextData.Format(Message), Button);
                Proceed(Expired);
            }
            else
            {
                Proceed(Next);
            }
        }
    }
}