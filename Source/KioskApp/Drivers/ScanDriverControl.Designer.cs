﻿namespace iRent.Drivers
{
    partial class ScanDriverControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>


        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblStatus = new MetroFramework.Controls.MetroLabel();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.txtDriverRes = new MetroFramework.Controls.MetroTextBox();
            this.txtVehicleCodes = new MetroFramework.Controls.MetroTextBox();
            this.txtValidDate = new MetroFramework.Controls.MetroTextBox();
            this.txtGender = new MetroFramework.Controls.MetroTextBox();
            this.txtIssueNo = new MetroFramework.Controls.MetroTextBox();
            this.txtIssueDate = new MetroFramework.Controls.MetroTextBox();
            this.txtLicNo = new MetroFramework.Controls.MetroTextBox();
            this.txtBDate = new MetroFramework.Controls.MetroTextBox();
            this.txtIDNo = new MetroFramework.Controls.MetroTextBox();
            this.txtIniSurname = new MetroFramework.Controls.MetroTextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.btnNext = new MetroFramework.Controls.MetroButton();
            this.tableLayoutPanel1.SuspendLayout();
            this.metroPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.lblStatus, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.metroPanel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnNext, 1, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(654, 550);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.lblStatus, 3);
            this.lblStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblStatus.FontSize = MetroFramework.MetroLabelSize.XL;
            this.lblStatus.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblStatus.Location = new System.Drawing.Point(3, 400);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(648, 50);
            this.lblStatus.Style = MetroFramework.MetroColorStyle.Red;
            this.lblStatus.TabIndex = 88;
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblStatus.UseStyleColors = true;
            // 
            // metroPanel1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.metroPanel1, 3);
            this.metroPanel1.Controls.Add(this.metroLabel11);
            this.metroPanel1.Controls.Add(this.metroLabel10);
            this.metroPanel1.Controls.Add(this.metroLabel9);
            this.metroPanel1.Controls.Add(this.metroLabel8);
            this.metroPanel1.Controls.Add(this.metroLabel7);
            this.metroPanel1.Controls.Add(this.metroLabel6);
            this.metroPanel1.Controls.Add(this.metroLabel5);
            this.metroPanel1.Controls.Add(this.metroLabel4);
            this.metroPanel1.Controls.Add(this.metroLabel2);
            this.metroPanel1.Controls.Add(this.txtDriverRes);
            this.metroPanel1.Controls.Add(this.txtVehicleCodes);
            this.metroPanel1.Controls.Add(this.txtValidDate);
            this.metroPanel1.Controls.Add(this.txtGender);
            this.metroPanel1.Controls.Add(this.txtIssueNo);
            this.metroPanel1.Controls.Add(this.txtIssueDate);
            this.metroPanel1.Controls.Add(this.txtLicNo);
            this.metroPanel1.Controls.Add(this.txtBDate);
            this.metroPanel1.Controls.Add(this.txtIDNo);
            this.metroPanel1.Controls.Add(this.txtIniSurname);
            this.metroPanel1.Controls.Add(this.pictureBox1);
            this.metroPanel1.Controls.Add(this.metroLabel1);
            this.metroPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(3, 3);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(648, 394);
            this.metroPanel1.TabIndex = 0;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.Location = new System.Drawing.Point(297, 146);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(71, 20);
            this.metroLabel11.TabIndex = 86;
            this.metroLabel11.Text = "Birth Date";
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(291, 117);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(78, 20);
            this.metroLabel10.TabIndex = 85;
            this.metroLabel10.Text = "ID Number";
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(252, 316);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(121, 20);
            this.metroLabel9.TabIndex = 84;
            this.metroLabel9.Text = "Driver Restrictions";
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(275, 287);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(97, 20);
            this.metroLabel8.TabIndex = 83;
            this.metroLabel8.Text = "Vehicle Codes";
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(329, 258);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(37, 20);
            this.metroLabel7.TabIndex = 82;
            this.metroLabel7.Text = "Valid";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(268, 229);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(104, 20);
            this.metroLabel6.TabIndex = 81;
            this.metroLabel6.Text = "Issued Number";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(290, 200);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(81, 20);
            this.metroLabel5.TabIndex = 80;
            this.metroLabel5.Text = "Issued Date";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(262, 171);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(112, 20);
            this.metroLabel4.TabIndex = 79;
            this.metroLabel4.Text = "License Number";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(239, 88);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(132, 20);
            this.metroLabel2.TabIndex = 78;
            this.metroLabel2.Text = "Initials and Surname";
            // 
            // txtDriverRes
            // 
            this.txtDriverRes.Lines = new string[0];
            this.txtDriverRes.Location = new System.Drawing.Point(371, 316);
            this.txtDriverRes.MaxLength = 32767;
            this.txtDriverRes.Name = "txtDriverRes";
            this.txtDriverRes.PasswordChar = '\0';
            this.txtDriverRes.ReadOnly = true;
            this.txtDriverRes.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtDriverRes.SelectedText = "";
            this.txtDriverRes.Size = new System.Drawing.Size(260, 23);
            this.txtDriverRes.TabIndex = 77;
            this.txtDriverRes.UseSelectable = true;
            // 
            // txtVehicleCodes
            // 
            this.txtVehicleCodes.Lines = new string[0];
            this.txtVehicleCodes.Location = new System.Drawing.Point(371, 287);
            this.txtVehicleCodes.MaxLength = 32767;
            this.txtVehicleCodes.Name = "txtVehicleCodes";
            this.txtVehicleCodes.PasswordChar = '\0';
            this.txtVehicleCodes.ReadOnly = true;
            this.txtVehicleCodes.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtVehicleCodes.SelectedText = "";
            this.txtVehicleCodes.Size = new System.Drawing.Size(260, 23);
            this.txtVehicleCodes.TabIndex = 76;
            this.txtVehicleCodes.UseSelectable = true;
            // 
            // txtValidDate
            // 
            this.txtValidDate.Lines = new string[0];
            this.txtValidDate.Location = new System.Drawing.Point(371, 258);
            this.txtValidDate.MaxLength = 32767;
            this.txtValidDate.Name = "txtValidDate";
            this.txtValidDate.PasswordChar = '\0';
            this.txtValidDate.ReadOnly = true;
            this.txtValidDate.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtValidDate.SelectedText = "";
            this.txtValidDate.Size = new System.Drawing.Size(260, 23);
            this.txtValidDate.TabIndex = 75;
            this.txtValidDate.UseSelectable = true;
            // 
            // txtGender
            // 
            this.txtGender.Lines = new string[0];
            this.txtGender.Location = new System.Drawing.Point(555, 113);
            this.txtGender.MaxLength = 32767;
            this.txtGender.Name = "txtGender";
            this.txtGender.PasswordChar = '\0';
            this.txtGender.ReadOnly = true;
            this.txtGender.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtGender.SelectedText = "";
            this.txtGender.Size = new System.Drawing.Size(76, 23);
            this.txtGender.TabIndex = 74;
            this.txtGender.UseSelectable = true;
            // 
            // txtIssueNo
            // 
            this.txtIssueNo.Lines = new string[0];
            this.txtIssueNo.Location = new System.Drawing.Point(371, 229);
            this.txtIssueNo.MaxLength = 32767;
            this.txtIssueNo.Name = "txtIssueNo";
            this.txtIssueNo.PasswordChar = '\0';
            this.txtIssueNo.ReadOnly = true;
            this.txtIssueNo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtIssueNo.SelectedText = "";
            this.txtIssueNo.Size = new System.Drawing.Size(260, 23);
            this.txtIssueNo.TabIndex = 73;
            this.txtIssueNo.UseSelectable = true;
            // 
            // txtIssueDate
            // 
            this.txtIssueDate.Lines = new string[0];
            this.txtIssueDate.Location = new System.Drawing.Point(371, 200);
            this.txtIssueDate.MaxLength = 32767;
            this.txtIssueDate.Name = "txtIssueDate";
            this.txtIssueDate.PasswordChar = '\0';
            this.txtIssueDate.ReadOnly = true;
            this.txtIssueDate.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtIssueDate.SelectedText = "";
            this.txtIssueDate.Size = new System.Drawing.Size(260, 23);
            this.txtIssueDate.TabIndex = 72;
            this.txtIssueDate.UseSelectable = true;
            // 
            // txtLicNo
            // 
            this.txtLicNo.Lines = new string[0];
            this.txtLicNo.Location = new System.Drawing.Point(371, 171);
            this.txtLicNo.MaxLength = 32767;
            this.txtLicNo.Name = "txtLicNo";
            this.txtLicNo.PasswordChar = '\0';
            this.txtLicNo.ReadOnly = true;
            this.txtLicNo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtLicNo.SelectedText = "";
            this.txtLicNo.Size = new System.Drawing.Size(260, 23);
            this.txtLicNo.TabIndex = 71;
            this.txtLicNo.UseSelectable = true;
            // 
            // txtBDate
            // 
            this.txtBDate.Lines = new string[0];
            this.txtBDate.Location = new System.Drawing.Point(371, 142);
            this.txtBDate.MaxLength = 32767;
            this.txtBDate.Name = "txtBDate";
            this.txtBDate.PasswordChar = '\0';
            this.txtBDate.ReadOnly = true;
            this.txtBDate.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtBDate.SelectedText = "";
            this.txtBDate.Size = new System.Drawing.Size(260, 23);
            this.txtBDate.TabIndex = 70;
            this.txtBDate.UseSelectable = true;
            // 
            // txtIDNo
            // 
            this.txtIDNo.Lines = new string[0];
            this.txtIDNo.Location = new System.Drawing.Point(371, 113);
            this.txtIDNo.MaxLength = 32767;
            this.txtIDNo.Name = "txtIDNo";
            this.txtIDNo.PasswordChar = '\0';
            this.txtIDNo.ReadOnly = true;
            this.txtIDNo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtIDNo.SelectedText = "";
            this.txtIDNo.Size = new System.Drawing.Size(178, 23);
            this.txtIDNo.TabIndex = 69;
            this.txtIDNo.UseSelectable = true;
            // 
            // txtIniSurname
            // 
            this.txtIniSurname.Lines = new string[0];
            this.txtIniSurname.Location = new System.Drawing.Point(371, 84);
            this.txtIniSurname.MaxLength = 32767;
            this.txtIniSurname.Name = "txtIniSurname";
            this.txtIniSurname.PasswordChar = '\0';
            this.txtIniSurname.ReadOnly = true;
            this.txtIniSurname.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtIniSurname.SelectedText = "";
            this.txtIniSurname.Size = new System.Drawing.Size(260, 23);
            this.txtIniSurname.TabIndex = 68;
            this.txtIniSurname.UseSelectable = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(17, 88);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(216, 247);
            this.pictureBox1.TabIndex = 67;
            this.pictureBox1.TabStop = false;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.XL;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel1.Location = new System.Drawing.Point(61, 18);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(522, 31);
            this.metroLabel1.TabIndex = 66;
            this.metroLabel1.Text = "Please scan the \'Barcode\' of your drivers license";
            // 
            // btnNext
            // 
            this.btnNext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNext.Enabled = false;
            this.btnNext.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnNext.Location = new System.Drawing.Point(230, 453);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(194, 94);
            this.btnNext.TabIndex = 1;
            this.btnNext.Text = "Next";
            this.btnNext.UseSelectable = true;
            this.btnNext.Visible = false;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // ScanDriverControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSize = true;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "ScanDriverControl";
            this.Size = new System.Drawing.Size(654, 550);
            this.Load += new System.EventHandler(this.Login_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroButton btnNext;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox txtDriverRes;
        private MetroFramework.Controls.MetroTextBox txtVehicleCodes;
        private MetroFramework.Controls.MetroTextBox txtValidDate;
        private MetroFramework.Controls.MetroTextBox txtGender;
        private MetroFramework.Controls.MetroTextBox txtIssueNo;
        private MetroFramework.Controls.MetroTextBox txtIssueDate;
        private MetroFramework.Controls.MetroTextBox txtLicNo;
        private MetroFramework.Controls.MetroTextBox txtBDate;
        private MetroFramework.Controls.MetroTextBox txtIDNo;
        private MetroFramework.Controls.MetroTextBox txtIniSurname;
        private System.Windows.Forms.PictureBox pictureBox1;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel lblStatus;


    }
}
