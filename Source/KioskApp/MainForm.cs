﻿using System;
using System.Drawing;
using System.Windows.Forms;
using CanTech.Common;
using CanTech.Common.Extensions;
using Castle.Core;
using iRent.Classes;
using iRent.Commands;
using iRent.Extensions;
using iRent.Properties;
using iRent.Services;
using iRentKiosk.Core.Services;
using MetroFramework.Forms;
using System.Configuration;
using System.Reflection;
using System.Net;
using iRentKiosk.Core.Util;
using iRent.Util;
using System.Threading;
using Gma.System.MouseKeyHook;

namespace iRent
{
    [CastleComponent]
    public partial class MainForm : MetroForm, IMainForm
    {
        private readonly CommandFactory _cmdFactory;
        private CancomScanner _canScanner;
 
        public string HeaderBackground { get; set; }
        public string HeaderImage { get; set; }
        public PictureBoxSizeMode? HeaderSizeMode { get; set; }

        private IKeyboardMouseEvents m_GlobalHook;

        DateTime StartTime = DateTime.Now;

        private int _inputeventcount;
        private int _inputset;
        public bool inProgress;

        //private string _appversion = null;
        //private string _executingApiUser = null;
        //private string _authorizedUser = null;
        //private string _internetStatus = null;
        //private string _serverStatus = null;

        public void Subscribe()
        {
            // Note: for the application hook, use the Hook.AppEvents() instead
            m_GlobalHook = Hook.GlobalEvents();

            m_GlobalHook.MouseDownExt += GlobalHookMouseDownExt;
            m_GlobalHook.KeyPress += GlobalHookKeyPress;
        }

        private void GlobalHookKeyPress(object sender, KeyPressEventArgs e)
        {
            //Console.WriteLine("KeyPress: \t{0}", e.KeyChar);
            _inputeventcount = _inputset;
        }

        private void GlobalHookMouseDownExt(object sender, MouseEventExtArgs e)
        {
            //Console.WriteLine("MouseDown: \t{0}; \t System Timestamp: \t{1}", e.Button, e.Timestamp);
            _inputeventcount = _inputset;

            // uncommenting the following line will suppress the middle mouse button click
            // if (e.Buttons == MouseButtons.Middle) { e.Handled = true; }
        }

        public void Unsubscribe()
        {
            m_GlobalHook.MouseDownExt -= GlobalHookMouseDownExt;
            m_GlobalHook.KeyPress -= GlobalHookKeyPress;

            //It is recommened to dispose it
            m_GlobalHook.Dispose();
        }

        private class MfStatusInfo
        {
            public string Appversion = null;
            public string ExecutingApiUser = null;
            public string AuthorizedUser = null;
            public string InternetStatus = null;
            public string ServerStatus = null;
        }

        private MfStatusInfo _statusInfo = new MfStatusInfo();


        public string AppVersion
        {
            get
            {
                lock (_statusInfo)
                {
                    return _statusInfo.Appversion;
                }
            }
            set
            {
                lock (_statusInfo)
                {
                    _statusInfo.Appversion = value;
                }
            }
        }

        public string ExecutingApiUser
        {
            get
            {
                lock (_statusInfo)
                {
                    return _statusInfo.ExecutingApiUser;
                }
            }
            set
            {
                lock (_statusInfo)
                {
                    _statusInfo.ExecutingApiUser = value;
                }
            }
        }

        public string AuthorizedUser
        {
            get
            {
                lock (_statusInfo)
                {
                    return _statusInfo.AuthorizedUser;
                }
            }
            set
            {
                lock (_statusInfo)
                {
                    _statusInfo.AuthorizedUser = value;
                }
            }
        }

        public string InternetStatus
        {
            get
            {
                lock (_statusInfo)
                {
                    return _statusInfo.InternetStatus;
                }
            }
            set
            {
                lock (_statusInfo)
                {
                    _statusInfo.InternetStatus = value;
                }
            }
        }

        public string ServerStatus
        {
            get
            {
                lock (_statusInfo)
                {
                    return _statusInfo.ServerStatus;
                }
            }
            set
            {
                lock (_statusInfo)
                {
                    _statusInfo.ServerStatus = value;
                }
            }
        }

        public MainForm(CommandFactory cmdFactory)
        {
            _cmdFactory = cmdFactory;
            _cmdFactory.Mainform = this;
            InitializeComponent();
            ApplicationWaitCursor.Cursor = Cursors.WaitCursor;
            ApplicationWaitCursor.Delay = new TimeSpan(0, 0, 0, 0, 250);
        }

        private void SetVersionInfoText()
        {
            lblVersionInfo.Text = "";
            if (_statusInfo.AuthorizedUser != null)
            {
                lblVersionInfo.Text += "Authorized Employee no: "+ _statusInfo.AuthorizedUser;
            }

            if (_statusInfo.InternetStatus != null)
            {
                lblVersionInfo.Text += "    Internet Status: " + _statusInfo.InternetStatus;
            }

            if (_statusInfo.ServerStatus != null)
            {
                lblVersionInfo.Text += "    Server Connection: " + _statusInfo.ServerStatus;
            }

            if (_statusInfo.ExecutingApiUser != null)
            {
                lblVersionInfo.Text += "    Kiosk No : " + _statusInfo.ExecutingApiUser;
            }

            if (_statusInfo.Appversion != null)
            {
                lblVersionInfo.Text += "    Kiosk Version : " + _statusInfo.Appversion;
            }
        }

        private void frmIrent_Load(object sender, EventArgs e)
        {
            _inputset = Convert.ToInt32(ConfigurationManager.AppSettings["InactivityTime"]);
            _inputeventcount = _inputset;
            inProgress = false;
            Subscribe();


            if (HeaderBackground.HasValue())
                picHeader.BackColor = ColorTranslator.FromHtml(HeaderBackground);
            if (HeaderImage.HasValue())
            {
                picHeader.Image = Image.FromFile(HeaderImage);
            }
            if (HeaderSizeMode.HasValue)
                picHeader.SizeMode = HeaderSizeMode.Value;


            if (ConfigurationManager.AppSettings["uimode"] == "desktop")
            {
                ControlBox = true;
            }
            else
            {
                ControlBox = false;
            }

            AppVersion = Assembly.GetExecutingAssembly().GetCustomAttribute<AssemblyFileVersionAttribute>().Version;


            if (ConfigurationManager.AppSettings["RequireScanner"] == "true")
            {
                //Test for Hardware. If hardware exists then continue else show error.
                Text = String.Empty;
                _canScanner = new CancomScanner();
                var result = _canScanner.DetectScanners(Settings.Default.ScanComPort);
                if (!result)
                {
                    //Show error. Hardware not detected. close application.
                    MessageForm.Show("Scanner not found.", "Scanner could not be detected. Please ensure it is connected. If the problem persist contact your administrator.", "OK");
                    Application.Exit();
                }
            }

            //Test for internet connectivity.
            try
            {
                using (var client = new WebClient())
                {
                    using (var stream = client.OpenRead("http://www.google.com"))
                    {
                        //HasInternet = true;
                        //show Internet indicator.
                        InternetStatus = "Connected";
                    }
                }
            }
            catch
            {
                MessageForm.Show("No Internet detected.", "No Internet connection could be detected. Please ensure your computer is on a network with access to the internet.", "OK");
                Application.Exit();
            }

            //Test for Server connectivity.
            try
            {
                using (var client = new WebClient())
                {
                    using (var stream = client.OpenRead("http://www.google.com"))
                    {
                        //show server indicator.
                        ServerStatus = "Up";
                    }
                }
            }
            catch
            {
                MessageForm.Show("Could not connect to Server.", "Could not connect to server. Please ensure your computer can access the resource.", "OK");
                Application.Exit();
            }
            SetVersionInfoText();
            Reset();
        }


        private void frmIrent_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_canScanner != null)
            {
                _canScanner.Dispose();
            }
            if (m_GlobalHook != null)
            {
                Unsubscribe();
            }
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            if (ConfirmMessageForm.Show("Cancel process.", "Are you sure you want to cancel the process", "Yes", "No", false, 15) == DialogResult.Yes)
            {
                Reset();
            }
        }

        public void Reset()
        {
            _cmdFactory.Invoke<ResetCommand>();
            inProgress = false;
            if (!timer1.Enabled)
                timer1.Start();
        }

        public void Accept()
        {
            if (UIFactory.Accept)
            {
                var next = UIFactory.NextControl;
                UIFactory.NextControl = "";
                ClearForm();

                var cmd = _cmdFactory.GetCommand(next, null, this);
                if (cmd != null)
                    cmd.Invoke();
                else
                    IoC.Resolve<RfidService>().ReturnCard();
            }

            UIFactory.Accept = false;
        }

        public void Back()
        {
            ClearForm();
            var cmd = _cmdFactory.GetCommand(UIFactory.PreviousControl, null, this);
            if (cmd != null)
                cmd.Invoke();
        }

        delegate void AddControLDelegate(Control c);

        public void AddToFormPanel(Control control)
        {
            if (this.InvokeRequired)
            {
                AddControLDelegate c = new AddControLDelegate(AddToFormPanel);
                this.Invoke(c, new object[] { control });
            }
            else
            {
                ClearForm();
                control.Anchor = AnchorStyles.None;
                control.Height = pnlForm.Height;
                control.Left = (pnlForm.ClientSize.Width - control.Width) / 2;
                control.Top = (pnlForm.ClientSize.Height - control.Height) / 2;
                pnlForm.Controls.Add(control);
            }
        }

        public void ClearForm()
        {
            this.InvokeIfRequired(x =>
            {
                foreach (Control ctl in pnlForm.Controls)
                    ctl.Dispose();
                pnlForm.Controls.Clear();
            });
        }

        public void ToggleHomeButton(bool visible)
        {
            this.InvokeIfRequired(x => btnHome.Visible = visible);
            //this.InvokeIfRequired(x => btnNext.Visible = visible);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            var cmd = _cmdFactory.GetCommand(UIFactory.CurrentControl, null, this);
            if (cmd != null)
                cmd.Invoke();
        }


        private void timer1_Tick(object sender, EventArgs e)
        {
            if (inProgress)
            {
                if (_inputeventcount > 0)
                {
                    _inputeventcount = _inputeventcount - timer1.Interval;
                }
                else
                {
                    timer1.Stop();
                    if (ConfirmMessageForm.Show("No Activity.", "There has been no activity for 1 minute.", "Cancel", "Continue", false, 15) == DialogResult.Yes)
                    {
                        Reset();
                    }
                    else
                    {
                        _inputeventcount = Convert.ToInt32(ConfigurationManager.AppSettings["InactivityTime"]);
                        timer1.Start();
                    }

                }
            }
            if (DateTime.Now.Subtract(StartTime).TotalSeconds > 20)
            {
                StartTime = DateTime.Now;
                try
                {
                    using (var client = new WebClient())
                    {
                        using (var stream = client.OpenRead("http://www.google.com"))
                        {
                            //HasInternet = true;
                            //show Internet indicator.
                            InternetStatus = "Connected";
                        }
                    }
                }
                catch
                {
                    MessageForm.Show("Could not connect to Internet.", "Sorry I have lost connectivity. You will be redirect to the start.", "OK", false);
                    Reset();
                    InternetStatus = "Disconnected!";
                }

                //Test for Server connectivity.
                try
                {
                    using (var client = new WebClient())
                    {
                        using (var stream = client.OpenRead("http://www.google.com"))
                        {
                            //show server indicator.
                            ServerStatus = "Up";
                        }
                    }
                }
                catch
                {
                    MessageForm.Show("Could not connect to Server.", "Sorry I have lost connectivity. You will be redirect to the start.", "OK", false);
                    Reset();
                    ServerStatus = "Down";
                }
            }

            SetVersionInfoText();
        }
    }
}