﻿namespace iRent
{
    partial class RegisterStep1
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing && (components != null))
        //    {
        //        components.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtIniSurname = new MetroFramework.Controls.MetroTextBox();
            this.txtIDNo = new MetroFramework.Controls.MetroTextBox();
            this.txtBDate = new MetroFramework.Controls.MetroTextBox();
            this.txtLicNo = new MetroFramework.Controls.MetroTextBox();
            this.txtIssueDate = new MetroFramework.Controls.MetroTextBox();
            this.txtIssueNo = new MetroFramework.Controls.MetroTextBox();
            this.txtGender = new MetroFramework.Controls.MetroTextBox();
            this.txtValidDate = new MetroFramework.Controls.MetroTextBox();
            this.txtVehicleCodes = new MetroFramework.Controls.MetroTextBox();
            this.txtDriverRes = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel1.Location = new System.Drawing.Point(151, 20);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(261, 25);
            this.metroLabel1.TabIndex = 0;
            this.metroLabel1.Text = "Please scan your driver\'s license";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(3, 78);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(216, 247);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // txtIniSurname
            // 
            this.txtIniSurname.Lines = new string[0];
            this.txtIniSurname.Location = new System.Drawing.Point(357, 74);
            this.txtIniSurname.MaxLength = 32767;
            this.txtIniSurname.Name = "txtIniSurname";
            this.txtIniSurname.PasswordChar = '\0';
            this.txtIniSurname.ReadOnly = true;
            this.txtIniSurname.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtIniSurname.SelectedText = "";
            this.txtIniSurname.Size = new System.Drawing.Size(260, 23);
            this.txtIniSurname.TabIndex = 2;
            this.txtIniSurname.UseSelectable = true;
            // 
            // txtIDNo
            // 
            this.txtIDNo.Lines = new string[0];
            this.txtIDNo.Location = new System.Drawing.Point(357, 103);
            this.txtIDNo.MaxLength = 32767;
            this.txtIDNo.Name = "txtIDNo";
            this.txtIDNo.PasswordChar = '\0';
            this.txtIDNo.ReadOnly = true;
            this.txtIDNo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtIDNo.SelectedText = "";
            this.txtIDNo.Size = new System.Drawing.Size(178, 23);
            this.txtIDNo.TabIndex = 3;
            this.txtIDNo.UseSelectable = true;
            // 
            // txtBDate
            // 
            this.txtBDate.Lines = new string[0];
            this.txtBDate.Location = new System.Drawing.Point(357, 132);
            this.txtBDate.MaxLength = 32767;
            this.txtBDate.Name = "txtBDate";
            this.txtBDate.PasswordChar = '\0';
            this.txtBDate.ReadOnly = true;
            this.txtBDate.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtBDate.SelectedText = "";
            this.txtBDate.Size = new System.Drawing.Size(260, 23);
            this.txtBDate.TabIndex = 4;
            this.txtBDate.UseSelectable = true;
            // 
            // txtLicNo
            // 
            this.txtLicNo.Lines = new string[0];
            this.txtLicNo.Location = new System.Drawing.Point(357, 161);
            this.txtLicNo.MaxLength = 32767;
            this.txtLicNo.Name = "txtLicNo";
            this.txtLicNo.PasswordChar = '\0';
            this.txtLicNo.ReadOnly = true;
            this.txtLicNo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtLicNo.SelectedText = "";
            this.txtLicNo.Size = new System.Drawing.Size(260, 23);
            this.txtLicNo.TabIndex = 5;
            this.txtLicNo.UseSelectable = true;
            // 
            // txtIssueDate
            // 
            this.txtIssueDate.Lines = new string[0];
            this.txtIssueDate.Location = new System.Drawing.Point(357, 190);
            this.txtIssueDate.MaxLength = 32767;
            this.txtIssueDate.Name = "txtIssueDate";
            this.txtIssueDate.PasswordChar = '\0';
            this.txtIssueDate.ReadOnly = true;
            this.txtIssueDate.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtIssueDate.SelectedText = "";
            this.txtIssueDate.Size = new System.Drawing.Size(260, 23);
            this.txtIssueDate.TabIndex = 6;
            this.txtIssueDate.UseSelectable = true;
            // 
            // txtIssueNo
            // 
            this.txtIssueNo.Lines = new string[0];
            this.txtIssueNo.Location = new System.Drawing.Point(357, 219);
            this.txtIssueNo.MaxLength = 32767;
            this.txtIssueNo.Name = "txtIssueNo";
            this.txtIssueNo.PasswordChar = '\0';
            this.txtIssueNo.ReadOnly = true;
            this.txtIssueNo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtIssueNo.SelectedText = "";
            this.txtIssueNo.Size = new System.Drawing.Size(260, 23);
            this.txtIssueNo.TabIndex = 7;
            this.txtIssueNo.UseSelectable = true;
            // 
            // txtGender
            // 
            this.txtGender.Lines = new string[0];
            this.txtGender.Location = new System.Drawing.Point(541, 103);
            this.txtGender.MaxLength = 32767;
            this.txtGender.Name = "txtGender";
            this.txtGender.PasswordChar = '\0';
            this.txtGender.ReadOnly = true;
            this.txtGender.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtGender.SelectedText = "";
            this.txtGender.Size = new System.Drawing.Size(76, 23);
            this.txtGender.TabIndex = 8;
            this.txtGender.UseSelectable = true;
            // 
            // txtValidDate
            // 
            this.txtValidDate.Lines = new string[0];
            this.txtValidDate.Location = new System.Drawing.Point(357, 248);
            this.txtValidDate.MaxLength = 32767;
            this.txtValidDate.Name = "txtValidDate";
            this.txtValidDate.PasswordChar = '\0';
            this.txtValidDate.ReadOnly = true;
            this.txtValidDate.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtValidDate.SelectedText = "";
            this.txtValidDate.Size = new System.Drawing.Size(260, 23);
            this.txtValidDate.TabIndex = 9;
            this.txtValidDate.UseSelectable = true;
            // 
            // txtVehicleCodes
            // 
            this.txtVehicleCodes.Lines = new string[0];
            this.txtVehicleCodes.Location = new System.Drawing.Point(357, 277);
            this.txtVehicleCodes.MaxLength = 32767;
            this.txtVehicleCodes.Name = "txtVehicleCodes";
            this.txtVehicleCodes.PasswordChar = '\0';
            this.txtVehicleCodes.ReadOnly = true;
            this.txtVehicleCodes.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtVehicleCodes.SelectedText = "";
            this.txtVehicleCodes.Size = new System.Drawing.Size(260, 23);
            this.txtVehicleCodes.TabIndex = 10;
            this.txtVehicleCodes.UseSelectable = true;
            // 
            // txtDriverRes
            // 
            this.txtDriverRes.Lines = new string[0];
            this.txtDriverRes.Location = new System.Drawing.Point(357, 306);
            this.txtDriverRes.MaxLength = 32767;
            this.txtDriverRes.Name = "txtDriverRes";
            this.txtDriverRes.PasswordChar = '\0';
            this.txtDriverRes.ReadOnly = true;
            this.txtDriverRes.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtDriverRes.SelectedText = "";
            this.txtDriverRes.Size = new System.Drawing.Size(260, 23);
            this.txtDriverRes.TabIndex = 11;
            this.txtDriverRes.UseSelectable = true;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(225, 78);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(126, 19);
            this.metroLabel2.TabIndex = 12;
            this.metroLabel2.Text = "Initials and Surname";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(248, 161);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(103, 19);
            this.metroLabel4.TabIndex = 14;
            this.metroLabel4.Text = "License Number";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(276, 190);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(75, 19);
            this.metroLabel5.TabIndex = 15;
            this.metroLabel5.Text = "Issued Date";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(254, 219);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(97, 19);
            this.metroLabel6.TabIndex = 16;
            this.metroLabel6.Text = "Issued Number";
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(315, 248);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(36, 19);
            this.metroLabel7.TabIndex = 17;
            this.metroLabel7.Text = "Valid";
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(261, 277);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(90, 19);
            this.metroLabel8.TabIndex = 18;
            this.metroLabel8.Text = "Vehicle Codes";
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(238, 306);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(113, 19);
            this.metroLabel9.TabIndex = 19;
            this.metroLabel9.Text = "Driver Restrictions";
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(277, 107);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(74, 19);
            this.metroLabel10.TabIndex = 20;
            this.metroLabel10.Text = "ID Number";
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.Location = new System.Drawing.Point(283, 136);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(68, 19);
            this.metroLabel11.TabIndex = 21;
            this.metroLabel11.Text = "Birth Date";
            // 
            // RegisterStep1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.metroLabel11);
            this.Controls.Add(this.metroLabel10);
            this.Controls.Add(this.metroLabel9);
            this.Controls.Add(this.metroLabel8);
            this.Controls.Add(this.metroLabel7);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.txtDriverRes);
            this.Controls.Add(this.txtVehicleCodes);
            this.Controls.Add(this.txtValidDate);
            this.Controls.Add(this.txtGender);
            this.Controls.Add(this.txtIssueNo);
            this.Controls.Add(this.txtIssueDate);
            this.Controls.Add(this.txtLicNo);
            this.Controls.Add(this.txtBDate);
            this.Controls.Add(this.txtIDNo);
            this.Controls.Add(this.txtIniSurname);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.metroLabel1);
            this.Name = "RegisterStep1";
            this.Size = new System.Drawing.Size(676, 369);
            this.Load += new System.EventHandler(this.RegisterStep1_Load);
            this.ControlRemoved += new System.Windows.Forms.ControlEventHandler(this.RegisterStep1_ControlRemoved);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel metroLabel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private MetroFramework.Controls.MetroTextBox txtIniSurname;
        private MetroFramework.Controls.MetroTextBox txtIDNo;
        private MetroFramework.Controls.MetroTextBox txtBDate;
        private MetroFramework.Controls.MetroTextBox txtLicNo;
        private MetroFramework.Controls.MetroTextBox txtIssueDate;
        private MetroFramework.Controls.MetroTextBox txtIssueNo;
        private MetroFramework.Controls.MetroTextBox txtGender;
        private MetroFramework.Controls.MetroTextBox txtValidDate;
        private MetroFramework.Controls.MetroTextBox txtVehicleCodes;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private MetroFramework.Controls.MetroTextBox txtDriverRes;

    }
}
