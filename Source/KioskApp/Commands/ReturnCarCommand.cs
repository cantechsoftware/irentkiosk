﻿using System.Windows.Forms;
using Castle.Core;
using iRent.Services;
using iRentKiosk.Core.Models;
using iRentKiosk.Core.Services;

namespace iRent.Commands
{
    [CastleComponent]
    public class ReturnCarCommand : ICommand
    {
        private readonly RfidService _rfidService;
        private readonly WorkflowService _workflowService;

        public ReturnCarCommand(WorkflowService workflowService, RfidService rfidService)
        {
            _workflowService = workflowService;
            _rfidService = rfidService;
        }


        public string Next { get; set; }
        public Form MainForm { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }

        public void Invoke()
        {
            if (Return())
                _workflowService.Execute(Next);
        }

        private bool Return()
        {
            return _rfidService.ReturnCard();
        }
    }
}