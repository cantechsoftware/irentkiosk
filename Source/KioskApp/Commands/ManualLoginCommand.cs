﻿using System;
using System.Windows.Forms;
using Castle.Core;
using iRent.Classes;
using iRent.Login;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Models;
using iRentKiosk.Core.Services;

namespace iRent.Commands
{
    [CastleComponent]
    public class ManualLoginCommand : ICommand
    {
        private readonly Func<ManualLogin> _factory;
        private MainForm _form;
        private readonly WorkflowService _workflowService;

        public ManualLoginCommand(MainForm form, Func<ManualLogin> factory, WorkflowService workflowService)
        {
            _form = form;
            _factory = factory;
            _workflowService = workflowService;
        }

        public Form MainForm
        {
            get { return _form; }
            set { _form = (MainForm)value; }
        }

        public string Registered { get; set; }
        public string NotRegistered { get; set; }
        public string ExistingCard { get; set; }

        public void Invoke()
        {
            var control = _factory();
            control.Dock = DockStyle.Fill;
            _form.AddToFormPanel(control);
            control.Next += OnNext;
        }

        private void OnNext(object sender, DriverModel driver)
        {
            ContextData.SetDriver(driver);

            if (driver.Licence == null || !driver.IsRegistered)
                _workflowService.Execute(NotRegistered);
            else if (driver.RfId != null)
                _workflowService.Execute(ExistingCard);
            else
                _workflowService.Execute(Registered);
        }
    }
}