﻿using System;
using System.Windows.Forms;
using Castle.Core;
using iRent.Classes;
using iRent.ViewModels;
using iRentKiosk.Core.Models;
using iRentKiosk.Core.Services;
using iRent.StaffAuthentication;

namespace iRent.Commands
{
    [CastleComponent]
    public class MainMenuCommand : ICommand
    {
        private readonly Func<MainMenu> _factory;
        private MainForm _mainForm;
        private readonly WorkflowService _workflowService;
        private StaffAuthenticationService _staffAuthSvc;

        public Form MainForm
        {
            get { return _mainForm; }
            set { _mainForm = (MainForm)value; }
        }

        public MainMenuCommand(MainForm mainForm,
                               Func<MainMenu> factory,
                               WorkflowService workflowService,
                               StaffAuthenticationService staffAuthSvc)
        {
            _mainForm = mainForm;
            _factory = factory;
            _workflowService = workflowService;
            _staffAuthSvc = staffAuthSvc;
        }

        public MenuOption[] Options { get; set; }

        public void Invoke()
        {
            UIFactory.Accept = false;
            var control = _factory();
            control.Dock = DockStyle.Fill;
            control.SetOptions(Options);
            _mainForm.AddToFormPanel(control);
            _mainForm.ToggleHomeButton(false);
            control.OptionClicked += OnOptionClicked;
        }

        private void OnOptionClicked(object sender, MenuOption opt)
        {
            _mainForm.inProgress = true;
            _workflowService.StartWorkflow(opt.Workflow, null);
        }
    }
}