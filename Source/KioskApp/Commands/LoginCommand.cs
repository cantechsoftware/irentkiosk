﻿using System;
using CanTech.Common.Extensions;
using Castle.Core;
using iRent.Framework;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Services;

namespace iRent.Commands
{
    [CastleComponent]
    public class LoginCommand : BaseCommand
    {
        private readonly Func<Login.Login> _factory;

        public LoginCommand(Func<Login.Login> factory)
        {
            _factory = factory;
        }

        public string Registered { get; set; }
        public string NotRegistered { get; set; }
        public string ExistingCard { get; set; }
        public string Back { get; set; }

        public override void Invoke()
        {
            var control = _factory();
            control.ClearControls();
            var main = GetFormAs<MainForm>();
            control.Dock = System.Windows.Forms.DockStyle.Fill;
            main.AddToFormPanel(control);
            main.ToggleHomeButton(true);
            control.Next += OnNext;
            control.Back += OnBack;
        }

        private void OnBack(object sender, DriverModel driver)
        {
            Proceed(Back);
        }

        private void OnNext(object sender, DriverModel driver)
        {

            ContextData.SetDriver(driver);

            if ((driver.Licence == null) || !driver.IsRegistered)
                Proceed(NotRegistered);
            else if (ExistingCard.HasValue() && (driver.RfId != null))
                Proceed(ExistingCard);
            else
                Proceed(Registered);
        }
    }
}