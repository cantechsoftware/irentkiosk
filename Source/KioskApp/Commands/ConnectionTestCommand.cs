﻿using System.Net;
using Castle.Core;
using iRent.Framework;
using iRent.StaffAuthentication;
using iRentKiosk.Core.Services;
using iRent.Classes;
using iRent.Properties;
using System.Configuration;
using System.Windows.Forms;
using iRent.Util;

namespace iRent.Commands
{
    [CastleComponent]
    public class ConnectionTestCommand : BaseCommand
    {
        private readonly WorkflowService _workflowService;
        private readonly StaffAuthenticationService _staffAuthService;
        private MainForm _form;

        public ConnectionTestCommand(MainForm form, WorkflowService workflowService, StaffAuthenticationService staffAuthenticationService)
        {
            _form = form;
            _workflowService = workflowService;
            _staffAuthService = staffAuthenticationService;
        }

        public string Next { get; set; }
        public string Failed { get; set; }

        public override void Invoke()
        {
            CancomScanner _canScanner = new CancomScanner();
            var result = _canScanner.DetectScanners(Settings.Default.ScanComPort);
            if (!result)
            {
                //Show error. Hardware not detected. close application.
                MessageForm.Show("Scanner not found.", "Scanner could not be detected. Please ensure it is connected. If the problem persist contact your administrator.", "OK");
                _workflowService.Execute(Failed);
                return;
            }

            //Test for internet connectivity.
            try
            {
                using (var client = new WebClient())
                {
                    using (var stream = client.OpenRead("http://www.google.com"))
                    {
                        _form.InternetStatus = "Up.";
                        //HasInternet = true;
                        //show Internet indicator.
                    }
                }
            }
            catch
            {
                MessageForm.Show("No Internet detected.", "No Internet connection could be detected. Please ensure your computer is on a network with access to the internet.", "OK");
                _form.InternetStatus = "Down.";
                _form.ServerStatus = "Disconnected!";
                _workflowService.Execute(Failed);
                return;
            }

            //Test for Server connectivity.
            try
            {
                using (var client = new WebClient())
                {
                    using (var stream = client.OpenRead("http://bcr.canrent.co.za/"))
                    {
                        //show server indicator.
                        _form.ServerStatus = "Connected.";
                    }
                }
            }
            catch
            {
                MessageForm.Show("Could not connect to Server.", "Could not connect to server. Please ensure your computer can access the resource.", "OK");
                _form.ServerStatus = "Disconnected!";
                _workflowService.Execute(Failed);
                return;
            }
            _workflowService.Execute(Next);
        }
    }
}
