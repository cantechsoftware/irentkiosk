﻿using System;
using System.Windows.Forms;
using Castle.Core;
using iRent.Login;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Models;
using iRentKiosk.Core.Services;

namespace iRent.Commands
{
    [CastleComponent]
    public class ManualLicenceCommand : ICommand
    {
        private readonly Func<ManualLicence> _factory;
        private MainForm _form;
        private readonly WorkflowService _workflowService;

        public Form MainForm
        {
            get { return _form; }
            set { _form = (MainForm)value; }
        }

        public ManualLicenceCommand(MainForm form,
                                    Func<ManualLicence> factory,
                                    WorkflowService workflowService)
        {
            _form = form;
            _factory = factory;
            _workflowService = workflowService;
        }

        public string Next { get; set; }

        public void Invoke()
        {
            var control = _factory();
            control.Dock = DockStyle.Fill;
            _form.AddToFormPanel(control);
            control.Next += OnNext;
        }

        private void OnNext(object sender, DriverModel driver)
        {
            _workflowService.Execute(Next);
        }
    }
}