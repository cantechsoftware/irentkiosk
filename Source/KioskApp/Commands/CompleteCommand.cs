﻿using System.Windows.Forms;
using Castle.Core;
using iRentKiosk.Core.Models;
using iRentKiosk.Core.Services;

namespace iRent.Commands
{
    [CastleComponent]
    public class CompleteCommand : ICommand
    {
        private readonly WorkflowService _workflowService;

        public CompleteCommand(WorkflowService workflowService)
        {
            _workflowService = workflowService;
        }

        public Form MainForm { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }

        public void Invoke()
        {
            _workflowService.CompleteWorkflow();
        }
    }
}