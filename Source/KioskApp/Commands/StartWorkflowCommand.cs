﻿using System.Windows.Forms;
using Castle.Core;
using iRentKiosk.Core.Models;
using iRentKiosk.Core.Services;

namespace iRent.Commands
{
    [CastleComponent]
    public class StartWorkflowCommand : ICommand
    {
        private readonly WorkflowService _workflowService;

        public string Workflow { get; set; }
        public string Next { get; set; }
        public Form MainForm { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }

        public StartWorkflowCommand(WorkflowService workflowService)
        {
            _workflowService = workflowService;
        }

        public void Invoke()
        {
            _workflowService.StartWorkflow(Workflow, Next);
        }
    }
}