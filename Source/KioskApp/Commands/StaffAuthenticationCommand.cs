﻿using Castle.Core;
using iRent.Framework;
using iRent.StaffAuthentication;
using iRentKiosk.Core.Services;

namespace iRent.Commands
{
    [CastleComponent]
    public class StaffAuthenticationCommand : BaseCommand
    {
        private readonly WorkflowService _workflowService;
        private readonly StaffAuthenticationService _staffAuthService;

        public StaffAuthenticationCommand(WorkflowService workflowService, StaffAuthenticationService staffAuthenticationService)
        {
            _workflowService = workflowService;
            _staffAuthService = staffAuthenticationService;
        }

        public string Next { get; set; }
        public string Failed { get; set; }

        public override void Invoke()
        {
            if (!StaffAuthenticationRequired())
                _workflowService.Execute(Next);
            else
                _workflowService.Execute(_staffAuthService.CheckAuthentication() ? Next : Failed);
        }

        private bool StaffAuthenticationRequired()
        {
            return ContextData.Kiosk.AuthRequired;
        }
    }
}