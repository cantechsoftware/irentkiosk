﻿using System;
using System.Windows.Forms;
using Castle.Core;
using iRent.Bookings;
using iRentKiosk.Core.Models;

namespace iRent.Commands
{
    [CastleComponent]
    public class ReturnDateTimeCommand : ICommand
    {
        private readonly Func<ReturnDetailsControl> _factory;
        private MainForm _form;

        public ReturnDateTimeCommand(MainForm form, Func<ReturnDetailsControl> factory)
        {
            _form = form;
            _factory = factory;
        }

        public Form MainForm
        {
            get { return _form; }
            set { _form = (MainForm)value; }
        }

        public void Invoke()
        {
            var control = _factory();
            control.Dock = DockStyle.Fill;
            _form.AddToFormPanel(control);
        }
    }
}