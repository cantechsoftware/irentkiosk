﻿using System;
using System.Windows.Forms;
using Castle.Core;
using iRentKiosk.Core.Models;

namespace iRent.Commands
{
    [CastleComponent]
    public class InsuranceCommand : ICommand
    {
        private readonly Func<Insurance> _factory;
        private MainForm _form;

        public Form MainForm
        {
            get { return _form; }
            set { _form = (MainForm)value; }
        }

        public InsuranceCommand(MainForm form, Func<Insurance> factory)
        {
            _form = form;
            _factory = factory;
        }

        public void Invoke()
        {
            var control = _factory();
            control.Dock = DockStyle.Fill;            
            _form.AddToFormPanel(control);
        }
    }
}