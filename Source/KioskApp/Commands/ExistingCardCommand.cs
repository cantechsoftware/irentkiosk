﻿using System;
using System.Windows.Forms;
using Castle.Core;
using iRentKiosk.Core.Models;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;

namespace iRent.Commands
{
    [CastleComponent]
    public class ExistingCardCommand : ICommand
    {
        private readonly DriverApi _driverApi;
        private readonly Func<ExistingCardControl> _factory;
        private MainForm _form;
        private readonly WorkflowService _workflowService;

        public ExistingCardCommand(MainForm form,
            Func<ExistingCardControl> factory,
            DriverApi driverApi, 
            WorkflowService workflowService)
        {
            _form = form;
            _factory = factory;
            _driverApi = driverApi;
            _workflowService = workflowService;
        }

        public Form MainForm
        {
            get { return _form; }
            set { _form = (MainForm)value; }
        }

        public string Terminate { get; set; }
        public string Return { get; set; }

        public void Invoke()
        {
            var control = _factory();
            control.Dock = DockStyle.Fill;
            _form.AddToFormPanel(control);
            control.Terminate += OnTerminate;
            control.ReturnCard += OnReturnCard;
        }

        private void OnReturnCard(object sender, EventArgs e)
        {
            _workflowService.Execute(Return);
        }

        private void OnTerminate(object sender, EventArgs e)
        {
            var driver = ContextData.Driver;
            _driverApi.Terminate(driver.Id);
            _workflowService.Execute(Terminate);
        }
    }
}