﻿//using System;
//using System.Windows.Forms;
//using Castle.Core;
//using iRent.Login;
//using iRentKiosk.Core.Models;

//namespace iRent.Commands
//{
//    [CastleComponent]
//    public class LoginPassportCommand : ICommand
//    {
//        private readonly Func<LoginPassport> _factory;
//        private MainForm _form;

//        public Form MainForm
//        {
//            get { return _form; }
//            set { _form = (MainForm)value; }
//        }

//        public LoginPassportCommand(MainForm form, Func<LoginPassport> factory)
//        {
//            _form = form;
//            _factory = factory;
//        }

//        public void Invoke()
//        {
//            var control = _factory();
//            control.ClearControls();
//            _form.AddToFormPanel(control);
//            _form.ToggleHomeButton(true);
//        }
//    }
//}