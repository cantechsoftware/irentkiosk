﻿using System.Windows.Forms;
using Castle.Core;
using iRent.Classes;
using iRentKiosk.Core.Models;
using iRentKiosk.Core.Services;

namespace iRent.Commands
{
    [CastleComponent]
    public class ResetCommand : ICommand
    {
        private readonly WorkflowService _workflowService;

        public ResetCommand(WorkflowService workflowService)
        {
            _workflowService = workflowService;
        }

        public Form MainForm
        {
            get { return null; }
            set {  }
        }

        public void Invoke()
        {
            ContextData.Reset();
            _workflowService.Start();
        }
    }
}