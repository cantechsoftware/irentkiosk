﻿using System;
using System.Windows.Forms;
using Castle.Core;
using Core.Crypto;
using iRent.Classes;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Models;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;

namespace iRent.Commands
{
    [CastleComponent]
    public class CancomOTPCommand : ICommand
    {
        private Func<CancomOTP> _factory;
        private MainForm _form;
        private OTP _otp;
        private SmsApi _smsApi;

        public Form MainForm
        {
            get { return _form; }
            set { _form = (MainForm)value; }
        }

        public CancomOTPCommand(MainForm form,
                                Func<CancomOTP> factory,
                                SmsApi smsApi,
                                OTP otp)
        {
            _form = form;
            _factory = factory;
            _smsApi = smsApi;
            _otp = otp;
        }

        public void Invoke()
        {
            if (SendOtp())
            {
                UIFactory.Accept = true;
                UIFactory.NextControl = "Payment";
                _form.Accept();
            }
        }

        private bool SendOtp()
        {
            var driver = ContextData.Driver;
            var otpstring = GenerateOTP();

            _smsApi.SendClientSms(new SmsModel
                {
                    Msisdn = driver.CellNo,
                    Message = "Please enter the following OTP :" + otpstring
                });

            using (var otpForm = _factory())
            {
                var result = otpForm.Verify(otpstring,
                                            "OTP sent to :27" + driver.CellNo +
                                            ". Please enter the OTP below",
                                            driver.CellNo);
                return (result == DialogResult.OK);
            }
        }

        private string GenerateOTP()
        {
            return _otp.GetNextOTP();
        }
    }
}