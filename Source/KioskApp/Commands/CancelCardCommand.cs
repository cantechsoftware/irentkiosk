﻿using Castle.Core;
using iRent.Framework;
using iRent.Services;
using iRent2.Contracts.Enums;
using iRent2.Contracts.Rfid;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;

namespace iRent.Commands
{
    [CastleComponent]
    public class CancelCardCommand: BaseCommand
    {
        private readonly RfidApi _rfidApi;

        public CancelCardCommand(RfidApi rfidApi)
        {
            _rfidApi = rfidApi;
        }

        public string Next { get; set; }

        public override void Invoke()
        {
            if (CancelCard())
                Proceed(Next);
        }

        private bool CancelCard()
        {
            var driver = ContextData.Driver;
            var resp =  _rfidApi.CancelCard(new CancelRfidRequest
            {
                Rfid = driver.RfId ,
                DriverId = driver.Id
            });
            return resp.Status ==(int) RfidStatus.Cancelled;
        }
    }
}