﻿namespace iRent
{
    partial class Checkin
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOK = new MetroFramework.Controls.MetroButton();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.txtRfid = new MetroFramework.Controls.MetroTextBox();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btnOK.Highlight = true;
            this.btnOK.Location = new System.Drawing.Point(87, 43);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(179, 79);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = "&OK";
            this.btnOK.UseSelectable = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // metroLabel1
            // 
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel1.Location = new System.Drawing.Point(0, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(362, 25);
            this.metroLabel1.TabIndex = 1;
            this.metroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtRfid
            // 
            this.txtRfid.Lines = new string[0];
            this.txtRfid.Location = new System.Drawing.Point(0, 0);
            this.txtRfid.MaxLength = 32767;
            this.txtRfid.Name = "txtRfid";
            this.txtRfid.PasswordChar = '\0';
            this.txtRfid.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtRfid.SelectedText = "";
            this.txtRfid.Size = new System.Drawing.Size(362, 25);
            this.txtRfid.TabIndex = 0;
            this.txtRfid.UseSelectable = true;
            // 
            // Checkin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.txtRfid);
            this.Name = "Checkin";
            this.Size = new System.Drawing.Size(362, 140);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroButton btnOK;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox txtRfid;
    }
}
