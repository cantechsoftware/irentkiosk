﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using MetroFramework.Controls;
using CefSharp.WinForms;
using CefSharp;
using Castle.Core;
using System.IO;
using System.Net;
using Castle.Components;



namespace iRent.WebHost
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class CefHost : MetroUserControl
    {
        ChromiumWebBrowser _browser;
        CefHostJSApi _cefhostapi;

        public EventHandler<string> Next;

        public CefHost()
        {
            InitializeComponent();
            string path = Path.GetFullPath(".\\LocalPages\\Default.html");
            //string path = "Default.html";
            _browser = new ChromiumWebBrowser("file://"+path.Replace('\\','/'));
            _cefhostapi = new CefHostJSApi(this);
            _browser.Parent = this;
            _browser.JavascriptObjectRepository.Register("cefHostAPI", _cefhostapi);
            //_browser.RegisterJsObject("cefHostAPI", _cefhostapi);
            _browser.Dock = DockStyle.Fill;
            _browser.IsBrowserInitializedChanged += _browser_IsBrowserInitializedChanged;
        }

        private void _browser_IsBrowserInitializedChanged(object sender, IsBrowserInitializedChangedEventArgs e)
        {
            if (e.IsBrowserInitialized && System.Diagnostics.Debugger.IsAttached)
            {
                //_browser.ShowDevTools();
            }
        }

        public void Bind(string name, object webdata)
        {
            _cefhostapi.InputData = name;
            _browser.JavascriptObjectRepository.Register(name, webdata);
        }

        public void Navigate(string address)
        {
            if (!address.Contains("http:") && !address.Contains("https:") && !address.Contains("file:"))
            {
                string path = Environment.CurrentDirectory + "\\LocalPages";
                path = "file:///" + path.Replace('\\', '/');
                address = path + "/" + address;
            }

            _browser.Load(address);
        }

        public void HandleNext(string data)
        {
            Next(this, data);
        }
    }
}
