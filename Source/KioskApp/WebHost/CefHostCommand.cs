﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iRent.Framework;
using MetroFramework;
using Castle.Core;
using Castle.Windsor;
using iRentKiosk.Core.Services;
using System.Windows.Forms;

namespace iRent.WebHost
{
    [CastleComponent]
    public class CefHostCommand : BaseCommand
    {
        public string Next { get; set; }
        public string Address { get; set; }
        public string Data { get; set; }
        public string ReturnData { get; set; }
        private readonly Func<CefHost> _factory;

        public CefHostCommand(Func<CefHost> factory)
        {
            _factory = factory;
        }

        public override void Invoke()
        {
            CefHost control = _factory();
            //control.Next
            control.Dock = DockStyle.Fill;
            var main = GetFormAs<MainForm>();
            main.AddToFormPanel(control);
            control.Next += OnNext;
            control.Bind(Data, ContextData.DynamicData[Data]);
            control.Navigate(Address);
        }

        public void OnNext(object Sender, string data)
        {
            if (ContextData.DynamicData.ContainsKey(ReturnData))
            {
                ContextData.DynamicData[ReturnData] = data;
            }
            else
            {
                ContextData.DynamicData.Add(ReturnData, data);
            }


            Proceed(Next);
        }
    }
}
