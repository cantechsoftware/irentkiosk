﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace iRent.WebHost
{
    public class CefHostJSApi
    {
        public CefHostJSApi(CefHost host)
        {
            _host = host;
        }

        private CefHost _host;

        public string InputData { get; set; }

        public void Next(string data)
        {
            _host.HandleNext(data);
        }
    }
}
