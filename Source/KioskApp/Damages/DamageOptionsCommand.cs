﻿using System;
using System.Windows.Forms;
using Castle.Core;
using iRent.Framework;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;

namespace iRent.Damages
{
    [CastleComponent]
    public class DamageOptionsCommand : BaseCommand
    {
        private readonly Func<DamageOptionsControl> _factory;
        private readonly TransactionApi _transactionApi;
        private readonly VehicleApi _vehicleApi;

        public DamageOptionsCommand(Func<DamageOptionsControl> factory,
                                    TransactionApi transactionApi,
                                    VehicleApi vehicleApi)
        {
            _factory = factory;
            _transactionApi = transactionApi;
            _vehicleApi = vehicleApi;
        }

        public string Swap { get; set; }
        public string Cancel { get; set; }
        public string Continue { get; set; }

        public override void Invoke()
        {
            var control = _factory();
            control.Dock = DockStyle.Fill;
            var main = GetFormAs<MainForm>();
            main.AddToFormPanel(control);
            control.Cancel += (o, e) => Proceed(Cancel);
            control.Swap += OnSwap;
            control.Continue += (o, e) => Proceed(Continue);
        }

        private void OnSwap(object sender, EventArgs e)
        {
            ContextData.SwapCar = true;
            try
            {
                var driver = ContextData.Driver;
                var trans = _transactionApi.GetLatestByDriver(driver.Id);
                ContextData.PreviousRfid = trans.RfIdNo;
                ContextData.Set(trans);
                Proceed(Swap);
            }
            catch (Exception ex)
            {
                Logger.Log(this, "SwopClick", "Error swopping vehicle", ex);
            }
        }
    }
}