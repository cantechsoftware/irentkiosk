﻿namespace iRent.Damages
{
    partial class DamagesControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DamagesControl));
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.lblHeader = new MetroFramework.Controls.MetroLabel();
            this.accessories = new System.Windows.Forms.Button();
            this.boot = new System.Windows.Forms.Button();
            this.roof = new System.Windows.Forms.Button();
            this.bonnet = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.frontleftwheel = new System.Windows.Forms.Button();
            this.frontrightwheel = new System.Windows.Forms.Button();
            this.rearleftwheel = new System.Windows.Forms.Button();
            this.rearrightwheel = new System.Windows.Forms.Button();
            this.btnNext = new MetroFramework.Controls.MetroButton();
            this.windscreen = new System.Windows.Forms.Button();
            this.rearwindow = new System.Windows.Forms.Button();
            this.frontleftwing = new System.Windows.Forms.Button();
            this.frontleftmirror = new System.Windows.Forms.Button();
            this.frontleftwindow = new System.Windows.Forms.Button();
            this.frontrightwing = new System.Windows.Forms.Button();
            this.frontrightmirror = new System.Windows.Forms.Button();
            this.frontrightwindow = new System.Windows.Forms.Button();
            this.rearleftwing = new System.Windows.Forms.Button();
            this.rearleftwindow = new System.Windows.Forms.Button();
            this.rearrightwing = new System.Windows.Forms.Button();
            this.rearrightwindow = new System.Windows.Forms.Button();
            this.leftside = new System.Windows.Forms.Button();
            this.rightside = new System.Windows.Forms.Button();
            this.rear = new System.Windows.Forms.Button();
            this.front = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rearrightlights = new System.Windows.Forms.Button();
            this.lineControl17 = new iRent.Util.LineControl();
            this.rearleftlights = new System.Windows.Forms.Button();
            this.frontrightlights = new System.Windows.Forms.Button();
            this.lineControl16 = new iRent.Util.LineControl();
            this.frontleftlights = new System.Windows.Forms.Button();
            this.lineControl1 = new iRent.Util.LineControl();
            this.lineControl15 = new iRent.Util.LineControl();
            this.lineControl14 = new iRent.Util.LineControl();
            this.lineControl13 = new iRent.Util.LineControl();
            this.lineControl12 = new iRent.Util.LineControl();
            this.lineControl11 = new iRent.Util.LineControl();
            this.lineControl10 = new iRent.Util.LineControl();
            this.lineControl9 = new iRent.Util.LineControl();
            this.lineControl8 = new iRent.Util.LineControl();
            this.lineControl7 = new iRent.Util.LineControl();
            this.lineControl6 = new iRent.Util.LineControl();
            this.lineControl5 = new iRent.Util.LineControl();
            this.lineControl4 = new iRent.Util.LineControl();
            this.lineControl3 = new iRent.Util.LineControl();
            this.lineControl2 = new iRent.Util.LineControl();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lineControl18 = new iRent.Util.LineControl();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel1.Location = new System.Drawing.Point(865, 6);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(167, 25);
            this.metroLabel1.TabIndex = 67;
            this.metroLabel1.Text = "Damage Summary";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(740, 34);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(435, 213);
            this.dataGridView1.TabIndex = 66;
            // 
            // lblHeader
            // 
            this.lblHeader.AutoSize = true;
            this.lblHeader.FontSize = MetroFramework.MetroLabelSize.XL;
            this.lblHeader.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblHeader.Location = new System.Drawing.Point(92, 6);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(561, 30);
            this.lblHeader.TabIndex = 65;
            this.lblHeader.Text = "Please press where damage is located on the vehicle.";
            // 
            // accessories
            // 
            this.accessories.BackColor = System.Drawing.Color.DarkGray;
            this.accessories.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.accessories.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.accessories.Location = new System.Drawing.Point(592, 25);
            this.accessories.Name = "accessories";
            this.accessories.Size = new System.Drawing.Size(120, 60);
            this.accessories.TabIndex = 20;
            this.accessories.Text = "ACCESSORIES";
            this.accessories.UseVisualStyleBackColor = false;
            this.accessories.Click += new System.EventHandler(this.accessories_Click);
            // 
            // boot
            // 
            this.boot.BackColor = System.Drawing.Color.DarkGray;
            this.boot.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.boot.Location = new System.Drawing.Point(209, 466);
            this.boot.Name = "boot";
            this.boot.Size = new System.Drawing.Size(120, 60);
            this.boot.TabIndex = 5;
            this.boot.Text = "BOOT";
            this.boot.UseVisualStyleBackColor = false;
            this.boot.Click += new System.EventHandler(this.boot_Click);
            // 
            // roof
            // 
            this.roof.BackColor = System.Drawing.Color.DarkGray;
            this.roof.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.roof.Location = new System.Drawing.Point(209, 295);
            this.roof.Name = "roof";
            this.roof.Size = new System.Drawing.Size(120, 60);
            this.roof.TabIndex = 6;
            this.roof.Text = "ROOF";
            this.roof.UseVisualStyleBackColor = false;
            this.roof.Click += new System.EventHandler(this.roof_Click);
            // 
            // bonnet
            // 
            this.bonnet.BackColor = System.Drawing.Color.DarkGray;
            this.bonnet.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bonnet.Location = new System.Drawing.Point(209, 124);
            this.bonnet.Name = "bonnet";
            this.bonnet.Size = new System.Drawing.Size(120, 60);
            this.bonnet.TabIndex = 7;
            this.bonnet.Text = "BONNET";
            this.bonnet.UseVisualStyleBackColor = false;
            this.bonnet.Click += new System.EventHandler(this.bonnet_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(592, 96);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(120, 120);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 12;
            this.pictureBox2.TabStop = false;
            // 
            // frontleftwheel
            // 
            this.frontleftwheel.BackColor = System.Drawing.Color.DarkGray;
            this.frontleftwheel.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.frontleftwheel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.frontleftwheel.Location = new System.Drawing.Point(592, 222);
            this.frontleftwheel.Name = "frontleftwheel";
            this.frontleftwheel.Size = new System.Drawing.Size(120, 60);
            this.frontleftwheel.TabIndex = 13;
            this.frontleftwheel.Text = "FRONT LEFT WHEEL";
            this.frontleftwheel.UseVisualStyleBackColor = false;
            this.frontleftwheel.Click += new System.EventHandler(this.frontleftwheel_Click);
            // 
            // frontrightwheel
            // 
            this.frontrightwheel.BackColor = System.Drawing.Color.DarkGray;
            this.frontrightwheel.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.frontrightwheel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.frontrightwheel.Location = new System.Drawing.Point(592, 307);
            this.frontrightwheel.Name = "frontrightwheel";
            this.frontrightwheel.Size = new System.Drawing.Size(120, 60);
            this.frontrightwheel.TabIndex = 17;
            this.frontrightwheel.Text = "FRONT RIGHT WHEEL";
            this.frontrightwheel.UseVisualStyleBackColor = false;
            this.frontrightwheel.Click += new System.EventHandler(this.frontrightwheel_Click);
            // 
            // rearleftwheel
            // 
            this.rearleftwheel.BackColor = System.Drawing.Color.DarkGray;
            this.rearleftwheel.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.rearleftwheel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rearleftwheel.Location = new System.Drawing.Point(592, 391);
            this.rearleftwheel.Name = "rearleftwheel";
            this.rearleftwheel.Size = new System.Drawing.Size(120, 60);
            this.rearleftwheel.TabIndex = 18;
            this.rearleftwheel.Text = "REAR LEFT WHEEL";
            this.rearleftwheel.UseVisualStyleBackColor = false;
            this.rearleftwheel.Click += new System.EventHandler(this.rearleftwheel_Click);
            // 
            // rearrightwheel
            // 
            this.rearrightwheel.BackColor = System.Drawing.Color.DarkGray;
            this.rearrightwheel.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.rearrightwheel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rearrightwheel.Location = new System.Drawing.Point(592, 476);
            this.rearrightwheel.Name = "rearrightwheel";
            this.rearrightwheel.Size = new System.Drawing.Size(120, 60);
            this.rearrightwheel.TabIndex = 19;
            this.rearrightwheel.Text = "REAR RIGHT WHEEL";
            this.rearrightwheel.UseVisualStyleBackColor = false;
            this.rearrightwheel.Click += new System.EventHandler(this.rearrightwheel_Click);
            // 
            // btnNext
            // 
            this.btnNext.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnNext.Location = new System.Drawing.Point(266, 609);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(194, 94);
            this.btnNext.TabIndex = 20;
            this.btnNext.Text = "&Next";
            this.btnNext.UseSelectable = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // windscreen
            // 
            this.windscreen.BackColor = System.Drawing.Color.DarkGray;
            this.windscreen.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.windscreen.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.windscreen.Location = new System.Drawing.Point(209, 205);
            this.windscreen.Name = "windscreen";
            this.windscreen.Size = new System.Drawing.Size(120, 60);
            this.windscreen.TabIndex = 23;
            this.windscreen.Text = "WINDSCREEN";
            this.windscreen.UseVisualStyleBackColor = false;
            // 
            // rearwindow
            // 
            this.rearwindow.BackColor = System.Drawing.Color.DarkGray;
            this.rearwindow.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.rearwindow.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rearwindow.Location = new System.Drawing.Point(209, 394);
            this.rearwindow.Name = "rearwindow";
            this.rearwindow.Size = new System.Drawing.Size(120, 60);
            this.rearwindow.TabIndex = 24;
            this.rearwindow.Text = "REAR WINDOW";
            this.rearwindow.UseVisualStyleBackColor = false;
            this.rearwindow.Click += new System.EventHandler(this.rearwindow_Click);
            // 
            // frontleftwing
            // 
            this.frontleftwing.BackColor = System.Drawing.Color.DarkGray;
            this.frontleftwing.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.frontleftwing.Location = new System.Drawing.Point(18, 96);
            this.frontleftwing.Name = "frontleftwing";
            this.frontleftwing.Size = new System.Drawing.Size(120, 60);
            this.frontleftwing.TabIndex = 1;
            this.frontleftwing.Text = "FRONT LEFT WING";
            this.frontleftwing.UseVisualStyleBackColor = false;
            this.frontleftwing.Click += new System.EventHandler(this.frontleftwing_Click);
            // 
            // frontleftmirror
            // 
            this.frontleftmirror.BackColor = System.Drawing.Color.DarkGray;
            this.frontleftmirror.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.frontleftmirror.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.frontleftmirror.Location = new System.Drawing.Point(18, 173);
            this.frontleftmirror.Name = "frontleftmirror";
            this.frontleftmirror.Size = new System.Drawing.Size(120, 60);
            this.frontleftmirror.TabIndex = 22;
            this.frontleftmirror.Text = "FRONT LEFT MIRROR";
            this.frontleftmirror.UseVisualStyleBackColor = false;
            this.frontleftmirror.Click += new System.EventHandler(this.frontleftmirror_Click);
            // 
            // frontleftwindow
            // 
            this.frontleftwindow.BackColor = System.Drawing.Color.DarkGray;
            this.frontleftwindow.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.frontleftwindow.Location = new System.Drawing.Point(18, 248);
            this.frontleftwindow.Name = "frontleftwindow";
            this.frontleftwindow.Size = new System.Drawing.Size(120, 60);
            this.frontleftwindow.TabIndex = 28;
            this.frontleftwindow.Text = "FRONT LEFT WINDOW";
            this.frontleftwindow.UseVisualStyleBackColor = false;
            this.frontleftwindow.Click += new System.EventHandler(this.frontleftwindow_Click);
            // 
            // frontrightwing
            // 
            this.frontrightwing.BackColor = System.Drawing.Color.DarkGray;
            this.frontrightwing.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.frontrightwing.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.frontrightwing.Location = new System.Drawing.Point(400, 96);
            this.frontrightwing.Name = "frontrightwing";
            this.frontrightwing.Size = new System.Drawing.Size(120, 60);
            this.frontrightwing.TabIndex = 9;
            this.frontrightwing.Text = "FRONT RIGHT WING";
            this.frontrightwing.UseVisualStyleBackColor = false;
            this.frontrightwing.Click += new System.EventHandler(this.frontrightwing_Click);
            // 
            // frontrightmirror
            // 
            this.frontrightmirror.BackColor = System.Drawing.Color.DarkGray;
            this.frontrightmirror.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.frontrightmirror.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.frontrightmirror.Location = new System.Drawing.Point(400, 173);
            this.frontrightmirror.Name = "frontrightmirror";
            this.frontrightmirror.Size = new System.Drawing.Size(120, 60);
            this.frontrightmirror.TabIndex = 21;
            this.frontrightmirror.Text = "RIGHT FRONT MIRROR";
            this.frontrightmirror.UseVisualStyleBackColor = false;
            this.frontrightmirror.Click += new System.EventHandler(this.frontrightmirror_Click);
            // 
            // frontrightwindow
            // 
            this.frontrightwindow.BackColor = System.Drawing.Color.DarkGray;
            this.frontrightwindow.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.frontrightwindow.Location = new System.Drawing.Point(400, 248);
            this.frontrightwindow.Name = "frontrightwindow";
            this.frontrightwindow.Size = new System.Drawing.Size(120, 60);
            this.frontrightwindow.TabIndex = 25;
            this.frontrightwindow.Text = "FRONT RIGHT WINDOW";
            this.frontrightwindow.UseVisualStyleBackColor = false;
            this.frontrightwindow.Click += new System.EventHandler(this.frontrightwindow_Click);
            // 
            // rearleftwing
            // 
            this.rearleftwing.BackColor = System.Drawing.Color.DarkGray;
            this.rearleftwing.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rearleftwing.Location = new System.Drawing.Point(17, 476);
            this.rearleftwing.Name = "rearleftwing";
            this.rearleftwing.Size = new System.Drawing.Size(120, 60);
            this.rearleftwing.TabIndex = 3;
            this.rearleftwing.Text = "REAR LEFT WING";
            this.rearleftwing.UseVisualStyleBackColor = false;
            this.rearleftwing.Click += new System.EventHandler(this.rearleftwing_Click);
            // 
            // rearleftwindow
            // 
            this.rearleftwindow.BackColor = System.Drawing.Color.DarkGray;
            this.rearleftwindow.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rearleftwindow.Location = new System.Drawing.Point(17, 402);
            this.rearleftwindow.Name = "rearleftwindow";
            this.rearleftwindow.Size = new System.Drawing.Size(120, 60);
            this.rearleftwindow.TabIndex = 27;
            this.rearleftwindow.Text = "REAR LEFT WINDOW";
            this.rearleftwindow.UseVisualStyleBackColor = false;
            this.rearleftwindow.Click += new System.EventHandler(this.rearleftwindow_Click);
            // 
            // rearrightwing
            // 
            this.rearrightwing.BackColor = System.Drawing.Color.DarkGray;
            this.rearrightwing.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rearrightwing.Location = new System.Drawing.Point(400, 476);
            this.rearrightwing.Name = "rearrightwing";
            this.rearrightwing.Size = new System.Drawing.Size(120, 60);
            this.rearrightwing.TabIndex = 11;
            this.rearrightwing.Text = "REAR RIGHT WING";
            this.rearrightwing.UseVisualStyleBackColor = false;
            this.rearrightwing.Click += new System.EventHandler(this.rearrightwing_Click);
            // 
            // rearrightwindow
            // 
            this.rearrightwindow.BackColor = System.Drawing.Color.DarkGray;
            this.rearrightwindow.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rearrightwindow.Location = new System.Drawing.Point(400, 402);
            this.rearrightwindow.Name = "rearrightwindow";
            this.rearrightwindow.Size = new System.Drawing.Size(120, 60);
            this.rearrightwindow.TabIndex = 26;
            this.rearrightwindow.Text = "REAR RIGHT WINDOW";
            this.rearrightwindow.UseVisualStyleBackColor = false;
            this.rearrightwindow.Click += new System.EventHandler(this.rearrightwindow_Click);
            // 
            // leftside
            // 
            this.leftside.BackColor = System.Drawing.Color.DarkGray;
            this.leftside.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.leftside.Location = new System.Drawing.Point(17, 325);
            this.leftside.Name = "leftside";
            this.leftside.Size = new System.Drawing.Size(120, 60);
            this.leftside.TabIndex = 2;
            this.leftside.Text = "LEFT SIDE";
            this.leftside.UseVisualStyleBackColor = false;
            this.leftside.Click += new System.EventHandler(this.leftside_Click);
            // 
            // rightside
            // 
            this.rightside.BackColor = System.Drawing.Color.DarkGray;
            this.rightside.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rightside.Location = new System.Drawing.Point(400, 325);
            this.rightside.Name = "rightside";
            this.rightside.Size = new System.Drawing.Size(120, 60);
            this.rightside.TabIndex = 10;
            this.rightside.Text = "RIGHT SIDE";
            this.rightside.UseVisualStyleBackColor = false;
            this.rightside.Click += new System.EventHandler(this.rightside_Click);
            // 
            // rear
            // 
            this.rear.BackColor = System.Drawing.Color.DarkGray;
            this.rear.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rear.Location = new System.Drawing.Point(209, 543);
            this.rear.Name = "rear";
            this.rear.Size = new System.Drawing.Size(120, 60);
            this.rear.TabIndex = 4;
            this.rear.Text = "REAR";
            this.rear.UseVisualStyleBackColor = false;
            this.rear.Click += new System.EventHandler(this.rear_Click);
            // 
            // front
            // 
            this.front.BackColor = System.Drawing.Color.DarkGray;
            this.front.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.front.Location = new System.Drawing.Point(209, 26);
            this.front.Name = "front";
            this.front.Size = new System.Drawing.Size(118, 60);
            this.front.TabIndex = 8;
            this.front.Text = "FRONT";
            this.front.UseVisualStyleBackColor = false;
            this.front.Click += new System.EventHandler(this.front_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.rearleftlights);
            this.panel1.Controls.Add(this.lineControl18);
            this.panel1.Controls.Add(this.rearrightlights);
            this.panel1.Controls.Add(this.lineControl17);
            this.panel1.Controls.Add(this.frontrightlights);
            this.panel1.Controls.Add(this.lineControl16);
            this.panel1.Controls.Add(this.frontleftlights);
            this.panel1.Controls.Add(this.lineControl1);
            this.panel1.Controls.Add(this.front);
            this.panel1.Controls.Add(this.rear);
            this.panel1.Controls.Add(this.accessories);
            this.panel1.Controls.Add(this.lineControl15);
            this.panel1.Controls.Add(this.lineControl14);
            this.panel1.Controls.Add(this.rightside);
            this.panel1.Controls.Add(this.lineControl13);
            this.panel1.Controls.Add(this.leftside);
            this.panel1.Controls.Add(this.lineControl12);
            this.panel1.Controls.Add(this.rearrightwindow);
            this.panel1.Controls.Add(this.rearrightwing);
            this.panel1.Controls.Add(this.lineControl11);
            this.panel1.Controls.Add(this.lineControl10);
            this.panel1.Controls.Add(this.rearleftwindow);
            this.panel1.Controls.Add(this.lineControl9);
            this.panel1.Controls.Add(this.rearleftwing);
            this.panel1.Controls.Add(this.lineControl8);
            this.panel1.Controls.Add(this.frontrightwindow);
            this.panel1.Controls.Add(this.frontrightmirror);
            this.panel1.Controls.Add(this.frontrightwing);
            this.panel1.Controls.Add(this.frontleftwindow);
            this.panel1.Controls.Add(this.frontleftmirror);
            this.panel1.Controls.Add(this.frontleftwing);
            this.panel1.Controls.Add(this.lineControl7);
            this.panel1.Controls.Add(this.lineControl6);
            this.panel1.Controls.Add(this.lineControl5);
            this.panel1.Controls.Add(this.lineControl4);
            this.panel1.Controls.Add(this.lineControl3);
            this.panel1.Controls.Add(this.lineControl2);
            this.panel1.Controls.Add(this.rearwindow);
            this.panel1.Controls.Add(this.windscreen);
            this.panel1.Controls.Add(this.btnNext);
            this.panel1.Controls.Add(this.rearrightwheel);
            this.panel1.Controls.Add(this.rearleftwheel);
            this.panel1.Controls.Add(this.frontrightwheel);
            this.panel1.Controls.Add(this.frontleftwheel);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Controls.Add(this.bonnet);
            this.panel1.Controls.Add(this.roof);
            this.panel1.Controls.Add(this.boot);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(7, 34);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(727, 706);
            this.panel1.TabIndex = 64;
            // 
            // rearrightlights
            // 
            this.rearrightlights.BackColor = System.Drawing.Color.DarkGray;
            this.rearrightlights.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rearrightlights.Location = new System.Drawing.Point(400, 543);
            this.rearrightlights.Name = "rearrightlights";
            this.rearrightlights.Size = new System.Drawing.Size(120, 60);
            this.rearrightlights.TabIndex = 84;
            this.rearrightlights.Text = "REAR RIGHT LIGHTS";
            this.rearrightlights.UseVisualStyleBackColor = false;
            this.rearrightlights.Click += new System.EventHandler(this.rearrightlights_Click);
            // 
            // lineControl17
            // 
            this.lineControl17.AutoSize = true;
            this.lineControl17.BackColor = System.Drawing.Color.Transparent;
            this.lineControl17.EndPoint = new System.Drawing.Point(60, 50);
            this.lineControl17.ForeColor = System.Drawing.Color.Transparent;
            this.lineControl17.LineWidth = 4F;
            this.lineControl17.Location = new System.Drawing.Point(346, 520);
            this.lineControl17.Margin = new System.Windows.Forms.Padding(0);
            this.lineControl17.Name = "lineControl17";
            this.lineControl17.Size = new System.Drawing.Size(70, 57);
            this.lineControl17.StartPoint = new System.Drawing.Point(0, 0);
            this.lineControl17.TabIndex = 85;
            // 
            // rearleftlights
            // 
            this.rearleftlights.BackColor = System.Drawing.Color.DarkGray;
            this.rearleftlights.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rearleftlights.Location = new System.Drawing.Point(18, 543);
            this.rearleftlights.Name = "rearleftlights";
            this.rearleftlights.Size = new System.Drawing.Size(120, 60);
            this.rearleftlights.TabIndex = 83;
            this.rearleftlights.Text = "REAR LEFT LIGHTS";
            this.rearleftlights.UseVisualStyleBackColor = false;
            this.rearleftlights.Click += new System.EventHandler(this.rearleftlights_Click);
            // 
            // frontrightlights
            // 
            this.frontrightlights.BackColor = System.Drawing.Color.DarkGray;
            this.frontrightlights.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.frontrightlights.Location = new System.Drawing.Point(400, 25);
            this.frontrightlights.Name = "frontrightlights";
            this.frontrightlights.Size = new System.Drawing.Size(120, 60);
            this.frontrightlights.TabIndex = 81;
            this.frontrightlights.Text = "FRONT RIGHT LIGHTS";
            this.frontrightlights.UseVisualStyleBackColor = false;
            this.frontrightlights.Click += new System.EventHandler(this.frontrightlights_Click);
            // 
            // lineControl16
            // 
            this.lineControl16.AutoSize = true;
            this.lineControl16.BackColor = System.Drawing.Color.Transparent;
            this.lineControl16.EndPoint = new System.Drawing.Point(70, 0);
            this.lineControl16.ForeColor = System.Drawing.Color.Transparent;
            this.lineControl16.LineWidth = 4F;
            this.lineControl16.Location = new System.Drawing.Point(337, 63);
            this.lineControl16.Margin = new System.Windows.Forms.Padding(0);
            this.lineControl16.Name = "lineControl16";
            this.lineControl16.Size = new System.Drawing.Size(73, 65);
            this.lineControl16.StartPoint = new System.Drawing.Point(0, 50);
            this.lineControl16.TabIndex = 82;
            // 
            // frontleftlights
            // 
            this.frontleftlights.BackColor = System.Drawing.Color.DarkGray;
            this.frontleftlights.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.frontleftlights.Location = new System.Drawing.Point(18, 26);
            this.frontleftlights.Name = "frontleftlights";
            this.frontleftlights.Size = new System.Drawing.Size(120, 60);
            this.frontleftlights.TabIndex = 79;
            this.frontleftlights.Text = "FRONT LEFT LIGHTS";
            this.frontleftlights.UseVisualStyleBackColor = false;
            this.frontleftlights.Click += new System.EventHandler(this.frontleftlights_Click);
            // 
            // lineControl1
            // 
            this.lineControl1.AutoSize = true;
            this.lineControl1.BackColor = System.Drawing.Color.Transparent;
            this.lineControl1.EndPoint = new System.Drawing.Point(60, 50);
            this.lineControl1.ForeColor = System.Drawing.Color.Transparent;
            this.lineControl1.LineWidth = 4F;
            this.lineControl1.Location = new System.Drawing.Point(135, 67);
            this.lineControl1.Margin = new System.Windows.Forms.Padding(0);
            this.lineControl1.Name = "lineControl1";
            this.lineControl1.Size = new System.Drawing.Size(70, 57);
            this.lineControl1.StartPoint = new System.Drawing.Point(0, 0);
            this.lineControl1.TabIndex = 80;
            // 
            // lineControl15
            // 
            this.lineControl15.AutoSize = true;
            this.lineControl15.BackColor = System.Drawing.Color.Transparent;
            this.lineControl15.EndPoint = new System.Drawing.Point(0, 100);
            this.lineControl15.ForeColor = System.Drawing.Color.Transparent;
            this.lineControl15.LineWidth = 6F;
            this.lineControl15.Location = new System.Drawing.Point(265, 51);
            this.lineControl15.Margin = new System.Windows.Forms.Padding(0);
            this.lineControl15.Name = "lineControl15";
            this.lineControl15.Size = new System.Drawing.Size(11, 50);
            this.lineControl15.StartPoint = new System.Drawing.Point(0, 0);
            this.lineControl15.TabIndex = 78;
            // 
            // lineControl14
            // 
            this.lineControl14.AutoSize = true;
            this.lineControl14.BackColor = System.Drawing.Color.Transparent;
            this.lineControl14.EndPoint = new System.Drawing.Point(0, 100);
            this.lineControl14.ForeColor = System.Drawing.Color.Transparent;
            this.lineControl14.LineWidth = 6F;
            this.lineControl14.Location = new System.Drawing.Point(266, 531);
            this.lineControl14.Margin = new System.Windows.Forms.Padding(0);
            this.lineControl14.Name = "lineControl14";
            this.lineControl14.Size = new System.Drawing.Size(10, 41);
            this.lineControl14.StartPoint = new System.Drawing.Point(0, 0);
            this.lineControl14.TabIndex = 77;
            // 
            // lineControl13
            // 
            this.lineControl13.AutoSize = true;
            this.lineControl13.BackColor = System.Drawing.Color.Transparent;
            this.lineControl13.EndPoint = new System.Drawing.Point(100, 0);
            this.lineControl13.ForeColor = System.Drawing.Color.Transparent;
            this.lineControl13.LineWidth = 6F;
            this.lineControl13.Location = new System.Drawing.Point(372, 353);
            this.lineControl13.Margin = new System.Windows.Forms.Padding(0);
            this.lineControl13.Name = "lineControl13";
            this.lineControl13.Size = new System.Drawing.Size(44, 14);
            this.lineControl13.StartPoint = new System.Drawing.Point(0, 0);
            this.lineControl13.TabIndex = 76;
            // 
            // lineControl12
            // 
            this.lineControl12.AutoSize = true;
            this.lineControl12.BackColor = System.Drawing.Color.Transparent;
            this.lineControl12.EndPoint = new System.Drawing.Point(100, 0);
            this.lineControl12.ForeColor = System.Drawing.Color.Transparent;
            this.lineControl12.LineWidth = 6F;
            this.lineControl12.Location = new System.Drawing.Point(122, 353);
            this.lineControl12.Margin = new System.Windows.Forms.Padding(0);
            this.lineControl12.Name = "lineControl12";
            this.lineControl12.Size = new System.Drawing.Size(44, 14);
            this.lineControl12.StartPoint = new System.Drawing.Point(0, 0);
            this.lineControl12.TabIndex = 75;
            // 
            // lineControl11
            // 
            this.lineControl11.AutoSize = true;
            this.lineControl11.BackColor = System.Drawing.Color.Transparent;
            this.lineControl11.EndPoint = new System.Drawing.Point(50, 40);
            this.lineControl11.ForeColor = System.Drawing.Color.Transparent;
            this.lineControl11.LineWidth = 4F;
            this.lineControl11.Location = new System.Drawing.Point(364, 473);
            this.lineControl11.Margin = new System.Windows.Forms.Padding(0);
            this.lineControl11.Name = "lineControl11";
            this.lineControl11.Size = new System.Drawing.Size(41, 47);
            this.lineControl11.StartPoint = new System.Drawing.Point(0, 0);
            this.lineControl11.TabIndex = 74;
            // 
            // lineControl10
            // 
            this.lineControl10.AutoSize = true;
            this.lineControl10.BackColor = System.Drawing.Color.Transparent;
            this.lineControl10.EndPoint = new System.Drawing.Point(50, 40);
            this.lineControl10.ForeColor = System.Drawing.Color.Transparent;
            this.lineControl10.LineWidth = 4F;
            this.lineControl10.Location = new System.Drawing.Point(353, 394);
            this.lineControl10.Margin = new System.Windows.Forms.Padding(0);
            this.lineControl10.Name = "lineControl10";
            this.lineControl10.Size = new System.Drawing.Size(49, 47);
            this.lineControl10.StartPoint = new System.Drawing.Point(0, 0);
            this.lineControl10.TabIndex = 73;
            // 
            // lineControl9
            // 
            this.lineControl9.AutoSize = true;
            this.lineControl9.BackColor = System.Drawing.Color.Transparent;
            this.lineControl9.EndPoint = new System.Drawing.Point(50, 0);
            this.lineControl9.ForeColor = System.Drawing.Color.Transparent;
            this.lineControl9.LineWidth = 4F;
            this.lineControl9.Location = new System.Drawing.Point(136, 394);
            this.lineControl9.Margin = new System.Windows.Forms.Padding(0);
            this.lineControl9.Name = "lineControl9";
            this.lineControl9.Size = new System.Drawing.Size(49, 49);
            this.lineControl9.StartPoint = new System.Drawing.Point(0, 40);
            this.lineControl9.TabIndex = 72;
            // 
            // lineControl8
            // 
            this.lineControl8.AutoSize = true;
            this.lineControl8.BackColor = System.Drawing.Color.Transparent;
            this.lineControl8.EndPoint = new System.Drawing.Point(50, 0);
            this.lineControl8.ForeColor = System.Drawing.Color.Transparent;
            this.lineControl8.LineWidth = 4F;
            this.lineControl8.Location = new System.Drawing.Point(135, 466);
            this.lineControl8.Margin = new System.Windows.Forms.Padding(0);
            this.lineControl8.Name = "lineControl8";
            this.lineControl8.Size = new System.Drawing.Size(41, 49);
            this.lineControl8.StartPoint = new System.Drawing.Point(0, 40);
            this.lineControl8.TabIndex = 71;
            // 
            // lineControl7
            // 
            this.lineControl7.AutoSize = true;
            this.lineControl7.BackColor = System.Drawing.Color.Transparent;
            this.lineControl7.EndPoint = new System.Drawing.Point(50, 40);
            this.lineControl7.ForeColor = System.Drawing.Color.Transparent;
            this.lineControl7.LineWidth = 4F;
            this.lineControl7.Location = new System.Drawing.Point(136, 276);
            this.lineControl7.Margin = new System.Windows.Forms.Padding(0);
            this.lineControl7.Name = "lineControl7";
            this.lineControl7.Size = new System.Drawing.Size(49, 47);
            this.lineControl7.StartPoint = new System.Drawing.Point(0, 0);
            this.lineControl7.TabIndex = 70;
            // 
            // lineControl6
            // 
            this.lineControl6.AutoSize = true;
            this.lineControl6.BackColor = System.Drawing.Color.Transparent;
            this.lineControl6.EndPoint = new System.Drawing.Point(50, 40);
            this.lineControl6.ForeColor = System.Drawing.Color.Transparent;
            this.lineControl6.LineWidth = 4F;
            this.lineControl6.Location = new System.Drawing.Point(135, 202);
            this.lineControl6.Margin = new System.Windows.Forms.Padding(0);
            this.lineControl6.Name = "lineControl6";
            this.lineControl6.Size = new System.Drawing.Size(41, 47);
            this.lineControl6.StartPoint = new System.Drawing.Point(0, 0);
            this.lineControl6.TabIndex = 69;
            // 
            // lineControl5
            // 
            this.lineControl5.AutoSize = true;
            this.lineControl5.BackColor = System.Drawing.Color.Transparent;
            this.lineControl5.EndPoint = new System.Drawing.Point(50, 40);
            this.lineControl5.ForeColor = System.Drawing.Color.Transparent;
            this.lineControl5.LineWidth = 4F;
            this.lineControl5.Location = new System.Drawing.Point(135, 126);
            this.lineControl5.Margin = new System.Windows.Forms.Padding(0);
            this.lineControl5.Name = "lineControl5";
            this.lineControl5.Size = new System.Drawing.Size(41, 47);
            this.lineControl5.StartPoint = new System.Drawing.Point(0, 0);
            this.lineControl5.TabIndex = 68;
            // 
            // lineControl4
            // 
            this.lineControl4.AutoSize = true;
            this.lineControl4.BackColor = System.Drawing.Color.Transparent;
            this.lineControl4.EndPoint = new System.Drawing.Point(50, 0);
            this.lineControl4.ForeColor = System.Drawing.Color.Transparent;
            this.lineControl4.LineWidth = 4F;
            this.lineControl4.Location = new System.Drawing.Point(361, 196);
            this.lineControl4.Margin = new System.Windows.Forms.Padding(0);
            this.lineControl4.Name = "lineControl4";
            this.lineControl4.Size = new System.Drawing.Size(41, 49);
            this.lineControl4.StartPoint = new System.Drawing.Point(0, 40);
            this.lineControl4.TabIndex = 31;
            // 
            // lineControl3
            // 
            this.lineControl3.AutoSize = true;
            this.lineControl3.BackColor = System.Drawing.Color.Transparent;
            this.lineControl3.EndPoint = new System.Drawing.Point(50, 0);
            this.lineControl3.ForeColor = System.Drawing.Color.Transparent;
            this.lineControl3.LineWidth = 4F;
            this.lineControl3.Location = new System.Drawing.Point(355, 275);
            this.lineControl3.Margin = new System.Windows.Forms.Padding(0);
            this.lineControl3.Name = "lineControl3";
            this.lineControl3.Size = new System.Drawing.Size(47, 49);
            this.lineControl3.StartPoint = new System.Drawing.Point(0, 40);
            this.lineControl3.TabIndex = 30;
            // 
            // lineControl2
            // 
            this.lineControl2.AutoSize = true;
            this.lineControl2.BackColor = System.Drawing.Color.Transparent;
            this.lineControl2.EndPoint = new System.Drawing.Point(50, 0);
            this.lineControl2.ForeColor = System.Drawing.Color.Transparent;
            this.lineControl2.LineWidth = 4F;
            this.lineControl2.Location = new System.Drawing.Point(361, 121);
            this.lineControl2.Margin = new System.Windows.Forms.Padding(0);
            this.lineControl2.Name = "lineControl2";
            this.lineControl2.Size = new System.Drawing.Size(41, 49);
            this.lineControl2.StartPoint = new System.Drawing.Point(0, 40);
            this.lineControl2.TabIndex = 29;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(162, 96);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(217, 440);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // lineControl18
            // 
            this.lineControl18.AutoSize = true;
            this.lineControl18.BackColor = System.Drawing.Color.Transparent;
            this.lineControl18.EndPoint = new System.Drawing.Point(70, 0);
            this.lineControl18.ForeColor = System.Drawing.Color.Transparent;
            this.lineControl18.LineWidth = 4F;
            this.lineControl18.Location = new System.Drawing.Point(123, 520);
            this.lineControl18.Margin = new System.Windows.Forms.Padding(0);
            this.lineControl18.Name = "lineControl18";
            this.lineControl18.Size = new System.Drawing.Size(73, 65);
            this.lineControl18.StartPoint = new System.Drawing.Point(0, 50);
            this.lineControl18.TabIndex = 86;
            // 
            // DamagesControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.lblHeader);
            this.Controls.Add(this.panel1);
            this.Name = "DamagesControl";
            this.Size = new System.Drawing.Size(1193, 743);
            this.Load += new System.EventHandler(this.Damages_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private MetroFramework.Controls.MetroLabel lblHeader;
        private System.Windows.Forms.Button accessories;
        private System.Windows.Forms.Button boot;
        private System.Windows.Forms.Button roof;
        private System.Windows.Forms.Button bonnet;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button frontleftwheel;
        private System.Windows.Forms.Button frontrightwheel;
        private System.Windows.Forms.Button rearleftwheel;
        private System.Windows.Forms.Button rearrightwheel;
        private MetroFramework.Controls.MetroButton btnNext;
        private System.Windows.Forms.Button windscreen;
        private System.Windows.Forms.Button rearwindow;
        private Util.LineControl lineControl2;
        private Util.LineControl lineControl3;
        private Util.LineControl lineControl4;
        private Util.LineControl lineControl5;
        private Util.LineControl lineControl6;
        private Util.LineControl lineControl7;
        private System.Windows.Forms.Button frontleftwing;
        private System.Windows.Forms.Button frontleftmirror;
        private System.Windows.Forms.Button frontleftwindow;
        private System.Windows.Forms.Button frontrightwing;
        private System.Windows.Forms.Button frontrightmirror;
        private System.Windows.Forms.Button frontrightwindow;
        private Util.LineControl lineControl8;
        private System.Windows.Forms.Button rearleftwing;
        private Util.LineControl lineControl9;
        private System.Windows.Forms.Button rearleftwindow;
        private Util.LineControl lineControl10;
        private Util.LineControl lineControl11;
        private System.Windows.Forms.Button rearrightwing;
        private System.Windows.Forms.Button rearrightwindow;
        private Util.LineControl lineControl12;
        private System.Windows.Forms.Button leftside;
        private Util.LineControl lineControl13;
        private System.Windows.Forms.Button rightside;
        private Util.LineControl lineControl14;
        private Util.LineControl lineControl15;
        private System.Windows.Forms.Button rear;
        private System.Windows.Forms.Button front;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button frontleftlights;
        private System.Windows.Forms.Button frontrightlights;
        private Util.LineControl lineControl1;
        private System.Windows.Forms.Button rearrightlights;
        private Util.LineControl lineControl17;
        private System.Windows.Forms.Button rearleftlights;
        private Util.LineControl lineControl16;
        private Util.LineControl lineControl18;
    }
}
