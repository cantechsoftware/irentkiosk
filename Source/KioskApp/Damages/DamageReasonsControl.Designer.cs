﻿namespace iRent.Damages
{
    partial class DamageReasonsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnNext = new MetroFramework.Controls.MetroButton();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.chk1 = new MetroFramework.Controls.MetroCheckBox();
            this.chk2 = new MetroFramework.Controls.MetroCheckBox();
            this.chk3 = new MetroFramework.Controls.MetroCheckBox();
            this.lblStatus = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.chk3, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.chk2, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.btnNext, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.metroLabel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.chk1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblStatus, 0, 5);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(578, 428);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // btnNext
            // 
            this.btnNext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNext.Enabled = false;
            this.btnNext.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnNext.Location = new System.Drawing.Point(192, 330);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(194, 95);
            this.btnNext.TabIndex = 5;
            this.btnNext.Text = "&Next";
            this.btnNext.UseSelectable = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.metroLabel1, 3);
            this.metroLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.XL;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel1.Location = new System.Drawing.Point(3, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(572, 50);
            this.metroLabel1.TabIndex = 0;
            this.metroLabel1.Text = "Please select a Reason";
            this.metroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chk1
            // 
            this.chk1.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.chk1, 3);
            this.chk1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chk1.FontSize = MetroFramework.MetroCheckBoxSize.Tall;
            this.chk1.FontWeight = MetroFramework.MetroCheckBoxWeight.Bold;
            this.chk1.Location = new System.Drawing.Point(3, 103);
            this.chk1.Name = "chk1";
            this.chk1.Size = new System.Drawing.Size(572, 53);
            this.chk1.TabIndex = 6;
            this.chk1.Text = "Vehicle was damaged.";
            this.chk1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chk1.UseSelectable = true;
            // 
            // chk2
            // 
            this.chk2.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.chk2, 3);
            this.chk2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chk2.FontSize = MetroFramework.MetroCheckBoxSize.Tall;
            this.chk2.FontWeight = MetroFramework.MetroCheckBoxWeight.Bold;
            this.chk2.Location = new System.Drawing.Point(3, 162);
            this.chk2.Name = "chk2";
            this.chk2.Size = new System.Drawing.Size(572, 53);
            this.chk2.TabIndex = 7;
            this.chk2.Text = "Vehicle was dirty.";
            this.chk2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chk2.UseSelectable = true;
            // 
            // chk3
            // 
            this.chk3.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.chk3, 3);
            this.chk3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chk3.FontSize = MetroFramework.MetroCheckBoxSize.Tall;
            this.chk3.FontWeight = MetroFramework.MetroCheckBoxWeight.Bold;
            this.chk3.Location = new System.Drawing.Point(3, 221);
            this.chk3.Name = "chk3";
            this.chk3.Size = new System.Drawing.Size(572, 53);
            this.chk3.TabIndex = 8;
            this.chk3.Text = "Vehicle was not needed.";
            this.chk3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chk3.UseSelectable = true;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.lblStatus, 3);
            this.lblStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblStatus.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblStatus.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblStatus.Location = new System.Drawing.Point(3, 277);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(572, 50);
            this.lblStatus.TabIndex = 9;
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DamageReasons
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "DamageReasonsControl";
            this.Size = new System.Drawing.Size(578, 428);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MetroFramework.Controls.MetroCheckBox chk3;
        private MetroFramework.Controls.MetroCheckBox chk2;
        private MetroFramework.Controls.MetroButton btnNext;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroCheckBox chk1;
        private MetroFramework.Controls.MetroLabel lblStatus;
    }
}
