﻿using System;
using System.Windows.Forms;
using Castle.Core;

namespace iRent.Damages
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class DamageReasonsControl : UserControl
    {
        public DamageReasonsControl()
        {
            InitializeComponent();
        }

        public event EventHandler Next;

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (Next != null)
                Next(this, e);
        }
    }
}