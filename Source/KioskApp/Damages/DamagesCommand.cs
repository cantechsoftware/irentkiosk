﻿using System;
using System.Collections.Generic;
using Castle.Core;
using iRent.Framework;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;

namespace iRent.Damages
{
    [CastleComponent]
    public class DamagesCommand : BaseCommand
    {
        private readonly Func<DamagesControl> _factory;
        private readonly TransactionApi _transactionApi;

        public DamagesCommand(Func<DamagesControl> factory, TransactionApi transactionApi)
        {
            _factory = factory;
            _transactionApi = transactionApi;
        }

        public string Next { get; set; }
        
        public override void Invoke()
        {
            var control = _factory();
            var main = GetFormAs<MainForm>();
            main.AddToFormPanel(control);
            main.ToggleHomeButton(true);
            control.Next += OnNext;
        }

        private void OnNext(object sender, List<DamageModel> damages)
        {
            if (damages.Count > 0)
            {
                var trans = ContextData.Transaction;
                trans.Damages = damages;
                ContextData.Set(trans);
                trans = _transactionApi.Update(trans);
                ContextData.Set(trans);
            }
            Proceed(Next);
        }
    }
}