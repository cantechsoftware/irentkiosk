﻿using Castle.Core;
using iRent.Framework;
using iRent2.Contracts;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;

namespace iRent.Damages
{
    [CastleComponent]
    public class SwapCarCommand : BaseCommand
    {
        private readonly TransactionApi _transactionApi;

        public SwapCarCommand(TransactionApi transactionApi)
        {
            _transactionApi = transactionApi;
        }

        public string NoVehicles { get; set; }
        public string Next { get; set; }
        public string Error { get; set; }

        public override void Invoke()
        {
            var trans = ContextData.Transaction;
            try
            {
                var vehId = ContextData.VehicleId;
                if (vehId == trans.Vehicle.Id)
                    vehId = null;

                var newTrans = _transactionApi.Swap(trans.Id,
                                                    ContextData.Kiosk.BranchId.Value,
                                                    ContextData.PreviousRfid,
                                                    ContextData.VehicleGroupId,
                                                    vehId);
                ContextData.Set(newTrans);
                Proceed(Next);
            }
            catch (ApiException aex)
            {
                if (aex.ApiStatusCode == ApiStatusCode.ValidationError)
                    Proceed(NoVehicles);
                else
                    Proceed(Error);
            }
        }
    }
}