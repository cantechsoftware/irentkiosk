﻿namespace iRent.Damages
{
    partial class DamageLocationForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOK = new MetroFramework.Controls.MetroButton();
            this.btnClose = new MetroFramework.Controls.MetroButton();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.mcbDamageType = new MetroFramework.Controls.MetroComboBox();
            this.lblDamageCount = new MetroFramework.Controls.MetroLabel();
            this.lblAccessory = new MetroFramework.Controls.MetroLabel();
            this.mcbAccessory = new MetroFramework.Controls.MetroComboBox();
            this.mbPlus = new MetroFramework.Controls.MetroButton();
            this.mbMinus = new MetroFramework.Controls.MetroButton();
            this.mlbDamageCount = new MetroFramework.Controls.MetroLabel();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnOK.Location = new System.Drawing.Point(293, 278);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(137, 82);
            this.btnOK.TabIndex = 4;
            this.btnOK.Text = "&OK";
            this.btnOK.UseSelectable = true;
            this.btnOK.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnClose
            // 
            this.btnClose.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnClose.Location = new System.Drawing.Point(23, 278);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(147, 82);
            this.btnClose.TabIndex = 24;
            this.btnClose.Text = "&Close";
            this.btnClose.UseSelectable = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel1.Location = new System.Drawing.Point(23, 51);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(128, 25);
            this.metroLabel1.TabIndex = 25;
            this.metroLabel1.Text = "Damage Type";
            // 
            // mcbDamageType
            // 
            this.mcbDamageType.FormattingEnabled = true;
            this.mcbDamageType.ItemHeight = 23;
            this.mcbDamageType.Location = new System.Drawing.Point(177, 47);
            this.mcbDamageType.Name = "mcbDamageType";
            this.mcbDamageType.Size = new System.Drawing.Size(280, 29);
            this.mcbDamageType.TabIndex = 31;
            this.mcbDamageType.UseSelectable = true;
            this.mcbDamageType.SelectedIndexChanged += new System.EventHandler(this.mcbDamageType_SelectedIndexChanged);
            // 
            // lblDamageCount
            // 
            this.lblDamageCount.AutoSize = true;
            this.lblDamageCount.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblDamageCount.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblDamageCount.Location = new System.Drawing.Point(25, 169);
            this.lblDamageCount.Name = "lblDamageCount";
            this.lblDamageCount.Size = new System.Drawing.Size(138, 25);
            this.lblDamageCount.TabIndex = 33;
            this.lblDamageCount.Text = "Damage Count";
            // 
            // lblAccessory
            // 
            this.lblAccessory.AutoSize = true;
            this.lblAccessory.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblAccessory.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblAccessory.Location = new System.Drawing.Point(23, 102);
            this.lblAccessory.Name = "lblAccessory";
            this.lblAccessory.Size = new System.Drawing.Size(98, 25);
            this.lblAccessory.TabIndex = 34;
            this.lblAccessory.Text = "Accessory";
            // 
            // mcbAccessory
            // 
            this.mcbAccessory.FormattingEnabled = true;
            this.mcbAccessory.ItemHeight = 23;
            this.mcbAccessory.Location = new System.Drawing.Point(177, 98);
            this.mcbAccessory.Name = "mcbAccessory";
            this.mcbAccessory.Size = new System.Drawing.Size(280, 29);
            this.mcbAccessory.TabIndex = 35;
            this.mcbAccessory.UseSelectable = true;
            // 
            // mbPlus
            // 
            this.mbPlus.Location = new System.Drawing.Point(162, 151);
            this.mbPlus.Name = "mbPlus";
            this.mbPlus.Size = new System.Drawing.Size(75, 70);
            this.mbPlus.TabIndex = 36;
            this.mbPlus.Text = "+";
            this.mbPlus.UseSelectable = true;
            this.mbPlus.Click += new System.EventHandler(this.mbPlus_Click);
            // 
            // mbMinus
            // 
            this.mbMinus.Location = new System.Drawing.Point(367, 151);
            this.mbMinus.Name = "mbMinus";
            this.mbMinus.Size = new System.Drawing.Size(75, 70);
            this.mbMinus.TabIndex = 37;
            this.mbMinus.Text = "-";
            this.mbMinus.UseSelectable = true;
            this.mbMinus.Click += new System.EventHandler(this.mbMinus_Click);
            // 
            // mlbDamageCount
            // 
            this.mlbDamageCount.FontSize = MetroFramework.MetroLabelSize.XL;
            this.mlbDamageCount.FontWeight = MetroFramework.MetroLabelWeight.XL;
            this.mlbDamageCount.Location = new System.Drawing.Point(273, 151);
            this.mlbDamageCount.Name = "mlbDamageCount";
            this.mlbDamageCount.Size = new System.Drawing.Size(58, 70);
            this.mlbDamageCount.TabIndex = 38;
            this.mlbDamageCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DamageLocationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(463, 392);
            this.ControlBox = false;
            this.Controls.Add(this.mlbDamageCount);
            this.Controls.Add(this.mbMinus);
            this.Controls.Add(this.mbPlus);
            this.Controls.Add(this.mcbAccessory);
            this.Controls.Add(this.lblAccessory);
            this.Controls.Add(this.lblDamageCount);
            this.Controls.Add(this.mcbDamageType);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnOK);
            this.Name = "DamageLocationForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Style = MetroFramework.MetroColorStyle.Red;
            this.Load += new System.EventHandler(this.DamageLocationForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroButton btnOK;
        private MetroFramework.Controls.MetroButton btnClose;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroComboBox mcbDamageType;
        private MetroFramework.Controls.MetroLabel lblDamageCount;
        private MetroFramework.Controls.MetroLabel lblAccessory;
        private MetroFramework.Controls.MetroComboBox mcbAccessory;
        private MetroFramework.Controls.MetroButton mbPlus;
        private MetroFramework.Controls.MetroButton mbMinus;
        private MetroFramework.Controls.MetroLabel mlbDamageCount;
    }
}
