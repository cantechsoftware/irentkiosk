﻿using System;
using Castle.Core;
using MetroFramework.Controls;

namespace iRent.Damages
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class DamageOptionsControl : MetroUserControl
    {
        public DamageOptionsControl()
        {
            InitializeComponent();
        }

        public event EventHandler Continue;
        public event EventHandler Swap;
        public event EventHandler Cancel;

        private void btnContinue_Click(object sender, EventArgs e)
        {
            if (Continue != null)
                Continue(this, e);
        }

        private void btnSwop_Click(object sender, EventArgs e)
        {
            if (Swap != null)
                Swap(this, e);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (Cancel != null)
                Cancel(this, e);
        }
    }
}