﻿using Castle.Core;
using iRent.Framework;
using iRent2.Contracts.Enums;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;

namespace iRent.Damages
{
    [CastleComponent]
    public class GetLastTransactionCommand : BaseCommand
    {
        private readonly TransactionApi _transactionApi;

        public GetLastTransactionCommand(TransactionApi transactionApi)
        {
            _transactionApi = transactionApi;
        }

        public string NotFound { get; set; }
        public string VehicleStarted { get; set; }
        public string Next { get; set; }

        public override void Invoke()
        {
            var trans = _transactionApi.GetLatestByDriver(ContextData.Driver.Id);
            if (trans == null || trans.Vehicle == null)
                Proceed(NotFound);
            else if (trans.Status == TransactionStatus.VehicleStarted)
                Proceed(VehicleStarted);
            else
            {
                ContextData.Set(trans);
                Proceed(Next);
            }
        }
    }
}