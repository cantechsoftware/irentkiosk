﻿using System;
using System.Linq.Expressions;
using CanTech.Common;
using Castle.Core;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;
using MetroFramework.Forms;

namespace iRent.Damages
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class DamageLocationForm : MetroForm
    {
        public DamageLocationForm(string location)
        {
            InitializeComponent();
            var accDs = ContextData.Vehicle.Accessories;
            mcbAccessory.DisplayMember = "Name";
            mcbAccessory.DataSource = accDs;
            

            var damageTypes = IoC.Resolve<DamagesApi>().DamageTypes(location);
            mcbDamageType.DisplayMember = "Type";
            mcbDamageType.DataSource = damageTypes;
            
        }

        public bool IsValid { get; set; }
        public string DamageType { get; set; }
        public int DamageCount { get; set; }
        public int Accessory { get; set; }
        
        private void btnOk_Click(object sender, EventArgs e)
        {
            IsValid = true;
            DamageType =((DamageTypeModel) mcbDamageType.SelectedValue).Type;
            DamageCount = !mlbDamageCount.Visible ? 0 : Convert.ToInt32(mlbDamageCount.Text);
            Accessory = !mcbAccessory.Visible ? 0 : ((AccessoryModel)mcbAccessory.SelectedValue).Id.Value;
            Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            IsValid = false;
            Close();
        }

        private void mcbDamageType_SelectedIndexChanged(object sender, EventArgs e)
        {
            var damageType =((DamageTypeModel)mcbDamageType.SelectedValue).Location.ToLower( );
            if (damageType == "accessory")
            {
                mcbAccessory.Visible = true;
                lblAccessory.Visible = true;
                lblDamageCount.Visible = false;
                mbMinus.Visible = false;
                mbPlus.Visible = false;
                mlbDamageCount.Visible = false;
            }
            else
            {
                lblDamageCount.Visible = true;
                mbMinus.Visible = true;
                mbPlus.Visible = true;
                mlbDamageCount.Visible = true;
                lblDamageCount.Visible = true;
                lblAccessory.Visible = false;
                mcbAccessory.Visible = false;

            }
        }

      

        private void mbPlus_Click(object sender, EventArgs e)
        {
            DamageCount += 1;
            mlbDamageCount.Text = DamageCount.ToString();
        }

        private void mbMinus_Click(object sender, EventArgs e)
        {
            DamageCount -= 1;
            mlbDamageCount.Text = DamageCount.ToString();
        }

        private void DamageLocationForm_Load(object sender, EventArgs e)
        {

        }
    }
}