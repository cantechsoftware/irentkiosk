﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Castle.Core;
using MetroFramework.Controls;
using iRent.Classes;
using iRent2.Contracts.Models;

namespace iRent.Damages
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class DamagesControl : MetroUserControl
    {
        private List<DamageLocationAdd> _dmglist;

        public DamagesControl()
        {
            InitializeComponent();
        }

        private void Damages_Load(object sender, EventArgs e)
        {
            _dmglist = new List<DamageLocationAdd>();
            UIFactory.CurrentControl = "Damages";
            UIFactory.NextControl = "DamageOptions";
        }

       
        public void AddDamageLocation( DamageLocationForm form, string location)
        {
            var count = _dmglist.Count(n => n.DamageLocationName ==location && n.DamageType == form.DamageType);
            if (count == 0)
            {
                var dloc = new DamageLocationAdd
                {
                    DamageLocationName = location,
                    DamageType = form.DamageType,
                    DamageCount = form.DamageCount,
                    Accessory = form.Accessory
                };
                _dmglist.Add(dloc);
                dataGridView1.DataSource = _dmglist.ToList();
            }
            else
            {
                var res = MessageBox.Show(
                    String.Format("{0} Damage Location Already Added Do you Want Update", location),
                    "Alert", MessageBoxButtons.YesNo);
                if (res.ToString() == "Yes")
                {
                    DamageLocationAdd  d1;
                    if (form.DamageType  == "Accessory")
                        d1 =
                            _dmglist.FirstOrDefault(
                                n =>
                                    n.DamageLocationName == location 
                                    && n.DamageType == form.DamageType 
                                    && n.Accessory ==form.Accessory);
                    else d1 = _dmglist.FirstOrDefault(n => n.DamageLocationName == location && n.DamageType  == form.DamageType );
                    d1.DamageCount = form.DamageCount ;
                    dataGridView1.DataSource = _dmglist.ToList();
                }
            }
        }

        public event EventHandler<List<DamageModel>> Next;

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (Next != null)
                Next(this, _dmglist.Select(x => x.GetDamageModel()).ToList());
        }

        private void frontleftwing_Click(object sender, EventArgs e)
        {
            frontleftwing.BackColor = Color.Red;
            var form = new DamageLocationForm("FrontLeftWing");
            if (form.ShowDialog() == DialogResult.Cancel)
            {
                if (form.IsValid)
                {
                    AddDamageLocation(form, "FrontLeftWing" );
                }
            }
        }

        private void leftside_Click(object sender, EventArgs e)
        {
            leftside.BackColor = Color.Red;
            var form = new DamageLocationForm("Left Side");
            if (form.ShowDialog() == DialogResult.Cancel)
            {
                if (form.IsValid)
                {
                    AddDamageLocation(form, "Left Side" );
                }
            }
        }

        private void rearleftwing_Click(object sender, EventArgs e)
        {
            rearleftwing.BackColor = Color.Red;
            var form = new DamageLocationForm("RearLeftWing");
            if (form.ShowDialog() == DialogResult.Cancel)
            {
                if (form.IsValid)
                {
                    AddDamageLocation(form, "RearLeftWing");
                }
            }
        }

        private void front_Click(object sender, EventArgs e)
        {
            front.BackColor = Color.Red;
            var form = new DamageLocationForm("Front");
            if (form.ShowDialog() == DialogResult.Cancel)
            {
                if (form.IsValid)
                {
                    AddDamageLocation(form, "Front");
                }
            }
        }

        private void bonnet_Click(object sender, EventArgs e)
        {
            bonnet.BackColor = Color.Red;
            var form = new DamageLocationForm("Bonnet");
            if (form.ShowDialog() == DialogResult.Cancel)
            {
                if (form.IsValid)
                {
                    AddDamageLocation(form, "Bonnet");
                }
            }
        }

        private void roof_Click(object sender, EventArgs e)
        {
            roof.BackColor = Color.Red;
            var form = new DamageLocationForm("Roof");
            if (form.ShowDialog() == DialogResult.Cancel)
            {
                if (form.IsValid)
                {
                    AddDamageLocation(form, "Roof" );
                }
            }
        }

        private void boot_Click(object sender, EventArgs e)
        {
            boot.BackColor = Color.Red;
            var form = new DamageLocationForm("Roof");
            if (form.ShowDialog() == DialogResult.Cancel)
            {
                if (form.IsValid)
                {
                    AddDamageLocation(form, "Roof");
                }
            }
        }

        private void rear_Click(object sender, EventArgs e)
        {
            rear.BackColor = Color.Red;
            var form = new DamageLocationForm("Rear");
            if (form.ShowDialog() == DialogResult.Cancel)
            {
                if (form.IsValid)
                {
                    AddDamageLocation(form , "Rear");
                }
            }
        }

        private void frontrightwing_Click(object sender, EventArgs e)
        {
            frontrightwing.BackColor = Color.Red;
            var form = new DamageLocationForm("FrontRightWing");
            if (form.ShowDialog() == DialogResult.Cancel)
            {
                if (form.IsValid)
                {
                    AddDamageLocation(form, "FrontRightWing");
                }
            }
        }

        private void rightside_Click(object sender, EventArgs e)
        {
            rightside.BackColor = Color.Red;
            var form = new DamageLocationForm("Right Side");
            if (form.ShowDialog() == DialogResult.Cancel)
            {
                if (form.IsValid)
                {
                    AddDamageLocation(form, "Right Side");
                }
            }
        }

        private void rearrightwing_Click(object sender, EventArgs e)
        {
            rearrightwing.BackColor = Color.Red;
            var form = new DamageLocationForm("RearRightWing");
            if (form.ShowDialog() == DialogResult.Cancel)
            {
                if (form.IsValid)
                {
                    AddDamageLocation(form, "RearRightWing");
                }
            }
        }

        private void frontleftwheel_Click(object sender, EventArgs e)
        {
            frontleftwheel.BackColor = Color.Red;
            var form = new DamageLocationForm("Front Left Wheel");
            if (form.ShowDialog() == DialogResult.Cancel)
            {
                if (form.IsValid)
                {
                    AddDamageLocation(form, "Front Left Wheel" );
                }
            }
        }

        private void frontrightwheel_Click(object sender, EventArgs e)
        {
            frontrightwheel.BackColor = Color.Red;
            var form = new DamageLocationForm("Front Right Wheel");
            if (form.ShowDialog() == DialogResult.Cancel)
            {
                if (form.IsValid)
                {
                    AddDamageLocation(form , "Front Right Wheel" );
                }
            }
        }

        private void rearleftwheel_Click(object sender, EventArgs e)
        {
            rearleftwheel.BackColor = Color.Red;
            var form = new DamageLocationForm("Rear Left Wheel");
            if (form.ShowDialog() == DialogResult.Cancel)
            {
                if (form.IsValid)
                {
                    AddDamageLocation(form, "Rear Left Wheel");
                }
            }
        }

        private void accessories_Click(object sender, EventArgs e)
        {
            accessories.BackColor = Color.Red;
            var form = new DamageLocationForm("Accessory");
            if (form.ShowDialog() == DialogResult.Cancel)
            {
                AddDamageLocation(form, "Accessory");
            }
            else
            {
                MessageBox.Show(@"Error Accessories Click");
            }
        }

        private void rearrightwheel_Click(object sender, EventArgs e)
        {
            rearrightwheel.BackColor = Color.Red;
            var form = new DamageLocationForm("Rear Right Wheel");
            if (form.ShowDialog() == DialogResult.Cancel)
            {
                if (form.IsValid)
                {
                    AddDamageLocation(form, "Rear Right Wheel");
                }
            }
            else
            {
                MessageBox.Show(@"Error Rear Right Wheel Click");
            }
        }

        private void frontleftwindow_Click(object sender, EventArgs e)
        {
            frontleftwindow.BackColor = Color.Red;
            var form = new DamageLocationForm("FrontLeftWindow");
            if (form.ShowDialog() == DialogResult.Cancel)
            {
                if (form.IsValid)
                {
                    AddDamageLocation(form, "FrontLeftWindow");
    }
            }
        }

        private void frontrightmirror_Click(object sender, EventArgs e)
        {
            frontrightmirror.BackColor = Color.Red;
            var form = new DamageLocationForm("FrontRightMirror");
            if (form.ShowDialog() == DialogResult.Cancel)
            {
                if (form.IsValid)
                {
                    AddDamageLocation(form, "FrontRightMirror");
                }
            }
        }

        private void frontleftmirror_Click(object sender, EventArgs e)
        {
            frontleftmirror.BackColor = Color.Red;
            var form = new DamageLocationForm("FrontLeftMirror");
            if (form.ShowDialog() == DialogResult.Cancel)
            {
                if (form.IsValid)
                {
                    AddDamageLocation(form, "FrontLeftMirror");
                }
            }
        }

        private void rearleftwindow_Click(object sender, EventArgs e)
        {
            rearleftwindow.BackColor = Color.Red;
            var form = new DamageLocationForm("RearLeftWindow");
            if (form.ShowDialog() == DialogResult.Cancel)
            {
                if (form.IsValid)
                {
                    AddDamageLocation(form, "RearLeftWindow");
                }
            }
        }

        private void rearwindow_Click(object sender, EventArgs e)
        {
            rearwindow.BackColor = Color.Red;
            var form = new DamageLocationForm("RearWindow");
            if (form.ShowDialog() == DialogResult.Cancel)
            {
                if (form.IsValid)
                {
                    AddDamageLocation(form, "RearWindow");
                }
            }
        }

        private void frontrightwindow_Click(object sender, EventArgs e)
        {
            frontrightwindow.BackColor = Color.Red;
            var form = new DamageLocationForm("FrontRightWindow");
            if (form.ShowDialog() == DialogResult.Cancel)
            {
                if (form.IsValid)
                {
                    AddDamageLocation(form, "FrontRightWindow");
                }
            }
        }

        private void rearrightwindow_Click(object sender, EventArgs e)
        {
            rearrightwindow.BackColor = Color.Red;
            var form = new DamageLocationForm("RearRightWindow");
            if (form.ShowDialog() == DialogResult.Cancel)
            {
                if (form.IsValid)
                {
                    AddDamageLocation(form, "RearRightWindow");
                }
            }
        }

        private void frontleftlights_Click(object sender, EventArgs e)
        {
            frontleftlights.BackColor = Color.Red;
            var form = new DamageLocationForm("FrontLeftLights");
            if (form.ShowDialog() == DialogResult.Cancel)
            {
                if (form.IsValid)
                {
                    AddDamageLocation(form, "FrontLeftLights");
                }
            }
        }

        private void rearrightlights_Click(object sender, EventArgs e)
        {
           rearrightlights.BackColor = Color.Red;
            var form = new DamageLocationForm("RearRightLights");
            if (form.ShowDialog() == DialogResult.Cancel)
            {
                if (form.IsValid)
                {
                    AddDamageLocation(form, "RearRightLights");
                }
            }
        }

        private void rearleftlights_Click(object sender, EventArgs e)
        {
            rearleftlights.BackColor = Color.Red;
            var form = new DamageLocationForm("RearLeftLights");
            if (form.ShowDialog() == DialogResult.Cancel)
            {
                if (form.IsValid)
                {
                    AddDamageLocation(form, "RearLeftLights");
                }
            }
        }

        private void frontrightlights_Click(object sender, EventArgs e)
        {
            frontrightlights.BackColor = Color.Red;
            var form = new DamageLocationForm("FrontRightLights");
            if (form.ShowDialog() == DialogResult.Cancel)
            {
                if (form.IsValid)
                {
                    AddDamageLocation(form, "FrontRightLights");
                }
            }
        }
    }
}