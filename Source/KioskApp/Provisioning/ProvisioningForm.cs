﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Forms;
using Castle.Core;
using MetroFramework.Forms;
using iRent.Extensions;
using iRent.Framework;
using iRentKiosk.Core.Services.Api;

namespace iRent.Provisioning
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class ProvisioningForm : MetroForm
    {
        private readonly BranchApi _branchApi;

        public ProvisioningForm(BranchApi branchApi)
        {
            _branchApi = branchApi;
            InitializeComponent();
        }

        public int? BranchId
        {
            get { return (int?) cboBranch.SelectedValue; }
        }

        public string Password
        {
            get { return txtPassword.Text; }
        }

        public string KioskName
        {
            get { return txtName.Text; }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            txtName.Text = Environment.MachineName;
            BusyForm.RunLongTask("Initializing", LoadProvinces);
            txtPassword.Focus();
        }

        private void LoadProvinces(object sender, DoWorkEventArgs e)
        {
            try
            {
                var provinces = _branchApi.GetProvinces();
                cboProvince.InvokeIfRequired(x => x.DataSource = provinces);
            }
            catch (Exception ex)
            {
                Logger.Log(this, "LoadProvinces", "Error loading provinces", ex);
                Errors.Notify("Error loading provinces", ex);
            }
        }

        private void btnProvision_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void cboProvince_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboProvince.SelectedItem != null)
            {
                var province = (string) cboProvince.SelectedItem;
                Task.Run(() => LoadBranches(province));
            }
        }

        private void LoadBranches(string province)
        {
            try
            {
                var branches = _branchApi.GetBranches(province);
                cboBranch.InvokeIfRequired(x =>
                    {
                        if (cboProvince.SelectedItem == province)
                            x.DataSource = branches;
                    });
            }
            catch (Exception ex)
            {
                Logger.Log(this, "LoadBranches", "Error loading branches", ex);
                Errors.Notify("Error loading branches", ex);
            }
        }
    }
}