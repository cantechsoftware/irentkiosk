﻿using System;
using System.IO;
//using System.IO.IsolatedStorage;
using System.Linq;
using System.Net.NetworkInformation;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using CanTech.Common.Extensions;
using Castle.Core;
using iRent2.Contracts.Kiosks;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;
using System.Management;
using log4net;

namespace iRent.Provisioning
{
    [CastleComponent]
    public class ProvisioningService : IStartable
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof (ProvisioningService));

        private readonly ApiClient _apiClient;
        private readonly Func<ProvisioningForm> _formFactory;
        private readonly KioskApi _kioskApi;

        public ProvisioningService(KioskApi kioskApi, ApiClient apiClient, Func<ProvisioningForm> formFactory)
        {
            _kioskApi = kioskApi;
            _apiClient = apiClient;
            _formFactory = formFactory;
            //Username = "newkiosk";
            //Password = "JF1%dwl)84&pQ1Nk$IckfhNAOpp(yz4h";

            Username = "newkiosk2";
            Password = "MkDkBPh#..ZA25a + 8";

        }

        public string Username { get; set; }
        public string Password { get; set; }

        public void Start()
        {
            try
            {
                string mac = GetMacAddress();
                string key = mac;
                string cpuid = null;
                if (ShouldUseCpuID())
                {
                    cpuid = GetCpuId();
                    key = cpuid;
                }

                if (cpuid == null)
                {
                    cpuid = GetCpuId();
                }

                var cred = GetCredentials(key);

                if (cred != null)
                {
                    _apiClient.Username = cred.Item1;
                    _apiClient.Password = cred.Item2;
                    if (InitKioskData(mac, cpuid))
                        return;
                }

                cred = Provision(mac);

                if (cred == null)
                {
                    return;
                }

                Save(cred, cpuid);
                _apiClient.Username = cred.Item1;
                _apiClient.Password = cred.Item2;
            }
            catch (Exception ex)
            {
                _log.Error("Error during ProvisioningService.Start: ", ex);
                MessageBox.Show("Error Occurred during ProvisioningService.Start: \n" + ex.Message);

            }
        }

        private bool ShouldUseCpuID()
        {
            if (File.Exists("cdt.dtb"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool InitKioskData(string mac, string cpuid)
        {
            try
            {
                var kiosk = _apiClient.GetJson<KioskModel>("api/v1/kiosks/get/" + mac + "/"+ cpuid);
                if (kiosk != null)
                {
                    ContextData.Kiosk = kiosk;
                    return true;
                }
            }
            catch (Exception ex)
            {
                _log.Error("Error in InitKioskData", ex);
                MessageBox.Show("Error Occurred during ProvisioningService.InitKioskData: \n" + ex.Message);
            }
            return false;
        }

        public void Stop()
        {
        }

        private Tuple<string, string> Provision(string mac)
        {
                _apiClient.Username = Username;
                _apiClient.Password = Password;
                using (var form = _formFactory())
                {
                    if (form.ShowDialog() == DialogResult.OK)
                    {
                        try
                        {
                            var rsp = _kioskApi.Provision(
                                new ProvisionKioskRequest
                                    {
                                        BranchId = form.BranchId,
                                        MacAddress = mac,
                                        Name = form.KioskName,
                                        Password = form.Password
                                    });

                            ContextData.Kiosk = rsp.Kiosk;
                            return Tuple.Create(rsp.Username, rsp.Password);
                        }
                        catch (ApiException ex)
                        {
                            _log.Error("Error during ProvisioningService.Provision: ", ex);
                            MessageBox.Show("Error provisioning ProvisioningService.Provision: "+ex.Message, "Error");
                        }
                        catch (Exception ex)
                        {
                            _log.Error("General Error during ProvisioningService.Provision: ", ex);
                            MessageBox.Show("General Error provisioning ProvisioningService.Provision: " + ex.Message, "Error");
                        }
                    }
                }
            Application.Exit();
            return null;
        }

        private Tuple<string, string> GetCredentials(string mac)
        {
            if (!File.Exists("auth.dat"))
                return null;
            try
            {
                var raw = File.ReadAllBytes("auth.dat");
                using (var sha = new SHA512Managed())
                {
                    var entropy = sha.ComputeHash(Encoding.Default.GetBytes(mac));
                    var decrypted = ProtectedData.Unprotect(raw, entropy, DataProtectionScope.LocalMachine);
                    var text = Encoding.Default.GetString(decrypted);
                    var parts = text.Split(':');
                    return Tuple.Create(parts[0], parts[1]);
                }
            }
            catch (Exception ex)
            {
                _log.Error("Error getting credentials", ex);
                return null;
            }
        }

        private string GetMacAddress()
        {
            var storedMac = GetStoredMac();
            if (storedMac.HasValue())
                return storedMac;

            const int MIN_MAC_ADDR_LENGTH = 12;
            var macAddress = string.Empty;
            long maxSpeed = -1;

            foreach (
                var nic in
                NetworkInterface.GetAllNetworkInterfaces()
                    .Where(
                        x =>
                            x.NetworkInterfaceType.In(NetworkInterfaceType.Ethernet,
                                NetworkInterfaceType.Ethernet3Megabit, NetworkInterfaceType.FastEthernetFx,
                                NetworkInterfaceType.FastEthernetT,
                                NetworkInterfaceType.GigabitEthernet, NetworkInterfaceType.Wireless80211)))
            {
                _log.Debug(
                    "Found MAC Address: " + nic.GetPhysicalAddress() +
                    " Type: " + nic.NetworkInterfaceType);

                var tempMac = nic.GetPhysicalAddress().ToString();
                if (nic.Speed > maxSpeed &&
                    !string.IsNullOrEmpty(tempMac) &&
                    tempMac.Length >= MIN_MAC_ADDR_LENGTH)
                {
                    _log.Debug("New Max Speed = " + nic.Speed + ", MAC: " + tempMac);
                    maxSpeed = nic.Speed;
                    macAddress = tempMac;
                }
            }

            StoreMac(macAddress);

            return macAddress;
        }

        private string GetCpuId()
        {
            //StringBuilder builder = new StringBuilder();
            string data = "";
            String query = "SELECT * FROM Win32_BIOS";
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(query);
            //  This should only find one
            foreach (ManagementObject item in searcher.Get())
            {
                data += item["Manufacturer"].ToString();
                data += "|" + item["SerialNumber"].ToString();
                data += "|" + Environment.MachineName;
                data = Convert.ToBase64String(Encoding.UTF8.GetBytes(data));
            }

            StoreCpuId(data);
            return data;
        }

        private void ReencodeToCpuId(string cpuid)
        {
            string mac = GetMacAddress();
            Tuple<string, string> credentials = GetCredentials(mac);
            Save(credentials, cpuid);
        }

        private void StoreCpuId(string cpuid)
        {
            if (!File.Exists("cdt.dtb"))
            {
                ReencodeToCpuId(cpuid);
            }

            using (FileStream fs = File.Open("cdt.dtb", FileMode.Create))
            {
                byte[] data = Encoding.UTF8.GetBytes(cpuid);
                using (var sha = new SHA512Managed())
                {
                    var entropy = sha.ComputeHash(Encoding.Default.GetBytes(Environment.MachineName));
                    var enc = ProtectedData.Protect(data, entropy, DataProtectionScope.CurrentUser);
                    fs.Write(enc, 0, enc.Length);
                }
            }
        }

        private string GetStoredCpuId(string cpuid)
        {
            if (!File.Exists("cdt.dtb"))
                return null;

            using (var fs = File.Open("cdt.dtb", FileMode.Open))
            {
                var raw = new byte[fs.Length];
                fs.Read(raw, 0, raw.Length);

                using (var sha = new SHA512Managed())
                {
                    try
                    {
                        var entropy = sha.ComputeHash(Encoding.Default.GetBytes(Environment.MachineName));
                        var decrypted = ProtectedData.Unprotect(raw, entropy, DataProtectionScope.CurrentUser);
                        return Encoding.Default.GetString(decrypted);
                    }
                    catch (Exception ex)
                    {
                        _log.Warn("Error decrypting stored Computer identity indicator", ex);
                        return null;
                    }
                }
            }
        }

        private void StoreMac(string mac)
        {
            using (var fs = File.Open("kmf.bin", FileMode.Create))
            {
                var raw = Encoding.Default.GetBytes(mac);
                using (var sha = new SHA512Managed())
                {
                    var entropy = sha.ComputeHash(Encoding.Default.GetBytes(Environment.MachineName));
                    var enc = ProtectedData.Protect(raw, entropy, DataProtectionScope.CurrentUser);
                    fs.Write(enc, 0, enc.Length);
                }
            }
        }

        private string GetStoredMac()
        {
            if (!File.Exists("kmf.bin"))
                return null;

            using (var fs = File.Open("kmf.bin", FileMode.Open))
            {
                var raw = new byte[fs.Length];
                fs.Read(raw, 0, raw.Length);

                using (var sha = new SHA512Managed())
                {
                    try
                    {
                        var entropy = sha.ComputeHash(Encoding.Default.GetBytes(Environment.MachineName));
                        var decrypted = ProtectedData.Unprotect(raw, entropy, DataProtectionScope.CurrentUser);
                        return Encoding.Default.GetString(decrypted);
                    }
                    catch (Exception ex)
                    {
                        _log.Warn("Error decrypting stored mac", ex);
                        return null;
                    }
                }
            }
        }

        private void Save(Tuple<string, string> cred, string key)
        {
            try
            {
                var text = cred.Item1 + ":" + cred.Item2;
                var data = Encoding.Default.GetBytes(text);
                using (var sha = new SHA512Managed())
                {
                    var entropy = sha.ComputeHash(Encoding.Default.GetBytes(key));
                    var enc = ProtectedData.Protect(data, entropy, DataProtectionScope.CurrentUser);
                    File.WriteAllBytes("auth.dat", enc);
                }
            }
            catch (Exception ex)
            {
                _log.Error("Error during ProvisioningService.Save: ", ex);
                MessageBox.Show("Error during ProvisioningService.Save: \n" + ex.Message);
            }
        }
    }
}