﻿using System;
using CanTech.Common;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Services.Api;
using log4net;

namespace iRent.Framework
{
    public static class Logger
    {
        public static ILog For<TCategory>()
        {
            return LogManager.GetLogger(typeof(TCategory));
        }

        private static readonly ApiClient ApiClient = IoC.Resolve<ApiClient>();

        public static Guid SessionID { get; set; }
        public static Guid RecordID { get; set; }

        public static void Log(object source, string activity, string description, Exception ex = null,
            bool billable = false)
        {
            Log(source.GetType().Name, activity, description, ex, billable);
        }

        public static void Log(string module,
            string activity,
            string description,
            Exception ex = null,
            bool billable = false)
        {
            try
            {
                var log = LogManager.GetLogger(module);
                if (ex != null)
                    log.Error($"{activity} - {description}", ex);
                else
                    log.Info($"{activity} - {description}");

                var logEntry = new LogEntryModel
                {
                    Application = "Kiosk",
                    Module = module,
                    Activity = activity,
                    Description = description,
                    EventDate = DateTime.Now,
                    SessionId = SessionID.ToString(),
                    Billable = billable
                };
                if (ex != null)
                    logEntry.Exception = ex.ToString();

                ApiClient.PostJson("api/v1/logs/save", logEntry);
            }
            catch (Exception ex2)
            {
                Errors.Notify("Unexpected error saving log", ex2);
            }
        }
    }
}