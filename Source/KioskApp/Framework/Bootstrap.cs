﻿using System.IO;
using CanTech.Common;
using CanTech.Common.Extensions;
using Castle.Facilities.TypedFactory;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Castle.Windsor.Installer;
using Core.Crypto;
using iRent.Services.RfidScanners;
using iRent.StaffAuthentication;
using iRentKiosk.Core.Config;
using iRentKiosk.Core.Infrastructure;

namespace iRent.Framework
{
    public static class Bootstrap
    {
        public static void Container()
        {
            var container = new WindsorContainer(ContainerConfig.ForSection("settings"));
            container.AddFacility<TypedFactoryFacility>()
                .Install(FromAssembly.Containing<iRentKioskCoreInstaller>(),
                    FromAssembly.This());

            foreach (var plugin in PluginConfig.GetPlugins("plugins"))
            {
                var pluginPath = Path.GetFullPath(plugin);
                container.Install(FromAssembly.Named(pluginPath));
            }

            container.RegisterComponents<MainForm>();

            container.Register(Component.For<OTP>());

            container.Register(
                Castle.MicroKernel.Registration.Classes.FromThisAssembly()
                    .BasedOn<IRfidMethod>()
                    .LifestyleSingleton()
                    .WithServiceAllInterfaces()
                    .WithServiceSelf()
                    .Configure(x => x.Named(x.Implementation.Name)));

            container.Register(Component.For<StaffAuthenticationMethod>());

            IoC.Init(container);
            IoC.Start();
        }
    }
}