﻿using System;
using System.Windows.Forms;
using iRent.Extensions;

namespace iRent.Framework
{
    public static class Errors
    {
        public static void Notify(string description, Exception ex)
        {
            Logger.Log(ex, ex.Source, description, ex);

            Form.ActiveForm.InvokeIfRequired(
                x => MessageBox.Show(
                    description + "\r\n" + ex.Message,
                    "An Error Occurred",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error));
        }
    }
}