﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using CanTech.Common;
using CanTech.Common.Util;
using iRent.Framework;
using log4net;
using log4net.Config;
using System.Net;
using System.Configuration;

namespace iRent
{
    internal static class Program
    {
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool SetForegroundWindow(IntPtr hWnd);

        private static readonly ILog _log = LogManager.GetLogger(typeof(Program));


        private static void OnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception ex = e.ExceptionObject as Exception;
            _log.Fatal("Unhandled exception", ex);
            MessageBox.Show(ex.Message);
        }

        private static bool TestForProxyConnectivity()
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["Proxy Uri"]);
                request.Timeout = 10000;
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        ///     The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            AppDomain.CurrentDomain.UnhandledException += OnUnhandledException;

            XmlConfigurator.ConfigureAndWatch(new FileInfo("log4net.config"));
            Log4NetFactoryAdapter.Init();

#if DEBUG
            var path = Environment.GetEnvironmentVariable("PATH");
            var dllDirectory = Path.GetFullPath("../../../lib");
            path += ";" + dllDirectory;
            var ntechDir = Path.Combine(dllDirectory, "NeuroTech");
            path += ";" + ntechDir;
            var winTone = Path.Combine(dllDirectory, "Wintone");
            path += ";" + winTone;
            Environment.SetEnvironmentVariable("PATH", path);
#else
            var path = Environment.GetEnvironmentVariable("PATH");
            var dllDirectory = Path.GetFullPath(".");
            var ntechDir = Path.Combine(dllDirectory, "NeuroTech");
            path += ";" + ntechDir;
              var winTone = Path.Combine(dllDirectory, "Wintone");
            path += ";" + winTone;
            Environment.SetEnvironmentVariable("PATH", path);
#endif

            if (ConfigurationManager.AppSettings["ProxyTest"] == "True")
            {
                if (TestForProxyConnectivity() == false)
                {
                    if (MessageBox.Show(ConfigurationManager.AppSettings["ProxyText"], ConfigurationManager.AppSettings["ProxyCaption"], MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        System.Diagnostics.Process.Start(ConfigurationManager.AppSettings["ProxySite"], ConfigurationManager.AppSettings["ProxySiteParams"]);
                    }
                    if (MessageBox.Show("Click Yes to continue or No to Quit?","Can we begin?",MessageBoxButtons.YesNo) == DialogResult.No)
                    {
                        Application.Exit();
                    }
                }
            }

            bool createdNew = true;
            using (new Mutex(true, "iRentKiosk", out createdNew))
            {
                if (createdNew)
                {
                    Run();
                }
                else
                {
                    var current = Process.GetCurrentProcess();
                    foreach (var process in Process.GetProcessesByName(current.ProcessName))
                    {
                        if (process.Id != current.Id)
                        {
                            SetForegroundWindow(process.MainWindowHandle);
                            break;
                        }
                    }
                }
            }

        }

        private static void Run()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Bootstrap.Container();
            Application.Run(IoC.Resolve<MainForm>());
        }
    }
}