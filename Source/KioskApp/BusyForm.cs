﻿using System;
using System.ComponentModel;
using MetroFramework.Forms;

namespace iRent
{
    public partial class BusyForm : MetroForm, IDisposable
    {
        protected BackgroundWorker Worker = new BackgroundWorker();

        private readonly object _argument;
        private readonly DoWorkEventHandler _callback;
        private Exception _error;
        private RunWorkerCompletedEventArgs _results;

        public BusyForm(string title)
        {
            SetExecutionText(title);
        }

        private BusyForm(string title, DoWorkEventHandler callback)
            : this(title, callback, false, null)
        {
        }

        private BusyForm(string title, DoWorkEventHandler callback, object args)
            : this(title, callback, false, args)
        {
        }

        private BusyForm(string title, DoWorkEventHandler callback, bool reportsProgress)
            : this(title, callback, reportsProgress, null)
        {
        }

        private BusyForm(string title, DoWorkEventHandler callback, bool reportsProgress, object args,
            EventHandler cancelHandler)
        {
            InitializeComponent();

            //if (!reportsProgress)
            //    progressBar.IsIndeterminate = true;


            SetExecutionText(title);
            this._callback = callback;
            _argument = args;
            Worker.WorkerReportsProgress = reportsProgress;
            Worker.DoWork += BusyForm_DoWork;
            Worker.RunWorkerCompleted += BusyForm_RunWorkerCompleted;
            Worker.ProgressChanged += BusyForm_ProgressChanged;

            if (cancelHandler != null)
                Worker.WorkerSupportsCancellation = true;
        }

        private BusyForm(string title, DoWorkEventHandler callback, bool reportsProgress, object args)
        {
            InitializeComponent();

            //if (!reportsProgress)
            //progressBar.IsIndeterminate = true;


            SetExecutionText(title);
            this._callback = callback;
            _argument = args;
            Worker.WorkerReportsProgress = reportsProgress;
            Worker.DoWork += BusyForm_DoWork;
            Worker.RunWorkerCompleted += BusyForm_RunWorkerCompleted;
            Worker.ProgressChanged += BusyForm_ProgressChanged;

            //if (cancelHandler != null)
            //{
            //    _worker.WorkerSupportsCancellation = true;
            //    //btnCancel.Click += cancelHandler;
            //    //btnCancel.Click += new RoutedEventHandler(PostOnCancelClick);
            //    //btnCancel.IsEnabled = true;
            //    //btnCancel.Visible = true;
            //}
        }

        public void Dispose()
        {
            //  throw new NotImplementedException();
        }

        private void PostOnCancelClick(object sender, EventArgs e)
        {
            //btnCancel.IsEnabled = false;
            //btnCancel.Refresh();
        }

        public static RunWorkerCompletedEventArgs RunLongTask(string title, DoWorkEventHandler callback)
        {
            return RunLongTask(title, callback, false);
        }

        public static RunWorkerCompletedEventArgs RunLongTask(string title, DoWorkEventHandler callback,
            bool reportsProgress)
        {
            using (var frmLongTask = new BusyForm(title, callback, reportsProgress))
            {
                frmLongTask.ShowDialog();
                if (frmLongTask._error != null)
                    throw new Exception("Error running task", frmLongTask._error);
                return frmLongTask._results;
            }
        }

        public static RunWorkerCompletedEventArgs RunLongTask(string title, DoWorkEventHandler callback,
            bool reportsProgress, object args)
        {
            using (var frmLongTask = new BusyForm(title, callback, reportsProgress, args))
            {
                frmLongTask.ShowDialog();
                if (frmLongTask._error != null)
                    throw frmLongTask._error;
                return frmLongTask._results;
            }
        }

        public static RunWorkerCompletedEventArgs RunLongTask(string title, DoWorkEventHandler callback,
            bool reportsProgress, object args, EventHandler cancelHandler)
        {
            using (var frmLongTask = new BusyForm(title, callback, reportsProgress, args, cancelHandler))
            {
                frmLongTask.ShowDialog();
                if (frmLongTask._error != null)
                {
                    frmLongTask.Close();
                    throw frmLongTask._error;
                }
                frmLongTask.Close();
                return frmLongTask._results;
            }
        }

        private void BusyForm_Shown(object sender, EventArgs e)
        {
            Worker.RunWorkerAsync(_argument);
        }


        private void Window_ContentRendered(object sender, EventArgs e)
        {
            Worker.RunWorkerAsync(_argument);
        }

        private void BusyForm_Load(object sender, EventArgs e)
        {
            Worker.RunWorkerAsync(_argument);
        }

        #region Background worker

        private void BusyForm_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                if (_callback != null)
                    _callback(sender, e);
            }
            catch (Exception ex)
            {
                _error = ex;
            }
        }

        private void BusyForm_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            var text = e.UserState as string;
            if (text != null)
                SetExecutionText(text);

            SetExecutionValue(e.ProgressPercentage);
        }

        private void BusyForm_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _results = e;
            Close();
        }

        #endregion

        #region Setters/Getters

        public void SetExecutionText(string text)
        {
            try
            {
                //	if (lblOperation.Dispatche)
                //{
                //    lblOperation.Dispatcher.Invoke((MethodInvoker)delegate()
                //    {
                //        lblOperation.Content = text;
                //    });
                //}
                //else
                {
                    lblOperation.Text = text;
                }
            }
            finally
            {
            }
        }

        public void SetExecutionValue(int value)
        {
            try
            {
                //	if ()
                {
                    //progressBar.Dispatcher.Invoke((MethodInvoker)delegate()
                    //{
                    //    progressBar.Value = value;
                    //});
                }
                //else
                {
                    progressBar.Value = value;
                }
            }
            finally
            {
            }
        }

        #endregion
    }
}