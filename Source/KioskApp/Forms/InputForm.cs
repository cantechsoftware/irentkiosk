﻿using System.Windows.Forms;
using MetroFramework.Forms;

namespace iRent.Forms
{
    public partial class InputForm : MetroForm
    {
        public InputForm(Control control)
        {
            InitializeComponent();
            pnlForm.Controls.Add(control);
        }        
    }
}