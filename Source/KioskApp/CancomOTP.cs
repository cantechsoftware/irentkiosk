﻿using System;
using System.Windows.Forms;
using Castle.Core;
using Core.Crypto;
using MetroFramework.Forms;
using iRent.Classes;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;

namespace iRent
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class CancomOTP : MetroForm
    {
        private readonly OTP _otp;
        private readonly SmsApi _smsApi;

        private string _cellNo;
        private string _pin;

        public CancomOTP(OTP otp, SmsApi smsApi)
        {
            _otp = otp;
            _smsApi = smsApi;
            InitializeComponent();
        }

        public DialogResult Verify(string pin, string header, string cellno)
        {
            metroLabel1.Text = @"Please enter the One-Time-Pin sent to :" + cellno;
            _pin = pin;
            _cellNo = cellno;            

            return ShowDialog();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (_pin == txtOTP.Text.Trim())
            {
                DialogResult = DialogResult.OK;
                Close();
            }
            else
            {
                lblStatus.Text = @"OTP Incorrect. Click Resend to try again.";
            }
        }

        private void btnResend_Click(object sender, EventArgs e)
        {
            _pin = _otp.GetNextOTP();

            txtOTP.Text = "";

            _smsApi.SendClientSms(new SmsModel
                {
                    Message = "Please enter the following OTP :" + _pin,
                    Msisdn = _cellNo
                });

            lblStatus.Text = @"OTP has been resent.";
        }

        private void CancomOTP_Load(object sender, EventArgs e)
        {

        }
    }
}