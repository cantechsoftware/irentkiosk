﻿using System;
using System.Windows.Forms;
using Castle.Core;
using iRent.Framework;

namespace iRent.Fleet
{
    [CastleComponent]
    public class SummaryCommand : BaseCommand
    {
        private readonly Func<SummaryControl> _factory;

        public SummaryCommand(Func<SummaryControl> factory)
        {
            _factory = factory;
        }

        public string Next { get; set; }

        public override void Invoke()
        {
            var control = _factory();
            control.Dock = DockStyle.Fill;
            var main = GetFormAs<MainForm>();
            main.AddToFormPanel(control);
            control.Next += (o, e) => Proceed(Next);
        }
    }
}