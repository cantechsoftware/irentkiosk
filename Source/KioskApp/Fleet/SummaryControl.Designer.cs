﻿namespace iRent.Fleet
{
    partial class SummaryControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlSummary = new MetroFramework.Controls.MetroPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.lblStatus = new MetroFramework.Controls.MetroLabel();
            this.txtTotalCost = new MetroFramework.Controls.MetroTextBox();
            this.txtReturnDateTime = new MetroFramework.Controls.MetroTextBox();
            this.txtVehicleCost = new MetroFramework.Controls.MetroTextBox();
            this.txtVehicle = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.lblIntroText = new MetroFramework.Controls.MetroLabel();
            this.lblstatus1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.txtVehicleGroup = new MetroFramework.Controls.MetroTextBox();
            this.btnNext = new MetroFramework.Controls.MetroButton();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pnlSummary.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlSummary
            // 
            this.pnlSummary.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel1.SetColumnSpan(this.pnlSummary, 3);
            this.pnlSummary.Controls.Add(this.tableLayoutPanel2);
            this.pnlSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSummary.HorizontalScrollbarBarColor = true;
            this.pnlSummary.HorizontalScrollbarHighlightOnWheel = false;
            this.pnlSummary.HorizontalScrollbarSize = 10;
            this.pnlSummary.Location = new System.Drawing.Point(3, 50);
            this.pnlSummary.Name = "pnlSummary";
            this.pnlSummary.Size = new System.Drawing.Size(601, 417);
            this.pnlSummary.TabIndex = 11;
            this.pnlSummary.VerticalScrollbarBarColor = true;
            this.pnlSummary.VerticalScrollbarHighlightOnWheel = false;
            this.pnlSummary.VerticalScrollbarSize = 10;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.66667F));
            this.tableLayoutPanel2.Controls.Add(this.lblStatus, 0, 7);
            this.tableLayoutPanel2.Controls.Add(this.txtTotalCost, 1, 6);
            this.tableLayoutPanel2.Controls.Add(this.txtReturnDateTime, 1, 5);
            this.tableLayoutPanel2.Controls.Add(this.txtVehicleCost, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.txtVehicle, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.metroLabel5, 0, 6);
            this.tableLayoutPanel2.Controls.Add(this.metroLabel4, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.metroLabel3, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.metroLabel2, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.lblIntroText, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblstatus1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.metroLabel1, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.txtVehicleGroup, 1, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 8;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(599, 415);
            this.tableLayoutPanel2.TabIndex = 14;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.lblStatus, 2);
            this.lblStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblStatus.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblStatus.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblStatus.Location = new System.Drawing.Point(3, 350);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(593, 65);
            this.lblStatus.TabIndex = 36;
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblStatus.Visible = false;
            // 
            // txtTotalCost
            // 
            this.txtTotalCost.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTotalCost.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtTotalCost.FontWeight = MetroFramework.MetroTextBoxWeight.Bold;
            this.txtTotalCost.Lines = new string[0];
            this.txtTotalCost.Location = new System.Drawing.Point(202, 303);
            this.txtTotalCost.MaxLength = 32767;
            this.txtTotalCost.Name = "txtTotalCost";
            this.txtTotalCost.PasswordChar = '\0';
            this.txtTotalCost.ReadOnly = true;
            this.txtTotalCost.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtTotalCost.SelectedText = "";
            this.txtTotalCost.Size = new System.Drawing.Size(394, 44);
            this.txtTotalCost.TabIndex = 35;
            this.txtTotalCost.UseSelectable = true;
            this.txtTotalCost.Visible = false;
            // 
            // txtReturnDateTime
            // 
            this.txtReturnDateTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtReturnDateTime.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtReturnDateTime.FontWeight = MetroFramework.MetroTextBoxWeight.Bold;
            this.txtReturnDateTime.Lines = new string[0];
            this.txtReturnDateTime.Location = new System.Drawing.Point(202, 253);
            this.txtReturnDateTime.MaxLength = 32767;
            this.txtReturnDateTime.Name = "txtReturnDateTime";
            this.txtReturnDateTime.PasswordChar = '\0';
            this.txtReturnDateTime.ReadOnly = true;
            this.txtReturnDateTime.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtReturnDateTime.SelectedText = "";
            this.txtReturnDateTime.Size = new System.Drawing.Size(394, 44);
            this.txtReturnDateTime.TabIndex = 34;
            this.txtReturnDateTime.UseSelectable = true;
            this.txtReturnDateTime.Visible = false;
            // 
            // txtVehicleCost
            // 
            this.txtVehicleCost.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVehicleCost.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtVehicleCost.FontWeight = MetroFramework.MetroTextBoxWeight.Bold;
            this.txtVehicleCost.Lines = new string[0];
            this.txtVehicleCost.Location = new System.Drawing.Point(202, 203);
            this.txtVehicleCost.MaxLength = 32767;
            this.txtVehicleCost.Name = "txtVehicleCost";
            this.txtVehicleCost.PasswordChar = '\0';
            this.txtVehicleCost.ReadOnly = true;
            this.txtVehicleCost.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtVehicleCost.SelectedText = "";
            this.txtVehicleCost.Size = new System.Drawing.Size(394, 44);
            this.txtVehicleCost.TabIndex = 33;
            this.txtVehicleCost.UseSelectable = true;
            this.txtVehicleCost.Visible = false;
            // 
            // txtVehicle
            // 
            this.txtVehicle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVehicle.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtVehicle.FontWeight = MetroFramework.MetroTextBoxWeight.Bold;
            this.txtVehicle.Lines = new string[0];
            this.txtVehicle.Location = new System.Drawing.Point(202, 153);
            this.txtVehicle.MaxLength = 32767;
            this.txtVehicle.Name = "txtVehicle";
            this.txtVehicle.PasswordChar = '\0';
            this.txtVehicle.ReadOnly = true;
            this.txtVehicle.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtVehicle.SelectedText = "";
            this.txtVehicle.Size = new System.Drawing.Size(394, 44);
            this.txtVehicle.TabIndex = 32;
            this.txtVehicle.UseSelectable = true;
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel5.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel5.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel5.Location = new System.Drawing.Point(3, 300);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(193, 50);
            this.metroLabel5.TabIndex = 30;
            this.metroLabel5.Text = "Total Cost";
            this.metroLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroLabel5.Visible = false;
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel4.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel4.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel4.Location = new System.Drawing.Point(3, 250);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(193, 50);
            this.metroLabel4.TabIndex = 29;
            this.metroLabel4.Text = "Return Date Time";
            this.metroLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroLabel4.Visible = false;
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel3.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel3.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel3.ForeColor = System.Drawing.Color.Black;
            this.metroLabel3.Location = new System.Drawing.Point(3, 200);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(193, 50);
            this.metroLabel3.TabIndex = 28;
            this.metroLabel3.Text = "Vehicle Cost";
            this.metroLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroLabel3.Visible = false;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel2.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel2.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel2.Location = new System.Drawing.Point(3, 150);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(193, 50);
            this.metroLabel2.TabIndex = 27;
            this.metroLabel2.Text = "Vehicle";
            this.metroLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblIntroText
            // 
            this.lblIntroText.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.lblIntroText, 2);
            this.lblIntroText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblIntroText.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblIntroText.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblIntroText.Location = new System.Drawing.Point(3, 0);
            this.lblIntroText.Name = "lblIntroText";
            this.lblIntroText.Size = new System.Drawing.Size(593, 50);
            this.lblIntroText.TabIndex = 25;
            this.lblIntroText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblstatus1
            // 
            this.lblstatus1.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.lblstatus1, 2);
            this.lblstatus1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblstatus1.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblstatus1.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblstatus1.Location = new System.Drawing.Point(3, 50);
            this.lblstatus1.Name = "lblstatus1";
            this.lblstatus1.Size = new System.Drawing.Size(593, 50);
            this.lblstatus1.TabIndex = 24;
            this.lblstatus1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel1.Location = new System.Drawing.Point(3, 100);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(193, 50);
            this.metroLabel1.TabIndex = 26;
            this.metroLabel1.Text = "Vehicle Group";
            this.metroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroLabel1.Visible = false;
            // 
            // txtVehicleGroup
            // 
            this.txtVehicleGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVehicleGroup.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtVehicleGroup.FontWeight = MetroFramework.MetroTextBoxWeight.Bold;
            this.txtVehicleGroup.Lines = new string[0];
            this.txtVehicleGroup.Location = new System.Drawing.Point(202, 103);
            this.txtVehicleGroup.MaxLength = 32767;
            this.txtVehicleGroup.Name = "txtVehicleGroup";
            this.txtVehicleGroup.PasswordChar = '\0';
            this.txtVehicleGroup.ReadOnly = true;
            this.txtVehicleGroup.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtVehicleGroup.SelectedText = "";
            this.txtVehicleGroup.Size = new System.Drawing.Size(394, 44);
            this.txtVehicleGroup.TabIndex = 31;
            this.txtVehicleGroup.UseSelectable = true;
            this.txtVehicleGroup.Visible = false;
            // 
            // btnNext
            // 
            this.btnNext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNext.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnNext.Location = new System.Drawing.Point(206, 473);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(194, 94);
            this.btnNext.TabIndex = 13;
            this.btnNext.Text = "Issue Card";
            this.btnNext.UseSelectable = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.pnlSummary, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnNext, 1, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(607, 570);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // SummaryControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "SummaryControl";
            this.Size = new System.Drawing.Size(607, 570);
            this.Load += new System.EventHandler(this.Summary_Load);
            this.pnlSummary.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroButton btnNext;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private MetroFramework.Controls.MetroLabel lblIntroText;
        private MetroFramework.Controls.MetroLabel lblstatus1;
        private MetroFramework.Controls.MetroTextBox txtTotalCost;
        private MetroFramework.Controls.MetroTextBox txtReturnDateTime;
        private MetroFramework.Controls.MetroTextBox txtVehicleCost;
        private MetroFramework.Controls.MetroTextBox txtVehicle;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox txtVehicleGroup;
        private MetroFramework.Controls.MetroPanel pnlSummary;
        private MetroFramework.Controls.MetroLabel lblStatus;
    }
}
