﻿using System;
using Castle.Core;
using MetroFramework.Controls;
using iRentKiosk.Core.Services;
using System.Configuration;

namespace iRent.Fleet
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class SummaryControl : MetroUserControl
    {
        public SummaryControl()
        {
            InitializeComponent();
        }

        private void Summary_Load(object sender, EventArgs e)
        {
            txtVehicleGroup.Text = ContextData.VehicleGroup.Name;
            txtVehicle.Text = ContextData.VehicleName;
            txtVehicleCost.Text = ContextData.VehicleCost.ToString("C");
            txtTotalCost.Text = ContextData.VehicleCost.ToString("C");
            txtReturnDateTime.Text = ContextData.VehicleReturnDate.ToString();
            lblIntroText.Text = "Thank you " + ContextData.DriverName + " for using our system:";
            lblstatus1.Text = "Please select Issue Card.";
        }


        public event EventHandler Next;

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (Next != null)
            {
                if (Properties.Settings.Default.RFIDType == "TapAndGo")
                    lblStatus.Text = "Place RFID on Tap & Go reader.";
                Next(this, e);
            }
        }
    }
}