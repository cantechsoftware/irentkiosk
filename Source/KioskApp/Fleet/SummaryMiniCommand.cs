﻿using System;
using System.Windows.Forms;
using Castle.Core;
using SmartFormat;
using iRent.Framework;
using iRentKiosk.Core.Services;

namespace iRent.Fleet
{
    [CastleComponent]
    public class SummaryMiniCommand : BaseCommand
    {
        private readonly Func<SummaryMiniControl> _factory;

        public SummaryMiniCommand(Func<SummaryMiniControl> factory)
        {
            _factory = factory;

            Intro = "Thank you {DriverName} for using our system:";
            Status = "Please click Next to proceed or Back to change your rental.";
        }

        public string Intro { get; set; }
        public string Status { get; set; }

        public string Next { get; set; }
        public string Back { get; set; }

        public override void Invoke()
        {
            var control = _factory();
            control.Dock = DockStyle.Fill;
            var main = GetFormAs<MainForm>();
            main.AddToFormPanel(control);
            control.Init(Intro.FormatSmart(ContextData.Instance),
                         Status.FormatSmart(ContextData.Instance));
            control.Next += (o, e) => Proceed(Next);
            control.Back += (o, e) => Proceed(Back);
        }
    }
}