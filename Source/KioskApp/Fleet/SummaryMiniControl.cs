﻿using System;
using Castle.Core;
using MetroFramework.Controls;
using iRentKiosk.Core.Services;

namespace iRent.Fleet
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class SummaryMiniControl : MetroUserControl
    {
        public SummaryMiniControl()
        {
            InitializeComponent();
        }

        private void Summary_Load(object sender, EventArgs e)
        {
            txtVehicleGroup.Text = ContextData.VehicleGroup.Name;
            txtVehicle.Text = ContextData.VehicleName;
            txtVehicleCost.Text = ContextData.VehicleCost.ToString("C");
            txtReturnDateTime.Text = ContextData.VehicleReturnDate.ToString();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            //reportViewer1.PrintDialog();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (Next != null)
                Next(this, e);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            if (Back != null)
                Back(this, e);
        }

        public void Init(string intro, string status)
        {
            lblIntroText.Text = intro;
            lblStatus.Text = status;
        }

        public event EventHandler Next;
        public event EventHandler Back;
    }
}