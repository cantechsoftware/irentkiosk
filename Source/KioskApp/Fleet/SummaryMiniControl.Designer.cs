﻿namespace iRent.Fleet
{
    partial class SummaryMiniControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            MetroFramework.Controls.MetroPanel pnlSummary;
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.txtVehicleCost = new MetroFramework.Controls.MetroTextBox();
            this.txtReturnDateTime = new MetroFramework.Controls.MetroTextBox();
            this.txtVehicle = new MetroFramework.Controls.MetroTextBox();
            this.lblStatus = new MetroFramework.Controls.MetroLabel();
            this.lblIntroText = new MetroFramework.Controls.MetroLabel();
            this.txtVehicleGroup = new MetroFramework.Controls.MetroTextBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnBack = new MetroFramework.Controls.MetroButton();
            this.btnPrint = new MetroFramework.Controls.MetroButton();
            this.lblSummary = new MetroFramework.Controls.MetroLabel();
            this.btnNext = new MetroFramework.Controls.MetroButton();
            pnlSummary = new MetroFramework.Controls.MetroPanel();
            pnlSummary.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlSummary
            // 
            pnlSummary.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel1.SetColumnSpan(pnlSummary, 3);
            pnlSummary.Controls.Add(this.tableLayoutPanel2);
            pnlSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlSummary.HorizontalScrollbarBarColor = true;
            pnlSummary.HorizontalScrollbarHighlightOnWheel = false;
            pnlSummary.HorizontalScrollbarSize = 10;
            pnlSummary.Location = new System.Drawing.Point(3, 40);
            pnlSummary.Name = "pnlSummary";
            pnlSummary.Size = new System.Drawing.Size(701, 327);
            pnlSummary.TabIndex = 11;
            pnlSummary.VerticalScrollbarBarColor = true;
            pnlSummary.VerticalScrollbarHighlightOnWheel = false;
            pnlSummary.VerticalScrollbarSize = 10;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.66667F));
            this.tableLayoutPanel2.Controls.Add(this.metroLabel4, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.metroLabel2, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.metroLabel3, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.metroLabel1, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.txtVehicleCost, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.txtReturnDateTime, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.txtVehicle, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblStatus, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.lblIntroText, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.txtVehicleGroup, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 7;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(699, 325);
            this.tableLayoutPanel2.TabIndex = 14;
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel4.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel4.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel4.Location = new System.Drawing.Point(3, 50);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(226, 50);
            this.metroLabel4.TabIndex = 38;
            this.metroLabel4.Text = "Vehicle";
            this.metroLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel2.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel2.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel2.Location = new System.Drawing.Point(3, 0);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(226, 50);
            this.metroLabel2.TabIndex = 37;
            this.metroLabel2.Text = "Vehicle Group";
            this.metroLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel3.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel3.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel3.Location = new System.Drawing.Point(3, 150);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(226, 50);
            this.metroLabel3.TabIndex = 36;
            this.metroLabel3.Text = "Vehicle Cost";
            this.metroLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel1.Location = new System.Drawing.Point(3, 100);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(226, 50);
            this.metroLabel1.TabIndex = 35;
            this.metroLabel1.Text = "ReturnDateTime";
            this.metroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtVehicleCost
            // 
            this.txtVehicleCost.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVehicleCost.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtVehicleCost.Lines = new string[0];
            this.txtVehicleCost.Location = new System.Drawing.Point(235, 153);
            this.txtVehicleCost.MaxLength = 32767;
            this.txtVehicleCost.Name = "txtVehicleCost";
            this.txtVehicleCost.PasswordChar = '\0';
            this.txtVehicleCost.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtVehicleCost.SelectedText = "";
            this.txtVehicleCost.Size = new System.Drawing.Size(461, 44);
            this.txtVehicleCost.TabIndex = 31;
            this.txtVehicleCost.UseSelectable = true;
            // 
            // txtReturnDateTime
            // 
            this.txtReturnDateTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtReturnDateTime.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtReturnDateTime.Lines = new string[0];
            this.txtReturnDateTime.Location = new System.Drawing.Point(235, 103);
            this.txtReturnDateTime.MaxLength = 32767;
            this.txtReturnDateTime.Name = "txtReturnDateTime";
            this.txtReturnDateTime.PasswordChar = '\0';
            this.txtReturnDateTime.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtReturnDateTime.SelectedText = "";
            this.txtReturnDateTime.Size = new System.Drawing.Size(461, 44);
            this.txtReturnDateTime.TabIndex = 29;
            this.txtReturnDateTime.UseSelectable = true;
            // 
            // txtVehicle
            // 
            this.txtVehicle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVehicle.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtVehicle.Lines = new string[0];
            this.txtVehicle.Location = new System.Drawing.Point(235, 53);
            this.txtVehicle.MaxLength = 32767;
            this.txtVehicle.Name = "txtVehicle";
            this.txtVehicle.PasswordChar = '\0';
            this.txtVehicle.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtVehicle.SelectedText = "";
            this.txtVehicle.Size = new System.Drawing.Size(461, 44);
            this.txtVehicle.TabIndex = 28;
            this.txtVehicle.UseSelectable = true;
            // 
            // lblVehicle
            // 
            this.lblStatus.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.lblStatus, 2);
            this.lblStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblStatus.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblStatus.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblStatus.Location = new System.Drawing.Point(3, 250);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(693, 50);
            this.lblStatus.TabIndex = 26;
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblIntroText
            // 
            this.lblIntroText.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.lblIntroText, 2);
            this.lblIntroText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblIntroText.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblIntroText.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblIntroText.Location = new System.Drawing.Point(3, 200);
            this.lblIntroText.Name = "lblIntroText";
            this.lblIntroText.Size = new System.Drawing.Size(693, 50);
            this.lblIntroText.TabIndex = 25;
            this.lblIntroText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtVehicleGroup
            // 
            this.txtVehicleGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVehicleGroup.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtVehicleGroup.Lines = new string[0];
            this.txtVehicleGroup.Location = new System.Drawing.Point(235, 3);
            this.txtVehicleGroup.MaxLength = 32767;
            this.txtVehicleGroup.Name = "txtVehicleGroup";
            this.txtVehicleGroup.PasswordChar = '\0';
            this.txtVehicleGroup.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtVehicleGroup.SelectedText = "";
            this.txtVehicleGroup.Size = new System.Drawing.Size(461, 44);
            this.txtVehicleGroup.TabIndex = 27;
            this.txtVehicleGroup.UseSelectable = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.btnBack, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.btnPrint, 1, 2);
            this.tableLayoutPanel1.Controls.Add(pnlSummary, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblSummary, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnNext, 2, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(707, 570);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // btnBack
            // 
            this.btnBack.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnBack.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnBack.Location = new System.Drawing.Point(3, 473);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(247, 94);
            this.btnBack.TabIndex = 14;
            this.btnBack.Text = "&Back";
            this.btnBack.UseSelectable = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btnPrint.Highlight = true;
            this.btnPrint.Location = new System.Drawing.Point(256, 373);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(194, 88);
            this.btnPrint.Style = MetroFramework.MetroColorStyle.Black;
            this.btnPrint.TabIndex = 12;
            this.btnPrint.Text = "Print";
            this.btnPrint.Theme = MetroFramework.MetroThemeStyle.Light;
            this.btnPrint.UseSelectable = true;
            this.btnPrint.Visible = false;
            // 
            // lblSummary
            // 
            this.lblSummary.AutoSize = true;
            this.lblSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSummary.FontSize = MetroFramework.MetroLabelSize.XL;
            this.lblSummary.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblSummary.Location = new System.Drawing.Point(256, 0);
            this.lblSummary.Name = "lblSummary";
            this.lblSummary.Size = new System.Drawing.Size(194, 37);
            this.lblSummary.TabIndex = 10;
            this.lblSummary.Text = "Pre-Summary";
            this.lblSummary.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnNext
            // 
            this.btnNext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNext.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnNext.Location = new System.Drawing.Point(456, 473);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(248, 94);
            this.btnNext.TabIndex = 13;
            this.btnNext.Text = "Next";
            this.btnNext.UseSelectable = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // SummaryMini
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "SummaryMiniControl";
            this.Size = new System.Drawing.Size(707, 570);
            this.Load += new System.EventHandler(this.Summary_Load);
            pnlSummary.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MetroFramework.Controls.MetroButton btnPrint;
        private MetroFramework.Controls.MetroLabel lblSummary;
        private MetroFramework.Controls.MetroButton btnNext;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private MetroFramework.Controls.MetroLabel lblIntroText;
        private MetroFramework.Controls.MetroButton btnBack;
        private MetroFramework.Controls.MetroLabel lblStatus;
        private MetroFramework.Controls.MetroTextBox txtVehicleCost;
        private MetroFramework.Controls.MetroTextBox txtReturnDateTime;
        private MetroFramework.Controls.MetroTextBox txtVehicle;
        private MetroFramework.Controls.MetroTextBox txtVehicleGroup;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel2;

    }
}
