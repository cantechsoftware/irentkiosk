﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CanTech.Common;
using CanTech.Common.Extensions;
using iRent.Classes;
using System.Windows;
using System.ComponentModel;
using System.Windows.Forms;
using iRent.Framework;
using iRent.Services;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;

namespace iRent.Model
{
    class AddressModel : IDataErrorInfo
    {
        #region declarations
        //iRentEntities db;
        

        #endregion

        #region State properties

        public int id { get; set; }

        public string HouseNumber { get; set; }

        public string Complex { get; set; }

        public string StreetNumber { get; set; }

        public string StreetName { get; set; }

        public string Suburb { get; set; }

        public string Town { get; set; }

        public string ProvinceState { get; set; }

        public string Country { get; set; }

        public string AreaCode { get; set; }

        public bool SameAsPhysicalAddress { get; set; }

        public string PostalAddress { get; set; }

        public string PostalAddressAreaCode { get; set; }    
        
        DateTime createdDate { get; set; }

        DateTime ModifiedDate { get; set; }

        bool isDEl { get; set; }

        #endregion

        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public string this[string propertyName]
        {
            get
            {
                string validationResult = null;
                switch (propertyName)
                {

                    case "HouseNumber":
                        validationResult = ValidateHouseNumber();
                        break;
                 
                    case "Street":
                        validationResult = ValidateStreet();
                        break;
                    case "Town":
                        validationResult = ValidateTown();
                        break;
                    case "ProvinceState":
                        validationResult = ValidateProvinceState();
                        break;
                    case "AreaCode":
                        validationResult = ValidateAreaCode();
                        break;
                    case "PostalAddress":
                        validationResult = ValidatePostalAddress();
                        break;
                    case "PostalAddressAreaCode":
                        validationResult = ValidatePostalAddressAreaCode();
                    break;
                        
                    // default:
                    // throw new ApplicationException("Unknown Property being validated on Product.");
                }
                return validationResult;
            }
        }

        private string ValidatePostalAddressAreaCode()
        {
            if (String.IsNullOrEmpty(this.PostalAddressAreaCode))
                return "Postal Address AreaCode needs to be entered.";
            else
                return String.Empty;
        }


        private string ValidatePostalAddress()
        {
            if (String.IsNullOrEmpty(this.PostalAddress))
                return "Postal Address  needs to be entered.";
            else
                return String.Empty;
        }

        private string ValidateAreaCode()
        {
            if (String.IsNullOrEmpty(this.AreaCode))
                return "Area Code needs to be entered.";
            else
                return String.Empty;
        }

        private string ValidateProvinceState()
        {
            if (String.IsNullOrEmpty(this.ProvinceState))
                return "Province/State needs to be entered.";
            else
                return String.Empty;
        }

        private string ValidateTown()
        {
            if (String.IsNullOrEmpty(this.Town))
                return "Town needs to be entered.";
            else
                return String.Empty;
        }

      

        private string ValidateStreet()
        {
            if (String.IsNullOrEmpty(this.StreetName))
                return "Street needs to be entered.";
            else
                return String.Empty;
        }

    
        private string ValidateHouseNumber()
        {
            if (String.IsNullOrEmpty(this.HouseNumber))
                return "HouseNumber needs to be entered.";
            else
                return String.Empty;
        }

        public void Update(DriverModel driver)
        {
            if (driver == null) return;

            if (SameAsPhysicalAddress)
            {
                PostalAddress = new[]
                    {
                        HouseNumber + " " + Complex,
                        StreetNumber + " " + StreetName,
                        Suburb,
                        Town,
                        ProvinceState,
                        AreaCode
                    }
                    .Select(x => x.Trim())
                    .Where(x => x.HasValue())
                    .Join(", ");
                PostalAddressAreaCode = AreaCode;
            }

            driver.PhysicalAddress = new iRent2.Contracts.Models.AddressModel
                {
                    HouseNo = HouseNumber,
                    Complex = Complex,
                    StreetNo = StreetNumber,
                    StreetName = StreetName,
                    Suburb = Suburb,
                    City = Town,
                    State = ProvinceState,
                    PostCode = AreaCode,
                    Country = Country
                };
            driver.PostalAddress =
                SameAsPhysicalAddress
                    ? new iRent2.Contracts.Models.AddressModel
                        {
                            HouseNo = HouseNumber,
                            Complex = Complex,
                            StreetName = StreetNumber,
                            StreetNo = StreetNumber,
                            Suburb = Suburb,
                            City = Town,
                            State = ProvinceState,
                            PostCode = AreaCode,
                            Country = Country
                        }
                    : new iRent2.Contracts.Models.AddressModel
                        {
                            StreetName = PostalAddress,
                            PostCode = PostalAddressAreaCode
                        };
        }

        public void SetAddressInfoByDriverID(int ID)
        {
            var driver = IoC.Resolve<DriverApi>().GetDriver(ID);
            var physical = driver.PhysicalAddress;
            var postal = driver.PostalAddress;

            if (physical != null && postal != null)
            {
                this.AreaCode = physical.PostCode;
                this.Complex = physical.Complex;
                this.Country =physical.Country;
                this.createdDate =physical.DateCreated;
                this.HouseNumber = physical.HouseNo;
                this.isDEl = physical.IsDeleted;
                this.ModifiedDate = physical.DateModified;
                this.PostalAddress = IoC.Resolve<AddressService>().BuildPostalAddress(postal);
                this.PostalAddressAreaCode =postal.PostCode;
                this.ProvinceState =physical.State;
                this.StreetName = physical.StreetName;
                this.StreetNumber = physical.StreetName;
                this.Suburb = physical.Suburb;
                this.Town = physical.City;
            }
            
        }
    }
}
