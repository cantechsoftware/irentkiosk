﻿using System;
using System.Linq;
using System.Windows.Forms;
using iRent.Properties;
using log4net;
using SmartFormat;

namespace iRent.Classes
{
    public class CancomRFID
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(CancomRFID));

        private IntPtr Hdle;
        private uint _baudrate;

        public CancomRFID(uint baudrate)
        {
            Hdle = new IntPtr(0);
            _baudrate = baudrate;
        }

        public void OpenComm()
        {
            try
            {
                Hdle = DllClass.CommOpenWithBaut(Settings.Default.RFIDComPort, _baudrate);
                _log.InfoFormat("Opened COM: {0} - Handle: {1}", Settings.Default.RFIDComPort, Hdle);
            }
            catch (Exception ex)
            {
                _log.Fatal("Error opening COM {0}".FormatSmart(Settings.Default.RFIDComPort), ex);
                MessageBox.Show(ex.Message);
            }
        }

        public void CloseComm()
        {
            _log.DebugFormat("Closing comm - Handle: {0}", Hdle);

            if (Hdle.ToInt64() != 0)
            {
                var i = DllClass.CommClose(Convert.ToUInt32(Hdle));
                Hdle = new IntPtr(0);
                //CommPortStatusLabel.Text = "Comm. Port is Closed";
            }
        }

        public void Initialize()
        {
            if (Hdle.ToInt64() == 0)
            {
                try
                {
                    //Incase the Comm port did close already
                    CloseComm();
                }
                catch
                {
                }
                OpenComm();
            }
            if (Hdle.ToInt64() != 0)
            {
                byte Addr;
                byte Cm, Pm;
                UInt16 TxDataLen, RxDataLen;
                var TxData = new byte[1024];
                var RxData = new byte[1024];
                byte ReType = 0;
                byte St0, St1, St2;

                Cm = 0x30;
                Pm = 0x30;
                St0 = St1 = St2 = 0;
                TxDataLen = 0;
                RxDataLen = 0;

                Addr = (byte.Parse("00"));
                var i = ExecCmd(Addr, Cm, Pm, TxDataLen, TxData, ref ReType, ref St0, ref St1, ref St2, ref RxDataLen,
                    RxData);

                _log.InfoFormat("Initialize result: {0}, result: {1}", i, ReType);

                if (i == 0)
                {
                    if (ReType == 0x50)
                    {
                        //MessageBox.Show("INITIALIZE OK" + "\r\n" + "Status Code : " + (char)St0 + (char)St1 + (char)St2, "INITIALIZE");
                    }
                }
            }
        }

        private int ExecCmd(byte Addr, byte Cm, byte Pm, UInt16 TxDataLen, byte[] TxData, ref byte ReType,
            ref byte St0, ref byte St1, ref byte St2, ref UInt16 RxDataLen, byte[] RxData)
        {
            
            var i = DllClass.ExecuteCommand(Hdle, Addr, Cm, Pm, TxDataLen, TxData,
                ref ReType, ref St0, ref St1, ref St2,
                ref RxDataLen, RxData);
            _log.DebugFormat("ExecuteCommand: Hdle: {0}, Addr: {1}, Cm: {2}, Pm: {3}, TxData: {4}\r\n" +
                             "ReType: {5}, Status: {6}, RxData: {7}\r\n" +
                             "Result: {8}",
                Hdle, Addr, Cm, Pm, ToHex(TxData, TxDataLen),
                ReType, Str(St0, St1, St2),
                ToHex(RxData, RxDataLen),
                i);

            return i;
        }

        private string Str(params byte[] chars)
        {
            return new string(chars.Select(x => (char) x).ToArray());
        }

        private string ToHex(byte[] data, int length)
        {
            if (data == null)
                return null;
            var buff = new byte[length];
            Array.Copy(data, buff, length);
            return BitConverter.ToString(buff).Replace("-", "");
        }

        public void MoveCardToRFBtn()
        {
            _log.DebugFormat("Move card to RF, Hdle: {0}", Hdle);

            if (Hdle.ToInt64() != 0)
            {
                byte Addr;
                byte Cm, Pm;
                UInt16 TxDataLen, RxDataLen;
                var TxData = new byte[1024];
                var RxData = new byte[1024];
                byte ReType = 0;
                byte St0, St1, St2;

                Cm = 0x32;
                Pm = 0x32;
                St0 = St1 = St2 = 0;
                TxDataLen = 0;
                RxDataLen = 0;

                Addr = (byte.Parse("00"));
                var i = ExecCmd(Addr, Cm, Pm, TxDataLen, TxData, ref ReType, ref St0, ref St1, ref St2, ref RxDataLen,
                    RxData);
                if (i == 0)
                {
                    if (ReType == 0x50)
                    {
                        //MessageBox.Show("Move Card OK" + "\r\n" + "Status Code : " + (char)St0 + (char)St1 + (char)St2, "Move Card");
                    }
                }
            }
        }

        public string CardActivate()
        {
            _log.InfoFormat("Card Activate: {0}", Hdle);
            string CardActivate;
            string TypeAuid;
            string TypeAatr;


            if (Hdle.ToInt64() != 0)
            {
                byte Addr;
                byte Cm, Pm;
                UInt16 TxDataLen, RxDataLen;
                var TxData = new byte[1024];
                var RxData = new byte[1024];
                byte ReType = 0;
                byte St0, St1, St2;

                Cm = 0x60;
                Pm = 0x30;
                St0 = St1 = St2 = 0;
                TxDataLen = 2;
                RxDataLen = 0;

                TxData[0] = 0x41;
                TxData[1] = 0x42;
                Addr = (byte.Parse("00"));
                var i = ExecCmd(Addr, Cm, Pm, TxDataLen, TxData, ref ReType, ref St0, ref St1, ref St2, ref RxDataLen,
                    RxData);
                if (i == 0)
                {
                    if (ReType == 0x50)
                    {
                        int n;
                        var StrBuf = "";

                        for (n = 0; n < RxDataLen; n++)
                        {
                            StrBuf += RxData[n].ToString("X2");
                        }
                        CardActivate = StrBuf;

                        switch (RxData[0])
                        {
                            case 0x4d: //mifare one Card
                            {
                                var CardUID = "";
                                for (n = 0; n < RxData[3]; n++)
                                {
                                    CardUID += RxData[n + 4].ToString("X2");
                                }
                                TypeAuid = CardUID;
                                var ret = RFIDFormat(CardUID);
                                _log.DebugFormat("Card Activate - mifare: {0}", ret);
                                return ret;
                            }
                            case 0x41: //type A card
                            {
                                var CardUID = "";
                                var CardATS = "";
                                for (n = 0; n < RxData[3]; n++)
                                {
                                    CardUID += RxData[n + 4].ToString("X2");
                                }
                                for (n = 0; n < RxDataLen - RxData[3] - 5; n++)
                                {
                                    CardATS += RxData[n + 5 + RxData[3]].ToString("X2");
                                }
                                TypeAuid = CardUID;
                                TypeAatr = CardATS;
                                var ret = "type A Card Activate Successed\nCardUID: " + CardUID + "\nCardATS: " +
                                          CardATS;
                                _log.DebugFormat("Card Activate - type A: {0}", ret);
                                return ret;
                            }
                            case 0x42: //type B card
                            {
                                var CardATS = "";
                                for (n = 0; n < RxDataLen - 1; n++)
                                {
                                    CardATS += RxData[n + 1].ToString("X2");
                                }
                                TypeAatr = CardATS;
                                var ret = "type B Card Activate Successed\nCardATS: " + CardATS;
                                _log.DebugFormat("Card Activate - type B: {0}", ret);
                                return ret;
                            }
                        }
                    }
                    else if ((ReType == 0x4e))
                    {
                        //MessageBox.Show("Cold Reset ERROR" + "\r\n" + "Error Code:  " + (char)St1 + (char)St2, "Cold Reset");
                    }
                }
            }
            return "Error";
        }

        public void MoveCardToFront()
        {
            _log.InfoFormat("Move Card To Front: {0}", Hdle);

            if (Hdle.ToInt64() != 0)
            {
                byte Addr;
                byte Cm, Pm;
                UInt16 TxDataLen, RxDataLen;
                var TxData = new byte[1024];
                var RxData = new byte[1024];
                byte ReType = 0;
                byte St0, St1, St2;

                Cm = 0x32;
                Pm = 0x30;
                St0 = St1 = St2 = 0;
                TxDataLen = 0;
                RxDataLen = 0;

                Addr = (byte.Parse("00"));
                var i = ExecCmd(Addr, Cm, Pm, TxDataLen, TxData, ref ReType, ref St0, ref St1, ref St2, ref RxDataLen,
                    RxData);
                if (i == 0)
                {
                    if (ReType == 0x50)
                    {
                        //MessageBox.Show("Move Card OK" + "\r\n" + "Status Code : " + (char)St0 + (char)St1 + (char)St2, "Move Card");
                    }
                }
            }
        }

        public void AllCardInBtn()
        {
            _log.InfoFormat("All Card In: {0}", Hdle);

            if (Hdle.ToInt64() == 0)
            {
                _log.Fatal("Comm port not open");
                return;
            }

            byte Addr;
            byte Cm, Pm;
            UInt16 TxDataLen, RxDataLen;
            var TxData = new byte[1024];
            var RxData = new byte[1024];
            byte ReType = 0;
            byte St0, St1, St2;

            Cm = 0x33;
            Pm = 0x30;
            St0 = St1 = St2 = 0;
            TxDataLen = 0;
            RxDataLen = 0;

            Addr = (byte.Parse("00"));
            var i = ExecCmd(Addr, Cm, Pm, TxDataLen, TxData, ref ReType, ref St0, ref St1, ref St2, ref RxDataLen,
                RxData);
            if (i != 0)
                _log.ErrorFormat("Communication error: {0}", i);
            else if (ReType == 0x50)
                _log.DebugFormat("Allowed card in OK: {0}", Str(St0, St1, St2));
            else
                _log.ErrorFormat("Allowed card in error: {0}", Str(St0, St1, St2));
        }

        public void MoveCardToRecycleBin()
        {
            _log.InfoFormat("Move Card To Recycle Bin: {0}", Hdle);
            if (Hdle.ToInt64() == 0)
            {
                _log.Fatal("Comm port is not open");
                return;
            }

            byte Addr;
            byte Cm, Pm;
            UInt16 TxDataLen, RxDataLen;
            var TxData = new byte[1024];
            var RxData = new byte[1024];
            byte ReType = 0;
            byte St0, St1, St2;

            Cm = 0x32;
            Pm = 0x33;
            St0 = St1 = St2 = 0;
            TxDataLen = 0;
            RxDataLen = 0;

            Addr = (byte.Parse("00"));
            var i = ExecCmd(Addr, Cm, Pm, TxDataLen, TxData, ref ReType, ref St0, ref St1, ref St2, ref RxDataLen,
                RxData);
            if (i != 0)
                _log.ErrorFormat("Communication error: {0}", i);
            else if (ReType != 0x50)
                _log.ErrorFormat("Move card error: {0}", Str(St0, St1, St2));
            else
                _log.DebugFormat("Move card OK: {0}", Str(St0, St1, St2));
        }

        public string CardActivateBtn()
        {
            _log.InfoFormat("Card Activate Btn: {0}", Hdle);

            if (Hdle.ToInt64() == 0)
            {
                _log.Fatal("Handle is 0");
                return string.Empty;
            }

            byte Addr;
            byte Cm, Pm;
            UInt16 TxDataLen, RxDataLen;
            var TxData = new byte[1024];
            var RxData = new byte[1024];
            byte ReType = 0;
            byte St0, St1, St2;

            Cm = 0x60;
            Pm = 0x30;
            St0 = St1 = St2 = 0;
            TxDataLen = 2;
            RxDataLen = 0;

            TxData[0] = 0x41;
            TxData[1] = 0x42;
            Addr = (byte.Parse("00"));
            var i = ExecCmd(Addr, Cm, Pm, TxDataLen, TxData, ref ReType, ref St0, ref St1, ref St2, ref RxDataLen,
                RxData);
            if (i != 0)
            {
                _log.Error("Communication Error");
                //MessageBox.Show("Communication Error", "Caution");
                return string.Empty;
            }

            if ((ReType == 0x4e))
            {
                _log.ErrorFormat("Cold Reset ERROR: {0}", Str(St0, St1, St2));
                //MessageBox.Show("Cold Reset ERROR" + "\r\n" + "Error Code:  " + (char)St1 + (char)St2, "Cold Reset");
                return string.Empty;
            }

            if (ReType != 0x50)
            {
                _log.Error("Communication Error");
                //MessageBox.Show("Communication Error", "Caution");
                return string.Empty;
            }

            int n;
            var StrBuf = "";

            for (n = 0; n < RxDataLen; n++)
            {
                StrBuf += RxData[n].ToString("X2");
            }
            //CardActivateTxt.Text = StrBuf;   //返回的总的数据包

            switch (RxData[0])
            {
                case 0x4d: //mifare one Card
                {
                    var CardUID = "";
                    for (n = 0; n < RxData[3]; n++)
                    {
                        CardUID += RxData[n + 4].ToString("X2");
                    }
                    var ret = RFIDFormat(CardUID);
                    _log.DebugFormat("Card Activate - mifare: {0}", ret);
                    return ret;
                }
                case 0x41: //type A card
                {
                    var CardUID = "";
                    var CardATS = "";
                    for (n = 0; n < RxData[3]; n++)
                    {
                        CardUID += RxData[n + 4].ToString("X2");
                    }
                    for (n = 0; n < RxDataLen - RxData[3] - 5; n++)
                    {
                        CardATS += RxData[n + 5 + RxData[3]].ToString("X2");
                    }
                    _log.DebugFormat("Card Activate - type A: {0}, {1}", CardUID, CardATS);
                    //TypeAUIDtxt.Text = CardUID;
                    //TypeAATRtxt.Text = CardATS;
                    //MessageBox.Show("type A Card Activate Successed\nCardUID: " + CardUID + "\nCardATS: " + CardATS, "Activate RF Card");
                    break;
                }
                case 0x42: //type B card
                {
                    var CardATS = "";
                    for (n = 0; n < RxDataLen - 1; n++)
                    {
                        CardATS += RxData[n + 1].ToString("X2");
                    }
                    //TypeAATRtxt.Text = CardATS;
                    //MessageBox.Show("type B Card Activate Successed\nCardATS: " + CardATS, "Activate RF Card");
                    _log.DebugFormat("Card Activate - type B: {0}", CardATS);
                    break;
                }
            }
            return "";
        }

        public string RFIDFormat(string RFID)
        {
            try
            {
                var newstring = "";

                while (RFID.Length > 0)
                {
                    newstring = newstring + RFID.Substring(RFID.Length - 2, 2);
                    RFID = RFID.Remove(RFID.Length - 2, 2);
                }
                return newstring;
            }
            catch (Exception ex)
            {
                _log.Error("Error in RFIDFormat " + RFID, ex);
                return "";
            }
        }
    }
}