﻿using System;
using System.ComponentModel;
using CanTech.Common;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;

namespace iRent.Classes
{
    internal class ContactInfo : IDataErrorInfo
    {
        #region State properties

        public string Email1 { get; set; }

        public string CellNumber { get; set; }

        public string HomeNumber { get; set; }

        public string WorkNumber { get; set; }

        public string MothersName { get; set; }

        public string CityName { get; set; }

        public string CarColour { get; set; }

        public string Other { get; set; }

        private DateTime createdDate { get; set; }

        private DateTime ModifiedDate { get; set; }

        #endregion.

        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public string this[string propertyName]
        {
            get
            {
                string validationResult = null;
                switch (propertyName)
                {
                    case "Email1":
                        validationResult = ValidateEmail1();
                        break;

                    case "CellNumber":
                        validationResult = ValidateCellNumber();
                        break;
                    case "WorkNumber":
                        validationResult = ValidateWorkNumber();
                        break;
                        //case "DateOfBirth":
                        //    validationResult = ValidateDateOfBirth();
                        //    break;                  

                        // default:
                        // throw new ApplicationException("Unknown Property being validated on Product.");
                }
                return validationResult;
            }
        }


        private string ValidateWorkNumber()
        {
            if (String.IsNullOrEmpty(WorkNumber) || WorkNumber == "0")
                return "WorkNumber needs to be entered.";
            else
                return String.Empty;
        }

        private string ValidateCellNumber()
        {
            if (String.IsNullOrEmpty(CellNumber) || CellNumber == "0")
                return "CellNumber needs to be entered.";
            else
                return String.Empty;
        }

        private string ValidateEmail1()
        {
            if (String.IsNullOrEmpty(Email1))
                return "Email1 needs to be entered.";
            else
                return String.Empty;
        }

        public void Save()
        {
            if (ContextData.Driver == null) 
                return;

            var driver = ContextData.Driver;
            driver.CellNo = CellNumber;
            driver.HomeNo = HomeNumber;
            //driver.IsRegistered = true;
            driver.Email = Email1;
            driver["WorkNo"] = WorkNumber;

            /*Cannot save the info below at this point as we do not have this info yet. I think we should carry the object
            to the security answers screen and then only save.*/

            //driver["MothersName"] = MothersName;
            //driver["CityBorn"] = CityName;
            //driver["CarColour"] = CarColour;

            IoC.Resolve<DriverApi>().Save(driver);
        }

        public void setContactInfoByDriver(DriverModel driver)
        {
            if (driver != null)
            {
                CarColour = driver["CarColour"];
                CellNumber = driver.CellNo;
                CityName = driver["CityBorn"];
                createdDate = driver.DateCreated;
                Email1 = driver.Email;
                HomeNumber = driver.HomeNo;
                ModifiedDate = driver.DateModified;
                MothersName = driver["MothersName"];
                WorkNumber = driver["WorkNo"];
            }
        }
    }
}