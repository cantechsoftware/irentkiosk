﻿using iRent2.Contracts.Models;

namespace iRent.Classes
{
    public class DamageLocationAdd
    {
        public string DamageLocationName { get; set; }
        public string DamageType { get;set; }
        public int DamageCount { get;set; }
        public int Accessory { get; set; }
       
        public DamageModel GetDamageModel()
        {
            return new DamageModel
                {
                    Location = DamageLocationName,
                    DamageType = DamageType,
                    DamageCount = DamageCount ,
                    AccessoryId = Accessory 
                };
        }
    }
}