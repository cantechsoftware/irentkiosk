﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.IO.Ports;
using System.Media;
using System.Text;
using System.Threading;
using System.Xml;
using CanTech.Common;
using CoreScanner;
using GalaSoft.MvvmLight.Messaging;
using Hhp.Series4000;
using iRent.CancomWebService;
using iRent.Framework;
using iRent.Properties;
using iRent.Services;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Models;
using iRentKiosk.Core.Services.Api;
using iRentKiosk.Core.Util;
using log4net;

namespace iRent.Classes
{
    public class CancomScanner
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(CancomScanner));

        private static bool HhpCon;
        private static bool MotoCon;

        private static BackgroundWorker checkHhpScanner;
        private static BackgroundWorker checkScanner;

        private static string outXML;
        private static int status;

        public CCoreScanner scanner;

        private static DriverApi DriverApi => IoC.Resolve<DriverApi>();
        private static LicenceScanner LicenceScanner => IoC.Resolve<LicenceScanner>();
        public static bool connected { get; set; }
        public static scannerType selectedScanner { get; set; }
        public LicenseAndImage LicAndImage { get; set; }

        private ScannerMessageItem ScannedMessageItem { get; set; }

        private void ExecCmd(int opcode, ref string inXML, out string outXML, out int status)
        {
            scanner.ExecCommand(opcode, ref inXML, out outXML, out status);
            _log.DebugFormat("EXEC: {0}, {1}, {2}, {3}", opcode, inXML, outXML, status);
        }

        public void Dispose()
        {
            var appHandle = 0;
            var status = STATUS_FALSE;
            try
            {
                checkHhpScanner.CancelAsync();
                checkHhpScanner.Dispose();
                //scanner.Close(appHandle, out status);                
            }
            catch
            {
            }
            try
            {
                checkScanner.CancelAsync();
                checkScanner.Dispose();
                ENDSCANNER();
            }
            catch
            {
            }
        }

        public bool DetectScanners(string _port)
        {
            var result = false;
            try
            {
                imager_destroy_object();
                ENDSCANNER();
            }
            catch
            {
            }
            var hhpScannerConnected = HhpScanner(_port, true);
            var moto = startScanner(true);
            if (moto)
            {
                selectedScanner = scannerType.motorola;
                result = true;
                // startScanner();
            }
            else if (hhpScannerConnected && !moto)
            {
                selectedScanner = scannerType.Hhp4800;
                HhpScanner(_port);
                result = true;
            }
            return result;
        }

        public void startScanner()
        {
            scanner = new CCoreScanner();
            var scannertypes = new short[1];
            scannertypes[0] = 1;
            short numOfScannersTypes = 1;
            var status = 1;

            scanner.Open(0, scannertypes, numOfScannersTypes, out status);

            try
            {
                var opcode = 1001;
                var inXML = "<inArgs><cmdArgs><arg-int>6</arg-int><arg-int>1,2,4,8,16,32</arg-int></cmdArgs></inArgs>";
                ExecCmd(opcode, ref inXML, out outXML, out status);
            }
            catch (Exception ex)
            {
            }

            checkScanner = new BackgroundWorker {WorkerSupportsCancellation = true};
            checkScanner.DoWork += checkScanner_DoWork;
            checkScanner.RunWorkerCompleted += checkScanner_RunWorkerCompleted;
            if (checkScanner.IsBusy)
            {
            }
            else
            {
                checkScanner.RunWorkerAsync();
            }
        }

        public bool startScanner(bool detection)
        {
            try
            {
                scanner = new CCoreScanner();
            }
            catch (Exception)
            {
                return false;
            }

            var scannertypes = new short[1];
            scannertypes[0] = 1;
            short numOfScannersTypes = 1;
            var status = 1;

            scanner.Open(0, scannertypes, numOfScannersTypes, out status);

            try
            {
                var opcode = 1001;
                var inXML = "<inArgs><cmdArgs><arg-int>6</arg-int><arg-int>1,2,4,8,16,32</arg-int></cmdArgs></inArgs>";
                ExecCmd(opcode, ref inXML, out outXML, out status);
            }
            catch (Exception ex)
            {
            }

            var output = new StringBuilder();
            short numberOfScanners;
            var connectedScannerIDList = new int[10];
            scanner.GetScanners(out numberOfScanners, connectedScannerIDList, out outXML, out status);

            Thread.Sleep(32);
            try
            {
                using (var reader = XmlReader.Create(new StringReader(outXML)))
                {
                    reader.ReadToFollowing("scannerID");
                    output.AppendLine(reader.ReadElementContentAsString());
                }
                var b = output.ToString();
                scanner.BarcodeEvent += scanner_BarcodeEvent;
                return true;
            }
            catch
            {
                return false;
            }
        }

        #region Hhp 4800

        public delegate void ScannedBarcodeEvent(object from, BarcodeReadEventArgs args);

        private static Imager ScanViewerImager;
        private static SerialPort serialPort_TEST;

        private static enumImagerStatus statusImager = enumImagerStatus.ssInit;
        private static enumSerialPortStatus statusSerialPort = enumSerialPortStatus.spsInit;

        private static string port = "";
        public static event ScannedBarcodeEvent ScannedBarcode;

        private enum enumImagerStatus
        {
            ssInit,
            ssOpen,
            ssClosed,
            ssException,
            ssObjectNull
        }

        private enum enumSerialPortStatus
        {
            spsInit,
            spsExceptionAccessDenied,
            spsExceptionPortNotFound,
            spsExceptionOther,
            spsPortOpened,
            spsClose
        }

        #endregion

        #region Lists

        private static bool _busy;
        public string[] LEDList = {"LED1", "LED2", "LED3", "LED4", "LED5"};

        public string[] beepList =
        {
            "ONE SHORT HIGH",
            "ONE SHORT HIGH",
            "TWO SHORT HIGH",
            "THREE SHORT HIGH",
            "FOUR SHORT HIGH",
            "FIVE SHORT HIGH",
            "ONE SHORT LOW",
            "TWO SHORT LOW",
            "THREE SHORT LOW",
            "FOUR SHORT LOW",
            "FIVE SHORT LOW",
            "ONE LONG HIGH",
            "TWO LONG HIGH",
            "THREE LONG HIGH",
            "FOUR LONG HIGH",
            "FIVE LONG HIGH",
            "ONE LONG LOW",
            "TWO LONG LOW",
            "THREE LONG LOW",
            "FOUR LONG LOW",
            "FIVE LONG LOW",
            "FAST HIG LOW HIGH LOW",
            "SLOW HIGH LOW HIGH LOW",
            "HIGH LOW",
            "LOW HIGH",
            "HIGH LOW HIGH",
            "LOW HIGH LOW"
        };

        #endregion

        #region CONST

        // available values for 'status' //
        private const int STATUS_SUCCESS = 0;
        private const int STATUS_FALSE = 1;
        private const int STATUS_LOCKED = 10;

        public const int LED_1 = 0x0001; /* Select LED 1                              */
        public const int LED_2 = 0x0002; /* Select LED 2                              */
        public const int LED_3 = 0x0004; /* Select LED 3                              */
        public const int LED_4 = 0x0008; /* Select LED 4                              */
        public const int LED_5 = 0x0010; /* Select LED 5                              */
        private const int DEVICE_LED_OFF = 2009;
        private const int DEVICE_LED_ON = 2010;
        private const int DEVICE_BEEP_CONTROL = 2018;
        private const int DEVICE_AIM_OFF = 2002;
        private const int DEVICE_AIM_ON = 2003;

        #endregion

        #region HhpScanner

        public static string ContainsTextScanner { get; set; }

        public static void HhpScanner(string _port)
        {
            port = _port;
            imager_create_object();

            checkHhpScanner = new BackgroundWorker {WorkerSupportsCancellation = true};
            checkHhpScanner.DoWork += checkHhpScanner_DoWork;
            checkHhpScanner.RunWorkerCompleted += checkHhpScanner_RunWorkerCompleted;
            if (checkHhpScanner.IsBusy)
            {
            }
            else
            {
                checkHhpScanner.RunWorkerAsync();
            }
        }

        public static bool HhpScanner(string _port, bool Detection)
        {
            port = _port;
            imager_create_object();

            return GETSCANNERSTATUS();
        }

        private static void checkHhpScanner_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (MotoCon || HhpCon)
            {
                connected = true;
            }
            else
            {
                connected = false;
            }

            if (checkHhpScanner.IsBusy)
            {
            }
            else
            {
                checkHhpScanner.RunWorkerAsync();
            }
        }

        private static void checkHhpScanner_DoWork(object sender, DoWorkEventArgs e)
        {
            if (selectedScanner == scannerType.motorola)
            {
            }
            else
            {
                if (GETSCANNERSTATUS())
                {
                    HhpCon = true;
                }
                else
                {
                    HhpCon = false;
                }
            }
        }

        private static void imager_create_object()
        {
            try
            {
                if (ScanViewerImager == null)
                {
                    statusImager = enumImagerStatus.ssObjectNull;
                    ScanViewerImager = new Imager();
                    ScanViewerImager.SynchronizingObject = null;
                    ScanViewerImager.PortName = port;
                    ScanViewerImager.BarcodeRead += ScanViewerImager_BarcodeRead;
                    statusImager = enumImagerStatus.ssInit;
                    ScanViewerImager.Open();
                    if (ScanViewerImager.IsOpen)
                    {
                        statusImager = enumImagerStatus.ssOpen;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }
        }

        public static bool GETSCANNERSTATUS()
        {
            return update_status_imager();
        }

        public static void update_Imager_Object()
        {
            try
            {
                switch (statusSerialPort)
                {
                    case enumSerialPortStatus.spsInit:
                        break;
                    case enumSerialPortStatus.spsPortOpened:
                        //Imager unplugged.
                        imager_create_object();
                        break;
                    case enumSerialPortStatus.spsClose:
                        imager_create_object();
                        //Imager unplugged.
                        break;
                    case enumSerialPortStatus.spsExceptionAccessDenied:
                        //Imager plugged in.
                        imager_create_object();
                        break;
                    case enumSerialPortStatus.spsExceptionPortNotFound:
                        //Imager unplugged. Destroy object.
                        imager_destroy_object();
                        break;
                    case enumSerialPortStatus.spsExceptionOther:
                        //Who knows?
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
            }
        }

        private static bool update_status_imager()
        {
            var bImagerOK = true;
            var sPort = "Invalid";

            //Update serial status first.
            update_status_serial_port();

            try
            {
                if (ScanViewerImager == null)
                {
                    //Imager object == null.
                    bImagerOK = false;
                    statusImager = enumImagerStatus.ssObjectNull;
                }
                else
                {
                    if (ScanViewerImager.IsOpen == false)
                    {
                        //Imager closed.
                        bImagerOK = false;
                        statusImager = enumImagerStatus.ssClosed;
                    }
                    else
                    {
                        //Looks ok,
                        bImagerOK = true;
                        sPort = ScanViewerImager.PortName;
                        statusImager = enumImagerStatus.ssOpen;
                    }
                }
            }
            catch (Exception ex)
            {
                bImagerOK = false;
                statusImager = enumImagerStatus.ssException;
            }

            update_Imager_Object();

            return bImagerOK;
        }

        private static bool containsText(string message, string try1, string try2)
        {
            var bResult = false;

            try
            {
                bResult = message.Contains(try1) && message.Contains(try2);
                ContainsTextScanner = message;
            }
            catch (Exception ex)
            {
            }
            return bResult;
        }

        private static void update_status_serial_port()
        {
            try
            {
                serialPort_TEST = new SerialPort();
                serialPort_TEST.PortName = port;
                // (string)Helpers.findConnectedScanner.findPort(@"\Device\HhpcdcVirtualPort");//(string)Helpers.RegistryValues.GetRegistryEntry("Imager", "Port", null);
                if (serialPort_TEST.IsOpen == false)
                {
                    serialPort_TEST.Open();
                }
                if (serialPort_TEST.IsOpen)
                {
                    statusSerialPort = enumSerialPortStatus.spsPortOpened;
                    //set_statuslabel_textAndColor_ValidOrInvalid(toolStripStatusLabel_SerialPort, "SP:Open", true);
                    serialPort_TEST.Close();
                }
                else
                {
                    statusSerialPort = enumSerialPortStatus.spsClose;
                    //set_statuslabel_textAndColor_ValidOrInvalid(toolStripStatusLabel_SerialPort, "SP:Close", true);
                }
            }
            catch (Exception ex)
            {
                var bScannerOnPort = containsText(ex.Message, "Access to the port", "is denied");
                var bPortNotFound = containsText(ex.Message, "The port", "does not exist");

                if (bScannerOnPort)
                {
                    /*
                     * Some device is using the serial port (hopefully the scanner).
                     */
                    statusSerialPort = enumSerialPortStatus.spsExceptionAccessDenied;
                }
                else
                {
                    if (bPortNotFound)
                    {
                        /*
                         * The port does not exist.  Indicated when the scanner is removed from the port.
                         */
                        statusSerialPort = enumSerialPortStatus.spsExceptionPortNotFound;
                    }
                    else
                    {
                        /*
                         * Some other exception.  
                         */
                        statusSerialPort = enumSerialPortStatus.spsExceptionOther;
                    }
                }
            }
        }

        public void issueScannedBarcodeEvent(BarcodeReadEventArgs args)
        {
            if (_busy) return;
            _busy = true;
            try
            {
                var raw = args.BarcodeData;

                LicenceScanner.Process(raw);

                ScannedBarcode?.Invoke(new object(), args);
            }
            finally
            {
                _busy = false;
            }
        }

        private static void ScanViewerImager_BarcodeRead(object sender, BarcodeReadEventArgs e)
        {
            if (_busy)
                return;
            _busy = true;

            try
            {
                ScanViewerImager.Open();
                if (ScanViewerImager.IsOpen)
                {
                    try
                    {
                        //issueScannedBarcodeEvent(new Hhp.Series4000.BarcodeReadEventArgs(e.BarcodeData));
                        LicenceScanner.Process(e.BarcodeData);
                    }
                    catch (Exception ex2)
                    {
                        //throw(ex2);
                    }
                }
                else
                {
                    try
                    {
                        ScanViewerImager.Open();
                    }
                    catch (Exception ex3)
                    {
                        //throw(ex3);
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == "The port is already open.")
                {
                    try
                    {
                        //issueScannedBarcodeEvent(new Hhp.Series4000.BarcodeReadEventArgs(e.BarcodeData));
                        LicenceScanner.Process(e.BarcodeData);
                    }
                    catch (Exception ex1)
                    {
                        //throw(ex1);
                    }
                }
                else if (ex.Message.Contains("The port is closed"))
                {
                    ScanViewerImager.Open();
                }
                else
                {
                    try
                    {
                        ScanViewerImager.Open();
                    }
                    catch (Exception ex3)
                    {
                    }
                }
            }
            finally
            {
                _busy = false;
            }
        }

        private static void imager_destroy_object()
        {
            try
            {
                if (ScanViewerImager != null)
                {
                    ScanViewerImager.Close();
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                try
                {
                    if (ScanViewerImager != null)
                    {
                        ScanViewerImager.Close();
                        ScanViewerImager.Dispose();
                    }
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    if (ScanViewerImager != null)
                    {
                        ScanViewerImager.Close();
                        ScanViewerImager = null;
                    }
                }
            }
        }

        public static void ENDSCANNER()
        {
            if (ScanViewerImager != null)
            {
                if (ScanViewerImager.IsOpen)
                {
                    try
                    {
                        ScanViewerImager.Close();
                    }
                    catch (Exception ex)
                    {
                        throw (ex);
                    }
                }
            }
        }

        #endregion

        #region Moto Scanner

        public void imager()
        {
            int opCode;
            string outXml;
            int status;
            string inXML;

            //registerForImager();

            var sleeptime = 256;

            try
            {
                Thread.Sleep(sleeptime);
                opCode = 3000; //
                outXml = "";
                status = 0;
                inXML = "<inArgs>" +
                        "<scannerID>1</scannerID>" +
                        "</inArgs>";
                ExecCmd(opCode, ref inXML, out outXML, out status);
                Thread.Sleep(sleeptime);
            }
            catch
            {
            }
        }

        private void checkScanner_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (MotoCon || HhpCon)
            {
                connected = true;
            }
            else
            {
                connected = false;
            }

            if (checkScanner.IsBusy)
            {
            }
            else
            {
                checkScanner.RunWorkerAsync();
            }
        }

        private void checkScanner_DoWork(object sender, DoWorkEventArgs e)
        {
            var output = new StringBuilder();
            short numberOfScanners;
            var connectedScannerIDList = new int[10];
            var outXML = "";
            scanner.GetScanners(out numberOfScanners, connectedScannerIDList, out outXML, out status);
            _log.DebugFormat("GetScanners: {0}, {1}", status, outXML);

            try
            {
                using (var reader = XmlReader.Create(new StringReader(outXML)))
                {
                    reader.ReadToFollowing("scannerID");
                    output.AppendLine(reader.ReadElementContentAsString());
                }
                var b = output.ToString();
                MotoCon = true;
            }
            catch
            {
                MotoCon = false;
            }
            Thread.Sleep(32);
        }

        public void ViewFinderEnable()
        {
            var inXml = "<inArgs>" +
                        "<scannerID>1</scannerID>" +
                        "<cmdArgs>" +
                        "<arg-xml>" +
                        "<attrib_list>" +
                        "<attribute>" +
                        "<id>324</id>" +
                        "<datatype>B</datatype>" +
                        "<value>1</value>" +
                        "</attribute></attrib_list>" +
                        "</arg-xml>" +
                        "</cmdArgs>" +
                        "</inArgs>";

            var opCode = 2016;
            var outXml = "";
            var status = 1;
            ExecCmd(opCode, ref inXml, out outXml, out status);
        }

        public void PushTrigger()
        {
            int opCode;
            string outXml;
            int status;
            string inXML;

            try
            {
                opCode = 2011;
                outXml = "";
                status = 0;
                inXML = "<inArgs>" +
                        "<scannerID>1</scannerID>" +
                        "</inArgs>";
                ExecCmd(opCode, ref inXML, out outXML, out status);
            }
            catch (Exception)
            {
            }
        }

        public void RebootScanner()
        {
            var inXml = "<inArgs>" +
                        "<scannerID>1</scannerID>" +
                        "</inArgs>";
            var opCode = 2019;
            var outXml = "";
            var status = 1;
            ExecCmd(opCode, ref inXml, out outXml, out status);
        }

        public void TurnLEDOn(byte byLed)
        {
            var inXml = "<inArgs>" +
                        "<scannerID>1</scannerID>" +
                        "<cmdArgs>" +
                        "<arg-int>" + Convert.ToInt32(byLed) + "</arg-int>" +
                        "</cmdArgs>" +
                        "</inArgs>";

            var opCode = DEVICE_LED_ON;
            var outXml = "";
            var status = 1;
            ExecCmd(opCode, ref inXml, out outXml, out status);
        }

        public void TurnLEDOff(byte byLed)
        {
            var inXml = "<inArgs>" +
                        "<scannerID>1</scannerID>" +
                        "<cmdArgs>" +
                        "<arg-int>" + Convert.ToInt32(byLed) + "</arg-int>" +
                        "</cmdArgs>" +
                        "</inArgs>";

            var opCode = DEVICE_LED_OFF;
            var outXml = "";
            var status = 1;
            ExecCmd(opCode, ref inXml, out outXml, out status);
        }

        public void setBeeper(int beep)
        {
            var inXml = "<inArgs>" +
                        "<scannerID>1</scannerID>" +
                        "<cmdArgs>" +
                        "<arg-int>" + beep
                        + "</arg-int>" +
                        "</cmdArgs>" +
                        "</inArgs>";

            var opCode = DEVICE_BEEP_CONTROL;
            var outXml = "";
            var status = STATUS_FALSE;
            ExecCmd(opCode, ref inXml, out outXml, out status);
        }

        public void AimON()
        {
            var inXml = "<inArgs>" +
                        "<scannerID>" + 1 + "</scannerID>" +
                        "</inArgs>";
            var opCode = DEVICE_AIM_ON;
            var outXml = "";
            var status = STATUS_FALSE;
            ExecCmd(opCode, ref inXml, out outXml, out status);
        }

        public void AimOFF()
        {
            try
            {
                var inXml = "<inArgs>" +
                            "<scannerID>" + 1 + "</scannerID>" +
                            "</inArgs>";
                var opCode = DEVICE_AIM_OFF;
                var outXml = "";
                var status = STATUS_FALSE;
                ExecCmd(opCode, ref inXml, out outXml, out status);
            }
            catch
            {
            }
        }

        public byte[] HexStrToByteArray(string aString)
        {
            byte[] res = null;
            var s = "";
            var arr = aString.Replace("\n", "").Replace("\r", "").Split(' ');
            res = new byte[arr.Length];
            var pos = 0;
            foreach (var _s in arr)
            {
                if (_s == "") continue;
                s = _s.Substring(2);
                var b = Convert.ToByte(_s, 16);
                res[pos] = b;
                pos++;
            }
            return res;
        }

        public List<ScannerMoto> ShowScanners()
        {
            ScannerMoto[] m_arScanners;

            short numOfScanners = 0;
            var nScannerCount = 0;
            var outXML = "";
            var status = STATUS_FALSE;
            var scannerIdList = new int[10];
            try
            {
                scanner.GetScanners(out numOfScanners, scannerIdList, out outXML, out status);
                if (STATUS_SUCCESS == status)
                {
                    m_arScanners = new ScannerMoto[numOfScanners];
                    for (var i = 0; i < numOfScanners; i++)
                    {
                        var scanr = new ScannerMoto();
                        m_arScanners.SetValue(scanr, i);
                    }

                    var reader = new ScannerMoto.XmlReader();
                    //XmlReader reader = new XmlReader();

                    reader.ReadXmlString_GetScanners(outXML, m_arScanners, numOfScanners, out nScannerCount);

                    return ScannerMoto.FillScannerList(m_arScanners);
                }
                return null;
            }
            catch (Exception exp)
            {
                return null;
            }
        }

        private void scanner_BarcodeEvent(short eventType, ref string pscanData)
        {
            _log.DebugFormat("BarcodeEvent (busy: {2}): {0}, {1}, ", eventType, pscanData, _busy);
            if (_busy) return;
            _busy = true;

            SystemSounds.Beep.Play();
            try
            {
                var b = pscanData;
                var output = new StringBuilder();

                // Create an XmlReader
                if (Settings.Default.ScanType == "Honeywell")
                {
                    using (var reader = XmlReader.Create(new StringReader(b)))
                    {
                        reader.ReadToFollowing("rawdata");
                        output.AppendLine(reader.ReadElementContentAsString());
                    }
                }
                else
                {
                    using (var reader = XmlReader.Create(new StringReader(b)))
                    {
                        reader.ReadToFollowing("datalabel");
                        output.AppendLine(reader.ReadElementContentAsString());
                    }
                }

                var raw = HexStrToByteArray(output.ToString());

                LicenceScanner.Process(raw);
            }
            catch (Exception ex)
            {
                Logger.Log(this, "Motorola Barcode", "Error decrypting licence", ex);
            }
            finally
            {
                _busy = false;
            }
        }


        #endregion
    }

    public enum scannerType
    {
        motorola,
        Hhp4800
    }
}