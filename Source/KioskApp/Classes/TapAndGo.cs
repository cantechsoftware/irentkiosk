﻿using System;
using System.Linq;
using System.Windows.Forms;
using iRent.Properties;
using log4net;
using SmartFormat;

namespace iRent.Classes
{
    public class TapAndGo
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(CancomRFID));

        private long _hdle;

        private static Reader _reader;

        public TapAndGo()
        {
            _hdle = 0;
            _reader = new Reader();
        }

        public void OpenComm()
        {

        }

        public void CloseComm()
        {

        }

        private int ExecCmd(byte Addr, byte Cm, byte Pm, UInt16 TxDataLen, byte[] TxData, ref byte ReType,
            ref byte St0, ref byte St1, ref byte St2, ref UInt16 RxDataLen, byte[] RxData)
        {
            IntPtr ptr = new IntPtr(_hdle);
            var i = DllClass.ExecuteCommand(ptr, Addr, Cm, Pm, TxDataLen, TxData,
                ref ReType, ref St0, ref St1, ref St2,
                ref RxDataLen, RxData);
            _log.DebugFormat("ExecuteCommand: Hdle: {0}, Addr: {1}, Cm: {2}, Pm: {3}, TxData: {4}\r\n" +
                             "ReType: {5}, Status: {6}, RxData: {7}\r\n" +
                             "Result: {8}",
                _hdle, Addr, Cm, Pm, ToHex(TxData, TxDataLen),
                ReType, Str(St0, St1, St2),
                ToHex(RxData, RxDataLen),
                i);

            return i;
        }

        private string Str(params byte[] chars)
        {
            return new string(chars.Select(x => (char) x).ToArray());
        }

        private string ToHex(byte[] data, int length)
        {
            if (data == null)
                return null;
            var buff = new byte[length];
            Array.Copy(data, buff, length);
            return BitConverter.ToString(buff).Replace("-", "");
        }

        public string CardActivate()
        {
            try
            {
                _log.InfoFormat("Card Activate: {0}", _hdle);
                byte[] snr = new byte[6];
                for (int j = 0; j < snr.Length; j++)
                    snr[j] = Convert.ToByte("FF", 16);
                Reader.GetSerNum(snr);
                byte mode1 = 0x00;
                byte mode2 = 0x00;
                byte mode = (byte)((mode1 << 1) | mode2);
                byte blk_add = Convert.ToByte("10", 16);
                byte num_blk = Convert.ToByte("01", 16);
                int nRet = 1;
                byte[] buffer = new byte[16 * num_blk];

                TimeSpan delta = new TimeSpan();
                DateTime StartTime = DateTime.Now;
                DateTime SampleTime = DateTime.Now;

                while (nRet != 0)
                {
                    nRet = Reader.MF_Read(mode, blk_add, num_blk, snr, buffer);
                    SampleTime = DateTime.Now;
                    delta = SampleTime.Subtract(StartTime);
                    if (delta.TotalMilliseconds > 9000)
                    {
                        throw new TimeoutException("No RFID Card found. The Scanner requires you to scan a card.");
                    }
                }
                int s = 0;
                int e = 4;
                for (int i = 0; i < e; i++)
                {
                    if (snr[s + i] < 0)
                        snr[s + i] = Convert.ToByte(Convert.ToInt32(snr[s + i]) + 256);
                }
                string ret = "";
                for (int i = 0; i < e; i++)
                {
                    _log.DebugFormat("Card Activate - type B: {0}", snr[s + i].ToString("X2") + " ");
                    ret = ret + snr[s + i].ToString("X2");
                    
                }
                ret = RFIDFormat(ret);
                _log.DebugFormat("Card Activate - mifare: {0}", ret);
                return ret;
            }
            catch(Exception ex)
            {
                _log.Error(ex);
                return "Error";
            }
        }

        //public string CardActivateBtn()
        //{
        //    _log.InfoFormat("Card Activate Btn: {0}", _hdle);

        //    if (_hdle == 0)
        //    {
        //        _log.Fatal("Handle is 0");
        //        return string.Empty;
        //    }

        //    byte Addr;
        //    byte Cm, Pm;
        //    UInt16 TxDataLen, RxDataLen;
        //    var TxData = new byte[1024];
        //    var RxData = new byte[1024];
        //    byte ReType = 0;
        //    byte St0, St1, St2;

        //    Cm = 0x60;
        //    Pm = 0x30;
        //    St0 = St1 = St2 = 0;
        //    TxDataLen = 2;
        //    RxDataLen = 0;

        //    TxData[0] = 0x41;
        //    TxData[1] = 0x42;
        //    Addr = (byte.Parse("00"));
        //    var i = ExecCmd(Addr, Cm, Pm, TxDataLen, TxData, ref ReType, ref St0, ref St1, ref St2, ref RxDataLen,
        //        RxData);
        //    if (i != 0)
        //    {
        //        _log.Error("Communication Error");
        //        //MessageBox.Show("Communication Error", "Caution");
        //        return string.Empty;
        //    }

        //    if ((ReType == 0x4e))
        //    {
        //        _log.ErrorFormat("Cold Reset ERROR: {0}", Str(St0, St1, St2));
        //        //MessageBox.Show("Cold Reset ERROR" + "\r\n" + "Error Code:  " + (char)St1 + (char)St2, "Cold Reset");
        //        return string.Empty;
        //    }

        //    if (ReType != 0x50)
        //    {
        //        _log.Error("Communication Error");
        //        //MessageBox.Show("Communication Error", "Caution");
        //        return string.Empty;
        //    }

        //    int n;
        //    var StrBuf = "";

        //    for (n = 0; n < RxDataLen; n++)
        //    {
        //        StrBuf += RxData[n].ToString("X2");
        //    }
        //    //CardActivateTxt.Text = StrBuf;   //返回的总的数据包

        //    switch (RxData[0])
        //    {
        //        case 0x4d: //mifare one Card
        //        {
        //            var CardUID = "";
        //            for (n = 0; n < RxData[3]; n++)
        //            {
        //                CardUID += RxData[n + 4].ToString("X2");
        //            }
        //            var ret = RFIDFormat(CardUID);
        //            _log.DebugFormat("Card Activate - mifare: {0}", ret);
        //            return ret;
        //        }
        //        case 0x41: //type A card
        //        {
        //            var CardUID = "";
        //            var CardATS = "";
        //            for (n = 0; n < RxData[3]; n++)
        //            {
        //                CardUID += RxData[n + 4].ToString("X2");
        //            }
        //            for (n = 0; n < RxDataLen - RxData[3] - 5; n++)
        //            {
        //                CardATS += RxData[n + 5 + RxData[3]].ToString("X2");
        //            }
        //            _log.DebugFormat("Card Activate - type A: {0}, {1}", CardUID, CardATS);
        //            //TypeAUIDtxt.Text = CardUID;
        //            //TypeAATRtxt.Text = CardATS;
        //            //MessageBox.Show("type A Card Activate Successed\nCardUID: " + CardUID + "\nCardATS: " + CardATS, "Activate RF Card");
        //            break;
        //        }
        //        case 0x42: //type B card
        //        {
        //            var CardATS = "";
        //            for (n = 0; n < RxDataLen - 1; n++)
        //            {
        //                CardATS += RxData[n + 1].ToString("X2");
        //            }
        //            //TypeAATRtxt.Text = CardATS;
        //            //MessageBox.Show("type B Card Activate Successed\nCardATS: " + CardATS, "Activate RF Card");
        //            _log.DebugFormat("Card Activate - type B: {0}", CardATS);
        //            break;
        //        }
        //    }
        //    return "";
        //}

        public string RFIDFormat(string RFID)
        {
            try
            {
                var newstring = "";

                while (RFID.Length > 0)
                {
                    newstring = newstring + RFID.Substring(RFID.Length - 2, 2);
                    RFID = RFID.Remove(RFID.Length - 2, 2);
                }
                return newstring;
            }
            catch (Exception ex)
            {
                _log.Error("Error in RFIDFormat " + RFID, ex);
                return "";
            }
        }
    }
}