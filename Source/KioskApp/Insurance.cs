﻿using System;
using System.Windows.Forms;
using Castle.Core;
using MetroFramework.Controls;
using iRent.Classes;
using iRent.iRentService;

namespace iRent
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class Insurance : MetroUserControl
    {
        public Insurance()
        {
            InitializeComponent();
        }

        private void metroRadioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (metroRadioButton1.Checked || metroRadioButton2.Checked)
            {
                btnNext.Enabled = true;
                UIFactory.Accept = true;
            }
        }

        private void metroRadioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (metroRadioButton1.Checked || metroRadioButton2.Checked)
            {
                btnNext.Enabled = true;
                UIFactory.Accept = true;
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            UIFactory.NextControl = "TermsAndConditions";
            var form = ParentForm as MainForm;
            Invoke(new MethodInvoker(delegate { form.Accept(); }));
        }

        private void Insurance_Load(object sender, EventArgs e)
        {
            btnNext.Enabled = false;
            UIFactory.CurrentControl = "Insurance";
        }
    }
}