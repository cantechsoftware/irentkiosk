﻿namespace iRent.Registration
{
    partial class UserDetailsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserDetailsControl));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.postalAddress = new iRent.Registration.AddressControl();
            this.physicalAddress = new iRent.Registration.AddressControl();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.txtFirstName = new MetroFramework.Controls.MetroTextBox();
            this.cmbTitle = new MetroFramework.Controls.MetroComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.txtConfirmEmail = new MetroFramework.Controls.MetroTextBox();
            this.btnSameAsPhysical = new MetroFramework.Controls.MetroButton();
            this.lblHeader2 = new MetroFramework.Controls.MetroLabel();
            this.txtCellNo = new MetroFramework.Controls.MetroTextBox();
            this.txtTelNo = new MetroFramework.Controls.MetroTextBox();
            this.txtWorkNo = new MetroFramework.Controls.MetroTextBox();
            this.txtEmail = new MetroFramework.Controls.MetroTextBox();
            this.lblHeader = new MetroFramework.Controls.MetroLabel();
            this.btnNext = new MetroFramework.Controls.MetroButton();
            this.lblStatus = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanel1.SuspendLayout();
            this.metroPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.metroPanel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnNext, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblStatus, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(789, 686);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // metroPanel1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.metroPanel1, 3);
            this.metroPanel1.Controls.Add(this.postalAddress);
            this.metroPanel1.Controls.Add(this.physicalAddress);
            this.metroPanel1.Controls.Add(this.metroLabel1);
            this.metroPanel1.Controls.Add(this.txtFirstName);
            this.metroPanel1.Controls.Add(this.cmbTitle);
            this.metroPanel1.Controls.Add(this.pictureBox1);
            this.metroPanel1.Controls.Add(this.metroLabel2);
            this.metroPanel1.Controls.Add(this.txtConfirmEmail);
            this.metroPanel1.Controls.Add(this.btnSameAsPhysical);
            this.metroPanel1.Controls.Add(this.lblHeader2);
            this.metroPanel1.Controls.Add(this.txtCellNo);
            this.metroPanel1.Controls.Add(this.txtTelNo);
            this.metroPanel1.Controls.Add(this.txtWorkNo);
            this.metroPanel1.Controls.Add(this.txtEmail);
            this.metroPanel1.Controls.Add(this.lblHeader);
            this.metroPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(3, 3);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(783, 530);
            this.metroPanel1.TabIndex = 0;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // postalAddress
            // 
            this.postalAddress.AutoScroll = true;
            this.postalAddress.AutoSize = true;
            this.postalAddress.Location = new System.Drawing.Point(-3, 345);
            this.postalAddress.Name = "postalAddress";
            this.postalAddress.Size = new System.Drawing.Size(784, 88);
            this.postalAddress.TabIndex = 8;
            this.postalAddress.UseSelectable = true;
            // 
            // physicalAddress
            // 
            this.physicalAddress.AutoScroll = true;
            this.physicalAddress.AutoSize = true;
            this.physicalAddress.Location = new System.Drawing.Point(-3, 214);
            this.physicalAddress.Name = "physicalAddress";
            this.physicalAddress.Size = new System.Drawing.Size(784, 88);
            this.physicalAddress.TabIndex = 7;
            this.physicalAddress.UseSelectable = true;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel1.Location = new System.Drawing.Point(3, 317);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(129, 25);
            this.metroLabel1.TabIndex = 46;
            this.metroLabel1.Text = "Postal Address";
            // 
            // txtFirstName
            // 
            this.txtFirstName.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtFirstName.Icon = global::iRent.Properties.Resources.icon_star;
            this.txtFirstName.IconRight = true;
            this.txtFirstName.Lines = new string[0];
            this.txtFirstName.Location = new System.Drawing.Point(268, 29);
            this.txtFirstName.MaxLength = 32767;
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.PasswordChar = '\0';
            this.txtFirstName.PromptText = "First Name";
            this.txtFirstName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtFirstName.SelectedText = "";
            this.txtFirstName.Size = new System.Drawing.Size(245, 30);
            this.txtFirstName.TabIndex = 1;
            this.txtFirstName.UseSelectable = true;
            // 
            // cmbTitle
            // 
            this.cmbTitle.BackColor = System.Drawing.SystemColors.Control;
            this.cmbTitle.FontSize = MetroFramework.MetroComboBoxSize.Tall;
            this.cmbTitle.ItemHeight = 29;
            this.cmbTitle.Items.AddRange(new object[] {
            "Mr",
            "Mrs",
            "Miss",
            "Ms",
            "Dr"});
            this.cmbTitle.Location = new System.Drawing.Point(3, 29);
            this.cmbTitle.Name = "cmbTitle";
            this.cmbTitle.PromptText = "Title";
            this.cmbTitle.Size = new System.Drawing.Size(245, 35);
            this.cmbTitle.TabIndex = 0;
            this.cmbTitle.UseCustomBackColor = true;
            this.cmbTitle.UseSelectable = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.ImageLocation = "iRent.Properties.Resources.icon_star";
            this.pictureBox1.Location = new System.Drawing.Point(574, 488);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(18, 18);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 36;
            this.pictureBox1.TabStop = false;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(590, 488);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(96, 19);
            this.metroLabel2.TabIndex = 35;
            this.metroLabel2.Text = "Required fields";
            // 
            // txtConfirmEmail
            // 
            this.txtConfirmEmail.BackColor = System.Drawing.Color.Red;
            this.txtConfirmEmail.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtConfirmEmail.Icon = ((System.Drawing.Image)(resources.GetObject("txtConfirmEmail.Icon")));
            this.txtConfirmEmail.IconRight = true;
            this.txtConfirmEmail.Lines = new string[0];
            this.txtConfirmEmail.Location = new System.Drawing.Point(268, 80);
            this.txtConfirmEmail.MaxLength = 32767;
            this.txtConfirmEmail.Name = "txtConfirmEmail";
            this.txtConfirmEmail.PasswordChar = '\0';
            this.txtConfirmEmail.PromptText = "Confirm Email";
            this.txtConfirmEmail.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtConfirmEmail.SelectedText = "";
            this.txtConfirmEmail.Size = new System.Drawing.Size(245, 30);
            this.txtConfirmEmail.TabIndex = 3;
            this.txtConfirmEmail.UseSelectable = true;
            // 
            // btnSameAsPhysical
            // 
            this.btnSameAsPhysical.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btnSameAsPhysical.Location = new System.Drawing.Point(3, 427);
            this.btnSameAsPhysical.Name = "btnSameAsPhysical";
            this.btnSameAsPhysical.Size = new System.Drawing.Size(146, 46);
            this.btnSameAsPhysical.TabIndex = 16;
            this.btnSameAsPhysical.Text = "Same as Physical";
            this.btnSameAsPhysical.UseSelectable = true;
            this.btnSameAsPhysical.Click += new System.EventHandler(this.btnSameAsPhysical_Click);
            // 
            // lblHeader2
            // 
            this.lblHeader2.AutoSize = true;
            this.lblHeader2.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblHeader2.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lblHeader2.Location = new System.Drawing.Point(3, 186);
            this.lblHeader2.Name = "lblHeader2";
            this.lblHeader2.Size = new System.Drawing.Size(144, 25);
            this.lblHeader2.TabIndex = 25;
            this.lblHeader2.Text = "Physical Address";
            // 
            // txtCellNo
            // 
            this.txtCellNo.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtCellNo.Icon = ((System.Drawing.Image)(resources.GetObject("txtCellNo.Icon")));
            this.txtCellNo.IconRight = true;
            this.txtCellNo.Lines = new string[0];
            this.txtCellNo.Location = new System.Drawing.Point(268, 130);
            this.txtCellNo.MaxLength = 32767;
            this.txtCellNo.Name = "txtCellNo";
            this.txtCellNo.PasswordChar = '\0';
            this.txtCellNo.PromptText = "Cell Number";
            this.txtCellNo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCellNo.SelectedText = "";
            this.txtCellNo.Size = new System.Drawing.Size(245, 30);
            this.txtCellNo.TabIndex = 5;
            this.txtCellNo.UseSelectable = true;
            // 
            // txtTelNo
            // 
            this.txtTelNo.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtTelNo.Lines = new string[0];
            this.txtTelNo.Location = new System.Drawing.Point(3, 130);
            this.txtTelNo.MaxLength = 32767;
            this.txtTelNo.Name = "txtTelNo";
            this.txtTelNo.PasswordChar = '\0';
            this.txtTelNo.PromptText = "Home Number";
            this.txtTelNo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtTelNo.SelectedText = "";
            this.txtTelNo.Size = new System.Drawing.Size(245, 30);
            this.txtTelNo.TabIndex = 4;
            this.txtTelNo.UseSelectable = true;
            // 
            // txtWorkNo
            // 
            this.txtWorkNo.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtWorkNo.Icon = global::iRent.Properties.Resources.icon_star;
            this.txtWorkNo.IconRight = true;
            this.txtWorkNo.Lines = new string[0];
            this.txtWorkNo.Location = new System.Drawing.Point(533, 130);
            this.txtWorkNo.MaxLength = 32767;
            this.txtWorkNo.Name = "txtWorkNo";
            this.txtWorkNo.PasswordChar = '\0';
            this.txtWorkNo.PromptText = "Work Number";
            this.txtWorkNo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtWorkNo.SelectedText = "";
            this.txtWorkNo.Size = new System.Drawing.Size(245, 30);
            this.txtWorkNo.TabIndex = 6;
            this.txtWorkNo.UseSelectable = true;
            // 
            // txtEmail
            // 
            this.txtEmail.BackColor = System.Drawing.Color.Red;
            this.txtEmail.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtEmail.Icon = ((System.Drawing.Image)(resources.GetObject("txtEmail.Icon")));
            this.txtEmail.IconRight = true;
            this.txtEmail.Lines = new string[0];
            this.txtEmail.Location = new System.Drawing.Point(3, 80);
            this.txtEmail.MaxLength = 32767;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.PasswordChar = '\0';
            this.txtEmail.PromptText = "Email";
            this.txtEmail.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtEmail.SelectedText = "";
            this.txtEmail.Size = new System.Drawing.Size(245, 30);
            this.txtEmail.TabIndex = 2;
            this.txtEmail.UseSelectable = true;
            // 
            // lblHeader
            // 
            this.lblHeader.AutoSize = true;
            this.lblHeader.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblHeader.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lblHeader.Location = new System.Drawing.Point(3, 0);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(131, 25);
            this.lblHeader.TabIndex = 17;
            this.lblHeader.Text = "Contact Details";
            // 
            // btnNext
            // 
            this.btnNext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNext.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnNext.Location = new System.Drawing.Point(297, 589);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(194, 94);
            this.btnNext.TabIndex = 19;
            this.btnNext.Text = "Next";
            this.btnNext.UseSelectable = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.lblStatus, 3);
            this.lblStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblStatus.FontSize = MetroFramework.MetroLabelSize.XL;
            this.lblStatus.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblStatus.Location = new System.Drawing.Point(3, 536);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(783, 50);
            this.lblStatus.Style = MetroFramework.MetroColorStyle.Red;
            this.lblStatus.TabIndex = 2;
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // UserDetailsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "UserDetailsControl";
            this.Size = new System.Drawing.Size(789, 686);
            this.Load += new System.EventHandler(this.UserDetails_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroButton btnSameAsPhysical;
        private MetroFramework.Controls.MetroLabel lblHeader2;
        private MetroFramework.Controls.MetroTextBox txtCellNo;
        private MetroFramework.Controls.MetroTextBox txtTelNo;
        private MetroFramework.Controls.MetroTextBox txtWorkNo;
        private MetroFramework.Controls.MetroTextBox txtEmail;
        private MetroFramework.Controls.MetroLabel lblHeader;
        private MetroFramework.Controls.MetroButton btnNext;
        private MetroFramework.Controls.MetroTextBox txtConfirmEmail;
        private MetroFramework.Controls.MetroLabel lblStatus;
        private System.Windows.Forms.PictureBox pictureBox1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox txtFirstName;
        private MetroFramework.Controls.MetroComboBox cmbTitle;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private Registration.AddressControl physicalAddress;
        private Registration.AddressControl postalAddress;
    }
}
