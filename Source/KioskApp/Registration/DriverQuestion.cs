﻿using System.Xml.Serialization;

namespace iRent.Registration
{
    [XmlType("Question")]
    public class DriverQuestion
    {
        public DriverQuestion(string answerKey, string text, bool password = false, int maxchars = int.MaxValue, bool numeric = false)
        {
            AnswerKey = answerKey;
            Text = text;
            Password = password;
            Maxchars = maxchars;
            Numeric = numeric;
        }

        public DriverQuestion()
        {
        }

        [XmlAttribute("text")]
        public string Text { get; set; }

        [XmlAttribute("key")]
        public string AnswerKey { get; set; }

        [XmlAttribute("password")]
        public bool Password { get; set; }

        [XmlAttribute("maxchars")]
        public int Maxchars { get; set; }

        [XmlAttribute("numeric")]
        public bool Numeric { get; set; }
    }
}