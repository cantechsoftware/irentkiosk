﻿using System;
using System.Windows.Forms;
using Castle.Core;
using SmartFormat;
using iRent.Framework;
using iRentKiosk.Core.Services;

namespace iRent.Registration
{
    [CastleComponent]
    public class SummaryRegCommand : BaseCommand
    {
        private readonly Func<SummaryRegControl> _factory;

        public SummaryRegCommand(Func<SummaryRegControl> factory)
        {
            _factory = factory;
            Intro = "Thank you for registering with us {DriverName}.";
            Status = "Return to the home screen to rent a vehicle.";
            Button = "Next";
        }

        public string Next { get; set; }
        public string Intro { get; set; }
        public string Status { get; set; }
        public string Button { get; set; }

        public override void Invoke()
        {
            var control = _factory();
            control.Dock = DockStyle.Fill;
            var main = GetFormAs<MainForm>();
            main.AddToFormPanel(control);
            main.ToggleHomeButton(true);
            control.Setup(Intro.FormatSmart(ContextData.Instance),
                          Status.FormatSmart(ContextData.Instance),
                          Button.FormatSmart(ContextData.Instance));
            control.Next += (o, e) => Proceed(Next);
        }
    }
}