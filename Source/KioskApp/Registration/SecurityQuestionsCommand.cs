﻿using System;
using Castle.Core;
using iRent.Framework;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;

namespace iRent.Registration
{
    [CastleComponent]
    public class SecurityQuestionsCommand : BaseCommand
    {
        private readonly DriverApi _driverApi;
        private readonly Func<DriverQuestionsControl> _factory;

        public SecurityQuestionsCommand(Func<DriverQuestionsControl> factory,
            DriverApi driverApi)
        {
            _factory = factory;
            _driverApi = driverApi;
            Questions = new[]
            {
                new DriverQuestion("NickName", "What was your childhood nickname?"),
                new DriverQuestion("PetName", "What was your first pets name?"),
                new DriverQuestion("CarColour", "What was the colour of your first car?")
            };
        }

        public DriverQuestion[] Questions { get; set; }
        public string Next { get; set; }

        public override void Invoke()
        {
            var driver = ContextData.Driver;
            var control = _factory();
            var main = GetFormAs<MainForm>();
            main.AddToFormPanel(control);
            control.Next += (o, e) =>
            {
                control.Update(driver);
                driver.IsRegistered = true;
                driver = _driverApi.Save(driver);
                ContextData.SetDriver(driver);
                Proceed(Next);
            };
            control.Bind(Questions, ContextData.Driver);
        }

    }
}
 