﻿using System;
using System.Drawing;
using CanTech.Common.Extensions;
using Castle.Core;
using iRent2.Contracts.Enums;
using iRent2.Contracts.Models;
using MetroFramework.Controls;
using log4net;
using System.Windows.Forms;

namespace iRent.Registration
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class CaptureAddressControl : MetroUserControl
    {
        private DriverModel _driver;
        private static readonly ILog _log = LogManager.GetLogger(typeof(CaptureAddressControl));

        public CaptureAddressControl()
        {
            InitializeComponent();
        }

        public string Title
        {
            get { return lblHeader.Text; }
            set { lblHeader.Text = value; }
        }

        private bool ValidateText()
        {
            var err = GetError();
            if (err == null)
            {
                lblStatus.Text = "";
                return true;
            }

            lblStatus.Text = err;
            lblStatus.ForeColor = Color.Red;
            return false;
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateText())
                    return;

                Next?.Invoke(this, e);
            }
            catch (Exception ex)
            {
                _log.Error("Error during CaptureAddress.btnNext_Click: ", ex);
                MessageBox.Show("Error Occurred during CaptureAddress.btnNext_Click: \n" + ex.Message);
            }
        }

        public event EventHandler Next;


        public string GetError()
        {
            if (rdbPOBox.Checked)
            {
                if (txtStreetNo.Text.Trim().IsEmpty())
                    return "Please enter a P.O. Box Number.";
                return null;
            }

            if (txtStreet.Text.Trim().Length == 0)
                return "Please enter a Street Number.";

            if (txtStreet.Text.Trim().Length == 0)
                return "Please enter a Street Name.";

            if (txtSuburb.Text.Trim().Length == 0)
                return "Please enter a Suburb.";

            if (txtTown.Text.Trim().Length == 0)
                return "Please enter a Town.";

            if (txtProvince.Text.Trim().Length == 0)
                return "Please enter a Province.";

            if (txtAreaCode.Text.Trim().Length == 0)
                return "Please enter an Area Code.";

            if (txtCountry.Text.Trim().Length == 0)
                return "Please enter a Country.";

            return null;
        }

        public AddressModel Update(AddressModel address)
        {
            try
            {
                if (address == null)
                    address = new AddressModel();

                if (rdbPOBox.Checked)
                {
                    address.POBox = txtStreetNo.Text;
                    address.StreetName = null;
                    address.StreetNo = null;
                    address.Complex = null;
                    address.HouseNo = null;
                }
                else
                {
                    address.POBox = null;
                    address.StreetNo = txtStreetNo.Text;
                    address.StreetName = txtStreet.Text;
                    address.Complex = txtComplex.Text;
                    address.HouseNo = txtUnitNo.Text;
                }
                address.Suburb = txtSuburb.Text;
                address.City = txtTown.Text;
                address.State = txtProvince.Text;
                address.PostCode = txtAreaCode.Text;
                address.Country = txtCountry.Text;
            }
            catch (Exception ex)
            {
                _log.Error("Error during CaptureAddress.btnNext_Click: ", ex);
                MessageBox.Show("Error Occurred during CaptureAddress.btnNext_Click: \n" + ex.Message);
            }
            return address;
        }

        public void Bind(DriverModel driver, AddressType addressType)
        {
            _driver = driver;

            if (addressType == AddressType.Physical)
            {
                HideRows(1, 2);
                rdbStreet.Checked = true;
                Bind(driver.PhysicalAddress);
            }
            else
            {
                if (driver.PostalAddress?.POBox.HasValue() == true)
                    rdbPOBox.Checked = true;
                else if (driver.PostalAddress != null)
                    rdbStreet.Checked = true;

                Bind(driver.PostalAddress);
            }
        }

        private void HideRows(params int[] rows)
        {
            foreach (var row in rows)
                pnlQuestions.RowStyles[row].Height = 0;
        }

        private void ShowRows(params int[] rows)
        {
            foreach (var row in rows)
                pnlQuestions.RowStyles[row].Height = 35;
        }

        public void Bind(AddressModel address)
        {
            if (address == null)
                return;

            if (address.POBox.HasValue())
                txtStreetNo.Text = address.POBox;
            else
            {
                txtUnitNo.Text = address.HouseNo;
                txtComplex.Text = address.Complex;
                txtStreet.Text = address.StreetName;
                txtStreetNo.Text = address.StreetNo;
            }
            txtCountry.Text = address.Country;
            txtSuburb.Text = address.Suburb;
            txtTown.Text = address.City;
            txtProvince.Text = address.State;
            txtAreaCode.Text = address.PostCode;
        }

        private void rdbPOBox_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbPOBox.Checked)
            {
                HideRows(3, 4, 6);
                txtStreet.Text = "P.O. Box";
                txtStreetNo.PromptText = "P.O. Box No";
                lblStreetNo.Text = "P.O. Box No:";
                txtStreetNo.Text = "";
                txtUnitNo.Text = "";
                txtComplex.Text = "";
            }
            else
            {
                ShowRows(3, 4, 6);
                txtStreet.Text = "";
                txtStreetNo.Text = "";
                lblStreetNo.Text = "Street No:";
                txtStreetNo.PromptText = "Street No";
            }
        }

        private void btnSameAsPhysical_Click(object sender, EventArgs e)
        {
            rdbStreet.Checked = true;
            Bind(_driver.PhysicalAddress);
        }
    }
}