﻿using System;
using System.Windows.Forms;
using Castle.Core;
using SmartFormat;
using iRent.Framework;
using iRentKiosk.Core.Services;

namespace iRent.Registration
{
    [CastleComponent]
    public class PleaseCompleteRegistrationCommand : BaseCommand
    {
        private readonly Func<PleaseCompleteRegistrationControl> _factory;

        public PleaseCompleteRegistrationCommand(Func<PleaseCompleteRegistrationControl> factory)
        {
            _factory = factory;
            Intro = "Your biometric information is incomplete.";
            Status = "Please retrun to the home screen and complete your registration.";
            Button = "Next";
        }

        public string Next { get; set; }
        public string Intro { get; set; }
        public string Status { get; set; }
        public string Button { get; set; }

        public override void Invoke()
        {
            var control = _factory();
            control.Dock = DockStyle.Fill;
            var main = GetFormAs<MainForm>();
            main.AddToFormPanel(control);
            main.ToggleHomeButton(true);
            control.Setup(Intro.FormatSmart(ContextData.Instance),
                          Status.FormatSmart(ContextData.Instance),
                          Button.FormatSmart(ContextData.Instance));
            control.Next += (o, e) => Proceed(Next);
        }
    }
}