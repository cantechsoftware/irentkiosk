﻿using MetroFramework.Controls;

namespace iRent.Registration
{
    partial class ContactDetailsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ContactDetailsControl));
            this.pnlQuestions = new System.Windows.Forms.TableLayoutPanel();
            this.txtDOB = new MetroFramework.Controls.MetroTextBox();
            this.txtIDNo = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.txtWorkNo = new MetroFramework.Controls.MetroTextBox();
            this.txtCellNo = new MetroFramework.Controls.MetroTextBox();
            this.txtHomeNo = new MetroFramework.Controls.MetroTextBox();
            this.txtSurname = new MetroFramework.Controls.MetroTextBox();
            this.txtEmail = new MetroFramework.Controls.MetroTextBox();
            this.txtConfirmEmail = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.txtFirstName = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.cmbTitle = new MetroFramework.Controls.MetroComboBox();
            this.lblStatus = new MetroFramework.Controls.MetroLabel();
            this.btnNext = new MetroFramework.Controls.MetroButton();
            this.lblHeader = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.pnlQuestions.SuspendLayout();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlQuestions
            // 
            this.pnlQuestions.ColumnCount = 4;
            this.tableLayoutPanel.SetColumnSpan(this.pnlQuestions, 3);
            this.pnlQuestions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnlQuestions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 267F));
            this.pnlQuestions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 267F));
            this.pnlQuestions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnlQuestions.Controls.Add(this.cmbTitle, 2, 3);
            this.pnlQuestions.Controls.Add(this.metroLabel2, 1, 3);
            this.pnlQuestions.Controls.Add(this.metroLabel1, 1, 4);
            this.pnlQuestions.Controls.Add(this.txtFirstName, 2, 4);
            this.pnlQuestions.Controls.Add(this.metroLabel3, 1, 5);
            this.pnlQuestions.Controls.Add(this.metroLabel4, 1, 6);
            this.pnlQuestions.Controls.Add(this.metroLabel5, 1, 7);
            this.pnlQuestions.Controls.Add(this.metroLabel6, 1, 8);
            this.pnlQuestions.Controls.Add(this.metroLabel7, 1, 9);
            this.pnlQuestions.Controls.Add(this.metroLabel8, 1, 10);
            this.pnlQuestions.Controls.Add(this.txtConfirmEmail, 2, 7);
            this.pnlQuestions.Controls.Add(this.txtEmail, 2, 6);
            this.pnlQuestions.Controls.Add(this.txtSurname, 2, 5);
            this.pnlQuestions.Controls.Add(this.txtHomeNo, 2, 8);
            this.pnlQuestions.Controls.Add(this.txtCellNo, 2, 9);
            this.pnlQuestions.Controls.Add(this.txtWorkNo, 2, 10);
            this.pnlQuestions.Controls.Add(this.metroLabel9, 1, 1);
            this.pnlQuestions.Controls.Add(this.metroLabel10, 1, 2);
            this.pnlQuestions.Controls.Add(this.txtIDNo, 2, 1);
            this.pnlQuestions.Controls.Add(this.txtDOB, 2, 2);
            this.pnlQuestions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlQuestions.Location = new System.Drawing.Point(4, 66);
            this.pnlQuestions.Margin = new System.Windows.Forms.Padding(4);
            this.pnlQuestions.Name = "pnlQuestions";
            this.pnlQuestions.RowCount = 12;
            this.pnlQuestions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnlQuestions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43F));
            this.pnlQuestions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43F));
            this.pnlQuestions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43F));
            this.pnlQuestions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43F));
            this.pnlQuestions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43F));
            this.pnlQuestions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43F));
            this.pnlQuestions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43F));
            this.pnlQuestions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43F));
            this.pnlQuestions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43F));
            this.pnlQuestions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43F));
            this.pnlQuestions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnlQuestions.Size = new System.Drawing.Size(1029, 481);
            this.pnlQuestions.TabIndex = 15;
            // 
            // txtDOB
            // 
            this.txtDOB.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtDOB.Icon = ((System.Drawing.Image)(resources.GetObject("txtDOB.Icon")));
            this.txtDOB.IconRight = true;
            this.txtDOB.Lines = new string[0];
            this.txtDOB.Location = new System.Drawing.Point(518, 72);
            this.txtDOB.Margin = new System.Windows.Forms.Padding(4);
            this.txtDOB.MaxLength = 32767;
            this.txtDOB.Name = "txtDOB";
            this.txtDOB.PasswordChar = '\0';
            this.txtDOB.PromptText = "yyyy/MM/dd";
            this.txtDOB.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtDOB.SelectedText = "";
            this.txtDOB.Size = new System.Drawing.Size(259, 35);
            this.txtDOB.TabIndex = 1;
            this.txtDOB.UseSelectable = true;
            // 
            // txtIDNo
            // 
            this.txtIDNo.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtIDNo.Icon = global::iRent.Properties.Resources.icon_star;
            this.txtIDNo.IconRight = true;
            this.txtIDNo.Lines = new string[0];
            this.txtIDNo.Location = new System.Drawing.Point(518, 29);
            this.txtIDNo.Margin = new System.Windows.Forms.Padding(4);
            this.txtIDNo.MaxLength = 32767;
            this.txtIDNo.Name = "txtIDNo";
            this.txtIDNo.PasswordChar = '\0';
            this.txtIDNo.ReadOnly = true;
            this.txtIDNo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtIDNo.SelectedText = "";
            this.txtIDNo.Size = new System.Drawing.Size(259, 35);
            this.txtIDNo.TabIndex = 0;
            this.txtIDNo.UseSelectable = true;
            // 
            // metroLabel10
            // 
            this.metroLabel10.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel10.FontSize = MetroFramework.MetroLabelSize.XL;
            this.metroLabel10.Location = new System.Drawing.Point(251, 68);
            this.metroLabel10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(259, 43);
            this.metroLabel10.TabIndex = 95;
            this.metroLabel10.Text = "Date of Birth:";
            this.metroLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroLabel9
            // 
            this.metroLabel9.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel9.FontSize = MetroFramework.MetroLabelSize.XL;
            this.metroLabel9.Location = new System.Drawing.Point(251, 25);
            this.metroLabel9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(259, 43);
            this.metroLabel9.TabIndex = 94;
            this.metroLabel9.Text = "ID Number:";
            this.metroLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtWorkNo
            // 
            this.txtWorkNo.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtWorkNo.Icon = global::iRent.Properties.Resources.icon_star;
            this.txtWorkNo.IconRight = true;
            this.txtWorkNo.Lines = new string[0];
            this.txtWorkNo.Location = new System.Drawing.Point(518, 416);
            this.txtWorkNo.Margin = new System.Windows.Forms.Padding(4);
            this.txtWorkNo.MaxLength = 32767;
            this.txtWorkNo.Name = "txtWorkNo";
            this.txtWorkNo.PasswordChar = '\0';
            this.txtWorkNo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtWorkNo.SelectedText = "";
            this.txtWorkNo.Size = new System.Drawing.Size(259, 35);
            this.txtWorkNo.TabIndex = 9;
            this.txtWorkNo.UseSelectable = true;
            // 
            // txtCellNo
            // 
            this.txtCellNo.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtCellNo.Icon = ((System.Drawing.Image)(resources.GetObject("txtCellNo.Icon")));
            this.txtCellNo.IconRight = true;
            this.txtCellNo.Lines = new string[0];
            this.txtCellNo.Location = new System.Drawing.Point(518, 373);
            this.txtCellNo.Margin = new System.Windows.Forms.Padding(4);
            this.txtCellNo.MaxLength = 32767;
            this.txtCellNo.Name = "txtCellNo";
            this.txtCellNo.PasswordChar = '\0';
            this.txtCellNo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCellNo.SelectedText = "";
            this.txtCellNo.Size = new System.Drawing.Size(259, 35);
            this.txtCellNo.TabIndex = 8;
            this.txtCellNo.UseSelectable = true;
            // 
            // txtHomeNo
            // 
            this.txtHomeNo.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtHomeNo.Lines = new string[0];
            this.txtHomeNo.Location = new System.Drawing.Point(518, 330);
            this.txtHomeNo.Margin = new System.Windows.Forms.Padding(4);
            this.txtHomeNo.MaxLength = 32767;
            this.txtHomeNo.Name = "txtHomeNo";
            this.txtHomeNo.PasswordChar = '\0';
            this.txtHomeNo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtHomeNo.SelectedText = "";
            this.txtHomeNo.Size = new System.Drawing.Size(259, 35);
            this.txtHomeNo.TabIndex = 7;
            this.txtHomeNo.UseSelectable = true;
            // 
            // txtSurname
            // 
            this.txtSurname.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtSurname.Icon = global::iRent.Properties.Resources.icon_star;
            this.txtSurname.IconRight = true;
            this.txtSurname.Lines = new string[0];
            this.txtSurname.Location = new System.Drawing.Point(518, 201);
            this.txtSurname.Margin = new System.Windows.Forms.Padding(4);
            this.txtSurname.MaxLength = 32767;
            this.txtSurname.Name = "txtSurname";
            this.txtSurname.PasswordChar = '\0';
            this.txtSurname.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtSurname.SelectedText = "";
            this.txtSurname.Size = new System.Drawing.Size(259, 35);
            this.txtSurname.TabIndex = 4;
            this.txtSurname.UseSelectable = true;
            // 
            // txtEmail
            // 
            this.txtEmail.BackColor = System.Drawing.Color.Red;
            this.txtEmail.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtEmail.Icon = ((System.Drawing.Image)(resources.GetObject("txtEmail.Icon")));
            this.txtEmail.IconRight = true;
            this.txtEmail.Lines = new string[0];
            this.txtEmail.Location = new System.Drawing.Point(518, 244);
            this.txtEmail.Margin = new System.Windows.Forms.Padding(4);
            this.txtEmail.MaxLength = 32767;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.PasswordChar = '\0';
            this.txtEmail.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtEmail.SelectedText = "";
            this.txtEmail.Size = new System.Drawing.Size(259, 35);
            this.txtEmail.TabIndex = 5;
            this.txtEmail.UseSelectable = true;
            // 
            // txtConfirmEmail
            // 
            this.txtConfirmEmail.BackColor = System.Drawing.Color.Red;
            this.txtConfirmEmail.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtConfirmEmail.Icon = ((System.Drawing.Image)(resources.GetObject("txtConfirmEmail.Icon")));
            this.txtConfirmEmail.IconRight = true;
            this.txtConfirmEmail.Lines = new string[0];
            this.txtConfirmEmail.Location = new System.Drawing.Point(518, 287);
            this.txtConfirmEmail.Margin = new System.Windows.Forms.Padding(4);
            this.txtConfirmEmail.MaxLength = 32767;
            this.txtConfirmEmail.Name = "txtConfirmEmail";
            this.txtConfirmEmail.PasswordChar = '\0';
            this.txtConfirmEmail.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtConfirmEmail.SelectedText = "";
            this.txtConfirmEmail.Size = new System.Drawing.Size(259, 35);
            this.txtConfirmEmail.TabIndex = 6;
            this.txtConfirmEmail.UseSelectable = true;
            // 
            // metroLabel8
            // 
            this.metroLabel8.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel8.FontSize = MetroFramework.MetroLabelSize.XL;
            this.metroLabel8.Location = new System.Drawing.Point(251, 412);
            this.metroLabel8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(259, 43);
            this.metroLabel8.TabIndex = 87;
            this.metroLabel8.Text = "Work Tel:";
            this.metroLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroLabel7
            // 
            this.metroLabel7.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel7.FontSize = MetroFramework.MetroLabelSize.XL;
            this.metroLabel7.Location = new System.Drawing.Point(251, 369);
            this.metroLabel7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(259, 43);
            this.metroLabel7.TabIndex = 86;
            this.metroLabel7.Text = "Cell No:";
            this.metroLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroLabel6
            // 
            this.metroLabel6.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel6.FontSize = MetroFramework.MetroLabelSize.XL;
            this.metroLabel6.Location = new System.Drawing.Point(251, 326);
            this.metroLabel6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(259, 43);
            this.metroLabel6.TabIndex = 85;
            this.metroLabel6.Text = "Home Tel:";
            this.metroLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroLabel5
            // 
            this.metroLabel5.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel5.FontSize = MetroFramework.MetroLabelSize.XL;
            this.metroLabel5.Location = new System.Drawing.Point(251, 283);
            this.metroLabel5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(259, 43);
            this.metroLabel5.TabIndex = 84;
            this.metroLabel5.Text = "Confirm Email:";
            this.metroLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroLabel4
            // 
            this.metroLabel4.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel4.FontSize = MetroFramework.MetroLabelSize.XL;
            this.metroLabel4.Location = new System.Drawing.Point(251, 240);
            this.metroLabel4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(259, 43);
            this.metroLabel4.TabIndex = 83;
            this.metroLabel4.Text = "Email:";
            this.metroLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroLabel3
            // 
            this.metroLabel3.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel3.FontSize = MetroFramework.MetroLabelSize.XL;
            this.metroLabel3.Location = new System.Drawing.Point(251, 197);
            this.metroLabel3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(259, 43);
            this.metroLabel3.TabIndex = 82;
            this.metroLabel3.Text = "Last Name:";
            this.metroLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFirstName
            // 
            this.txtFirstName.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtFirstName.Icon = global::iRent.Properties.Resources.icon_star;
            this.txtFirstName.IconRight = true;
            this.txtFirstName.Lines = new string[0];
            this.txtFirstName.Location = new System.Drawing.Point(518, 158);
            this.txtFirstName.Margin = new System.Windows.Forms.Padding(4);
            this.txtFirstName.MaxLength = 32767;
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.PasswordChar = '\0';
            this.txtFirstName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtFirstName.SelectedText = "";
            this.txtFirstName.Size = new System.Drawing.Size(259, 35);
            this.txtFirstName.TabIndex = 3;
            this.txtFirstName.UseSelectable = true;
            // 
            // metroLabel1
            // 
            this.metroLabel1.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.XL;
            this.metroLabel1.Location = new System.Drawing.Point(251, 154);
            this.metroLabel1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(259, 43);
            this.metroLabel1.TabIndex = 80;
            this.metroLabel1.Text = "First Name:";
            this.metroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroLabel2
            // 
            this.metroLabel2.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel2.FontSize = MetroFramework.MetroLabelSize.XL;
            this.metroLabel2.Location = new System.Drawing.Point(251, 111);
            this.metroLabel2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(259, 43);
            this.metroLabel2.TabIndex = 79;
            this.metroLabel2.Text = "Title:";
            this.metroLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbTitle
            // 
            this.cmbTitle.BackColor = System.Drawing.SystemColors.Control;
            this.cmbTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbTitle.FontSize = MetroFramework.MetroComboBoxSize.Tall;
            this.cmbTitle.ItemHeight = 29;
            this.cmbTitle.Items.AddRange(new object[] {
            "Mr",
            "Mrs",
            "Miss",
            "Ms",
            "Dr"});
            this.cmbTitle.Location = new System.Drawing.Point(518, 115);
            this.cmbTitle.Margin = new System.Windows.Forms.Padding(4);
            this.cmbTitle.Name = "cmbTitle";
            this.cmbTitle.Size = new System.Drawing.Size(259, 35);
            this.cmbTitle.TabIndex = 2;
            this.cmbTitle.UseCustomBackColor = true;
            this.cmbTitle.UseCustomForeColor = true;
            this.cmbTitle.UseSelectable = true;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.tableLayoutPanel.SetColumnSpan(this.lblStatus, 3);
            this.lblStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblStatus.Location = new System.Drawing.Point(4, 551);
            this.lblStatus.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(1029, 62);
            this.lblStatus.Style = MetroFramework.MetroColorStyle.Red;
            this.lblStatus.TabIndex = 14;
            // 
            // btnNext
            // 
            this.btnNext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNext.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnNext.Location = new System.Drawing.Point(389, 617);
            this.btnNext.Margin = new System.Windows.Forms.Padding(4);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(259, 115);
            this.btnNext.TabIndex = 4;
            this.btnNext.Text = "&Next";
            this.btnNext.UseSelectable = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // lblHeader
            // 
            this.lblHeader.AutoSize = true;
            this.tableLayoutPanel.SetColumnSpan(this.lblHeader, 3);
            this.lblHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblHeader.FontSize = MetroFramework.MetroLabelSize.XL;
            this.lblHeader.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblHeader.Location = new System.Drawing.Point(4, 0);
            this.lblHeader.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(1029, 62);
            this.lblHeader.TabIndex = 12;
            this.lblHeader.Text = "Personal Information && Contact Details";
            this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 3;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 267F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.Controls.Add(this.lblHeader, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.btnNext, 1, 3);
            this.tableLayoutPanel.Controls.Add(this.lblStatus, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.pnlQuestions, 0, 1);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 4;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 123F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(1037, 736);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // ContactDetailsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.tableLayoutPanel);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "ContactDetailsControl";
            this.Size = new System.Drawing.Size(1037, 736);
            this.pnlQuestions.ResumeLayout(false);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel pnlQuestions;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private MetroLabel lblHeader;
        private MetroButton btnNext;
        private MetroLabel lblStatus;
        private MetroComboBox cmbTitle;
        private MetroLabel metroLabel2;
        private MetroLabel metroLabel1;
        private MetroTextBox txtFirstName;
        private MetroLabel metroLabel3;
        private MetroLabel metroLabel4;
        private MetroLabel metroLabel5;
        private MetroLabel metroLabel6;
        private MetroLabel metroLabel7;
        private MetroLabel metroLabel8;
        private MetroTextBox txtConfirmEmail;
        private MetroTextBox txtEmail;
        private MetroTextBox txtSurname;
        private MetroTextBox txtHomeNo;
        private MetroTextBox txtCellNo;
        private MetroTextBox txtWorkNo;
        private MetroLabel metroLabel9;
        private MetroLabel metroLabel10;
        private MetroTextBox txtIDNo;
        private MetroTextBox txtDOB;
    }
}
