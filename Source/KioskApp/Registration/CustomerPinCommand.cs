﻿using System;
using System.Collections.Generic;
using CanTech.Common.Extensions;
using Castle.Core;
using iRent.Framework;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;
using iRent2.Contracts.Drivers;

namespace iRent.Registration
{
    [CastleComponent]
    public class CustomerPinCommand : BaseCommand
    {
        private readonly DriverApi _driverApi;
        private readonly Func<DriverQuestionsControl> _factory;

        public CustomerPinCommand(Func<DriverQuestionsControl> factory, DriverApi driverApi)
        {
            _factory = factory;
            _driverApi = driverApi;
        }

        public string Next { get; set; }

        public override void Invoke()
        {
            var driver = ContextData.Driver;

            //if (driver["CustomerPin"].HasValue())
            //{
            //    Proceed(Next);
            //    return;
            //}

            var control = _factory();
            GetFormAs<MainForm>().AddToFormPanel(control);

            //driver["CustomerPin"] = "";
            //driver["SnapdriveCardNo"] = "";

            control.Next += (o, e) =>
            {

                if (control.GetValue("CustomerPin") != control.GetValue("ConfirmPin"))
                {
                    control.Error("password does not match");
                    return;
                }

                //driver["CustomerPin"] = control.GetValue("CustomerPin");
                //driver["SnapdriveCardNo"] = control.GetValue("SnapdriveCardNo");

                ChangePasswordRequest chng = new ChangePasswordRequest();
                chng.DriverId = driver.Id;
                Dictionary<string, string> answers = new Dictionary<string, string>();
                answers.Add("NickName", control.GetValue("NickName"));
                answers.Add("PetName", control.GetValue("PetName"));
                answers.Add("CarColour", control.GetValue("CarColour"));
                chng.Answers = answers;
                chng.NewPassword = control.GetValue("CustomerPin");

                _driverApi.SaveNewPassword(chng);

                //driver = _driverApi.Save(driver);
                //ContextData.SetDriver(driver);
                Proceed(Next);
            };

            control.Title = "CliQ Details";
            var questions = new List<DriverQuestion>();

            //if (driver["CustomerPin"].IsEmpty())
            //{
            //    //questions.Add(new DriverQuestion("CustomerPin", "Please enter your Customer Pin:", true));
            //    //questions.Add(new DriverQuestion("ConfirmPin", "Please confirm your Customer Pin:", true));
            //}
            questions.Add(new DriverQuestion("CustomerPin", "Please enter your Password:", true,10,false));
            questions.Add(new DriverQuestion("ConfirmPin", "Please confirm your Password:", true, 10, false));

            questions.Add(new DriverQuestion("NickName", "What was your nickname at school:",false, 50, false));
            questions.Add(new DriverQuestion("PetName", "What was the name of your first pet:", false, 50, false));
            questions.Add(new DriverQuestion("CarColour", "What was the color of your first car:", false, 50, false));

            control.Bind(questions, driver);
        }
    }
}