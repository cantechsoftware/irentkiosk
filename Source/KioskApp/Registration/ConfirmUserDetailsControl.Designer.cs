﻿using MetroFramework.Controls;

namespace iRent.Registration
{
    partial class ConfirmUserDetailsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfirmUserDetailsControl));
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnBack = new MetroFramework.Controls.MetroButton();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.txtSurname = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.btnSameAsPhysical = new MetroFramework.Controls.MetroButton();
            this.lblHeader2 = new MetroFramework.Controls.MetroLabel();
            this.txtWorkNo = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.txtDOB = new MetroFramework.Controls.MetroTextBox();
            this.txtIDNo = new MetroFramework.Controls.MetroTextBox();
            this.txtFirstName = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.txtCellNo = new MetroFramework.Controls.MetroTextBox();
            this.txtEmail = new MetroFramework.Controls.MetroTextBox();
            this.cmbTitle = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.lblStatus = new MetroFramework.Controls.MetroLabel();
            this.btnNext = new MetroFramework.Controls.MetroButton();
            this.postalAddress = new iRent.Registration.AddressControl();
            this.physicalAddress = new iRent.Registration.AddressControl();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.metroPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 53.84615F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 267F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 46.15385F));
            this.tableLayoutPanel1.Controls.Add(this.btnBack, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.metroPanel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.metroLabel3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblStatus, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnNext, 2, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 123F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1060, 773);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // btnBack
            // 
            this.btnBack.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnBack.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnBack.Location = new System.Drawing.Point(4, 654);
            this.btnBack.Margin = new System.Windows.Forms.Padding(4);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(418, 115);
            this.btnBack.TabIndex = 39;
            this.btnBack.Text = "Back";
            this.btnBack.UseSelectable = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // metroPanel1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.metroPanel1, 3);
            this.metroPanel1.Controls.Add(this.txtSurname);
            this.metroPanel1.Controls.Add(this.postalAddress);
            this.metroPanel1.Controls.Add(this.physicalAddress);
            this.metroPanel1.Controls.Add(this.metroLabel4);
            this.metroPanel1.Controls.Add(this.btnSameAsPhysical);
            this.metroPanel1.Controls.Add(this.lblHeader2);
            this.metroPanel1.Controls.Add(this.txtWorkNo);
            this.metroPanel1.Controls.Add(this.metroLabel2);
            this.metroPanel1.Controls.Add(this.txtDOB);
            this.metroPanel1.Controls.Add(this.txtIDNo);
            this.metroPanel1.Controls.Add(this.txtFirstName);
            this.metroPanel1.Controls.Add(this.metroLabel1);
            this.metroPanel1.Controls.Add(this.txtCellNo);
            this.metroPanel1.Controls.Add(this.txtEmail);
            this.metroPanel1.Controls.Add(this.cmbTitle);
            this.metroPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 12;
            this.metroPanel1.Location = new System.Drawing.Point(4, 4);
            this.metroPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(1052, 555);
            this.metroPanel1.TabIndex = 0;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 13;
            // 
            // txtSurname
            // 
            this.txtSurname.BackColor = System.Drawing.Color.Red;
            this.txtSurname.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtSurname.Icon = global::iRent.Properties.Resources.icon_star;
            this.txtSurname.IconRight = true;
            this.txtSurname.Lines = new string[0];
            this.txtSurname.Location = new System.Drawing.Point(716, 78);
            this.txtSurname.Margin = new System.Windows.Forms.Padding(4);
            this.txtSurname.MaxLength = 32767;
            this.txtSurname.Name = "txtSurname";
            this.txtSurname.PasswordChar = '\0';
            this.txtSurname.PromptText = "Surname";
            this.txtSurname.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtSurname.SelectedText = "";
            this.txtSurname.Size = new System.Drawing.Size(327, 37);
            this.txtSurname.TabIndex = 3;
            this.txtSurname.UseSelectable = true;
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel4.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel4.Location = new System.Drawing.Point(8, 374);
            this.metroLabel4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(134, 25);
            this.metroLabel4.TabIndex = 51;
            this.metroLabel4.Text = "Postal Address";
            this.metroLabel4.Visible = false;
            // 
            // btnSameAsPhysical
            // 
            this.btnSameAsPhysical.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btnSameAsPhysical.Location = new System.Drawing.Point(188, 374);
            this.btnSameAsPhysical.Margin = new System.Windows.Forms.Padding(4);
            this.btnSameAsPhysical.Name = "btnSameAsPhysical";
            this.btnSameAsPhysical.Size = new System.Drawing.Size(195, 41);
            this.btnSameAsPhysical.TabIndex = 11;
            this.btnSameAsPhysical.Text = "Same as Physical";
            this.btnSameAsPhysical.UseSelectable = true;
            this.btnSameAsPhysical.Visible = false;
            this.btnSameAsPhysical.Click += new System.EventHandler(this.btnSameAsPhysical_Click);
            // 
            // lblHeader2
            // 
            this.lblHeader2.AutoSize = true;
            this.lblHeader2.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblHeader2.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lblHeader2.Location = new System.Drawing.Point(8, 228);
            this.lblHeader2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHeader2.Name = "lblHeader2";
            this.lblHeader2.Size = new System.Drawing.Size(79, 25);
            this.lblHeader2.TabIndex = 50;
            this.lblHeader2.Text = "Address";
            // 
            // txtWorkNo
            // 
            this.txtWorkNo.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtWorkNo.Icon = ((System.Drawing.Image)(resources.GetObject("txtWorkNo.Icon")));
            this.txtWorkNo.IconRight = true;
            this.txtWorkNo.Lines = new string[0];
            this.txtWorkNo.Location = new System.Drawing.Point(716, 187);
            this.txtWorkNo.Margin = new System.Windows.Forms.Padding(4);
            this.txtWorkNo.MaxLength = 32767;
            this.txtWorkNo.Name = "txtWorkNo";
            this.txtWorkNo.PasswordChar = '\0';
            this.txtWorkNo.PromptText = "Work Number";
            this.txtWorkNo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtWorkNo.SelectedText = "";
            this.txtWorkNo.Size = new System.Drawing.Size(327, 37);
            this.txtWorkNo.TabIndex = 8;
            this.txtWorkNo.UseSelectable = true;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.FontSize = MetroFramework.MetroLabelSize.XL;
            this.metroLabel2.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel2.Location = new System.Drawing.Point(265, 15);
            this.metroLabel2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(310, 31);
            this.metroLabel2.TabIndex = 34;
            this.metroLabel2.Text = "Please Confirm your Details";
            // 
            // txtDOB
            // 
            this.txtDOB.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtDOB.Icon = ((System.Drawing.Image)(resources.GetObject("txtDOB.Icon")));
            this.txtDOB.IconRight = true;
            this.txtDOB.Lines = new string[0];
            this.txtDOB.Location = new System.Drawing.Point(357, 135);
            this.txtDOB.Margin = new System.Windows.Forms.Padding(4);
            this.txtDOB.MaxLength = 32767;
            this.txtDOB.Name = "txtDOB";
            this.txtDOB.PasswordChar = '\0';
            this.txtDOB.PromptText = "Date of Birth";
            this.txtDOB.ReadOnly = true;
            this.txtDOB.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtDOB.SelectedText = "";
            this.txtDOB.Size = new System.Drawing.Size(327, 37);
            this.txtDOB.TabIndex = 5;
            this.txtDOB.UseSelectable = true;
            // 
            // txtIDNo
            // 
            this.txtIDNo.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtIDNo.Icon = global::iRent.Properties.Resources.icon_star;
            this.txtIDNo.IconRight = true;
            this.txtIDNo.Lines = new string[0];
            this.txtIDNo.Location = new System.Drawing.Point(4, 135);
            this.txtIDNo.Margin = new System.Windows.Forms.Padding(4);
            this.txtIDNo.MaxLength = 32767;
            this.txtIDNo.Name = "txtIDNo";
            this.txtIDNo.PasswordChar = '\0';
            this.txtIDNo.PromptText = "ID Number";
            this.txtIDNo.ReadOnly = true;
            this.txtIDNo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtIDNo.SelectedText = "";
            this.txtIDNo.Size = new System.Drawing.Size(327, 37);
            this.txtIDNo.TabIndex = 4;
            this.txtIDNo.UseSelectable = true;
            // 
            // txtFirstName
            // 
            this.txtFirstName.BackColor = System.Drawing.Color.Red;
            this.txtFirstName.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtFirstName.Icon = global::iRent.Properties.Resources.icon_star;
            this.txtFirstName.IconRight = true;
            this.txtFirstName.Lines = new string[0];
            this.txtFirstName.Location = new System.Drawing.Point(357, 78);
            this.txtFirstName.Margin = new System.Windows.Forms.Padding(4);
            this.txtFirstName.MaxLength = 32767;
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.PasswordChar = '\0';
            this.txtFirstName.PromptText = "First Name";
            this.txtFirstName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtFirstName.SelectedText = "";
            this.txtFirstName.Size = new System.Drawing.Size(327, 37);
            this.txtFirstName.TabIndex = 2;
            this.txtFirstName.UseSelectable = true;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel1.Location = new System.Drawing.Point(8, 43);
            this.metroLabel1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(146, 25);
            this.metroLabel1.TabIndex = 30;
            this.metroLabel1.Text = "Personal Details";
            // 
            // txtCellNo
            // 
            this.txtCellNo.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtCellNo.Icon = ((System.Drawing.Image)(resources.GetObject("txtCellNo.Icon")));
            this.txtCellNo.IconRight = true;
            this.txtCellNo.Lines = new string[0];
            this.txtCellNo.Location = new System.Drawing.Point(357, 187);
            this.txtCellNo.Margin = new System.Windows.Forms.Padding(4);
            this.txtCellNo.MaxLength = 32767;
            this.txtCellNo.Name = "txtCellNo";
            this.txtCellNo.PasswordChar = '\0';
            this.txtCellNo.PromptText = "Cell Number";
            this.txtCellNo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCellNo.SelectedText = "";
            this.txtCellNo.Size = new System.Drawing.Size(327, 37);
            this.txtCellNo.TabIndex = 7;
            this.txtCellNo.UseSelectable = true;
            // 
            // txtEmail
            // 
            this.txtEmail.BackColor = System.Drawing.Color.Red;
            this.txtEmail.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtEmail.Icon = ((System.Drawing.Image)(resources.GetObject("txtEmail.Icon")));
            this.txtEmail.IconRight = true;
            this.txtEmail.Lines = new string[0];
            this.txtEmail.Location = new System.Drawing.Point(5, 187);
            this.txtEmail.Margin = new System.Windows.Forms.Padding(4);
            this.txtEmail.MaxLength = 32767;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.PasswordChar = '\0';
            this.txtEmail.PromptText = "Email";
            this.txtEmail.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtEmail.SelectedText = "";
            this.txtEmail.Size = new System.Drawing.Size(327, 37);
            this.txtEmail.TabIndex = 6;
            this.txtEmail.UseSelectable = true;
            // 
            // cmbTitle
            // 
            this.cmbTitle.BackColor = System.Drawing.SystemColors.Control;
            this.cmbTitle.FontSize = MetroFramework.MetroComboBoxSize.Tall;
            this.cmbTitle.ItemHeight = 29;
            this.cmbTitle.Items.AddRange(new object[] {
            "Mr",
            "Mrs",
            "Miss",
            "Ms",
            "Dr"});
            this.cmbTitle.Location = new System.Drawing.Point(4, 78);
            this.cmbTitle.Margin = new System.Windows.Forms.Padding(4);
            this.cmbTitle.Name = "cmbTitle";
            this.cmbTitle.PromptText = "Title";
            this.cmbTitle.Size = new System.Drawing.Size(325, 35);
            this.cmbTitle.Style = MetroFramework.MetroColorStyle.Blue;
            this.cmbTitle.TabIndex = 1;
            this.cmbTitle.UseCustomBackColor = true;
            this.cmbTitle.UseSelectable = true;
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.metroLabel3, 3);
            this.metroLabel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel3.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel3.Location = new System.Drawing.Point(4, 625);
            this.metroLabel3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(1052, 25);
            this.metroLabel3.TabIndex = 35;
            this.metroLabel3.Text = "I hereby accept that by clicking \"Next\" that all the information as per above is " +
    "correct.\r\n";
            this.metroLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.lblStatus, 3);
            this.lblStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblStatus.FontSize = MetroFramework.MetroLabelSize.XL;
            this.lblStatus.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblStatus.Location = new System.Drawing.Point(4, 563);
            this.lblStatus.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(1052, 62);
            this.lblStatus.Style = MetroFramework.MetroColorStyle.Red;
            this.lblStatus.TabIndex = 36;
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnNext
            // 
            this.btnNext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNext.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnNext.Location = new System.Drawing.Point(696, 653);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(361, 117);
            this.btnNext.TabIndex = 41;
            this.btnNext.Text = "Continue";
            this.btnNext.UseSelectable = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // postalAddress
            // 
            this.postalAddress.AutoScroll = true;
            this.postalAddress.AutoSize = true;
            this.postalAddress.Location = new System.Drawing.Point(-4, 422);
            this.postalAddress.Margin = new System.Windows.Forms.Padding(5);
            this.postalAddress.Name = "postalAddress";
            this.postalAddress.Size = new System.Drawing.Size(1395, 124);
            this.postalAddress.TabIndex = 10;
            this.postalAddress.UseSelectable = true;
            this.postalAddress.Visible = false;
            // 
            // physicalAddress
            // 
            this.physicalAddress.AutoScroll = true;
            this.physicalAddress.AutoSize = true;
            this.physicalAddress.Location = new System.Drawing.Point(-4, 262);
            this.physicalAddress.Margin = new System.Windows.Forms.Padding(5);
            this.physicalAddress.Name = "physicalAddress";
            this.physicalAddress.Size = new System.Drawing.Size(1395, 124);
            this.physicalAddress.TabIndex = 9;
            this.physicalAddress.UseSelectable = true;
            this.physicalAddress.Load += new System.EventHandler(this.physicalAddress_Load);
            // 
            // ConfirmUserDetailsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ConfirmUserDetailsControl";
            this.Size = new System.Drawing.Size(1060, 773);
            this.Load += new System.EventHandler(this.UserDetails_Load);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox txtDOB;
        private MetroFramework.Controls.MetroTextBox txtIDNo;
        private MetroFramework.Controls.MetroTextBox txtFirstName;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTextBox txtCellNo;
        private MetroFramework.Controls.MetroTextBox txtEmail;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel lblStatus;
        private MetroComboBox cmbTitle;
        private MetroTextBox txtWorkNo;
        private AddressControl postalAddress;
        private AddressControl physicalAddress;
        private MetroLabel metroLabel4;
        private MetroButton btnSameAsPhysical;
        private MetroLabel lblHeader2;
        private MetroTextBox txtSurname;
        private MetroButton btnBack;
        private MetroButton btnNext;
    }
}
