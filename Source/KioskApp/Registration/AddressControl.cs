﻿using CanTech.Common.Extensions;
using iRent2.Contracts.Models;
using MetroFramework.Controls;

namespace iRent.Registration
{
    public partial class AddressControl : MetroUserControl
    {
        public AddressControl()
        {
            InitializeComponent();
        }

        public void Bind(AddressModel address)
        {
            if (address == null)
                return;

            if (address.POBox.HasValue())
            {
                txtUnitNo.Text = "";
                txtComplex.Text = "";
                txtStreet.Text = "P.O. Box";
                txtStreetNo.Text = address.POBox;
            }
            else
            {
                txtUnitNo.Text = address.HouseNo;
                txtComplex.Text = address.Complex;
                txtStreet.Text = address.StreetName;
                txtStreetNo.Text = address.StreetNo;
            }
            txtCountry.Text = address.Country;
            txtSuburb.Text = address.Suburb;
            txtTown.Text = address.City;
            txtProvince.Text = address.State;
            txtAreaCode.Text = address.PostCode;
        }

        public string GetError()
        {
            if (txtStreet.Text.Trim().Length == 0)
                return "Please enter a Street Number.";

            if (txtStreet.Text.Trim().Length == 0)
                return "Please enter a Street Name.";

            if (txtSuburb.Text.Trim().Length == 0)
                return "Please enter a Suburb.";

            if (txtTown.Text.Trim().Length == 0)
                return "Please enter a City.";

            if (txtProvince.Text.Trim().Length == 0)
                return "Please enter an Province/State.";

            if (txtAreaCode.Text.Trim().Length == 0)
                return "Please enter an Area Code.";

            if (txtCountry.Text.Trim().Length == 0)
                return "Please enter an Country.";

            return null;
        }

        public AddressModel Update(AddressModel address)
        {
            if (txtStreet.Text == "P.O. Box")
            {
                address.POBox = txtStreetNo.Text;
                address.StreetNo = null;
                address.StreetName = null;
                address.HouseNo = null;
                address.Complex = null;
            }
            else
            {
                address.POBox = null;
                address.StreetNo = txtStreetNo.Text;
                address.StreetName = txtStreet.Text;
                address.Complex = txtComplex.Text;
                address.HouseNo = txtUnitNo.Text;
            }
            address.Suburb = txtSuburb.Text;
            address.City = txtTown.Text;
            address.State = txtProvince.Text;
            address.PostCode = txtAreaCode.Text;
            address.Country = txtCountry.Text;

            return address;
        }

        public void ClearControls(string country)
        {
            txtStreetNo.Text = "";
            txtStreet.Text = "";
            txtSuburb.Text = "";
            txtTown.Text = "";
            txtProvince.Text = "";
            txtAreaCode.Text = "";
            txtComplex.Text = "";
            txtUnitNo.Text = "";

            if (string.IsNullOrEmpty(country))
                txtCountry.Text = "South Africa";
            else
                txtCountry.Text = country;
        }

        public void CopyTo(AddressControl dest)
        {
            dest.txtStreetNo.Text = txtStreetNo.Text;
            dest.txtStreet.Text = txtStreet.Text;
            dest.txtSuburb.Text = txtSuburb.Text;
            dest.txtTown.Text = txtTown.Text;
            dest.txtProvince.Text = txtProvince.Text;
            dest.txtAreaCode.Text = txtAreaCode.Text;
            dest.txtCountry.Text = txtCountry.Text;
            dest.txtComplex.Text = txtComplex.Text;
            dest.txtUnitNo.Text = txtUnitNo.Text;
        }
    }
}