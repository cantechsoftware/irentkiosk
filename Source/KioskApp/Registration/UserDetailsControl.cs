﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using CanTech.Common.Extensions;
using Castle.Core;
using iRent.Classes;
using iRent.Framework;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Services;
using libphonenumber;
using MetroFramework;
using MetroFramework.Controls;

namespace iRent.Registration
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class UserDetailsControl : MetroUserControl
    {
        private readonly CountryService _countryService;

        public bool isValid;

        public UserDetailsControl(CountryService countryService)
        {
            _countryService = countryService;
            InitializeComponent();
            UIFactory.CurrentControl = "UserDetails";
        }

        private string GetError()
        {
            if (((string) cmbTitle.SelectedItem).IsEmpty())
                return "Please select your Title";

            if (txtFirstName.Text.Trim().IsEmpty())
                return "Please enter your first name";

            if (txtEmail.Text.Trim().Length == 0)
                return "Please enter your Email Address";

            if (!ValidEmail(txtEmail.Text))
                return "Please enter a valid Email.";

            if (txtConfirmEmail.Text.Trim() != txtEmail.Text)
                return "Emails do not match";

            if (txtCellNo.Text.Trim().Length == 0)
                return "Please enter your Cell No";

            if (!CellnoValid(txtCellNo.Text))
                return "Please enter a valid Cell No";

            var err = physicalAddress.GetError() ?? postalAddress.GetError();
            if (err != null)
                return err;

            return null;
        }

        public bool ValidateForm()
        {
            var error = GetError();
            if (error != null)
            {
                lblStatus.Style = MetroColorStyle.Red;
                lblStatus.Text = "Please fix the following error: " + error;
                return false;
            }

            lblStatus.Text = "";
            return true;
        }

        public bool UpdateDriver(DriverModel driver)
        {
            lblStatus.Text = "";
            try
            {
                driver.PhysicalAddress = physicalAddress.Update(driver.PhysicalAddress ?? new AddressModel());
                driver.PostalAddress = postalAddress.Update(driver.PostalAddress ?? new AddressModel());
                driver.Email = txtEmail.Text;
                driver.CellNo = txtCellNo.Text;
                driver.HomeNo = txtTelNo.Text;
                driver["WorkNo"] = txtWorkNo.Text;
                driver.Title = (string) cmbTitle.SelectedItem;
                driver.FirstName = txtFirstName.Text;
                return true;
            }
            catch (Exception ex)
            {
                Logger.Log(this, "Validate", "Error in Validate", ex);
                Errors.Notify("Error Validating User Details", ex);
                return false;
            }
        }

        private void UserDetails_Load(object sender, EventArgs e)
        {
            ClearControls();
        }

        private void ClearControls()
        {
            physicalAddress.ClearControls(_countryService.GetDefaultCountry().Name);
            postalAddress.ClearControls(_countryService.GetDefaultCountry().Name);
            txtCellNo.Text = "";
            txtEmail.Text = "";
            txtTelNo.Text = "";
            txtWorkNo.Text = "";
            txtConfirmEmail.Text = "";
            cmbTitle.SelectedIndex = -1;
            txtFirstName.Text = "";
        }

        private bool ValidEmail(string email)
        {
            try
            {
                var m = new MailAddress(email);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        private bool CellnoValid(string cellno)
        {
            //string countryCode = cmbDialCountry.Text.ToString();
            //countryCode = countryCode.Substring(0, 2);
            var number = PhoneNumberUtil.Instance.Parse(cellno, _countryService.DefaultCode);
            if (number.IsValidNumber)
            {
                txtCellNo.Text = number.Format(PhoneNumberUtil.PhoneNumberFormat.E164);
                return true;
            }
            else
                return false;
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (!ValidateForm()) return;

            var driver = ContextData.Driver;

            if (!UpdateDriver(driver)) return;

            Next?.Invoke(this, driver);
        }

        public event EventHandler<DriverModel> Next;

        private void btnSameAsPhysical_Click(object sender, EventArgs e)
        {
            physicalAddress.CopyTo(postalAddress);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            foreach (var txt in Controls.OfType<MetroTextBox>())
                txt.GotFocus += (o, e2) => txt.SelectAll();
        }
    }
}