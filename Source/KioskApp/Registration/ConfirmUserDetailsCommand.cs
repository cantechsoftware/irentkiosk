﻿using System;
using System.Windows.Forms;
using Castle.Core;
using iRent.Framework;

namespace iRent.Registration
{
    [CastleComponent]
    public class ConfirmUserDetailsCommand : BaseCommand
    {
        private readonly Func<ConfirmUserDetailsControl> _factory;

        public ConfirmUserDetailsCommand(Func<ConfirmUserDetailsControl> factory)
        {
            _factory = factory;
        }

        public string Next { get; set; }
        public string Back { get; set; }

        public override void Invoke()
        {
            var control = _factory();
            control.Dock = DockStyle.Fill;
            var main = GetFormAs<MainForm>();
            main.AddToFormPanel(control);
            main.ToggleHomeButton(true);
            control.Next += (o, e) => Proceed(Next);
            control.Back += (o, e) => Proceed(Back);
        }
    }
}