﻿using System;
using System.Windows.Forms;
using CanTech.Common.Extensions;
using Castle.Core;
using iRent.Framework;
using iRent2.Contracts.Enums;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;

namespace iRent.Registration
{
    [CastleComponent]
    public class CaptureAddressCommand : BaseCommand
    {
        private readonly Func<CaptureAddressControl> _factory;
        private readonly DriverApi _driverApi;
        private CaptureAddressControl _control;

        public CaptureAddressCommand(Func<CaptureAddressControl> factory, DriverApi driverApi)
        {
            _factory = factory;
            _driverApi = driverApi;
        }

        public string Next { get; set; }        
        public AddressType AddressType { get; set; }
        public string Title { get; set; }

        public override void Invoke()
        {
            _control = _factory();
            _control.Dock = DockStyle.Fill;
            GetFormAs<MainForm>().AddToFormPanel(_control);
            if (Title.IsEmpty())
                Title = AddressType == AddressType.Physical ? "Physical Address" : "Postal Address";
            _control.Title = Title;
            _control.Bind(ContextData.Driver, AddressType);
            _control.Next += OnNext;
        }

        private void OnNext(object sender, EventArgs ea)
        {
            var driver = ContextData.Driver;
            if (AddressType == AddressType.Physical)
                driver.PhysicalAddress = _control.Update(driver.PhysicalAddress);
            else
                driver.PostalAddress = _control.Update(driver.PostalAddress);
            driver = _driverApi.Save(driver);
            ContextData.SetDriver(driver);
            Proceed(Next);
        }
    }
}