﻿using System;
using System.Drawing;
using System.Net.Mail;
using System.Text.RegularExpressions;
using CanTech.Common.Extensions;
using Castle.Core;
using iRent.Classes;
using iRent.Framework;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;
using MetroFramework.Controls;

namespace iRent.Registration
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class ConfirmUserDetailsControl : MetroUserControl
    {
        private readonly CountryService _countryService;
        private readonly DriverApi _driverApi;
        public bool isValid;
        private readonly bool sameasphysical = false;

        public ConfirmUserDetailsControl(DriverApi driverApi, CountryService countryService)
        {
            _driverApi = driverApi;
            _countryService = countryService;
            InitializeComponent();
            UIFactory.CurrentControl = "ConfirmUserDetails";
        }

        private void UserDetails_Load(object sender, EventArgs e)
        {
            ClearControls();
            LoadUserDetails();
        }

        private void LoadUserDetails()
        {
            var driver = ContextData.Driver;

            try
            {
                txtFirstName.Text = driver.FirstName;
                txtSurname.Text = driver.Surname;
                txtIDNo.Text = driver.IdNumber;
                txtDOB.Text = string.Format("{0:yyyy/MM/dd}", driver.DateOfBirth);
                txtEmail.Text = driver.Email;
                txtCellNo.Text = driver.CellNo;
                physicalAddress.Bind(driver.PostalAddress);
                //postalAddress.Bind(driver.PostalAddress);
                cmbTitle.SelectedItem = driver.Title;
                txtWorkNo.Text = driver["WorkNo"];
            }
            catch (Exception ex)
            {
                Logger.Log(this, "LoadUserDetails", "Error Loading User Details", ex);
            }
        }

        private string GetError()
        {
            if (((string)cmbTitle.SelectedItem).IsEmpty())
                return "Please select your Title";
            
            if (txtFirstName.Text.Trim().IsEmpty())
                return "Please enter your first name";

            if (txtSurname.Text.Trim().IsEmpty())
                return "Please enter your surname";

            if (txtEmail.Text.Trim().Length == 0)
                return "Please enter your Email Address";

            if (!ValidEmail(txtEmail.Text))
                return "Please enter a valid Email.";

            if (txtCellNo.Text.Trim().Length == 0)
                return "Please enter your Cell No";

            if (!CellnoValid(txtCellNo.Text))
                return "Please enter a valid Cell No";

            if (txtWorkNo.Text.Trim().IsEmpty())
                return "Please enter your Work No";

            if (!CellnoValid(txtWorkNo.Text))
                return "Please enter a valid Work No";

            if (txtDOB.Text.Trim().IsEmpty())
                return "Please enter your date of birth";

            DateTime dt = DateTime.MinValue;
            if (!(txtDOB.Text == null || DateTime.TryParse(txtDOB.Text,out dt)))
                return "Please enter a valid date of birth";

            //var err = physicalAddress.GetError() ?? postalAddress.GetError();
            var err = physicalAddress.GetError();
            if (err != null)
                return err;

            return null;

        }
        public bool Validate()
        {
            var err = GetError();
            if (err != null)
            {
                lblStatus.Text = "Please fix the following error: " + err;
                lblStatus.ForeColor = Color.Red;
                return false;
            }
            return true;
        }

        public bool Update(DriverModel driver)
        {
            try
            {
                driver.Title = (string)cmbTitle.SelectedItem;
                driver.FirstName = txtFirstName.Text;
                driver.Surname = txtSurname.Text;
                driver.IdNumber = txtIDNo.Text;
                driver.DateOfBirth = txtDOB.Text != null ? DateTime.Parse(txtDOB.Text) : DateTime.MinValue;
                    
                if (driver.DateOfBirth == DateTime.MinValue)
                {
                    driver.DateOfBirth = null;
                }

                driver.Email = txtEmail.Text;
                driver.CellNo = txtCellNo.Text;
                driver["WorkNo"] = txtWorkNo.Text;
                //driver.PhysicalAddress = physicalAddress.Update(driver.PhysicalAddress ?? new AddressModel());
                driver.PostalAddress = physicalAddress.Update(driver.PostalAddress ?? new AddressModel());
                _driverApi.Save(driver);
            }
            catch
            {
                return false;
            }
            return true;
        }

        private void ClearControls()
        {
            cmbTitle.SelectedIndex = -1;
            txtFirstName.Text = "";
            txtSurname.Text = "";
            txtDOB.Text = "";
            txtCellNo.Text = "";
            txtEmail.Text = "";
            txtWorkNo.Text = "";
            physicalAddress.ClearControls(_countryService.GetDefaultCountry().Name);
            postalAddress.ClearControls(_countryService.GetDefaultCountry().Name);
        }

        private bool ValidEmail(string email)
        {
            try
            {
                var m = new MailAddress(email);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        private bool CellnoValid(string cellno)
        {
            return Regex.IsMatch(cellno, @"^(0|\+?27)\d{9}$");            
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (Validate() && Update(ContextData.Driver))
                Next?.Invoke(this, e);
        }

        public event EventHandler Next;
        public event EventHandler Back;

        private void btnSameAsPhysical_Click(object sender, EventArgs e)
        {
            physicalAddress.CopyTo(postalAddress);
        }

        private void physicalAddress_Load(object sender, EventArgs e)
        {

        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            Back?.Invoke(this, e);
        }
    }
}