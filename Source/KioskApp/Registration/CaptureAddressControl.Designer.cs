﻿using MetroFramework.Controls;

namespace iRent.Registration
{
    partial class CaptureAddressControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CaptureAddressControl));
            this.pnlQuestions = new System.Windows.Forms.TableLayoutPanel();
            this.lblStreetNo = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.rdbPOBox = new MetroFramework.Controls.MetroRadioButton();
            this.rdbStreet = new MetroFramework.Controls.MetroRadioButton();
            this.txtUnitNo = new MetroFramework.Controls.MetroTextBox();
            this.txtComplex = new MetroFramework.Controls.MetroTextBox();
            this.txtStreetNo = new MetroFramework.Controls.MetroTextBox();
            this.txtStreet = new MetroFramework.Controls.MetroTextBox();
            this.txtSuburb = new MetroFramework.Controls.MetroTextBox();
            this.txtAreaCode = new MetroFramework.Controls.MetroTextBox();
            this.txtTown = new MetroFramework.Controls.MetroTextBox();
            this.txtProvince = new MetroFramework.Controls.MetroTextBox();
            this.txtCountry = new MetroFramework.Controls.MetroTextBox();
            this.metroPanel2 = new MetroFramework.Controls.MetroPanel();
            this.btnSameAsPhysical = new MetroFramework.Controls.MetroButton();
            this.lblStatus = new MetroFramework.Controls.MetroLabel();
            this.btnNext = new MetroFramework.Controls.MetroButton();
            this.lblHeader = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.pnlQuestions.SuspendLayout();
            this.metroPanel1.SuspendLayout();
            this.metroPanel2.SuspendLayout();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlQuestions
            // 
            this.pnlQuestions.ColumnCount = 4;
            this.tableLayoutPanel.SetColumnSpan(this.pnlQuestions, 3);
            this.pnlQuestions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnlQuestions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.pnlQuestions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.pnlQuestions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnlQuestions.Controls.Add(this.lblStreetNo, 1, 5);
            this.pnlQuestions.Controls.Add(this.metroLabel1, 1, 6);
            this.pnlQuestions.Controls.Add(this.metroLabel3, 1, 7);
            this.pnlQuestions.Controls.Add(this.metroLabel4, 1, 8);
            this.pnlQuestions.Controls.Add(this.metroLabel5, 1, 9);
            this.pnlQuestions.Controls.Add(this.metroLabel6, 1, 10);
            this.pnlQuestions.Controls.Add(this.metroLabel7, 1, 11);
            this.pnlQuestions.Controls.Add(this.metroLabel9, 1, 3);
            this.pnlQuestions.Controls.Add(this.metroLabel10, 1, 4);
            this.pnlQuestions.Controls.Add(this.metroPanel1, 1, 2);
            this.pnlQuestions.Controls.Add(this.txtUnitNo, 2, 3);
            this.pnlQuestions.Controls.Add(this.txtComplex, 2, 4);
            this.pnlQuestions.Controls.Add(this.txtStreetNo, 2, 5);
            this.pnlQuestions.Controls.Add(this.txtStreet, 2, 6);
            this.pnlQuestions.Controls.Add(this.txtSuburb, 2, 7);
            this.pnlQuestions.Controls.Add(this.txtAreaCode, 2, 8);
            this.pnlQuestions.Controls.Add(this.txtTown, 2, 9);
            this.pnlQuestions.Controls.Add(this.txtProvince, 2, 10);
            this.pnlQuestions.Controls.Add(this.txtCountry, 2, 11);
            this.pnlQuestions.Controls.Add(this.metroPanel2, 1, 1);
            this.pnlQuestions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlQuestions.Location = new System.Drawing.Point(3, 53);
            this.pnlQuestions.Name = "pnlQuestions";
            this.pnlQuestions.RowCount = 13;
            this.pnlQuestions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnlQuestions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.pnlQuestions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.pnlQuestions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.pnlQuestions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.pnlQuestions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.pnlQuestions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.pnlQuestions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.pnlQuestions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.pnlQuestions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.pnlQuestions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.pnlQuestions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.pnlQuestions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnlQuestions.Size = new System.Drawing.Size(772, 392);
            this.pnlQuestions.TabIndex = 15;
            // 
            // lblStreetNo
            // 
            this.lblStreetNo.BackColor = System.Drawing.Color.Transparent;
            this.lblStreetNo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblStreetNo.FontSize = MetroFramework.MetroLabelSize.XL;
            this.lblStreetNo.Location = new System.Drawing.Point(189, 143);
            this.lblStreetNo.Name = "lblStreetNo";
            this.lblStreetNo.Size = new System.Drawing.Size(194, 35);
            this.lblStreetNo.TabIndex = 79;
            this.lblStreetNo.Text = "Street No:";
            this.lblStreetNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroLabel1
            // 
            this.metroLabel1.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.XL;
            this.metroLabel1.Location = new System.Drawing.Point(189, 178);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(194, 35);
            this.metroLabel1.TabIndex = 80;
            this.metroLabel1.Text = "Street Name:";
            this.metroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroLabel3
            // 
            this.metroLabel3.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel3.FontSize = MetroFramework.MetroLabelSize.XL;
            this.metroLabel3.Location = new System.Drawing.Point(189, 213);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(194, 35);
            this.metroLabel3.TabIndex = 82;
            this.metroLabel3.Text = "Suburb:";
            this.metroLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroLabel4
            // 
            this.metroLabel4.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel4.FontSize = MetroFramework.MetroLabelSize.XL;
            this.metroLabel4.Location = new System.Drawing.Point(189, 248);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(194, 35);
            this.metroLabel4.TabIndex = 83;
            this.metroLabel4.Text = "Area Code:";
            this.metroLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroLabel5
            // 
            this.metroLabel5.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel5.FontSize = MetroFramework.MetroLabelSize.XL;
            this.metroLabel5.Location = new System.Drawing.Point(189, 283);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(194, 35);
            this.metroLabel5.TabIndex = 84;
            this.metroLabel5.Text = "City / Town:";
            this.metroLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroLabel6
            // 
            this.metroLabel6.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel6.FontSize = MetroFramework.MetroLabelSize.XL;
            this.metroLabel6.Location = new System.Drawing.Point(189, 318);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(194, 35);
            this.metroLabel6.TabIndex = 85;
            this.metroLabel6.Text = "Province:";
            this.metroLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroLabel7
            // 
            this.metroLabel7.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel7.FontSize = MetroFramework.MetroLabelSize.XL;
            this.metroLabel7.Location = new System.Drawing.Point(189, 353);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(194, 35);
            this.metroLabel7.TabIndex = 86;
            this.metroLabel7.Text = "Country:";
            this.metroLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroLabel9
            // 
            this.metroLabel9.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel9.FontSize = MetroFramework.MetroLabelSize.XL;
            this.metroLabel9.Location = new System.Drawing.Point(189, 73);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(194, 35);
            this.metroLabel9.TabIndex = 94;
            this.metroLabel9.Text = "Unit No:";
            this.metroLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroLabel10
            // 
            this.metroLabel10.BackColor = System.Drawing.Color.Transparent;
            this.metroLabel10.FontSize = MetroFramework.MetroLabelSize.XL;
            this.metroLabel10.Location = new System.Drawing.Point(189, 108);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(194, 35);
            this.metroLabel10.TabIndex = 95;
            this.metroLabel10.Text = "Building / Complex:";
            this.metroLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // metroPanel1
            // 
            this.pnlQuestions.SetColumnSpan(this.metroPanel1, 2);
            this.metroPanel1.Controls.Add(this.rdbPOBox);
            this.metroPanel1.Controls.Add(this.rdbStreet);
            this.metroPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(186, 38);
            this.metroPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(400, 35);
            this.metroPanel1.TabIndex = 100;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // rdbPOBox
            // 
            this.rdbPOBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.rdbPOBox.AutoSize = true;
            this.rdbPOBox.FontSize = MetroFramework.MetroCheckBoxSize.Tall;
            this.rdbPOBox.Location = new System.Drawing.Point(100, 4);
            this.rdbPOBox.Name = "rdbPOBox";
            this.rdbPOBox.Size = new System.Drawing.Size(94, 25);
            this.rdbPOBox.TabIndex = 98;
            this.rdbPOBox.Text = "P.O. Box";
            this.rdbPOBox.UseSelectable = true;
            this.rdbPOBox.CheckedChanged += new System.EventHandler(this.rdbPOBox_CheckedChanged);
            // 
            // rdbStreet
            // 
            this.rdbStreet.Appearance = System.Windows.Forms.Appearance.Button;
            this.rdbStreet.AutoSize = true;
            this.rdbStreet.FontSize = MetroFramework.MetroCheckBoxSize.Tall;
            this.rdbStreet.Location = new System.Drawing.Point(200, 4);
            this.rdbStreet.Name = "rdbStreet";
            this.rdbStreet.Size = new System.Drawing.Size(143, 25);
            this.rdbStreet.TabIndex = 99;
            this.rdbStreet.Text = "Street Address";
            this.rdbStreet.UseSelectable = true;
            // 
            // txtUnitNo
            // 
            this.txtUnitNo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtUnitNo.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtUnitNo.Lines = new string[0];
            this.txtUnitNo.Location = new System.Drawing.Point(389, 76);
            this.txtUnitNo.MaxLength = 32767;
            this.txtUnitNo.Name = "txtUnitNo";
            this.txtUnitNo.PasswordChar = '\0';
            this.txtUnitNo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtUnitNo.SelectedText = "";
            this.txtUnitNo.Size = new System.Drawing.Size(194, 29);
            this.txtUnitNo.TabIndex = 101;
            this.txtUnitNo.UseSelectable = true;
            // 
            // txtComplex
            // 
            this.txtComplex.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtComplex.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtComplex.Lines = new string[0];
            this.txtComplex.Location = new System.Drawing.Point(389, 111);
            this.txtComplex.MaxLength = 32767;
            this.txtComplex.Name = "txtComplex";
            this.txtComplex.PasswordChar = '\0';
            this.txtComplex.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtComplex.SelectedText = "";
            this.txtComplex.Size = new System.Drawing.Size(194, 29);
            this.txtComplex.TabIndex = 102;
            this.txtComplex.UseSelectable = true;
            // 
            // txtStreetNo
            // 
            this.txtStreetNo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtStreetNo.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtStreetNo.Icon = ((System.Drawing.Image)(resources.GetObject("txtStreetNo.Icon")));
            this.txtStreetNo.IconRight = true;
            this.txtStreetNo.Lines = new string[0];
            this.txtStreetNo.Location = new System.Drawing.Point(389, 146);
            this.txtStreetNo.MaxLength = 32767;
            this.txtStreetNo.Name = "txtStreetNo";
            this.txtStreetNo.PasswordChar = '\0';
            this.txtStreetNo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtStreetNo.SelectedText = "";
            this.txtStreetNo.Size = new System.Drawing.Size(194, 29);
            this.txtStreetNo.TabIndex = 103;
            this.txtStreetNo.UseSelectable = true;
            // 
            // txtStreet
            // 
            this.txtStreet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtStreet.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtStreet.Icon = ((System.Drawing.Image)(resources.GetObject("txtStreet.Icon")));
            this.txtStreet.IconRight = true;
            this.txtStreet.Lines = new string[0];
            this.txtStreet.Location = new System.Drawing.Point(389, 181);
            this.txtStreet.MaxLength = 32767;
            this.txtStreet.Name = "txtStreet";
            this.txtStreet.PasswordChar = '\0';
            this.txtStreet.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtStreet.SelectedText = "";
            this.txtStreet.Size = new System.Drawing.Size(194, 29);
            this.txtStreet.TabIndex = 104;
            this.txtStreet.UseSelectable = true;
            // 
            // txtSuburb
            // 
            this.txtSuburb.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSuburb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtSuburb.Icon = global::iRent.Properties.Resources.icon_star;
            this.txtSuburb.IconRight = true;
            this.txtSuburb.Lines = new string[0];
            this.txtSuburb.Location = new System.Drawing.Point(389, 216);
            this.txtSuburb.MaxLength = 32767;
            this.txtSuburb.Name = "txtSuburb";
            this.txtSuburb.PasswordChar = '\0';
            this.txtSuburb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtSuburb.SelectedText = "";
            this.txtSuburb.Size = new System.Drawing.Size(194, 29);
            this.txtSuburb.TabIndex = 105;
            this.txtSuburb.UseSelectable = true;
            // 
            // txtAreaCode
            // 
            this.txtAreaCode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAreaCode.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtAreaCode.Icon = global::iRent.Properties.Resources.icon_star;
            this.txtAreaCode.IconRight = true;
            this.txtAreaCode.Lines = new string[0];
            this.txtAreaCode.Location = new System.Drawing.Point(389, 251);
            this.txtAreaCode.MaxLength = 32767;
            this.txtAreaCode.Name = "txtAreaCode";
            this.txtAreaCode.PasswordChar = '\0';
            this.txtAreaCode.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtAreaCode.SelectedText = "";
            this.txtAreaCode.Size = new System.Drawing.Size(194, 29);
            this.txtAreaCode.TabIndex = 106;
            this.txtAreaCode.UseSelectable = true;
            // 
            // txtTown
            // 
            this.txtTown.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTown.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtTown.Icon = global::iRent.Properties.Resources.icon_star;
            this.txtTown.IconRight = true;
            this.txtTown.Lines = new string[0];
            this.txtTown.Location = new System.Drawing.Point(389, 286);
            this.txtTown.MaxLength = 32767;
            this.txtTown.Name = "txtTown";
            this.txtTown.PasswordChar = '\0';
            this.txtTown.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtTown.SelectedText = "";
            this.txtTown.Size = new System.Drawing.Size(194, 29);
            this.txtTown.TabIndex = 107;
            this.txtTown.UseSelectable = true;
            // 
            // txtProvince
            // 
            this.txtProvince.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtProvince.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtProvince.Icon = global::iRent.Properties.Resources.icon_star;
            this.txtProvince.IconRight = true;
            this.txtProvince.Lines = new string[0];
            this.txtProvince.Location = new System.Drawing.Point(389, 321);
            this.txtProvince.MaxLength = 32767;
            this.txtProvince.Name = "txtProvince";
            this.txtProvince.PasswordChar = '\0';
            this.txtProvince.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtProvince.SelectedText = "";
            this.txtProvince.Size = new System.Drawing.Size(194, 29);
            this.txtProvince.TabIndex = 108;
            this.txtProvince.UseSelectable = true;
            // 
            // txtCountry
            // 
            this.txtCountry.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCountry.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtCountry.Icon = global::iRent.Properties.Resources.icon_star;
            this.txtCountry.IconRight = true;
            this.txtCountry.Lines = new string[] {
        "South Africa"};
            this.txtCountry.Location = new System.Drawing.Point(389, 356);
            this.txtCountry.MaxLength = 32767;
            this.txtCountry.Name = "txtCountry";
            this.txtCountry.PasswordChar = '\0';
            this.txtCountry.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCountry.SelectedText = "";
            this.txtCountry.Size = new System.Drawing.Size(194, 29);
            this.txtCountry.TabIndex = 109;
            this.txtCountry.Text = "South Africa";
            this.txtCountry.UseSelectable = true;
            // 
            // metroPanel2
            // 
            this.pnlQuestions.SetColumnSpan(this.metroPanel2, 2);
            this.metroPanel2.Controls.Add(this.btnSameAsPhysical);
            this.metroPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel2.HorizontalScrollbarBarColor = true;
            this.metroPanel2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel2.HorizontalScrollbarSize = 10;
            this.metroPanel2.Location = new System.Drawing.Point(186, 3);
            this.metroPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.metroPanel2.Name = "metroPanel2";
            this.metroPanel2.Size = new System.Drawing.Size(400, 35);
            this.metroPanel2.TabIndex = 111;
            this.metroPanel2.VerticalScrollbarBarColor = true;
            this.metroPanel2.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel2.VerticalScrollbarSize = 10;
            // 
            // btnSameAsPhysical
            // 
            this.btnSameAsPhysical.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.btnSameAsPhysical.Location = new System.Drawing.Point(75, 0);
            this.btnSameAsPhysical.Name = "btnSameAsPhysical";
            this.btnSameAsPhysical.Size = new System.Drawing.Size(244, 30);
            this.btnSameAsPhysical.TabIndex = 110;
            this.btnSameAsPhysical.Text = "Same as Physical Address";
            this.btnSameAsPhysical.UseSelectable = true;
            this.btnSameAsPhysical.Click += new System.EventHandler(this.btnSameAsPhysical_Click);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.tableLayoutPanel.SetColumnSpan(this.lblStatus, 3);
            this.lblStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblStatus.Location = new System.Drawing.Point(3, 448);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(772, 50);
            this.lblStatus.Style = MetroFramework.MetroColorStyle.Red;
            this.lblStatus.TabIndex = 14;
            // 
            // btnNext
            // 
            this.btnNext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNext.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnNext.Location = new System.Drawing.Point(292, 501);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(194, 94);
            this.btnNext.TabIndex = 4;
            this.btnNext.Text = "&Next";
            this.btnNext.UseSelectable = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // lblHeader
            // 
            this.lblHeader.AutoSize = true;
            this.tableLayoutPanel.SetColumnSpan(this.lblHeader, 3);
            this.lblHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblHeader.FontSize = MetroFramework.MetroLabelSize.XL;
            this.lblHeader.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblHeader.Location = new System.Drawing.Point(3, 0);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(772, 50);
            this.lblHeader.TabIndex = 12;
            this.lblHeader.Text = "Personal Information && Contact Details";
            this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 3;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.Controls.Add(this.lblHeader, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.btnNext, 1, 3);
            this.tableLayoutPanel.Controls.Add(this.lblStatus, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.pnlQuestions, 0, 1);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 4;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(778, 598);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // CaptureAddressControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.tableLayoutPanel);
            this.Name = "CaptureAddressControl";
            this.Size = new System.Drawing.Size(778, 598);
            this.pnlQuestions.ResumeLayout(false);
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel1.PerformLayout();
            this.metroPanel2.ResumeLayout(false);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel pnlQuestions;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private MetroLabel lblHeader;
        private MetroButton btnNext;
        private MetroLabel lblStatus;
        private MetroLabel lblStreetNo;
        private MetroLabel metroLabel1;
        private MetroLabel metroLabel3;
        private MetroLabel metroLabel4;
        private MetroLabel metroLabel5;
        private MetroLabel metroLabel6;
        private MetroLabel metroLabel7;
        private MetroLabel metroLabel9;
        private MetroLabel metroLabel10;
        private MetroRadioButton rdbPOBox;
        private MetroRadioButton rdbStreet;
        private MetroPanel metroPanel1;
        private MetroTextBox txtUnitNo;
        private MetroTextBox txtComplex;
        private MetroTextBox txtStreetNo;
        private MetroTextBox txtStreet;
        private MetroTextBox txtSuburb;
        private MetroTextBox txtAreaCode;
        private MetroTextBox txtTown;
        private MetroTextBox txtProvince;
        private MetroTextBox txtCountry;
        private MetroPanel metroPanel2;
        private MetroButton btnSameAsPhysical;
    }
}
