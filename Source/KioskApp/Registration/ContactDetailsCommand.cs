﻿using System;
using System.Windows.Forms;
using Castle.Core;
using iRent.Framework;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;

namespace iRent.Registration
{
    [CastleComponent]
    public class ContactDetailsCommand : BaseCommand
    {
        private readonly Func<ContactDetailsControl> _factory;
        private readonly DriverApi _driverApi;

        public ContactDetailsCommand(Func<ContactDetailsControl> factory, DriverApi driverApi)
        {
            _factory = factory;
            _driverApi = driverApi;
        }

        public string Next { get; set; }

        public override void Invoke()
        {
            var control = _factory();
            control.Dock = DockStyle.Fill;
            GetFormAs<MainForm>().AddToFormPanel(control);
            control.Bind(ContextData.Driver);
            control.Next += OnNext;
        }

        private void OnNext(object sender, DriverModel driver)
        {
            driver.IsRegistered = false;
            driver = _driverApi.Save(driver);
            ContextData.SetDriver(driver);
            Proceed(Next);
        }
    }
}