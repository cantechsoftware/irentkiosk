﻿using System;
using System.Windows.Forms;
using Castle.Core;
using Microsoft.Exchange.WebServices.Data;

namespace iRent.Registration
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class ThankYouResetControl : UserControl
    {
        public ThankYouResetControl()
        {
            InitializeComponent();
        }

        public void Setup(string intro, string status, string button)
        {
            lblIntroText.Text = intro;
            lblstatus1.Text = status;
            btnNext.Text = button;
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (Next != null)
                Next(this, e);
        }

        public event EventHandler Next;

        public ExchangeService GetBinding(string strUser, string strPassword)
        {
            // Create the binding.
            var service = new ExchangeService(ExchangeVersion.Exchange2013);

            // Define credentials.
            service.Credentials = new WebCredentials(strUser, strPassword);

            // Use the AutodiscoverUrl method to locate the service endpoint.
            try
            {
                service.AutodiscoverUrl(strUser, RedirectionUrlValidationCallback);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception thrown: " + ex.Message);
                var _url = new Uri("https://outlook.office365.com/EWS/Exchange.asmx");
                service.Url = _url;
            }

            // Display the service URL.
            Console.WriteLine("AutodiscoverURL: " + service.Url);
            return service;
        }

        public bool RedirectionUrlValidationCallback(String redirectionUrl)
        {
            // Perform validation.
            return (redirectionUrl == "https://autodiscover-s.outlook.com/autodiscover/autodiscover.xml");
        }
    }
}