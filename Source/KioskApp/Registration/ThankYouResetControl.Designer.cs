﻿namespace iRent.Registration
{
    partial class ThankYouResetControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            MetroFramework.Controls.MetroPanel pnlSummary;
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.lblIntroText = new MetroFramework.Controls.MetroLabel();
            this.lblstatus1 = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblSummary = new MetroFramework.Controls.MetroLabel();
            this.btnNext = new MetroFramework.Controls.MetroButton();
            this.lblfooter = new MetroFramework.Controls.MetroLabel();
            pnlSummary = new MetroFramework.Controls.MetroPanel();
            pnlSummary.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlSummary
            // 
            this.tableLayoutPanel1.SetColumnSpan(pnlSummary, 3);
            pnlSummary.Controls.Add(this.tableLayoutPanel2);
            pnlSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlSummary.HorizontalScrollbarBarColor = true;
            pnlSummary.HorizontalScrollbarHighlightOnWheel = false;
            pnlSummary.HorizontalScrollbarSize = 12;
            pnlSummary.Location = new System.Drawing.Point(4, 40);
            pnlSummary.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            pnlSummary.Name = "pnlSummary";
            pnlSummary.Size = new System.Drawing.Size(928, 316);
            pnlSummary.TabIndex = 11;
            pnlSummary.VerticalScrollbarBarColor = true;
            pnlSummary.VerticalScrollbarHighlightOnWheel = false;
            pnlSummary.VerticalScrollbarSize = 13;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.66667F));
            this.tableLayoutPanel2.Controls.Add(this.lblIntroText, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblstatus1, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 7;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(928, 316);
            this.tableLayoutPanel2.TabIndex = 14;
            // 
            // lblIntroText
            // 
            this.lblIntroText.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.lblIntroText, 2);
            this.lblIntroText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblIntroText.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblIntroText.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblIntroText.Location = new System.Drawing.Point(4, 0);
            this.lblIntroText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblIntroText.Name = "lblIntroText";
            this.lblIntroText.Size = new System.Drawing.Size(920, 62);
            this.lblIntroText.TabIndex = 25;
            this.lblIntroText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblstatus1
            // 
            this.lblstatus1.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.lblstatus1, 2);
            this.lblstatus1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblstatus1.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblstatus1.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblstatus1.Location = new System.Drawing.Point(4, 62);
            this.lblstatus1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblstatus1.Name = "lblstatus1";
            this.lblstatus1.Size = new System.Drawing.Size(920, 62);
            this.lblstatus1.TabIndex = 24;
            this.lblstatus1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 267F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(pnlSummary, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblSummary, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnNext, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblfooter, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 123F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(936, 545);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // lblSummary
            // 
            this.lblSummary.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.lblSummary, 3);
            this.lblSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSummary.FontSize = MetroFramework.MetroLabelSize.XL;
            this.lblSummary.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblSummary.Location = new System.Drawing.Point(4, 0);
            this.lblSummary.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSummary.Name = "lblSummary";
            this.lblSummary.Size = new System.Drawing.Size(928, 36);
            this.lblSummary.TabIndex = 10;
            this.lblSummary.Text = "Registration Complete";
            this.lblSummary.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnNext
            // 
            this.btnNext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNext.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnNext.Location = new System.Drawing.Point(338, 426);
            this.btnNext.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(259, 115);
            this.btnNext.TabIndex = 13;
            this.btnNext.Text = "&Next";
            this.btnNext.UseSelectable = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // lblfooter
            // 
            this.lblfooter.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.lblfooter, 3);
            this.lblfooter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblfooter.FontSize = MetroFramework.MetroLabelSize.XL;
            this.lblfooter.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblfooter.Location = new System.Drawing.Point(4, 360);
            this.lblfooter.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblfooter.Name = "lblfooter";
            this.lblfooter.Size = new System.Drawing.Size(928, 62);
            this.lblfooter.Style = MetroFramework.MetroColorStyle.Red;
            this.lblfooter.TabIndex = 14;
            this.lblfooter.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SummaryRegControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "SummaryRegControl";
            this.Size = new System.Drawing.Size(936, 545);
            pnlSummary.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroLabel lblstatus1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private MetroFramework.Controls.MetroLabel lblIntroText;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MetroFramework.Controls.MetroLabel lblSummary;
        private MetroFramework.Controls.MetroButton btnNext;
        private MetroFramework.Controls.MetroLabel lblfooter;
    }
}
