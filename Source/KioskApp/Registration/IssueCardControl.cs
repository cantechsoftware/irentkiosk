﻿using System;
using Castle.Core;
using MetroFramework.Controls;

namespace iRent.Registration
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class IssueCardControl : MetroUserControl
    {
        public IssueCardControl()
        {
            InitializeComponent();
        }

        public string Title
        {
            get { return lblTitle.Text; }
            set { lblTitle.Text = value; }
        }

        public string Message
        {
            get { return lblMessage.Text; }
            set { lblMessage.Text = value; }
        }

        public string Button
        {
            get { return btnYes.Text; }
            set { btnYes.Text = value; }
        }

        public event EventHandler Yes;
        public event EventHandler Back;

        private void btnYes_Click(object sender, EventArgs e)
        {
            Yes?.Invoke(this, e);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            Back?.Invoke(this, e);
        }
    }
}