﻿using System;
using CanTech.Common.Extensions;
using Castle.Core;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Util;
using MetroFramework.Controls;
using System.Windows.Forms;
using log4net;

namespace iRent.Registration
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class ContactDetailsControl : MetroUserControl
    {
        private DriverModel _driver;
        private static readonly ILog _log = LogManager.GetLogger(typeof(ContactDetailsControl));

        public ContactDetailsControl()
        {
            InitializeComponent();
        }

        public string Title
        {
            get { return lblHeader.Text; }
            set { lblHeader.Text = value; }
        }

        private bool ValidateText()
        {
            var err = GetError();
            if (err == null)
            {
                lblStatus.Text = "";
                return true;
            }

            lblStatus.Text = err;
            return false;
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateText())
                    return;

                Update(_driver);
                Next?.Invoke(this, _driver);
            }
            catch (Exception ex)
            {
                _log.Error("Error Occurred during ContactDetails.Invoke: ", ex);
                MessageBox.Show("Error During ContactDetails.Invoke: \n" + ex.Message);
            }
        }

        public event EventHandler<DriverModel> Next;

        public void Update(DriverModel driver)
        {
            try
            {


                driver.Title = (string)cmbTitle.SelectedItem;
                driver.FirstName = txtFirstName.Text;
                driver.Surname = txtSurname.Text;
                driver.IdNumber = txtIDNo.Text;

                driver.DateOfBirth = txtDOB.Text != null ? DateTime.Parse(txtDOB.Text) : DateTime.MinValue;
                if (driver.DateOfBirth == DateTime.MinValue)
                {
                    driver.DateOfBirth = null;
                }

                driver.Email = txtEmail.Text;
                driver.CellNo = txtCellNo.Text;
                driver.HomeNo = txtHomeNo.Text;
                driver["WorkNo"] = txtWorkNo.Text;
            }
            catch (Exception ex)
            {
                _log.Error("Error Occurred during ContactDetails.Update: ", ex);
                MessageBox.Show("Error During ContactDetails.Update: \n" + ex.Message);

            }
        }

        public void Bind(DriverModel driver)
        {
            try
            {

                txtIDNo.Text = driver.IdNumber;
                txtIDNo.Enabled = false;
                txtDOB.Text = $"{driver.DateOfBirth:yyyy/MM/dd}";
                txtDOB.Enabled = !driver.DateOfBirth.HasValue;
                cmbTitle.SelectedItem = driver.Title;
                txtFirstName.Text = driver.FirstName;
                txtSurname.Enabled = false;
                txtSurname.Text = driver.Surname;
                txtEmail.Text = driver.Email;
                txtConfirmEmail.Text = driver.Email;
                txtHomeNo.Text = driver.HomeNo;
                txtCellNo.Text = driver.CellNo;
                txtWorkNo.Text = driver["WorkNo"];
                if (txtDOB.Enabled)
                    txtDOB.Focus();
                else
                    cmbTitle.Focus();

                _driver = driver;
            }
            catch (Exception ex)
            {
                _log.Error("Error Occurred during ContactDetails.Bind: ", ex);
                MessageBox.Show("Error During ContactDetails.Bind: \n" + ex.Message);
            }
        }

        private string GetError()
        {
            if (((string) cmbTitle.SelectedItem).IsEmpty())
                return "Please select your Title";

            if (txtFirstName.Text.Trim().IsEmpty())
                return "Please enter your first name";

            if (txtEmail.Text.Trim().Length == 0)
                return "Please enter your Email Address";

            if (!Valid.Email(txtEmail.Text))
                return "Please enter a valid Email.";

            if (txtConfirmEmail.Text.Trim() != txtEmail.Text)
                return "Emails do not match";

            if (txtCellNo.Text.Trim().Length == 0)
                return "Please enter your Cell No";

            if (!Valid.TelNo(txtCellNo.Text))
                return "Please enter a valid Cell No";

            if (txtWorkNo.Text.Trim().IsEmpty())
                return "Please enter your Work No";

            if (!Valid.TelNo(txtWorkNo.Text))
                return "Please enter a valid Work No";

            if (txtDOB.Text.Trim().IsEmpty())
                return "Please enter your date of birth";

            DateTime date = DateTime.MinValue;

            if (txtDOB.Text == null && DateTime.TryParse(txtDOB.Text,out  date))
            {
                return "Please enter a valid date of birth";
            }

            return null;
        }
    }
}