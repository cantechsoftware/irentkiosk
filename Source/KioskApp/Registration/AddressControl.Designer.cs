﻿using MetroFramework.Controls;

namespace iRent.Registration
{
    partial class AddressControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddressControl));
            this.txtCountry = new MetroFramework.Controls.MetroTextBox();
            this.txtStreetNo = new MetroFramework.Controls.MetroTextBox();
            this.txtAreaCode = new MetroFramework.Controls.MetroTextBox();
            this.txtStreet = new MetroFramework.Controls.MetroTextBox();
            this.txtProvince = new MetroFramework.Controls.MetroTextBox();
            this.txtSuburb = new MetroFramework.Controls.MetroTextBox();
            this.txtComplex = new MetroFramework.Controls.MetroTextBox();
            this.txtTown = new MetroFramework.Controls.MetroTextBox();
            this.txtUnitNo = new MetroFramework.Controls.MetroTextBox();
            this.SuspendLayout();
            // 
            // txtCountry
            // 
            this.txtCountry.Enabled = false;
            this.txtCountry.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtCountry.Icon = global::iRent.Properties.Resources.icon_star;
            this.txtCountry.IconRight = true;
            this.txtCountry.Lines = new string[] {
        "South Africa"};
            this.txtCountry.Location = new System.Drawing.Point(715, 60);
            this.txtCountry.Margin = new System.Windows.Forms.Padding(4);
            this.txtCountry.MaxLength = 32767;
            this.txtCountry.Name = "txtCountry";
            this.txtCountry.PasswordChar = '\0';
            this.txtCountry.PromptText = "Suburb";
            this.txtCountry.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCountry.SelectedText = "";
            this.txtCountry.Size = new System.Drawing.Size(327, 37);
            this.txtCountry.TabIndex = 9;
            this.txtCountry.Text = "South Africa";
            this.txtCountry.UseSelectable = true;
            // 
            // txtStreetNo
            // 
            this.txtStreetNo.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtStreetNo.Icon = ((System.Drawing.Image)(resources.GetObject("txtStreetNo.Icon")));
            this.txtStreetNo.IconRight = true;
            this.txtStreetNo.Lines = new string[0];
            this.txtStreetNo.Location = new System.Drawing.Point(361, 4);
            this.txtStreetNo.Margin = new System.Windows.Forms.Padding(4);
            this.txtStreetNo.MaxLength = 32767;
            this.txtStreetNo.Name = "txtStreetNo";
            this.txtStreetNo.PasswordChar = '\0';
            this.txtStreetNo.PromptText = "Street No";
            this.txtStreetNo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtStreetNo.SelectedText = "";
            this.txtStreetNo.Size = new System.Drawing.Size(129, 37);
            this.txtStreetNo.TabIndex = 3;
            this.txtStreetNo.UseSelectable = true;
            // 
            // txtAreaCode
            // 
            this.txtAreaCode.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtAreaCode.Icon = global::iRent.Properties.Resources.icon_star;
            this.txtAreaCode.IconRight = true;
            this.txtAreaCode.Lines = new string[0];
            this.txtAreaCode.Location = new System.Drawing.Point(900, 4);
            this.txtAreaCode.Margin = new System.Windows.Forms.Padding(4);
            this.txtAreaCode.MaxLength = 32767;
            this.txtAreaCode.Name = "txtAreaCode";
            this.txtAreaCode.PasswordChar = '\0';
            this.txtAreaCode.PromptText = "Area Code";
            this.txtAreaCode.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtAreaCode.SelectedText = "";
            this.txtAreaCode.Size = new System.Drawing.Size(141, 37);
            this.txtAreaCode.TabIndex = 6;
            this.txtAreaCode.UseSelectable = true;
            // 
            // txtStreet
            // 
            this.txtStreet.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtStreet.Icon = ((System.Drawing.Image)(resources.GetObject("txtStreet.Icon")));
            this.txtStreet.IconRight = true;
            this.txtStreet.Lines = new string[0];
            this.txtStreet.Location = new System.Drawing.Point(499, 4);
            this.txtStreet.Margin = new System.Windows.Forms.Padding(4);
            this.txtStreet.MaxLength = 32767;
            this.txtStreet.Name = "txtStreet";
            this.txtStreet.PasswordChar = '\0';
            this.txtStreet.PromptText = "Street Name";
            this.txtStreet.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtStreet.SelectedText = "";
            this.txtStreet.Size = new System.Drawing.Size(189, 37);
            this.txtStreet.TabIndex = 4;
            this.txtStreet.UseSelectable = true;
            // 
            // txtProvince
            // 
            this.txtProvince.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtProvince.Icon = global::iRent.Properties.Resources.icon_star;
            this.txtProvince.IconRight = true;
            this.txtProvince.Lines = new string[0];
            this.txtProvince.Location = new System.Drawing.Point(361, 60);
            this.txtProvince.Margin = new System.Windows.Forms.Padding(4);
            this.txtProvince.MaxLength = 32767;
            this.txtProvince.Name = "txtProvince";
            this.txtProvince.PasswordChar = '\0';
            this.txtProvince.PromptText = "Province/State";
            this.txtProvince.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtProvince.SelectedText = "";
            this.txtProvince.Size = new System.Drawing.Size(327, 37);
            this.txtProvince.TabIndex = 8;
            this.txtProvince.UseSelectable = true;
            // 
            // txtSuburb
            // 
            this.txtSuburb.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtSuburb.Icon = global::iRent.Properties.Resources.icon_star;
            this.txtSuburb.IconRight = true;
            this.txtSuburb.Lines = new string[0];
            this.txtSuburb.Location = new System.Drawing.Point(715, 4);
            this.txtSuburb.Margin = new System.Windows.Forms.Padding(4);
            this.txtSuburb.MaxLength = 32767;
            this.txtSuburb.Name = "txtSuburb";
            this.txtSuburb.PasswordChar = '\0';
            this.txtSuburb.PromptText = "Suburb";
            this.txtSuburb.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtSuburb.SelectedText = "";
            this.txtSuburb.Size = new System.Drawing.Size(177, 37);
            this.txtSuburb.TabIndex = 5;
            this.txtSuburb.UseSelectable = true;
            // 
            // txtComplex
            // 
            this.txtComplex.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtComplex.Lines = new string[0];
            this.txtComplex.Location = new System.Drawing.Point(157, 4);
            this.txtComplex.Margin = new System.Windows.Forms.Padding(4);
            this.txtComplex.MaxLength = 32767;
            this.txtComplex.Name = "txtComplex";
            this.txtComplex.PasswordChar = '\0';
            this.txtComplex.PromptText = "Complex";
            this.txtComplex.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtComplex.SelectedText = "";
            this.txtComplex.Size = new System.Drawing.Size(177, 37);
            this.txtComplex.TabIndex = 2;
            this.txtComplex.UseSelectable = true;
            // 
            // txtTown
            // 
            this.txtTown.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtTown.Icon = global::iRent.Properties.Resources.icon_star;
            this.txtTown.IconRight = true;
            this.txtTown.Lines = new string[0];
            this.txtTown.Location = new System.Drawing.Point(8, 60);
            this.txtTown.Margin = new System.Windows.Forms.Padding(4);
            this.txtTown.MaxLength = 32767;
            this.txtTown.Name = "txtTown";
            this.txtTown.PasswordChar = '\0';
            this.txtTown.PromptText = "City";
            this.txtTown.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtTown.SelectedText = "";
            this.txtTown.Size = new System.Drawing.Size(327, 37);
            this.txtTown.TabIndex = 7;
            this.txtTown.UseSelectable = true;
            // 
            // txtUnitNo
            // 
            this.txtUnitNo.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.txtUnitNo.Lines = new string[0];
            this.txtUnitNo.Location = new System.Drawing.Point(8, 4);
            this.txtUnitNo.Margin = new System.Windows.Forms.Padding(4);
            this.txtUnitNo.MaxLength = 32767;
            this.txtUnitNo.Name = "txtUnitNo";
            this.txtUnitNo.PasswordChar = '\0';
            this.txtUnitNo.PromptText = "Unit No";
            this.txtUnitNo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtUnitNo.SelectedText = "";
            this.txtUnitNo.Size = new System.Drawing.Size(129, 37);
            this.txtUnitNo.TabIndex = 1;
            this.txtUnitNo.UseSelectable = true;
            // 
            // AddressControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.Controls.Add(this.txtCountry);
            this.Controls.Add(this.txtStreetNo);
            this.Controls.Add(this.txtAreaCode);
            this.Controls.Add(this.txtStreet);
            this.Controls.Add(this.txtProvince);
            this.Controls.Add(this.txtSuburb);
            this.Controls.Add(this.txtComplex);
            this.Controls.Add(this.txtTown);
            this.Controls.Add(this.txtUnitNo);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "AddressControl";
            this.Size = new System.Drawing.Size(1052, 107);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroTextBox txtCountry;
        private MetroTextBox txtStreetNo;
        private MetroTextBox txtAreaCode;
        private MetroTextBox txtStreet;
        private MetroTextBox txtProvince;
        private MetroTextBox txtSuburb;
        private MetroTextBox txtComplex;
        private MetroTextBox txtTown;
        private MetroTextBox txtUnitNo;
    }
}
