﻿using System;
using Castle.Core;
using iRent.Framework;
using iRent.Services;
using iRent.Util;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;

namespace iRent.Registration
{
    [CastleComponent]
    public class IssuePermanentCardCommand : BaseCommand
    {
        private readonly RfidService _rfidService;
        private readonly DriverApi _driverApi;
        private readonly Func<IssueCardControl> _factory;

        public IssuePermanentCardCommand(RfidService rfidService, DriverApi driverApi, Func<IssueCardControl> factory)
        {
            _rfidService = rfidService;
            _driverApi = driverApi;
            _factory = factory;
            Title = "Issue Card";
            Message = "Please click on Issue Card to get your card";
            Button = "Issue Card";
        }

        public string Title { get; set; }
        public string Message { get; set; }
        public string Button { get; set; }

        public string Next { get; set; }
        public string Failed { get; set; }
        public string Back { get; set; }

        public override void Invoke()
        {
            var control = _factory();
            control.Title = Title;
            control.Message = Message;
            control.Button = Button;
            GetFormAs<MainForm>().AddToFormPanel(control);
            control.Yes += (sender, args) => Issue();
            control.Back += (sender, args) => OnBack();
        }

        private void OnBack()
        {
            Proceed(Back);
        }

        private void Issue()
        {
            try
            {                
                var rfid = _rfidService.GetCard(false);

                if (rfid == null || rfid == "Error")
                {
                    MessageForm.Show("Error Retrieving RFID Card",
                        @"There was an error retrieving your RFID card.
Please contact the service desk for assistance");
                    Proceed(Failed);
                    return;
                }
                else
                {
                    ContextData.Driver.PermanentRfid = rfid;
                    ContextData.Driver.RfId = rfid;
                    _driverApi.Save(ContextData.Driver);
                }

                Proceed(Next);
            }
            catch (Exception ex)
            {
                Logger.Log(this, "Save", "Error occurred issuing permanent rfid", ex);
                MessageForm.Show("Error Issue RFID Card",
                    @"There was an error issuing your RFID card. 
Please contact the service desk for assistance.");
                Proceed(Failed);
            }
        }
    }
}