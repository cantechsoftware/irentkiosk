﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using CanTech.Common.Extensions;
using Castle.Core;
using iRent2.Contracts.Models;
using MetroFramework;
using MetroFramework.Controls;

namespace iRent.Registration
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class DriverQuestionsControl : MetroUserControl
    {
        public DriverQuestionsControl()
        {
            InitializeComponent();
        }

        public string Title {  get { return lblHeader.Text; } set { lblHeader.Text = value; } }

        private bool ValidateText()
        {
            var valid = pnlQuestions.Controls.OfType<MetroTextBox>()
                .All(x => x.Text.Trim().HasValue());

            if (valid)
                return true;

            Error("Please complete all fields.");
            return false;
        }
        

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (!ValidateText())
                return;

            Next?.Invoke(this, e);
        }

        public event EventHandler Next;

        public void Update(DriverModel driver)
        {
            foreach (var txt in pnlQuestions.Controls.OfType<MetroTextBox>())
                driver[(string) txt.Tag] = txt.Text;
        }

        public void Bind(IList<DriverQuestion> questions, DriverModel driver)
        {
            var row = 0;
            pnlQuestions.RowCount = questions.Count + 1;
            pnlQuestions.RowStyles.Clear();
            foreach (var ques in questions)
            {
                var lbl = new MetroLabel
                {
                    FontSize = MetroLabelSize.XL,
                    Dock = DockStyle.Fill,
                    Text = ques.Text
                };
                var txt = new MetroTextBox
                {
                    FontSize = MetroTextBoxSize.XL,
                    Dock = DockStyle.Fill,
                    Tag = ques.AnswerKey,
                    MaxLength = ques.Maxchars,
                    Text = (string) driver[ques.AnswerKey],
                };
                if (ques.Password)
                    txt.PasswordChar = '*';                
                if (ques.Numeric)
                {
                    txt.TextChanged += Txt_TextChanged_NumericOnly;
                }
                pnlQuestions.RowStyles.Add(new RowStyle {Height = 50, SizeType = SizeType.Absolute});
                pnlQuestions.Controls.Add(lbl, 1, row);
                pnlQuestions.Controls.Add(txt, 2, row++);
            }
            pnlQuestions.RowStyles.Add(new RowStyle {SizeType = SizeType.AutoSize});
        }

        private void Txt_TextChanged_NumericOnly(object sender, EventArgs e)
        {
            MetroTextBox t = sender as MetroTextBox;
            string finaltext = "";
            for (int i = 0; i < t.Text.Length; i++)
            {
                if (char.IsDigit(t.Text[i]))
                {
                    finaltext += t.Text[i];
                }
            }

            t.Text = finaltext;
        }

        public string GetValue(string key)
        {
            return pnlQuestions.Controls.OfType<MetroTextBox>()
                .FirstOrDefault(x => (string) x.Tag == key)?.Text;
        }

        public void Error(string message)
        {
            lblStatus.Text = message;
            lblStatus.ForeColor = Color.Red;
        }
    }
}