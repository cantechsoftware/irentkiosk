﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Castle.Core;
using GalaSoft.MvvmLight.Messaging;
using MetroFramework;
using MetroFramework.Controls;
using iRent.Classes;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Models;

namespace iRent
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class RegisterStep1 : MetroUserControl
    {
        public RegisterStep1()
        {
            InitializeComponent();
            Messenger.Default.Register<NotificationMessage<ScannerMessageItem>>(
                this,
                up =>
                    {
                        if (up.Content.msg.ToString() == "driver")
                        {
                            SetUserName(up.Content.Driver);
                        }
                    });
            UIFactory.CurrentControl = "RegisterStep1";
        }

        private void SetUserName(DriverModel driver)
        {
            if (driver != null)
            {
                Invoke((MethodInvoker) delegate
                    {
                        txtBDate.Text = driver.DateOfBirth.ToString();
                        txtDriverRes.Text = String.Format("{0},{1}",
                                                          driver.Licence.DriverRestrictions[0],
                                                          driver.Licence.DriverRestrictions[1]);
                        txtGender.Text = driver.Gender.ToString();
                        txtIDNo.Text = driver.IdNumber;
                        txtIniSurname.Text = String.Format("{0} {1}",
                                                           driver.Initials,
                                                           driver.Surname);
                        txtIssueDate.Text = driver.Licence.Classes[0].FirstIssueDate.ToString();
                        txtIssueNo.Text = driver.Licence.IssueNo;
                        txtLicNo.Text = driver.Licence.CertificateNumber;
                        txtValidDate.Text = String.Format("{0} - {1}",
                                                          driver.Licence.ValidFrom,
                                                          driver.Licence.ValidUntil);
                        txtVehicleCodes.Text = String.Format("{0},{1},{2},{3}", driver.Licence.Classes[0].VehicleCode,
                                                             driver.Licence.Classes[1].VehicleCode,
                                                             driver.Licence.Classes[2].VehicleCode,
                                                             driver.Licence.Classes[3].VehicleCode);

                        txtValidDate.ForeColor = driver.Licence.ValidUntil > DateTime.Now ? Color.Green : Color.Red;
                        pictureBox1.Image = byteArrayToImage(driver.Picture.Data);
                    });
                UIFactory.Accept = true;
                UIFactory.CurrentControl = "RegsiterStep1";
                UIFactory.NextControl = "UserDetails";
            }
            else
            {
                MetroMessageBox.Show(ParentForm, "Not a valid Drivers License/Passport");
            }
        }


        public static byte[] ImageToByte(Image img)
        {
            var converter = new ImageConverter();
            return (byte[]) converter.ConvertTo(img, typeof (byte[]));
        }

        public Image byteArrayToImage(byte[] byteArrayIn)
        {
            var ms = new MemoryStream(byteArrayIn);
            var returnImage = Image.FromStream(ms);
            return returnImage;
        }

        private void RegisterStep1_Load(object sender, EventArgs e)
        {
            ClearControls();
        }

        public void ClearControls()
        {
            Messenger.Default.Register<NotificationMessage<ScannerMessageItem>>(
                this,
                up =>
                    {
                        if (up.Content.msg.ToString() == "driver")
                        {
                            SetUserName(up.Content.Driver);
                        }
                    });
            UIFactory.CurrentControl = "RegisterStep1";

            try
            {
                txtBDate.Text = "";
                txtDriverRes.Text = "";
                txtGender.Text = "";
                txtIDNo.Text = "";
                txtIniSurname.Text = "";
                txtIssueDate.Text = "";
                txtIssueNo.Text = "";
                txtLicNo.Text = "";
                txtValidDate.Text = "";
                txtVehicleCodes.Text = "";
                pictureBox1.Image = null;
            }
            catch
            {
            }
        }

        private void RegisterStep1_ControlRemoved(object sender, ControlEventArgs e)
        {
            //Messenger.Default.Unregister(this);
        }

        protected override void Dispose(bool disposing)
        {
            Messenger.Default.Unregister(this);
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}