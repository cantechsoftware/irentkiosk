﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Castle.Core;
using MetroFramework;
using MetroFramework.Controls;
using iRent.Framework;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Services.Api;

namespace iRent.Vehicles
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class VehicleGroupsControl : MetroUserControl
    {
        private readonly VehicleApi _vehicleApi;
        private VehicleGroupModel[] _vehicleGroups;

        public VehicleGroupsControl(VehicleApi vehicleApi)
        {
            _vehicleApi = vehicleApi;
            InitializeComponent();
        }

        private void VehicleGroup_Load(object sender, EventArgs e)
        {
            lblStatus.Text = "";
            btnNext.Visible = false;
            BusyForm.RunLongTask("Loading Vehicle Groups",
                                 (o, e2) => LoadVehicleGroups());

            if (_vehicleGroups.Length == 0)
                NoGroups();
            else
            {
                var top = 50;
                var left = 100;

                foreach (var v in _vehicleGroups)
                {
                    var button = new MetroButton();
                    button.Left = left;
                    button.Top = top;
                    button.Click += button_Click;
                    button.Text = v.Description;
                    button.Tag = v;
                    button.Height = 100;
                    button.Width = 170;
                    button.FontSize = MetroButtonSize.Tall;
                    button.FontWeight = MetroButtonWeight.Bold;
                    pnlVehicles.Controls.Add(button);
                    top += button.Height + 2;
                }
            }
        }

        private void NoGroups()
        {
            lblStatus.Text = @"Sorry, there are no vehicle groups available
Please contact the service desk for assistance.";

            btnNext.Visible = false;
        }

        private void LoadVehicleGroups()
        {
            try
            {
                _vehicleGroups = _vehicleApi.VehicleGroups().VehicleGroups;
            }
            catch (Exception ex)
            {
                Logger.Log(this, "LoadVehicleGroups", "Error loading vehicle groups", ex);
            }
        }

        private void button_Click(object sender, EventArgs e)
        {
            var button = (MetroButton) sender;
            SelectedGroup = (VehicleGroupModel) button.Tag;
            lblStatus.Text =
                string.Format(
                    "You have selected the following Vehicle Group: {0}. Please click Next to select a vehicle",
                    SelectedGroup.Name);
            btnNext.Visible = true;
        }

        public VehicleGroupModel SelectedGroup { get; set; }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (Next != null)
                Next(this, SelectedGroup);
        }

        public event EventHandler<VehicleGroupModel> Next;
    }
}