﻿using System;
using Castle.Core;
using MetroFramework;
using MetroFramework.Controls;
using SmartFormat;
using iRent.Classes;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Services;

namespace iRent.Vehicles
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class VehiclesControl : MetroUserControl
    {
        public VehiclesControl()
        {
            InitializeComponent();
        }

        public VehicleModel SelectedVehicle { get; set; }

        private void Vehicles_Load(object sender, EventArgs e)
        {
            UIFactory.PreviousControl = "VehicleGroup";
            UIFactory.CurrentControl = "Vehicle";
            lblStatus.Text = "";
        }

        public void AddVehicle(VehicleModel vehicle)
        {
            var button = new MetroButton();
            button.Click += button_Click;
            button.Tag = vehicle;
            button.Text = Smart.Format("{Make}-{Model}\r\n{RegNo}", vehicle);
            button.Height = 100;
            button.Width = 170;
            button.FontSize = MetroButtonSize.Tall;
            button.FontWeight = MetroButtonWeight.Bold;
            pnlVehicles.Controls.Add(button);
        }

        public void button_Click(object sender, EventArgs e)
        {
            var button = (MetroButton) sender;
            ContextData.Vehicle = (VehicleModel) button.Tag;
            ContextData.VehicleName = button.Text;
            UIFactory.Accept = true;
            SelectedVehicle = (VehicleModel) button.Tag;

            if (ContextData.SwapCar)
            {
                lblStatus.Text = Smart.Format(
                    "You have selected the following Vehicle: {Make} {Model} - {RegNo}.\r\nPlease click Next to change your car",
                    SelectedVehicle);
            }
            else
            {
                lblStatus.Text = Smart.Format(
                    "You have selected the following Vehicle: {Make} {Model} - {RegNo}.\r\nPlease click Next to continue.",
                    SelectedVehicle);
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (SelectedVehicle == null)
                lblStatus.Text = @"Please select a vehicle before proceeding.";
            else
                Next(this, SelectedVehicle);
        }

        public event EventHandler<VehicleModel> Next;

        public void SetVehicles(VehicleModel[] vehicles)
        {
            pnlVehicles.Controls.Clear();
            foreach (var v in vehicles)
                AddVehicle(v);
        }
    }
}