﻿using System;
using System.Windows.Forms;
using Castle.Core;
using iRent.Extensions;
using iRent.Framework;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Models;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;

namespace iRent.Vehicles
{
    [CastleComponent]
    public class VehiclesCommand : ICommand
    {
        private readonly Func<VehiclesControl> _factory;
        private MainForm _form;
        private readonly VehicleApi _vehicleApi;
        private readonly WorkflowService _workflowService;

        public VehiclesCommand(MainForm form,
                               Func<VehiclesControl> factory,
                               WorkflowService workflowService,
                               VehicleApi vehicleApi)
        {
            _form = form;
            _factory = factory;
            _workflowService = workflowService;
            _vehicleApi = vehicleApi;
        }

        public string Next { get; set; }
        public Form MainForm
        {
            get { return _form; }
            set { _form = (MainForm)value; }
        }

        public void Invoke()
        {
            var control = _factory();
            control.Dock = DockStyle.Fill;
            _form.AddToFormPanel(control);
            control.Next += OnNext;
            BusyForm.RunLongTask("Loading Vehicles", (o, e2) => LoadVehicles(control));
        }

        private void OnNext(object sender, VehicleModel e)
        {
            _workflowService.Execute(Next);
        }

        private void LoadVehicles(VehiclesControl control)
        {
            try
            {
                var vehicles = _vehicleApi.GetByGroupId(ContextData.VehicleGroup.Id);
                control.InvokeIfRequired(
                    x => control.SetVehicles(vehicles));
            }
            catch (Exception ex)
            {
                Logger.Log(this, "LoadVehicles", "Error loading vehicles", ex);
                Errors.Notify("Error loading vehicles", ex);
            }
        }
    }
}