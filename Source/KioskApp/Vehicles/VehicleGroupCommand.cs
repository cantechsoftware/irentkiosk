﻿using System;
using System.Windows.Forms;
using Castle.Core;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Models;
using iRentKiosk.Core.Services;

namespace iRent.Vehicles
{
    [CastleComponent]
    public class VehicleGroupCommand : ICommand
    {
        private readonly Func<VehicleGroupsControl> _factory;
        private MainForm _form;
        private readonly WorkflowService _workflowService;

        public VehicleGroupCommand(MainForm form,
                                   Func<VehicleGroupsControl> factory,
                                   WorkflowService workflowService)
        {
            _form = form;
            _factory = factory;
            _workflowService = workflowService;
        }

        public string Next { get; set; }
        public Form MainForm
        {
            get { return _form; }
            set { _form = (MainForm)value; }
        }

        public void Invoke()
        {
            var control = _factory();
            _form.AddToFormPanel(control);
            control.Dock = DockStyle.Fill;
            control.Next += OnNext;
        }

        private void OnNext(object sender, VehicleGroupModel group)
        {
            ContextData.Set(group);
            _workflowService.Execute(Next);
        }
    }
}