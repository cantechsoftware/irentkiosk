﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.Core;
using iRent.Framework;
using iRentKiosk.Core.Services;
using iRent2.Contracts.PayGate;
using iRentKiosk.Core.Services.Api;

namespace iRent.PayGate
{
    [CastleComponent]
    public class PayGateRegisterPaymentCommand : BaseCommand
    {
        public PayGateRegisterPaymentCommand(PayGateApi payGateApi)
        {
            _payGateApi = payGateApi;
        }

        private PayGateApi _payGateApi;

        public string InputDataContext { get; set; }
        public string OutputDataContext { get; set; }
        public string Next { get; set; }
        public string RedirectURL { get; set; }
        public string PaymentSuccess { get; set; }

        public override void Invoke()
        {
            RegisterPaymentRequest request = ContextData.DynamicData[InputDataContext];

            //RegisterPaymentRequest request = new RegisterPaymentRequest();
            if (!RedirectURL.Contains("http:") && !RedirectURL.Contains("https:") && !RedirectURL.Contains("file:"))
            {
                string path = Environment.CurrentDirectory + "\\LocalPages";
                path = "file:///" + path.Replace('\\', '/');
                request.ReturnUrl = path + "/" + RedirectURL;
            }
            else
            {
                request.ReturnUrl = RedirectURL;
            }
            RegisterPaymentResponse response = _payGateApi.RegisterPayment(request);
            ContextData.SetDynamicData(OutputDataContext, response);
            if (!response.CompletePayWeb)
            {
                ContextData.SetDynamicData("PayGateStatus", "Thank you for your booking. \n\rYou will be notified as soon as your booking is ready.");
                Proceed(PaymentSuccess);
            }
            else
            {
                Proceed(Next);
            }
        }
    }
}

