﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using CanTech.Common.Extensions;
using Castle.Core;
using MetroFramework.Controls;
using iRent.Classes;
using iRent.Extensions;
using iRent.Properties;
using iRent2.Contracts.Enums;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Extensions;
using iRentKiosk.Core.Services;

namespace iRent.Login
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class ManualLicence : MetroUserControl
    {
        private readonly MainForm _form;

        public ManualLicence(MainForm form)
        {
            _form = form;
            InitializeComponent();
        }

        private void Login_Load(object sender, EventArgs e)
        {
            UIFactory.CurrentControl = "ManualLicence";
            btnNext.Visible = true;
            btnNext.Enabled = true;
            MarkRequiredFields();
            SetTabIndexes(txtName,
                          txtIDNo,
                          cboGender,
                          txtBDate,
                          txtLicNo,
                          txtIssueDate,
                          txtIssueNo,
                          txtValidDate,
                          txtVehicleCodes,
                          txtDriverRes);
            BindToLoginForm(ContextData.Driver);
        }

        private void SetTabIndexes(params Control[] order)
        {
            for (var i = 0; i < order.Length; i++)
                order[i].TabIndex = i;
        }

        private void MarkRequiredFields()
        {
            var req = new Control[] {txtName, txtIDNo, cboGender, txtBDate, txtLicNo, txtValidDate};

            foreach (var ctrl in req)
            {
                var pos = tableLayoutPanel.GetPositionFromControl(ctrl);
                var bmp = new PictureBox();
                bmp.Image = Resources.icon_star;
                tableLayoutPanel.Controls.Add(bmp, 3, pos.Row);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void ClearForm()
        {
            txtBDate.Text = "";
            txtDriverRes.Text = "";
            cboGender.Text = "";
            txtIDNo.Text = "";
            txtName.Text = "";
            txtIssueDate.Text = "";
            txtIssueNo.Text = "";
            txtLicNo.Text = "";
            txtValidDate.Text = "";
            txtVehicleCodes.Text = "";
        }

        private void BindToLoginForm(DriverModel driver)
        {
            ClearForm();

            txtBDate.Text = string.Format("{0:yyyy-MM-dd}", driver.DateOfBirth);
            cboGender.Text = driver.Gender.ToString();
            txtIDNo.Text = driver.IdNumber;
            txtName.Text = driver.Surname;

            if (driver.Licence != null)
            {
                if (driver.Licence.DriverRestrictions != null)
                    txtDriverRes.Text = driver.Licence.DriverRestrictions.Join(",");
                if (driver.Licence.Classes != null && driver.Licence.Classes.Count > 0)
                    txtIssueDate.Text = String.Format("{0:yyyy-MM-dd}", driver.Licence.Classes[0].FirstIssueDate);
                txtIssueNo.Text = driver.Licence.IssueNo;
                txtLicNo.Text = driver.Licence.CertificateNumber;
                txtValidDate.Text = String.Format("{0:yyyy-MM-dd} - {1:yyyy-MM-dd}",
                                                  driver.Licence.ValidFrom, driver.Licence.ValidUntil);
                if (driver.Licence.Classes != null)
                    txtVehicleCodes.Text = driver.Licence.Classes.Select(x => x.VehicleCode).Join(",");
            }
        }

        private void BindFromLoginForm(DriverModel driver)
        {
            foreach (var ctrl in _form.AllControls().OfType<MetroTextBox>())
            {
                var tag = ctrl.Tag;
                if (tag == null)
                    continue;
                if (tag.ToString().StartsWith("DriverLicence."))
                {
                    var propInfo = driver.Licence.GetType().GetProperties();
                    tag = tag.ToString().Replace("DriverLicence.", "");
                    foreach (var prop in propInfo)
                    {
                        if (prop.Name == tag.ToString())
                            prop.SetValue(driver.Licence, ctrl.Text);
                    }
                }
                if (tag.ToString().StartsWith("Driver."))
                {
                    var propInfo = driver.GetType().GetProperties();
                    tag = tag.ToString().Replace("Driver.", "");
                    foreach (var prop in propInfo)
                    {
                        if (prop.Name == tag.ToString())
                            prop.SetValue(driver, ctrl.Text);
                    }
                }
            }
            var parts = txtName.Text.Split(new[] {' '}, 2);
            driver.Initials = parts.Length > 1 ? parts[0] : "";
            driver.Surname = parts.Length > 1 ? parts[1] : parts[0];

            driver.DateOfBirth = txtBDate.Text == null ? DateTime.Parse(txtBDate.Text) : DateTime.MinValue;
            if (driver.DateOfBirth == DateTime.MinValue)
            {
                driver.DateOfBirth = null;
            }

            driver.Licence.ValidUntil = txtValidDate.Text == null ? DateTime.Parse(txtValidDate.Text) : DateTime.MinValue;

            if (driver.Licence.ValidUntil == DateTime.MinValue)
            {
                driver.Licence = null;
            }

            driver.Licence.DriverRestrictions = txtDriverRes.Text.Split(',').ToList();
            driver.Gender = cboGender.SelectedItem.ToString().ToEnum<Gender>();
            driver.Licence.Classes = txtVehicleCodes.Text.Split(',')
                                                    .Select(x => new DriverLicenceClassModel {VehicleCode = x})
                                                    .ToList();
            if (txtIssueDate.Text.HasValue())
            {
                if (driver.Licence.Classes.Count == 0)
                    driver.Licence.Classes.Add(new DriverLicenceClassModel());
                if (txtIssueDate.Text != null && txtIssueDate.Text.Trim() != "")
                {
                    driver.Licence.Classes[0].FirstIssueDate = DateTime.Parse(txtIssueDate.Text);
                }
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (!ValidateForm())
                return;

            var driver = ContextData.Driver;
            if (driver.Licence == null)
                driver.Licence = new DriverLicenceModel();
            BindFromLoginForm(driver);
            ContextData.SetDriver(driver);

            if (Next != null)
                Next(this, driver);
        }

        public event EventHandler<DriverModel> Next;

        private bool ValidateForm()
        {
            var error = GetError();
            if (error.IsEmpty())
                return true;

            lblStatus.Text = "Please fix the following error: " + error;
            return false;
        }

        private string GetError()
        {
            if (txtName.Text.IsEmpty())
                return "Name is required";
            if (txtIDNo.Text.IsEmpty())
                return "ID number is required";
            if (cboGender.SelectedItem == null)
                return "Gender is required";
            if (txtBDate.Text.IsEmpty())
                return "Date of birth is required";
            if (!txtBDate.Text.IsDate("yyyy-MM-dd"))
                return "Invalid date of birth";
            if (txtLicNo.Text.IsEmpty())
                return "Licence number is required";
            if (txtValidDate.Text.IsEmpty())
                return "Licence date is required";
            if (!txtValidDate.Text.IsDate("yyyy-MM-dd"))
                return "Invalid licence date";
            if (txtIssueDate.Text.HasValue() && !txtIssueDate.Text.IsDate("yyyy-MM-dd"))
                return "Invalid issue date";

            return null;
        }
    }
}