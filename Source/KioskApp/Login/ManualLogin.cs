﻿using System;
using Castle.Core;
using GalaSoft.MvvmLight.Messaging;
using MetroFramework.Controls;
using iRent.Classes;
using iRent2.Contracts.Enums;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Models;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;

namespace iRent.Login
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class ManualLogin : MetroUserControl
    {
        private readonly CountryService _countryService;
        private readonly DriverApi _driverApi;

        public ManualLogin(DriverApi driverApi, CountryService countryService)
        {
            _driverApi = driverApi;
            _countryService = countryService;
            UIFactory.CurrentControl = "ManualLogin";
            InitializeComponent();
        }

        private void Login_Load(object sender, EventArgs e)
        {
            UIFactory.CurrentControl = "ManualLogin";

            drpIdType.DisplayMember = "Description";
            drpIdType.ValueMember = "Value";
            drpIdType.DataSource = new[]
                {
                    new {Value = IdType.ID, Description = "ID Document"},
                    new {Value = IdType.PP, Description = "Passport"}
                };

            drpCountry.DisplayMember = "Name";
            drpCountry.ValueMember = "Code";
            drpCountry.DataSource = _countryService.Countries();
            drpCountry.SelectedValue = _countryService.DefaultCode;
        }
        
        public event EventHandler<DriverModel> Next;

        private void Proceed(DriverModel driver)
        {
            if (Next != null)
                Next(this, driver);
        }

        protected override void Dispose(bool disposing)
        {
            Messenger.Default.Unregister(this);
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private DriverModel GetDriver(string idNumber, IdType idType, string countryCode)
        {
            var driverId = new IdInfo(idNumber, idType, countryCode);
            var driver = _driverApi.GetByIdNo(driverId);
            if (driver == null)
                driver = new DriverModel {IdType = idType, IdCountry = countryCode, IdNumber = idNumber};
            ContextData.SetDriver(driver);
            return driver;
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            var driver = GetDriver(txtIDNo.Text, (IdType) drpIdType.SelectedValue, drpCountry.SelectedValue.ToString());
            Proceed(driver);
        }
    }
}