﻿//using System;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.Drawing;
//using System.IO;
//using System.Runtime.InteropServices;
//using System.Windows.Forms;
//using Castle.Core;
//using GalaSoft.MvvmLight.Messaging;
//using MetroFramework.Controls;
//using iRent.Classes;
//using iRent.Extensions;
//using iRent2.Contracts.Models;
//using iRentKiosk.Core.Models;
//using Timer = System.Timers.Timer;

//namespace iRent.Login
//{
//    [CastleComponent(Lifestyle = LifestyleType.Transient)]
//    public partial class LoginPassport : MetroUserControl
//    {
//        private readonly BackgroundWorker bw;
//        private readonly bool m_bIsIDCardLoaded;
//        private readonly Timer t = new Timer();
//        private MainForm form;

//        public LoginPassport()
//        {
//            InitializeComponent();

//            ApplicationWaitCursor.Cursor = Cursors.WaitCursor;
//            ApplicationWaitCursor.Delay = new TimeSpan(0, 0, 0, 0, 250);

//            bw = new BackgroundWorker();
//            bw.DoWork += bw_DoWork;
//            bw.RunWorkerCompleted += bw_RunWorkerCompleted;

//            //t.AutoReset = false;
//            //t.Elapsed += new System.Timers.ElapsedEventHandler(this.AutoClassAndRecognize);
//            //t.Interval = 200;
//            //t.Enabled = true;

//            //Messenger.Default.Register<NotificationMessage<ScannerMessageItem>>(
//            //    this,
//            //    up =>
//            //    {
//            //        if (up.Content.msg.ToString() == "driver")
//            //        {
//            //            SetUserName(up.Content.obj);
//            //        }
//            //    });
//            var EngineID = "5317480525913680254";
//            var arr = EngineID.ToCharArray();

//            int nRet;
//            nRet = MyDll.LoadLibrary("IDCard.DLL");
//            if (nRet != 0)
//            {
//                nRet = MyDll.InitIDCard(arr, 0, null);
//                if (nRet == 0)
//                {
//                    MyDll.SetSpecialAttribute(1, 1);
//                    m_bIsIDCardLoaded = true;
//                }
//            }
//            bw.RunWorkerAsync();
//        }

//        private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
//        {
//            var output = new Dictionary<int, string>();
//            output = (Dictionary<int, string>) e.Result;
//            if (output != null)
//            {
//                if (output.Count > 21)
//                {
//                    pictureBox1.SizeMode = PictureBoxSizeMode.CenterImage;
//                    pictureBox1.Image = Image.FromFile(output[output.Count]);
//                    txtPassportNumber.Text = output[1];
//                    txtBDate.Text = output[5];
//                    txtCountry.Text = output[7];
//                    txtExpiryDate.Text = output[6];
//                    txtIDNumber.Text = output[21];
//                    txtIssueDate.Text = output[16];
//                    txtName.Text = output[2];
//                    btnNext.Enabled = true;
//                    btnRetry.Enabled = true;
//                    lblStatus.Text = "Please click Next to continue or Retry";
//                }
//                else
//                {
//                    btnRetry.Enabled = true;
//                    btnNext.Enabled = true;
//                    lblStatus.Text = "An error has occured, please try again.";
//                }
//            }
//            else
//            {
//                btnNext.Enabled = true;
//                btnRetry.Enabled = true;
//                lblStatus.Text = "An error has occured, please try again.";
//            }
//        }

//        private void bw_DoWork(object sender, DoWorkEventArgs e)
//        {
//            var a = 0;
//            var output = new Dictionary<int, string>();

//            if (!m_bIsIDCardLoaded)
//            {
//                MessageBox.Show("Passport scanner is fauly!");
//                return;
//            }

//            while (a != 1)
//            {
//                a = MyDll.GetGrabSignalType();
//            }

//            var nRet = MyDll.AcquireImage(21);
//            if (nRet != 0)
//            {
//                //textBoxDisplayResult.Text = "Returen value:";
//                //textBoxDisplayResult.Text += nRet.ToString();
//                //textBoxDisplayResult.Text += "\r\n";
//                //textBoxDisplayResult.Text += "Image acquisition failure";
//            }

//            var nSubID = new int[1];
//            nSubID[0] = 0;
//            MyDll.AddIDCardID(1, nSubID, 1);
//            MyDll.AddIDCardID(2, nSubID, 1);
//            MyDll.AddIDCardID(3, nSubID, 1);
//            MyDll.AddIDCardID(4, nSubID, 1);
//            MyDll.AddIDCardID(5, nSubID, 1);
//            MyDll.AddIDCardID(6, nSubID, 1);
//            MyDll.AddIDCardID(7, nSubID, 1);
//            MyDll.AddIDCardID(9, nSubID, 1);
//            MyDll.AddIDCardID(10, nSubID, 1);
//            MyDll.AddIDCardID(11, nSubID, 1);
//            MyDll.AddIDCardID(12, nSubID, 1);
//            MyDll.AddIDCardID(13, nSubID, 1);
//            MyDll.AddIDCardID(14, nSubID, 1);
//            MyDll.AddIDCardID(15, nSubID, 1);
//            MyDll.AddIDCardID(16, nSubID, 1);
//            MyDll.AddIDCardID(1000, nSubID, 1);
//            MyDll.AddIDCardID(1001, nSubID, 1);
//            MyDll.AddIDCardID(1003, nSubID, 1);
//            MyDll.AddIDCardID(1004, nSubID, 1);
//            MyDll.AddIDCardID(1005, nSubID, 1);
//            MyDll.AddIDCardID(1107, nSubID, 1);
//            MyDll.AddIDCardID(1008, nSubID, 1);
//            MyDll.AddIDCardID(1009, nSubID, 1);
//            MyDll.AddIDCardID(1010, nSubID, 1);

//            nRet = MyDll.RecogIDCard();
//            if (nRet <= 0)
//            {
//                //textBoxDisplayResult.Text = "Return value:";
//                //textBoxDisplayResult.Text += nRet.ToString();
//                //textBoxDisplayResult.Text += "\r\n";
//                //textBoxDisplayResult.Text += "recognition failure";
//                //return;
//            }

//            var MAX_CH_NUM = 128;
//            var cArrFieldValue = new char[MAX_CH_NUM];
//            var cArrFieldName = new char[MAX_CH_NUM];
//            //textBoxDisplayResult.Text = "recognition successful\r\n";
//            for (var i = 1;; i++)
//            {
//                nRet = MyDll.GetRecogResult(i, cArrFieldValue, ref MAX_CH_NUM);
//                if (nRet == 3)
//                {
//                    break;
//                }
//                MyDll.GetFieldName(i, cArrFieldName, ref MAX_CH_NUM);

//                var FieldName = new String(cArrFieldName);
//                var FieldValue = new String(cArrFieldValue);
//                output.Add(i, FieldValue.Replace("\0", ""));
//            }

//            //save image
//            var count = output.Count;
//            var strRunPath = Application.StartupPath + "\\";
//            var strImgPath = strRunPath + "test.jpg";

//            var carrImgPath = strImgPath.ToCharArray();
//            nRet = MyDll.SaveImage(carrImgPath);
//            if (nRet != 0)
//            {
//                MessageBox.Show("Failed to save image.");
//            }

//            //save head image

//            var strHeadPath = strRunPath + "test_Head.jpg";
//            var carrHeadPath = strHeadPath.ToCharArray();
//            MyDll.SaveHeadImage(carrHeadPath);
//            output.Add(count + 1, strHeadPath);
//            e.Result = output;
//        }

//        private void AutoClassAndRecognize(object sender, EventArgs e)
//        {
//            var output = new Dictionary<int, string>();
//            if (!m_bIsIDCardLoaded)
//            {
//                t.Start();
//                return;
//            }
//            var a = MyDll.GetGrabSignalType();
//            if (a == 1)
//            {
//                var nRet = MyDll.AcquireImage(21);
//                if (nRet != 0)
//                {
//                    //textBoxDisplayResult.Text = "Returen value:";
//                    //textBoxDisplayResult.Text += nRet.ToString();
//                    //textBoxDisplayResult.Text += "\r\n";
//                    //textBoxDisplayResult.Text += "Image acquisition failure";
//                }

//                var nSubID = new int[1];
//                nSubID[0] = 0;
//                MyDll.AddIDCardID(1, nSubID, 1);
//                MyDll.AddIDCardID(2, nSubID, 1);
//                MyDll.AddIDCardID(3, nSubID, 1);
//                MyDll.AddIDCardID(4, nSubID, 1);
//                MyDll.AddIDCardID(5, nSubID, 1);
//                MyDll.AddIDCardID(6, nSubID, 1);
//                MyDll.AddIDCardID(7, nSubID, 1);
//                MyDll.AddIDCardID(9, nSubID, 1);
//                MyDll.AddIDCardID(10, nSubID, 1);
//                MyDll.AddIDCardID(11, nSubID, 1);
//                MyDll.AddIDCardID(12, nSubID, 1);
//                MyDll.AddIDCardID(13, nSubID, 1);
//                MyDll.AddIDCardID(14, nSubID, 1);
//                MyDll.AddIDCardID(15, nSubID, 1);
//                MyDll.AddIDCardID(16, nSubID, 1);
//                MyDll.AddIDCardID(1000, nSubID, 1);
//                MyDll.AddIDCardID(1001, nSubID, 1);
//                MyDll.AddIDCardID(1003, nSubID, 1);
//                MyDll.AddIDCardID(1004, nSubID, 1);
//                MyDll.AddIDCardID(1005, nSubID, 1);
//                MyDll.AddIDCardID(1107, nSubID, 1);
//                MyDll.AddIDCardID(1008, nSubID, 1);
//                MyDll.AddIDCardID(1009, nSubID, 1);
//                MyDll.AddIDCardID(1010, nSubID, 1);

//                nRet = MyDll.RecogIDCard();
//                if (nRet <= 0)
//                {
//                    //textBoxDisplayResult.Text = "Return value:";
//                    //textBoxDisplayResult.Text += nRet.ToString();
//                    //textBoxDisplayResult.Text += "\r\n";
//                    //textBoxDisplayResult.Text += "recognition failure";
//                    //return;
//                }

//                var MAX_CH_NUM = 128;
//                var cArrFieldValue = new char[MAX_CH_NUM];
//                var cArrFieldName = new char[MAX_CH_NUM];
//                //textBoxDisplayResult.Text = "recognition successful\r\n";
//                for (var i = 1;; i++)
//                {
//                    nRet = MyDll.GetRecogResult(i, cArrFieldValue, ref MAX_CH_NUM);
//                    if (nRet == 3)
//                    {
//                        break;
//                    }
//                    MyDll.GetFieldName(i, cArrFieldName, ref MAX_CH_NUM);

//                    var FieldName = new String(cArrFieldName);
//                    var FieldValue = new String(cArrFieldValue);
//                    output.Add(i, FieldValue.Replace("\0", ""));
//                }

//                //save image

//                var strRunPath = Application.StartupPath + "\\";
//                var strImgPath = strRunPath + "test.jpg";
//                var carrImgPath = strImgPath.ToCharArray();
//                nRet = MyDll.SaveImage(carrImgPath);
//                if (nRet != 0)
//                {
//                    MessageBox.Show("Failed to save image.");
//                    return;
//                }

//                //save head image

//                var strHeadPath = strRunPath + "test_Head.jpg";
//                var carrHeadPath = strHeadPath.ToCharArray();
//                MyDll.SaveHeadImage(carrHeadPath);
//                t.Stop();
//                Invoke((MethodInvoker) delegate
//                    {
//                        try
//                        {
//                            pictureBox1.Image = Image.FromFile(strHeadPath);
//                            txtPassportNumber.Text = output[1];
//                            txtBDate.Text = output[5];
//                            txtCountry.Text = output[7];
//                            txtExpiryDate.Text = output[6];
//                            txtIDNumber.Text = output[21];
//                            txtIssueDate.Text = output[16];
//                            txtName.Text = output[2];
//                        }
//                        catch (Exception ex)
//                        {
//                            lblStatus.Text = "An error has occured. Please try and scan your passport again.";
//                        }
//                    });


//                return;
//            }
//            else
//            {
//                t.Enabled = true;
//                t.Start();
//            }
//            //get image from device
//        }

//        public void ClearControls()
//        {
//            Messenger.Default.Register<NotificationMessage<ScannerMessageItem>>(
//                this,
//                up =>
//                    {
//                        if (up.Content.msg.ToString() == "driver")
//                        {
//                            SetUserName(up.Content.Driver);
//                        }
//                    });
//            UIFactory.CurrentControl = "Login";

//            try
//            {
//                txtBDate.Text = "";
//                txtPassportNumber.Text = "";
//                txtName.Text = "";
//                txtIssueDate.Text = "";
//                txtIDNumber.Text = "";
//                txtCountry.Text = "";
//                txtExpiryDate.Text = "";
//                pictureBox1.Image = null;
//                lblStatus.Text = "";
//            }
//            catch
//            {
//            }
//        }

//        private void CancelScanningHandler(object sender, EventArgs args)
//        {
//        }

//        private void doEnroll(object sender, DoWorkEventArgs args)
//        {
//            //args.Result = myFides.enrolAndVerify();
//        }

//        private bool doEnroll()
//        {
//            Invoke(new MethodInvoker(delegate { form.Enabled = false; }));

//            //return myFides.enrolAndVerify();
//            return false;
//        }

//        private void SetUserName(DriverModel driver)
//        {
//            form = ParentForm as MainForm;
//        }

//        private void loadprevious()
//        {
//        }

//        private void btnLAccept_Click(object sender, EventArgs e)
//        {
//            //if(validation)
//        }

//        public static byte[] ImageToByte(Image img)
//        {
//            var converter = new ImageConverter();
//            return (byte[]) converter.ConvertTo(img, typeof (byte[]));
//        }

//        public Image byteArrayToImage(byte[] byteArrayIn)
//        {
//            var ms = new MemoryStream(byteArrayIn);
//            var returnImage = Image.FromStream(ms);
//            return returnImage;
//        }

//        private void Login_ControlRemoved(object sender, ControlEventArgs e)
//        {
//            //Messenger.Default.Unregister(this);
//        }

//        protected override void Dispose(bool disposing)
//        {
//            if (disposing && (components != null))
//            {
//                components.Dispose();
//            }
//            base.Dispose(disposing);
//            Messenger.Default.Unregister(this);
//        }

//        private void btnNext_Click(object sender, EventArgs e)
//        {
//            if (txtPassportNumber.Text != "")
//            {
//                var form = ParentForm as MainForm;
//                //TODO: myHelper.SetDriver(txtPassportNumber.Text);
//                throw new NotImplementedException("Need to get driver from passport");
//                UIFactory.Accept = true;
//                UIFactory.NextControl = "FPLogin";
//                form.InvokeIfRequired(x => x.Accept());
//            }
//            else
//            {
//                lblStatus.Text = "Passport number invalid!";
//            }
//        }

//        private void Login_Load(object sender, EventArgs e)
//        {
//            UIFactory.CurrentControl = "Login";
//        }

//        private void btnRetry_Click(object sender, EventArgs e)
//        {
//            bw.RunWorkerAsync();
//            lblStatus.Text = "Please place your passport in the reader.";
//        }

//        private void metroPanel1_Paint(object sender, PaintEventArgs e)
//        {
//        }

//        //public static class MyDll
//        //{
//        //    [DllImport("kernel32", SetLastError = true)]
//        //    public static extern int LoadLibrary(string strDllName);

//        //    [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
//        //    public static extern int InitIDCard(char[] cArrUserID, int nType, char[] cArrDirectory);

//        //    [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
//        //    public static extern int GetRecogResult(int nIndex, char[] cArrBuffer, ref int nBufferLen);

//        //    [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
//        //    public static extern int RecogIDCard();

//        //    [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
//        //    public static extern int GetFieldName(int nIndex, char[] cArrBuffer, ref int nBufferLen);

//        //    [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
//        //    public static extern int AcquireImage(int nCardType);

//        //    [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
//        //    public static extern int SaveImage(char[] cArrFileName);

//        //    [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
//        //    public static extern int SaveHeadImage(char[] cArrFileName);


//        //    [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
//        //    public static extern int GetCurrentDevice(char[] cArrDeviceName, int nLength);

//        //    [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
//        //    public static extern void GetVersionInfo(char[] cArrVersion, int nLength);

//        //    [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
//        //    public static extern bool CheckDeviceOnline();

//        //    [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
//        //    public static extern bool SetAcquireImageType(int nLightType, int nImageType);

//        //    [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
//        //    public static extern bool SetUserDefinedImageSize(int nWidth, int nHeight);

//        //    [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
//        //    public static extern bool SetAcquireImageResolution(int nResolutionX, int nResolutionY);

//        //    [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
//        //    public static extern int SetIDCardID(int nMainID, int[] nSubID, int nSubIdCount);

//        //    [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
//        //    public static extern int AddIDCardID(int nMainID, int[] nSubID, int nSubIdCount);

//        //    [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
//        //    public static extern int RecogIDCardEX(int nMainID, int nSubID);

//        //    [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
//        //    public static extern int GetButtonDownType();

//        //    [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
//        //    public static extern int GetGrabSignalType();

//        //    [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
//        //    public static extern int SetSpecialAttribute(int nType, int nSet);

//        //    [DllImport("IDCard", CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Winapi)]
//        //    public static extern void FreeIDCard();
//        //}
//    }
//}