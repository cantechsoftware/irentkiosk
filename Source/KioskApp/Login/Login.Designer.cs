﻿namespace iRent.Login
{
    partial class Login
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>


        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login));
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.btnBack = new MetroFramework.Controls.MetroButton();
            this.btnNext = new MetroFramework.Controls.MetroButton();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.txtDriverRes = new MetroFramework.Controls.MetroTextBox();
            this.txtVehicleCodes = new MetroFramework.Controls.MetroTextBox();
            this.txtValidDate = new MetroFramework.Controls.MetroTextBox();
            this.txtGender = new MetroFramework.Controls.MetroTextBox();
            this.txtIssueNo = new MetroFramework.Controls.MetroTextBox();
            this.txtIssueDate = new MetroFramework.Controls.MetroTextBox();
            this.txtLicNo = new MetroFramework.Controls.MetroTextBox();
            this.txtBDate = new MetroFramework.Controls.MetroTextBox();
            this.txtIDNo = new MetroFramework.Controls.MetroTextBox();
            this.txtIniSurname = new MetroFramework.Controls.MetroTextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.axWindowsMediaPlayer1 = new AxWMPLib.AxWindowsMediaPlayer();
            this.lblStatus = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanel2.SuspendLayout();
            this.metroPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axWindowsMediaPlayer1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.Controls.Add(this.metroLabel1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnBack, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.btnNext, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.metroPanel1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.axWindowsMediaPlayer1, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblStatus, 0, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(720, 444);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.metroLabel1, 2);
            this.metroLabel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.XL;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel1.Location = new System.Drawing.Point(3, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(714, 50);
            this.metroLabel1.TabIndex = 109;
            this.metroLabel1.Text = "Please scan the \'Barcode\' of your drivers license";
            this.metroLabel1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnBack
            // 
            this.btnBack.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnBack.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnBack.Location = new System.Drawing.Point(3, 377);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(194, 64);
            this.btnBack.TabIndex = 90;
            this.btnBack.Text = "Back";
            this.btnBack.UseSelectable = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnNext
            // 
            this.btnNext.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnNext.Enabled = false;
            this.btnNext.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnNext.Location = new System.Drawing.Point(579, 377);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(138, 64);
            this.btnNext.TabIndex = 93;
            this.btnNext.Text = "Next";
            this.btnNext.UseSelectable = true;
            this.btnNext.Visible = false;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // metroPanel1
            // 
            this.metroPanel1.Controls.Add(this.metroLabel11);
            this.metroPanel1.Controls.Add(this.metroLabel10);
            this.metroPanel1.Controls.Add(this.metroLabel9);
            this.metroPanel1.Controls.Add(this.metroLabel8);
            this.metroPanel1.Controls.Add(this.metroLabel7);
            this.metroPanel1.Controls.Add(this.metroLabel6);
            this.metroPanel1.Controls.Add(this.metroLabel5);
            this.metroPanel1.Controls.Add(this.metroLabel4);
            this.metroPanel1.Controls.Add(this.metroLabel2);
            this.metroPanel1.Controls.Add(this.txtDriverRes);
            this.metroPanel1.Controls.Add(this.txtVehicleCodes);
            this.metroPanel1.Controls.Add(this.txtValidDate);
            this.metroPanel1.Controls.Add(this.txtGender);
            this.metroPanel1.Controls.Add(this.txtIssueNo);
            this.metroPanel1.Controls.Add(this.txtIssueDate);
            this.metroPanel1.Controls.Add(this.txtLicNo);
            this.metroPanel1.Controls.Add(this.txtBDate);
            this.metroPanel1.Controls.Add(this.txtIDNo);
            this.metroPanel1.Controls.Add(this.txtIniSurname);
            this.metroPanel1.Controls.Add(this.pictureBox1);
            this.metroPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(3, 53);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(570, 268);
            this.metroPanel1.TabIndex = 94;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.Location = new System.Drawing.Point(297, 65);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(71, 20);
            this.metroLabel11.TabIndex = 107;
            this.metroLabel11.Text = "Birth Date";
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(291, 36);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(78, 20);
            this.metroLabel10.TabIndex = 106;
            this.metroLabel10.Text = "ID Number";
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(252, 235);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(121, 20);
            this.metroLabel9.TabIndex = 105;
            this.metroLabel9.Text = "Driver Restrictions";
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(275, 206);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(97, 20);
            this.metroLabel8.TabIndex = 104;
            this.metroLabel8.Text = "Vehicle Codes";
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(329, 177);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(37, 20);
            this.metroLabel7.TabIndex = 103;
            this.metroLabel7.Text = "Valid";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(268, 148);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(104, 20);
            this.metroLabel6.TabIndex = 102;
            this.metroLabel6.Text = "Issued Number";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(290, 119);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(81, 20);
            this.metroLabel5.TabIndex = 101;
            this.metroLabel5.Text = "Issued Date";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(262, 90);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(112, 20);
            this.metroLabel4.TabIndex = 100;
            this.metroLabel4.Text = "License Number";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(239, 7);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(132, 20);
            this.metroLabel2.TabIndex = 99;
            this.metroLabel2.Text = "Initials and Surname";
            // 
            // txtDriverRes
            // 
            this.txtDriverRes.Lines = new string[0];
            this.txtDriverRes.Location = new System.Drawing.Point(371, 235);
            this.txtDriverRes.MaxLength = 32767;
            this.txtDriverRes.Name = "txtDriverRes";
            this.txtDriverRes.PasswordChar = '\0';
            this.txtDriverRes.ReadOnly = true;
            this.txtDriverRes.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtDriverRes.SelectedText = "";
            this.txtDriverRes.Size = new System.Drawing.Size(196, 23);
            this.txtDriverRes.TabIndex = 98;
            this.txtDriverRes.UseSelectable = true;
            // 
            // txtVehicleCodes
            // 
            this.txtVehicleCodes.Lines = new string[0];
            this.txtVehicleCodes.Location = new System.Drawing.Point(371, 206);
            this.txtVehicleCodes.MaxLength = 32767;
            this.txtVehicleCodes.Name = "txtVehicleCodes";
            this.txtVehicleCodes.PasswordChar = '\0';
            this.txtVehicleCodes.ReadOnly = true;
            this.txtVehicleCodes.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtVehicleCodes.SelectedText = "";
            this.txtVehicleCodes.Size = new System.Drawing.Size(196, 23);
            this.txtVehicleCodes.TabIndex = 97;
            this.txtVehicleCodes.UseSelectable = true;
            // 
            // txtValidDate
            // 
            this.txtValidDate.Lines = new string[0];
            this.txtValidDate.Location = new System.Drawing.Point(371, 177);
            this.txtValidDate.MaxLength = 32767;
            this.txtValidDate.Name = "txtValidDate";
            this.txtValidDate.PasswordChar = '\0';
            this.txtValidDate.ReadOnly = true;
            this.txtValidDate.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtValidDate.SelectedText = "";
            this.txtValidDate.Size = new System.Drawing.Size(196, 23);
            this.txtValidDate.TabIndex = 96;
            this.txtValidDate.UseSelectable = true;
            // 
            // txtGender
            // 
            this.txtGender.Lines = new string[0];
            this.txtGender.Location = new System.Drawing.Point(758, 23);
            this.txtGender.MaxLength = 32767;
            this.txtGender.Name = "txtGender";
            this.txtGender.PasswordChar = '\0';
            this.txtGender.ReadOnly = true;
            this.txtGender.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtGender.SelectedText = "";
            this.txtGender.Size = new System.Drawing.Size(76, 23);
            this.txtGender.TabIndex = 95;
            this.txtGender.UseSelectable = true;
            this.txtGender.Visible = false;
            // 
            // txtIssueNo
            // 
            this.txtIssueNo.Lines = new string[0];
            this.txtIssueNo.Location = new System.Drawing.Point(371, 148);
            this.txtIssueNo.MaxLength = 32767;
            this.txtIssueNo.Name = "txtIssueNo";
            this.txtIssueNo.PasswordChar = '\0';
            this.txtIssueNo.ReadOnly = true;
            this.txtIssueNo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtIssueNo.SelectedText = "";
            this.txtIssueNo.Size = new System.Drawing.Size(196, 23);
            this.txtIssueNo.TabIndex = 94;
            this.txtIssueNo.UseSelectable = true;
            // 
            // txtIssueDate
            // 
            this.txtIssueDate.Lines = new string[0];
            this.txtIssueDate.Location = new System.Drawing.Point(371, 119);
            this.txtIssueDate.MaxLength = 32767;
            this.txtIssueDate.Name = "txtIssueDate";
            this.txtIssueDate.PasswordChar = '\0';
            this.txtIssueDate.ReadOnly = true;
            this.txtIssueDate.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtIssueDate.SelectedText = "";
            this.txtIssueDate.Size = new System.Drawing.Size(196, 23);
            this.txtIssueDate.TabIndex = 93;
            this.txtIssueDate.UseSelectable = true;
            // 
            // txtLicNo
            // 
            this.txtLicNo.Lines = new string[0];
            this.txtLicNo.Location = new System.Drawing.Point(371, 90);
            this.txtLicNo.MaxLength = 32767;
            this.txtLicNo.Name = "txtLicNo";
            this.txtLicNo.PasswordChar = '\0';
            this.txtLicNo.ReadOnly = true;
            this.txtLicNo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtLicNo.SelectedText = "";
            this.txtLicNo.Size = new System.Drawing.Size(196, 23);
            this.txtLicNo.TabIndex = 92;
            this.txtLicNo.UseSelectable = true;
            // 
            // txtBDate
            // 
            this.txtBDate.Lines = new string[0];
            this.txtBDate.Location = new System.Drawing.Point(371, 61);
            this.txtBDate.MaxLength = 32767;
            this.txtBDate.Name = "txtBDate";
            this.txtBDate.PasswordChar = '\0';
            this.txtBDate.ReadOnly = true;
            this.txtBDate.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtBDate.SelectedText = "";
            this.txtBDate.Size = new System.Drawing.Size(196, 23);
            this.txtBDate.TabIndex = 91;
            this.txtBDate.UseSelectable = true;
            // 
            // txtIDNo
            // 
            this.txtIDNo.Lines = new string[0];
            this.txtIDNo.Location = new System.Drawing.Point(371, 32);
            this.txtIDNo.MaxLength = 32767;
            this.txtIDNo.Name = "txtIDNo";
            this.txtIDNo.PasswordChar = '\0';
            this.txtIDNo.ReadOnly = true;
            this.txtIDNo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtIDNo.SelectedText = "";
            this.txtIDNo.Size = new System.Drawing.Size(178, 23);
            this.txtIDNo.TabIndex = 90;
            this.txtIDNo.UseSelectable = true;
            // 
            // txtIniSurname
            // 
            this.txtIniSurname.Lines = new string[0];
            this.txtIniSurname.Location = new System.Drawing.Point(371, 3);
            this.txtIniSurname.MaxLength = 32767;
            this.txtIniSurname.Name = "txtIniSurname";
            this.txtIniSurname.PasswordChar = '\0';
            this.txtIniSurname.ReadOnly = true;
            this.txtIniSurname.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtIniSurname.SelectedText = "";
            this.txtIniSurname.Size = new System.Drawing.Size(196, 23);
            this.txtIniSurname.TabIndex = 89;
            this.txtIniSurname.UseSelectable = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(17, 7);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(216, 247);
            this.pictureBox1.TabIndex = 88;
            this.pictureBox1.TabStop = false;
            // 
            // axWindowsMediaPlayer1
            // 
            this.axWindowsMediaPlayer1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.axWindowsMediaPlayer1.Enabled = true;
            this.axWindowsMediaPlayer1.Location = new System.Drawing.Point(579, 53);
            this.axWindowsMediaPlayer1.Name = "axWindowsMediaPlayer1";
            this.axWindowsMediaPlayer1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axWindowsMediaPlayer1.OcxState")));
            this.axWindowsMediaPlayer1.Size = new System.Drawing.Size(138, 125);
            this.axWindowsMediaPlayer1.TabIndex = 96;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.lblStatus, 2);
            this.lblStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblStatus.Location = new System.Drawing.Point(3, 324);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(714, 50);
            this.lblStatus.TabIndex = 97;
            // 
            // Login
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSize = true;
            this.Controls.Add(this.tableLayoutPanel2);
            this.Name = "Login";
            this.Size = new System.Drawing.Size(720, 444);
            this.Load += new System.EventHandler(this.Login_Load);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axWindowsMediaPlayer1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private MetroFramework.Controls.MetroButton btnNext;
        private MetroFramework.Controls.MetroButton btnBack;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox txtDriverRes;
        private MetroFramework.Controls.MetroTextBox txtVehicleCodes;
        private MetroFramework.Controls.MetroTextBox txtValidDate;
        private MetroFramework.Controls.MetroTextBox txtGender;
        private MetroFramework.Controls.MetroTextBox txtIssueNo;
        private MetroFramework.Controls.MetroTextBox txtIssueDate;
        private MetroFramework.Controls.MetroTextBox txtLicNo;
        private MetroFramework.Controls.MetroTextBox txtBDate;
        private MetroFramework.Controls.MetroTextBox txtIDNo;
        private MetroFramework.Controls.MetroTextBox txtIniSurname;
        private System.Windows.Forms.PictureBox pictureBox1;
        private AxWMPLib.AxWindowsMediaPlayer axWindowsMediaPlayer1;
        private MetroFramework.Controls.MetroLabel lblStatus;
    }
}
