﻿//namespace iRent.Login
//{
//    partial class LoginPassport
//    {
//        /// <summary> 
//        /// Required designer variable.
//        /// </summary>
//        private System.ComponentModel.IContainer components = null;

//        /// <summary> 
//        /// Clean up any resources being used.
//        /// </summary>
//        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>


//        #region Component Designer generated code

//        /// <summary> 
//        /// Required method for Designer support - do not modify 
//        /// the contents of this method with the code editor.
//        /// </summary>
//        private void InitializeComponent()
//        {
//            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
//            this.btnRetry = new MetroFramework.Controls.MetroButton();
//            this.lblStatus = new MetroFramework.Controls.MetroLabel();
//            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
//            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
//            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
//            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
//            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
//            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
//            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
//            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
//            this.txtExpiryDate = new MetroFramework.Controls.MetroTextBox();
//            this.txtIDNumber = new MetroFramework.Controls.MetroTextBox();
//            this.txtIssueDate = new MetroFramework.Controls.MetroTextBox();
//            this.txtCountry = new MetroFramework.Controls.MetroTextBox();
//            this.txtBDate = new MetroFramework.Controls.MetroTextBox();
//            this.txtPassportNumber = new MetroFramework.Controls.MetroTextBox();
//            this.txtName = new MetroFramework.Controls.MetroTextBox();
//            this.pictureBox1 = new System.Windows.Forms.PictureBox();
//            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
//            this.btnNext = new MetroFramework.Controls.MetroButton();
//            this.tableLayoutPanel1.SuspendLayout();
//            this.metroPanel1.SuspendLayout();
//            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
//            this.SuspendLayout();
//            // 
//            // tableLayoutPanel1
//            // 
//            this.tableLayoutPanel1.ColumnCount = 3;
//            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
//            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
//            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
//            this.tableLayoutPanel1.Controls.Add(this.btnRetry, 0, 2);
//            this.tableLayoutPanel1.Controls.Add(this.lblStatus, 0, 1);
//            this.tableLayoutPanel1.Controls.Add(this.metroPanel1, 0, 0);
//            this.tableLayoutPanel1.Controls.Add(this.btnNext, 2, 2);
//            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
//            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
//            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
//            this.tableLayoutPanel1.RowCount = 3;
//            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
//            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
//            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
//            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
//            this.tableLayoutPanel1.Size = new System.Drawing.Size(654, 550);
//            this.tableLayoutPanel1.TabIndex = 0;
//            // 
//            // btnRetry
//            // 
//            this.btnRetry.Dock = System.Windows.Forms.DockStyle.Fill;
//            this.btnRetry.Enabled = false;
//            this.btnRetry.FontSize = MetroFramework.MetroButtonSize.XL;
//            this.btnRetry.Location = new System.Drawing.Point(3, 453);
//            this.btnRetry.Name = "btnRetry";
//            this.btnRetry.Size = new System.Drawing.Size(221, 94);
//            this.btnRetry.TabIndex = 89;
//            this.btnRetry.Text = "Retry";
//            this.btnRetry.UseSelectable = true;
//            this.btnRetry.Click += new System.EventHandler(this.btnRetry_Click);
//            // 
//            // lblStatus
//            // 
//            this.lblStatus.AutoSize = true;
//            this.tableLayoutPanel1.SetColumnSpan(this.lblStatus, 3);
//            this.lblStatus.Dock = System.Windows.Forms.DockStyle.Fill;
//            this.lblStatus.FontSize = MetroFramework.MetroLabelSize.XL;
//            this.lblStatus.FontWeight = MetroFramework.MetroLabelWeight.Bold;
//            this.lblStatus.Location = new System.Drawing.Point(3, 400);
//            this.lblStatus.Name = "lblStatus";
//            this.lblStatus.Size = new System.Drawing.Size(648, 50);
//            this.lblStatus.Style = MetroFramework.MetroColorStyle.Red;
//            this.lblStatus.TabIndex = 88;
//            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
//            this.lblStatus.UseStyleColors = true;
//            // 
//            // metroPanel1
//            // 
//            this.tableLayoutPanel1.SetColumnSpan(this.metroPanel1, 3);
//            this.metroPanel1.Controls.Add(this.metroLabel11);
//            this.metroPanel1.Controls.Add(this.metroLabel10);
//            this.metroPanel1.Controls.Add(this.metroLabel7);
//            this.metroPanel1.Controls.Add(this.metroLabel6);
//            this.metroPanel1.Controls.Add(this.metroLabel5);
//            this.metroPanel1.Controls.Add(this.metroLabel4);
//            this.metroPanel1.Controls.Add(this.metroLabel2);
//            this.metroPanel1.Controls.Add(this.txtExpiryDate);
//            this.metroPanel1.Controls.Add(this.txtIDNumber);
//            this.metroPanel1.Controls.Add(this.txtIssueDate);
//            this.metroPanel1.Controls.Add(this.txtCountry);
//            this.metroPanel1.Controls.Add(this.txtBDate);
//            this.metroPanel1.Controls.Add(this.txtPassportNumber);
//            this.metroPanel1.Controls.Add(this.txtName);
//            this.metroPanel1.Controls.Add(this.pictureBox1);
//            this.metroPanel1.Controls.Add(this.metroLabel1);
//            this.metroPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
//            this.metroPanel1.HorizontalScrollbarBarColor = true;
//            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
//            this.metroPanel1.HorizontalScrollbarSize = 10;
//            this.metroPanel1.Location = new System.Drawing.Point(3, 3);
//            this.metroPanel1.Name = "metroPanel1";
//            this.metroPanel1.Size = new System.Drawing.Size(648, 394);
//            this.metroPanel1.TabIndex = 0;
//            this.metroPanel1.VerticalScrollbarBarColor = true;
//            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
//            this.metroPanel1.VerticalScrollbarSize = 10;
//            this.metroPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.metroPanel1_Paint);
//            // 
//            // metroLabel11
//            // 
//            this.metroLabel11.AutoSize = true;
//            this.metroLabel11.Location = new System.Drawing.Point(297, 146);
//            this.metroLabel11.Name = "metroLabel11";
//            this.metroLabel11.Size = new System.Drawing.Size(68, 19);
//            this.metroLabel11.TabIndex = 86;
//            this.metroLabel11.Text = "Birth Date";
//            // 
//            // metroLabel10
//            // 
//            this.metroLabel10.AutoSize = true;
//            this.metroLabel10.Location = new System.Drawing.Point(253, 117);
//            this.metroLabel10.Name = "metroLabel10";
//            this.metroLabel10.Size = new System.Drawing.Size(112, 19);
//            this.metroLabel10.TabIndex = 85;
//            this.metroLabel10.Text = "Passport Number";
//            // 
//            // metroLabel7
//            // 
//            this.metroLabel7.AutoSize = true;
//            this.metroLabel7.Location = new System.Drawing.Point(289, 258);
//            this.metroLabel7.Name = "metroLabel7";
//            this.metroLabel7.Size = new System.Drawing.Size(76, 19);
//            this.metroLabel7.TabIndex = 82;
//            this.metroLabel7.Text = "Expiry Date";
//            // 
//            // metroLabel6
//            // 
//            this.metroLabel6.AutoSize = true;
//            this.metroLabel6.Location = new System.Drawing.Point(291, 229);
//            this.metroLabel6.Name = "metroLabel6";
//            this.metroLabel6.Size = new System.Drawing.Size(74, 19);
//            this.metroLabel6.TabIndex = 81;
//            this.metroLabel6.Text = "ID Number";
//            // 
//            // metroLabel5
//            // 
//            this.metroLabel5.AutoSize = true;
//            this.metroLabel5.Location = new System.Drawing.Point(290, 200);
//            this.metroLabel5.Name = "metroLabel5";
//            this.metroLabel5.Size = new System.Drawing.Size(75, 19);
//            this.metroLabel5.TabIndex = 80;
//            this.metroLabel5.Text = "Issued Date";
//            // 
//            // metroLabel4
//            // 
//            this.metroLabel4.AutoSize = true;
//            this.metroLabel4.Location = new System.Drawing.Point(267, 171);
//            this.metroLabel4.Name = "metroLabel4";
//            this.metroLabel4.Size = new System.Drawing.Size(98, 19);
//            this.metroLabel4.TabIndex = 79;
//            this.metroLabel4.Text = "Issuing Country";
//            // 
//            // metroLabel2
//            // 
//            this.metroLabel2.AutoSize = true;
//            this.metroLabel2.Location = new System.Drawing.Point(247, 88);
//            this.metroLabel2.Name = "metroLabel2";
//            this.metroLabel2.Size = new System.Drawing.Size(118, 19);
//            this.metroLabel2.TabIndex = 78;
//            this.metroLabel2.Text = "Name on Passport";
//            // 
//            // txtExpiryDate
//            // 
//            this.txtExpiryDate.Lines = new string[0];
//            this.txtExpiryDate.Location = new System.Drawing.Point(371, 258);
//            this.txtExpiryDate.MaxLength = 32767;
//            this.txtExpiryDate.Name = "txtExpiryDate";
//            this.txtExpiryDate.PasswordChar = '\0';
//            this.txtExpiryDate.ReadOnly = true;
//            this.txtExpiryDate.ScrollBars = System.Windows.Forms.ScrollBars.None;
//            this.txtExpiryDate.SelectedText = "";
//            this.txtExpiryDate.Size = new System.Drawing.Size(260, 23);
//            this.txtExpiryDate.TabIndex = 75;
//            this.txtExpiryDate.UseSelectable = true;
//            // 
//            // txtIDNumber
//            // 
//            this.txtIDNumber.Lines = new string[0];
//            this.txtIDNumber.Location = new System.Drawing.Point(371, 229);
//            this.txtIDNumber.MaxLength = 32767;
//            this.txtIDNumber.Name = "txtIDNumber";
//            this.txtIDNumber.PasswordChar = '\0';
//            this.txtIDNumber.ReadOnly = true;
//            this.txtIDNumber.ScrollBars = System.Windows.Forms.ScrollBars.None;
//            this.txtIDNumber.SelectedText = "";
//            this.txtIDNumber.Size = new System.Drawing.Size(260, 23);
//            this.txtIDNumber.TabIndex = 73;
//            this.txtIDNumber.UseSelectable = true;
//            // 
//            // txtIssueDate
//            // 
//            this.txtIssueDate.Lines = new string[0];
//            this.txtIssueDate.Location = new System.Drawing.Point(371, 200);
//            this.txtIssueDate.MaxLength = 32767;
//            this.txtIssueDate.Name = "txtIssueDate";
//            this.txtIssueDate.PasswordChar = '\0';
//            this.txtIssueDate.ReadOnly = true;
//            this.txtIssueDate.ScrollBars = System.Windows.Forms.ScrollBars.None;
//            this.txtIssueDate.SelectedText = "";
//            this.txtIssueDate.Size = new System.Drawing.Size(260, 23);
//            this.txtIssueDate.TabIndex = 72;
//            this.txtIssueDate.UseSelectable = true;
//            // 
//            // txtCountry
//            // 
//            this.txtCountry.Lines = new string[0];
//            this.txtCountry.Location = new System.Drawing.Point(371, 171);
//            this.txtCountry.MaxLength = 32767;
//            this.txtCountry.Name = "txtCountry";
//            this.txtCountry.PasswordChar = '\0';
//            this.txtCountry.ReadOnly = true;
//            this.txtCountry.ScrollBars = System.Windows.Forms.ScrollBars.None;
//            this.txtCountry.SelectedText = "";
//            this.txtCountry.Size = new System.Drawing.Size(260, 23);
//            this.txtCountry.TabIndex = 71;
//            this.txtCountry.UseSelectable = true;
//            // 
//            // txtBDate
//            // 
//            this.txtBDate.Lines = new string[0];
//            this.txtBDate.Location = new System.Drawing.Point(371, 142);
//            this.txtBDate.MaxLength = 32767;
//            this.txtBDate.Name = "txtBDate";
//            this.txtBDate.PasswordChar = '\0';
//            this.txtBDate.ReadOnly = true;
//            this.txtBDate.ScrollBars = System.Windows.Forms.ScrollBars.None;
//            this.txtBDate.SelectedText = "";
//            this.txtBDate.Size = new System.Drawing.Size(260, 23);
//            this.txtBDate.TabIndex = 70;
//            this.txtBDate.UseSelectable = true;
//            // 
//            // txtPassportNumber
//            // 
//            this.txtPassportNumber.Lines = new string[0];
//            this.txtPassportNumber.Location = new System.Drawing.Point(371, 113);
//            this.txtPassportNumber.MaxLength = 32767;
//            this.txtPassportNumber.Name = "txtPassportNumber";
//            this.txtPassportNumber.PasswordChar = '\0';
//            this.txtPassportNumber.ReadOnly = true;
//            this.txtPassportNumber.ScrollBars = System.Windows.Forms.ScrollBars.None;
//            this.txtPassportNumber.SelectedText = "";
//            this.txtPassportNumber.Size = new System.Drawing.Size(260, 23);
//            this.txtPassportNumber.TabIndex = 69;
//            this.txtPassportNumber.UseSelectable = true;
//            // 
//            // txtName
//            // 
//            this.txtName.Lines = new string[0];
//            this.txtName.Location = new System.Drawing.Point(371, 84);
//            this.txtName.MaxLength = 32767;
//            this.txtName.Name = "txtName";
//            this.txtName.PasswordChar = '\0';
//            this.txtName.ReadOnly = true;
//            this.txtName.ScrollBars = System.Windows.Forms.ScrollBars.None;
//            this.txtName.SelectedText = "";
//            this.txtName.Size = new System.Drawing.Size(260, 23);
//            this.txtName.TabIndex = 68;
//            this.txtName.UseSelectable = true;
//            // 
//            // pictureBox1
//            // 
//            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
//            this.pictureBox1.Location = new System.Drawing.Point(17, 88);
//            this.pictureBox1.Name = "pictureBox1";
//            this.pictureBox1.Size = new System.Drawing.Size(216, 247);
//            this.pictureBox1.TabIndex = 67;
//            this.pictureBox1.TabStop = false;
//            // 
//            // metroLabel1
//            // 
//            this.metroLabel1.AutoSize = true;
//            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.XL;
//            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Bold;
//            this.metroLabel1.Location = new System.Drawing.Point(41, 19);
//            this.metroLabel1.Name = "metroLabel1";
//            this.metroLabel1.Size = new System.Drawing.Size(555, 30);
//            this.metroLabel1.TabIndex = 66;
//            this.metroLabel1.Text = "Please insert your passport into the passport reader";
//            // 
//            // btnNext
//            // 
//            this.btnNext.Dock = System.Windows.Forms.DockStyle.Fill;
//            this.btnNext.Enabled = false;
//            this.btnNext.FontSize = MetroFramework.MetroButtonSize.XL;
//            this.btnNext.Location = new System.Drawing.Point(430, 453);
//            this.btnNext.Name = "btnNext";
//            this.btnNext.Size = new System.Drawing.Size(221, 94);
//            this.btnNext.TabIndex = 1;
//            this.btnNext.Text = "Next";
//            this.btnNext.UseSelectable = true;
//            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
//            // 
//            // LoginPassport
//            // 
//            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
//            this.AutoSize = true;
//            this.Controls.Add(this.tableLayoutPanel1);
//            this.Name = "LoginPassport";
//            this.Size = new System.Drawing.Size(654, 550);
//            this.Load += new System.EventHandler(this.Login_Load);
//            this.ControlRemoved += new System.Windows.Forms.ControlEventHandler(this.Login_ControlRemoved);
//            this.tableLayoutPanel1.ResumeLayout(false);
//            this.tableLayoutPanel1.PerformLayout();
//            this.metroPanel1.ResumeLayout(false);
//            this.metroPanel1.PerformLayout();
//            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
//            this.ResumeLayout(false);

//        }

//        #endregion

//        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
//        private MetroFramework.Controls.MetroPanel metroPanel1;
//        private MetroFramework.Controls.MetroButton btnNext;
//        private MetroFramework.Controls.MetroLabel metroLabel11;
//        private MetroFramework.Controls.MetroLabel metroLabel10;
//        private MetroFramework.Controls.MetroLabel metroLabel7;
//        private MetroFramework.Controls.MetroLabel metroLabel6;
//        private MetroFramework.Controls.MetroLabel metroLabel5;
//        private MetroFramework.Controls.MetroLabel metroLabel4;
//        private MetroFramework.Controls.MetroLabel metroLabel2;
//        private MetroFramework.Controls.MetroTextBox txtExpiryDate;
//        private MetroFramework.Controls.MetroTextBox txtIDNumber;
//        private MetroFramework.Controls.MetroTextBox txtIssueDate;
//        private MetroFramework.Controls.MetroTextBox txtCountry;
//        private MetroFramework.Controls.MetroTextBox txtBDate;
//        private MetroFramework.Controls.MetroTextBox txtPassportNumber;
//        private MetroFramework.Controls.MetroTextBox txtName;
//        private System.Windows.Forms.PictureBox pictureBox1;
//        private MetroFramework.Controls.MetroLabel metroLabel1;
//        private MetroFramework.Controls.MetroLabel lblStatus;
//        private MetroFramework.Controls.MetroButton btnRetry;


//    }
//}
