﻿using System.Drawing;
using MetroFramework;
using iRent.Properties;

namespace iRent.Login
{
    partial class ManualLicence
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>


        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblStatus = new MetroFramework.Controls.MetroLabel();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.lblLicNo = new MetroFramework.Controls.MetroLabel();
            this.lblName = new MetroFramework.Controls.MetroLabel();
            this.txtDriverRes = new MetroFramework.Controls.MetroTextBox();
            this.lblBirthDate = new MetroFramework.Controls.MetroLabel();
            this.txtBDate = new MetroFramework.Controls.MetroTextBox();
            this.lblIDNo = new MetroFramework.Controls.MetroLabel();
            this.txtIDNo = new MetroFramework.Controls.MetroTextBox();
            this.lblDriverRes = new MetroFramework.Controls.MetroLabel();
            this.lblVehicleCodes = new MetroFramework.Controls.MetroLabel();
            this.lblValid = new MetroFramework.Controls.MetroLabel();
            this.lblIssueNo = new MetroFramework.Controls.MetroLabel();
            this.lblIssueDate = new MetroFramework.Controls.MetroLabel();
            this.txtVehicleCodes = new MetroFramework.Controls.MetroTextBox();
            this.txtValidDate = new MetroFramework.Controls.MetroTextBox();
            this.txtIssueNo = new MetroFramework.Controls.MetroTextBox();
            this.txtIssueDate = new MetroFramework.Controls.MetroTextBox();
            this.txtLicNo = new MetroFramework.Controls.MetroTextBox();
            this.txtName = new MetroFramework.Controls.MetroTextBox();
            this.cboGender = new MetroFramework.Controls.MetroComboBox();
            this.lblGender = new MetroFramework.Controls.MetroLabel();
            this.lblTitle = new MetroFramework.Controls.MetroLabel();
            this.btnNext = new MetroFramework.Controls.MetroButton();
            this.tableLayoutPanel1.SuspendLayout();
            this.metroPanel1.SuspendLayout();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.lblStatus, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.metroPanel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnNext, 1, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(654, 550);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // lblStatus
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.lblStatus, 3);
            this.lblStatus.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblStatus.FontSize = MetroFramework.MetroLabelSize.XL;
            this.lblStatus.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblStatus.ForeColor = System.Drawing.Color.Red;
            this.lblStatus.Location = new System.Drawing.Point(3, 410);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(648, 40);
            this.lblStatus.Style = MetroFramework.MetroColorStyle.Red;
            this.lblStatus.TabIndex = 88;
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblStatus.UseStyleColors = true;
            // 
            // metroPanel1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.metroPanel1, 3);
            this.metroPanel1.Controls.Add(this.tableLayoutPanel);
            this.metroPanel1.Controls.Add(this.lblTitle);
            this.metroPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(3, 3);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(648, 394);
            this.metroPanel1.TabIndex = 0;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel.ColumnCount = 5;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.Controls.Add(this.lblLicNo, 1, 4);
            this.tableLayoutPanel.Controls.Add(this.lblName, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.txtDriverRes, 2, 9);
            this.tableLayoutPanel.Controls.Add(this.lblBirthDate, 1, 3);
            this.tableLayoutPanel.Controls.Add(this.txtBDate, 2, 3);
            this.tableLayoutPanel.Controls.Add(this.lblIDNo, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.txtIDNo, 2, 1);
            this.tableLayoutPanel.Controls.Add(this.lblDriverRes, 1, 9);
            this.tableLayoutPanel.Controls.Add(this.lblVehicleCodes, 1, 8);
            this.tableLayoutPanel.Controls.Add(this.lblValid, 1, 7);
            this.tableLayoutPanel.Controls.Add(this.lblIssueNo, 1, 6);
            this.tableLayoutPanel.Controls.Add(this.lblIssueDate, 1, 5);
            this.tableLayoutPanel.Controls.Add(this.txtVehicleCodes, 2, 8);
            this.tableLayoutPanel.Controls.Add(this.txtValidDate, 2, 7);
            this.tableLayoutPanel.Controls.Add(this.txtIssueNo, 2, 6);
            this.tableLayoutPanel.Controls.Add(this.txtIssueDate, 2, 5);
            this.tableLayoutPanel.Controls.Add(this.txtLicNo, 2, 4);
            this.tableLayoutPanel.Controls.Add(this.txtName, 2, 0);
            this.tableLayoutPanel.Controls.Add(this.cboGender, 2, 2);
            this.tableLayoutPanel.Controls.Add(this.lblGender, 1, 2);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 45);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.Padding = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel.RowCount = 9;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(648, 349);
            this.tableLayoutPanel.TabIndex = 87;
            // 
            // lblLicNo
            // 
            this.lblLicNo.Location = new System.Drawing.Point(117, 142);
            this.lblLicNo.Name = "lblLicNo";
            this.lblLicNo.Size = new System.Drawing.Size(194, 35);
            this.lblLicNo.TabIndex = 79;
            this.lblLicNo.Text = "License Number";
            this.lblLicNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblName
            // 
            this.lblName.Location = new System.Drawing.Point(117, 2);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(194, 35);
            this.lblName.TabIndex = 78;
            this.lblName.Text = "Name on drivers card";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtDriverRes
            // 
            this.txtDriverRes.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txtDriverRes.Lines = new string[0];
            this.txtDriverRes.Location = new System.Drawing.Point(317, 320);
            this.txtDriverRes.MaxLength = 32767;
            this.txtDriverRes.Name = "txtDriverRes";
            this.txtDriverRes.PasswordChar = '\0';
            this.txtDriverRes.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtDriverRes.SelectedText = "";
            this.txtDriverRes.Size = new System.Drawing.Size(194, 29);
            this.txtDriverRes.TabIndex = 77;
            this.txtDriverRes.UseSelectable = true;
            // 
            // lblBirthDate
            // 
            this.lblBirthDate.Location = new System.Drawing.Point(117, 107);
            this.lblBirthDate.Name = "lblBirthDate";
            this.lblBirthDate.Size = new System.Drawing.Size(194, 35);
            this.lblBirthDate.TabIndex = 86;
            this.lblBirthDate.Text = "Birth Date";
            this.lblBirthDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtBDate
            // 
            this.txtBDate.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txtBDate.Lines = new string[0];
            this.txtBDate.Location = new System.Drawing.Point(317, 110);
            this.txtBDate.MaxLength = 32767;
            this.txtBDate.MinimumSize = new System.Drawing.Size(0, 29);
            this.txtBDate.Name = "txtBDate";
            this.txtBDate.PasswordChar = '\0';
            this.txtBDate.PromptText = "yyyy-MM-dd";
            this.txtBDate.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtBDate.SelectedText = "";
            this.txtBDate.Size = new System.Drawing.Size(194, 29);
            this.txtBDate.TabIndex = 70;
            this.txtBDate.UseSelectable = true;
            // 
            // lblIDNo
            // 
            this.lblIDNo.Location = new System.Drawing.Point(117, 37);
            this.lblIDNo.Name = "lblIDNo";
            this.lblIDNo.Size = new System.Drawing.Size(194, 35);
            this.lblIDNo.TabIndex = 85;
            this.lblIDNo.Text = "ID Number";
            this.lblIDNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtIDNo
            // 
            this.txtIDNo.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            
            this.txtIDNo.Lines = new string[0];
            this.txtIDNo.Location = new System.Drawing.Point(317, 40);
            this.txtIDNo.MaxLength = 32767;
            this.txtIDNo.Name = "txtIDNo";
            this.txtIDNo.PasswordChar = '\0';
            this.txtIDNo.ReadOnly = true;
            this.txtIDNo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtIDNo.SelectedText = "";
            this.txtIDNo.Size = new System.Drawing.Size(194, 29);
            this.txtIDNo.TabIndex = 69;
            this.txtIDNo.Tag = "Driver.IdNumber";
            this.txtIDNo.UseSelectable = true;
            // 
            // lblDriverRes
            // 
            this.lblDriverRes.Location = new System.Drawing.Point(117, 317);
            this.lblDriverRes.Name = "lblDriverRes";
            this.lblDriverRes.Size = new System.Drawing.Size(194, 35);
            this.lblDriverRes.TabIndex = 84;
            this.lblDriverRes.Text = "Driver Restrictions";
            this.lblDriverRes.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblVehicleCodes
            // 
            this.lblVehicleCodes.Location = new System.Drawing.Point(117, 282);
            this.lblVehicleCodes.Name = "lblVehicleCodes";
            this.lblVehicleCodes.Size = new System.Drawing.Size(194, 35);
            this.lblVehicleCodes.TabIndex = 83;
            this.lblVehicleCodes.Text = "Vehicle Codes";
            this.lblVehicleCodes.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblValid
            // 
            this.lblValid.Location = new System.Drawing.Point(117, 247);
            this.lblValid.Name = "lblValid";
            this.lblValid.Size = new System.Drawing.Size(194, 35);
            this.lblValid.TabIndex = 82;
            this.lblValid.Text = "Valid";
            this.lblValid.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblIssueNo
            // 
            this.lblIssueNo.Location = new System.Drawing.Point(117, 212);
            this.lblIssueNo.Name = "lblIssueNo";
            this.lblIssueNo.Size = new System.Drawing.Size(194, 35);
            this.lblIssueNo.TabIndex = 81;
            this.lblIssueNo.Text = "Issued Number";
            this.lblIssueNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblIssueDate
            // 
            this.lblIssueDate.Location = new System.Drawing.Point(117, 177);
            this.lblIssueDate.Name = "lblIssueDate";
            this.lblIssueDate.Size = new System.Drawing.Size(194, 35);
            this.lblIssueDate.TabIndex = 80;
            this.lblIssueDate.Text = "Issued Date";
            this.lblIssueDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtVehicleCodes
            // 
            this.txtVehicleCodes.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txtVehicleCodes.Lines = new string[0];
            this.txtVehicleCodes.Location = new System.Drawing.Point(317, 285);
            this.txtVehicleCodes.MaxLength = 32767;
            this.txtVehicleCodes.Name = "txtVehicleCodes";
            this.txtVehicleCodes.PasswordChar = '\0';
            this.txtVehicleCodes.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtVehicleCodes.SelectedText = "";
            this.txtVehicleCodes.Size = new System.Drawing.Size(194, 29);
            this.txtVehicleCodes.TabIndex = 76;
            this.txtVehicleCodes.UseSelectable = true;
            // 
            // txtValidDate
            // 
            this.txtValidDate.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txtValidDate.Lines = new string[0];
            this.txtValidDate.Location = new System.Drawing.Point(317, 250);
            this.txtValidDate.MaxLength = 32767;
            this.txtValidDate.Name = "txtValidDate";
            this.txtValidDate.PasswordChar = '\0';
            this.txtValidDate.PromptText = "yyyy-MM-dd";
            this.txtValidDate.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtValidDate.SelectedText = "";
            this.txtValidDate.Size = new System.Drawing.Size(194, 29);
            this.txtValidDate.TabIndex = 75;
            this.txtValidDate.UseSelectable = true;
            // 
            // txtIssueNo
            // 
            this.txtIssueNo.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txtIssueNo.Lines = new string[0];
            this.txtIssueNo.Location = new System.Drawing.Point(317, 215);
            this.txtIssueNo.MaxLength = 32767;
            this.txtIssueNo.Name = "txtIssueNo";
            this.txtIssueNo.PasswordChar = '\0';
            this.txtIssueNo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtIssueNo.SelectedText = "";
            this.txtIssueNo.Size = new System.Drawing.Size(194, 29);
            this.txtIssueNo.TabIndex = 73;
            this.txtIssueNo.Tag = "DriverLicence.IssueNo";
            this.txtIssueNo.UseSelectable = true;
            // 
            // txtIssueDate
            // 
            this.txtIssueDate.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txtIssueDate.Lines = new string[0];
            this.txtIssueDate.Location = new System.Drawing.Point(317, 180);
            this.txtIssueDate.MaxLength = 32767;
            this.txtIssueDate.MinimumSize = new System.Drawing.Size(0, 29);
            this.txtIssueDate.Name = "txtIssueDate";
            this.txtIssueDate.PasswordChar = '\0';
            this.txtIssueDate.PromptText = "yyyy-MM-dd";
            this.txtIssueDate.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtIssueDate.SelectedText = "";
            this.txtIssueDate.Size = new System.Drawing.Size(194, 29);
            this.txtIssueDate.TabIndex = 72;
            this.txtIssueDate.UseSelectable = true;
            // 
            // txtLicNo
            // 
            this.txtLicNo.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txtLicNo.Lines = new string[0];
            this.txtLicNo.Location = new System.Drawing.Point(317, 145);
            this.txtLicNo.MaxLength = 32767;
            this.txtLicNo.Name = "txtLicNo";
            this.txtLicNo.PasswordChar = '\0';
            this.txtLicNo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtLicNo.SelectedText = "";
            this.txtLicNo.Size = new System.Drawing.Size(194, 29);
            this.txtLicNo.TabIndex = 71;
            this.txtLicNo.Tag = "DriverLicence.CertificateNumber";
            this.txtLicNo.UseSelectable = true;
            // 
            // txtName
            // 
            this.txtName.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            
            this.txtName.Lines = new string[0];
            this.txtName.Location = new System.Drawing.Point(317, 5);
            this.txtName.MaxLength = 32767;
            this.txtName.Name = "txtName";
            this.txtName.PasswordChar = '\0';
            this.txtName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtName.SelectedText = "";
            this.txtName.Size = new System.Drawing.Size(194, 29);
            this.txtName.TabIndex = 68;
            this.txtName.Tag = "DriverLicence.DriverFullName";
            this.txtName.UseSelectable = true;
            // 
            // cboGender
            // 
            this.cboGender.ItemHeight = 23;
            this.cboGender.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.cboGender.Location = new System.Drawing.Point(312, 75);
            this.cboGender.MaxLength = 32767;
            this.cboGender.Name = "cboGender";
            this.cboGender.Size = new System.Drawing.Size(194, 29);
            this.cboGender.TabIndex = 74;
            this.cboGender.UseSelectable = true;
            // 
            // lblGender
            // 
            this.lblGender.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblGender.FontSize = MetroFramework.MetroLabelSize.Small;
            this.lblGender.Location = new System.Drawing.Point(117, 72);
            this.lblGender.Name = "lblTitle";
            this.lblGender.Size = new System.Drawing.Size(194, 35);
            this.lblGender.TabIndex = 66;
            this.lblGender.Text = "Gender";
            this.lblGender.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblTitle
            // 
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitle.FontSize = MetroFramework.MetroLabelSize.XL;
            this.lblTitle.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(648, 45);
            this.lblTitle.TabIndex = 66;
            this.lblTitle.Text = "Please enter the details for your driver\'s license or passport";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnNext
            // 
            this.btnNext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNext.Enabled = false;
            this.btnNext.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnNext.Location = new System.Drawing.Point(230, 453);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(194, 94);
            this.btnNext.TabIndex = 1;
            this.btnNext.Text = "Next";
            this.btnNext.UseSelectable = true;
            this.btnNext.Visible = false;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // ManualLicence
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSize = true;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "ManualLicence";
            this.Size = new System.Drawing.Size(654, 550);
            this.Load += new System.EventHandler(this.Login_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.metroPanel1.ResumeLayout(false);
            this.tableLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroButton btnNext;
        private MetroFramework.Controls.MetroLabel lblBirthDate;
        private MetroFramework.Controls.MetroLabel lblIDNo;
        private MetroFramework.Controls.MetroLabel lblDriverRes;
        private MetroFramework.Controls.MetroLabel lblVehicleCodes;
        private MetroFramework.Controls.MetroLabel lblValid;
        private MetroFramework.Controls.MetroLabel lblIssueNo;
        private MetroFramework.Controls.MetroLabel lblIssueDate;
        private MetroFramework.Controls.MetroLabel lblLicNo;
        private MetroFramework.Controls.MetroLabel lblName;
        private MetroFramework.Controls.MetroTextBox txtDriverRes;
        private MetroFramework.Controls.MetroTextBox txtVehicleCodes;
        private MetroFramework.Controls.MetroTextBox txtValidDate;
        private MetroFramework.Controls.MetroComboBox cboGender;
        private MetroFramework.Controls.MetroTextBox txtIssueNo;
        private MetroFramework.Controls.MetroTextBox txtIssueDate;
        private MetroFramework.Controls.MetroTextBox txtLicNo;
        private MetroFramework.Controls.MetroTextBox txtBDate;
        private MetroFramework.Controls.MetroTextBox txtIDNo;
        private MetroFramework.Controls.MetroTextBox txtName;
        private MetroFramework.Controls.MetroLabel lblTitle;
        private MetroFramework.Controls.MetroLabel lblStatus;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private MetroFramework.Controls.MetroLabel lblGender;        
    }
}
