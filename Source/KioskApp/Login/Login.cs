﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using CanTech.Common.Extensions;
using Castle.Core;
using GalaSoft.MvvmLight.Messaging;
using MetroFramework.Controls;
using iRent.Classes;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Models;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;
using iRent.Extensions;

namespace iRent.Login
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class Login : MetroUserControl
    {
        private readonly DataApi _dataApi;
        private readonly DriverApi _driverApi;
        public string Filename { get; set; }
        private int playcount;

        public Login(DataApi dataApi, DriverApi driverApi)
        {
            _dataApi = dataApi;
            _driverApi = driverApi;
            UIFactory.CurrentControl = "Login";
            InitializeComponent();

            Messenger.Default.Register<NotificationMessage<ScannerMessageItem>>(
                this,
                up =>
                    {
                        if (up.Content.msg.ToString() == "driver")
                        {
                            this.InvokeIfRequired(x => BindToLoginForm(up.Content.Driver));
                            Thread.Sleep(5000);
                            this.InvokeIfRequired(x => Process(up.Content.Driver));
                        }
                    });
        }

        private void axWindowsMediaPlayer1_PlayStateChange(object sender, AxWMPLib._WMPOCXEvents_PlayStateChangeEvent e)
        {
            if  (e.newState == 8) // MediaEnded // call function to play the video again     
            {
                axWindowsMediaPlayer1.Ctlcontrols.currentPosition = 0;
                //if (playcount > 0)
                //{
                //    axWindowsMediaPlayer1.Visible = false;
                //    axWindowsMediaPlayer1.Ctlcontrols.stop();
                //}
                //playcount++;
            }
        }

        private void Login_Load(object sender, EventArgs e)
        {
            playcount = 0;
            axWindowsMediaPlayer1.Visible = true;
            UIFactory.CurrentControl = "Login";
            axWindowsMediaPlayer1.PlayStateChange += axWindowsMediaPlayer1_PlayStateChange;
            axWindowsMediaPlayer1.uiMode = "None";
            Filename = Environment.CurrentDirectory + @"\Documents\Licence scanning 300x300.mp4";
            axWindowsMediaPlayer1.URL = Filename;
            axWindowsMediaPlayer1.settings.setMode("loop", true);
            axWindowsMediaPlayer1.Ctlcontrols.play();

#if DEBUG
            txtIDNo.ReadOnly = false;
            txtIDNo.KeyUp += txtIDNo_KeyUp;
#endif
        }

        private void txtIDNo_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
                Prepopulate(txtIDNo.Text);
        }

        public void ClearControls()
        {
            Messenger.Default.Register<NotificationMessage<ScannerMessageItem>>(
                this,
                up =>
                {
                    if (up.Content.msg.ToString() == "driver")
                    {
                        BindToLoginForm(up.Content.Driver);
                        Thread.Sleep(5000);
                        Process(up.Content.Driver);
                    }
                });
            UIFactory.CurrentControl = "Login";
            txtIniSurname.Text = "";
            txtIDNo.Text = "";
            txtBDate.Text = "";
            txtLicNo.Text = "";
            txtIssueDate.Text = "";
            txtIssueNo.Text = "";
            txtValidDate.Text = "";
            txtVehicleCodes.Text = "";
            txtDriverRes.Text = "";
            txtGender.Text = "";
            pictureBox1.Image = null;
            
        }

        private void Process(DriverModel driver)
        {
            if (Next != null)
                Next(this, driver);
        }


        public Image ByteArrayToImage(byte[] byteArrayIn)
        {
            var ms = new MemoryStream(byteArrayIn);
            var returnImage = Image.FromStream(ms);
            return returnImage;
        }

        protected override void Dispose(bool disposing)
        {
            Messenger.Default.Unregister(this);
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void BindToLoginForm(DriverModel driver)
        {
            txtBDate.Text = string.Format("{0:yyyy-MM-dd}", driver.DateOfBirth);
            txtDriverRes.Text = driver.Licence != null && driver.Licence.DriverRestrictions != null
                                    ? driver.Licence.DriverRestrictions.Join(",")
                                    : null;
            txtGender.Text = driver.Gender.ToString();
            txtIDNo.Text = driver.IdNumber;
            ContextData.SetDriver(driver);
            txtIniSurname.Text = String.Format("{0} {1}", driver.Initials, driver.Surname);
            txtIssueDate.Text = String.Format("{0:yyyy-MM-dd}",
                                              driver.Licence.Classes.Select(x => x.FirstIssueDate).FirstOrDefault());
            txtIssueNo.Text = driver.Licence.IssueNo;
            txtLicNo.Text = driver.Licence.CertificateNumber;
            txtValidDate.Text = String.Format("{0:yyyy-MM-dd} - {1:yyyy-MM-dd}",
                                              driver.Licence.ValidFrom, driver.Licence.ValidUntil);
            txtVehicleCodes.Text = driver.Licence.Classes.Select(x => x.VehicleCode).Join(",");
            if (driver.Picture != null)
                LoadImage(driver.Picture);
        }

        private void LoadImage(BlobModel doc)
        {
            var buff = _dataApi.Download(doc.Id);
            if (buff == null)
                return;
            try
            {
                var img = ByteArrayToImage(buff);
                pictureBox1.Image = img;
            }
            catch
            {
                pictureBox1.Image = null;
            }
        }

        private void Prepopulate(string idNumber)
        {
            var driver = _driverApi.GetByIdNo(new IdInfo(idNumber));
            if (driver == null)
                return;
            BindToLoginForm(driver);
            btnNext.Enabled = true;
            btnNext.Visible = true;
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            Process(ContextData.Driver);
        }

        public event EventHandler<DriverModel> Next;
        public event EventHandler<DriverModel> Back;

        private void btnBack_Click(object sender, EventArgs e)
        {
            Back(this, null);
        }
    }
}