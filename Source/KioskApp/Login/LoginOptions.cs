﻿using System;
using System.Windows.Forms;
using Castle.Core;
using MetroFramework.Controls;
using iRent.Classes;

namespace iRent.Login
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class LoginOptions : MetroUserControl
    {
        public LoginOptions()
        {
            InitializeComponent();
        }

        private void btnPassport_Click(object sender, EventArgs e)
        {
            UIFactory.Accept = true;
            UIFactory.NextControl = "LoginPassport";
            var form = ParentForm as MainForm;
            form.Invoke(new MethodInvoker(delegate { form.Accept(); }));
        }

        private void btnDriversLicense_Click(object sender, EventArgs e)
        {
            UIFactory.Accept = true;
            UIFactory.NextControl = "Login";
            lblStatus.Text = "Please press accept to cancel rental.";
            var form = ParentForm as MainForm;
            form.Invoke(new MethodInvoker(delegate { form.Accept(); }));
        }
    }
}