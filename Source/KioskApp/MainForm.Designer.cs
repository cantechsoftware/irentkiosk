﻿namespace iRent
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.metroPanel2 = new MetroFramework.Controls.MetroPanel();
            this.btnHome = new MetroFramework.Controls.MetroButton();
            this.lblStatus = new MetroFramework.Controls.MetroLabel();
            this.lblToolStrip = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblVersionInfo = new MetroFramework.Controls.MetroLabel();
            this.pnlForm = new MetroFramework.Controls.MetroPanel();
            this.picHeader = new System.Windows.Forms.PictureBox();
            this.btnNextItem = new MetroFramework.Controls.MetroButton();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.metroPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picHeader)).BeginInit();
            this.SuspendLayout();
            // 
            // metroPanel2
            // 
            this.metroPanel2.Controls.Add(this.btnHome);
            this.metroPanel2.Controls.Add(this.lblStatus);
            this.metroPanel2.Controls.Add(this.lblToolStrip);
            this.metroPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel2.HorizontalScrollbarBarColor = true;
            this.metroPanel2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel2.HorizontalScrollbarSize = 12;
            this.metroPanel2.Location = new System.Drawing.Point(4, 774);
            this.metroPanel2.Margin = new System.Windows.Forms.Padding(4);
            this.metroPanel2.Name = "metroPanel2";
            this.metroPanel2.Size = new System.Drawing.Size(162, 86);
            this.metroPanel2.TabIndex = 10;
            this.metroPanel2.VerticalScrollbarBarColor = true;
            this.metroPanel2.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel2.VerticalScrollbarSize = 13;
            // 
            // btnHome
            // 
            this.btnHome.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnHome.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnHome.Highlight = true;
            this.btnHome.Location = new System.Drawing.Point(0, 0);
            this.btnHome.Margin = new System.Windows.Forms.Padding(4);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(162, 86);
            this.btnHome.Style = MetroFramework.MetroColorStyle.Red;
            this.btnHome.TabIndex = 101;
            this.btnHome.Text = "Cancel";
            this.btnHome.UseSelectable = true;
            this.btnHome.Visible = false;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // lblStatus
            // 
            this.lblStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblStatus.FontSize = MetroFramework.MetroLabelSize.XL;
            this.lblStatus.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.lblStatus.Location = new System.Drawing.Point(0, 0);
            this.lblStatus.Margin = new System.Windows.Forms.Padding(5);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(162, 86);
            this.lblStatus.Style = MetroFramework.MetroColorStyle.Red;
            this.lblStatus.TabIndex = 89;
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblStatus.UseStyleColors = true;
            // 
            // lblToolStrip
            // 
            this.lblToolStrip.AutoSize = true;
            this.lblToolStrip.Location = new System.Drawing.Point(284, 12);
            this.lblToolStrip.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblToolStrip.Name = "lblToolStrip";
            this.lblToolStrip.Size = new System.Drawing.Size(0, 0);
            this.lblToolStrip.TabIndex = 7;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.981167F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80.03766F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.981167F));
            this.tableLayoutPanel1.Controls.Add(this.lblVersionInfo, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.metroPanel2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.pnlForm, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.picHeader, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnNextItem, 2, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 67);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17.64706F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 82.35294F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 94F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1707, 893);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // lblVersionInfo
            // 
            this.lblVersionInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblVersionInfo.Location = new System.Drawing.Point(173, 864);
            this.lblVersionInfo.Name = "lblVersionInfo";
            this.lblVersionInfo.Size = new System.Drawing.Size(1360, 29);
            this.lblVersionInfo.TabIndex = 101;
            this.lblVersionInfo.Text = "[Version Info]";
            this.lblVersionInfo.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // pnlForm
            // 
            this.pnlForm.AutoScroll = true;
            this.pnlForm.AutoSize = true;
            this.pnlForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlForm.HorizontalScrollbar = true;
            this.pnlForm.HorizontalScrollbarBarColor = false;
            this.pnlForm.HorizontalScrollbarHighlightOnWheel = false;
            this.pnlForm.HorizontalScrollbarSize = 12;
            this.pnlForm.Location = new System.Drawing.Point(174, 140);
            this.pnlForm.Margin = new System.Windows.Forms.Padding(4);
            this.pnlForm.Name = "pnlForm";
            this.pnlForm.Padding = new System.Windows.Forms.Padding(13, 12, 13, 12);
            this.tableLayoutPanel1.SetRowSpan(this.pnlForm, 2);
            this.pnlForm.Size = new System.Drawing.Size(1358, 720);
            this.pnlForm.TabIndex = 14;
            this.pnlForm.VerticalScrollbar = true;
            this.pnlForm.VerticalScrollbarBarColor = true;
            this.pnlForm.VerticalScrollbarHighlightOnWheel = true;
            this.pnlForm.VerticalScrollbarSize = 21;
            // 
            // picHeader
            // 
            this.picHeader.BackColor = System.Drawing.Color.White;
            this.picHeader.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picHeader.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel1.SetColumnSpan(this.picHeader, 3);
            this.picHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picHeader.Image = ((System.Drawing.Image)(resources.GetObject("picHeader.Image")));
            this.picHeader.InitialImage = ((System.Drawing.Image)(resources.GetObject("picHeader.InitialImage")));
            this.picHeader.Location = new System.Drawing.Point(0, 0);
            this.picHeader.Margin = new System.Windows.Forms.Padding(0);
            this.picHeader.Name = "picHeader";
            this.picHeader.Size = new System.Drawing.Size(1707, 136);
            this.picHeader.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picHeader.TabIndex = 15;
            this.picHeader.TabStop = false;
            // 
            // btnNextItem
            // 
            this.btnNextItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNextItem.FontSize = MetroFramework.MetroButtonSize.XL;
            this.btnNextItem.Highlight = true;
            this.btnNextItem.Location = new System.Drawing.Point(1540, 774);
            this.btnNextItem.Margin = new System.Windows.Forms.Padding(4);
            this.btnNextItem.Name = "btnNextItem";
            this.btnNextItem.Size = new System.Drawing.Size(163, 86);
            this.btnNextItem.Style = MetroFramework.MetroColorStyle.Red;
            this.btnNextItem.TabIndex = 100;
            this.btnNextItem.Text = "Next";
            this.btnNextItem.UseSelectable = true;
            this.btnNextItem.Visible = false;
            this.btnNextItem.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = MetroFramework.Forms.MetroFormBorderStyle.FixedSingle;
            this.ClientSize = new System.Drawing.Size(1707, 960);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.Padding = new System.Windows.Forms.Padding(0, 67, 0, 0);
            this.Resizable = false;
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.None;
            this.Style = MetroFramework.MetroColorStyle.Silver;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmIrent_FormClosing);
            this.Load += new System.EventHandler(this.frmIrent_Load);
            this.metroPanel2.ResumeLayout(false);
            this.metroPanel2.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picHeader)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroPanel metroPanel2;
        private MetroFramework.Controls.MetroLabel lblToolStrip;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MetroFramework.Controls.MetroPanel pnlForm;
        private System.Windows.Forms.PictureBox picHeader;
        private MetroFramework.Controls.MetroButton btnNextItem;
        private MetroFramework.Controls.MetroButton btnHome;
        private MetroFramework.Controls.MetroLabel lblStatus;
        private MetroFramework.Controls.MetroLabel lblVersionInfo;
        private System.Windows.Forms.Timer timer1;
    }
}

