﻿using System;
using Castle.Core;
using MetroFramework.Controls;

namespace iRent
{
    [CastleComponent(Lifestyle = LifestyleType.Transient)]
    public partial class ExistingCardControl : MetroPanel
    {
        public ExistingCardControl()
        {
            InitializeComponent();
        }


        public event EventHandler ReturnCard;
        public event EventHandler Terminate;

        private void btnReturnCard_Click(object sender, EventArgs e)
        {
            if (ReturnCard != null)
                ReturnCard(this, e);
        }

        private void btnTerminatePrevious_Click(object sender, EventArgs e)
        {
            if (Terminate != null)
                Terminate(this, e);
        }
    }
}