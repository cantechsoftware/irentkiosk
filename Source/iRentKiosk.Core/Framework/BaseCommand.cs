﻿using System.Xml.Linq;
using iRentKiosk.Core.Models;
using iRentKiosk.Core.Services;
using iRentKiosk.Core.Services.Api;
using System.Windows.Forms;

namespace iRent.Framework
{
    public abstract class BaseCommand : ICommand
    {
        public WorkflowService WorkflowService { get; set; }
        public Form MainForm { get; set; }
        public BookingApi BookingApi { get; set; }
        public XElement Config { get; set; }

        public T GetFormAs<T>() where T : Form
        {
            return (T)MainForm;
        }

        public string NextButtonText { get; set; }
        public string BackButtonText { get; set; }

        public abstract void Invoke();

        protected virtual void Proceed(string result)
        {
            WorkflowService.Execute(result);
        }
    }
}