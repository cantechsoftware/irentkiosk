﻿using System.Configuration;
using System.Linq;
using System.Xml;
using System.Xml.Linq;

namespace iRentKiosk.Core.Config
{
    public class PluginConfig : IConfigurationSectionHandler
    {
        private XElement _root;

        public object Create(object parent, object configContext, XmlNode section)
        {
            using (var xr = new XmlNodeReader(section))
            {
                _root = XElement.Load(xr);
            }
            return this;
        }

        public static string[] GetPlugins(string section)
        {
            var impl = ConfigurationManager.GetSection(section) as PluginConfig;
            if (impl == null)
                return new string[0];

            return impl._root.Elements("plugin").Select(x => (string) x).ToArray();
        }
    }
}