﻿using System;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace iRentKiosk.Core.Util
{
    public static class Valid
    {
        public static bool Email(string email)
        {
            try
            {
                var m = new MailAddress(email);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        public static bool TelNo(string cellno)
        {
            return Regex.IsMatch(cellno, @"^(0|\+?27)\d{9}$");
        }
    }
}