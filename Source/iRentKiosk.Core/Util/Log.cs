﻿using log4net;

namespace iRentKiosk.Core.Util
{
    public static class Log
    {
        public static ILog For<T>()
        {
            return LogManager.GetLogger(typeof(T));
        }
    }
}