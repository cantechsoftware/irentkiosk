﻿using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using iRentKiosk.Core.Services.Api;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace iRentKiosk.Core.Util
{
    public class LoggingHandler : DelegatingHandler
    {
        private static readonly ILog _log = Log.For<ApiClient>();

        private static readonly DateTime _startTime = DateTime.Now;

        public LoggingHandler(HttpMessageHandler innerHandler)
            : base(innerHandler)
        {
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
            CancellationToken cancellationToken)
        {
            var sb = new StringWriter();
            WriteRequest(sb, request);
            if (request.Content != null)
            {
                var json = await request.Content.ReadAsStringAsync().ConfigureAwait(false);
                sb.WriteLine(JToken.Parse(json).ToString(Formatting.Indented));
            }
            sb.WriteLine();

            try
            {
                var response = await base.SendAsync(request, cancellationToken).ConfigureAwait(false);

                sb.WriteLine("Response {0} - {1}:", (int) response.StatusCode, response.StatusCode);
                if (response.Headers.Any())
                    sb.WriteLine();
                foreach (var header in response.Headers)
                    foreach (var value in header.Value)
                        sb.WriteLine("{0}: {1}", header.Key, value);
                sb.WriteLine();

                if (response.Content != null)
                {
                    var json = await response.Content.ReadAsStringAsync();
                    try
                    {
                        sb.WriteLine(JToken.Parse(json).ToString(Formatting.Indented));
                    }
                    catch (JsonException)
                    {
                        sb.WriteLine(json);
                    }
                }
                sb.WriteLine();

                _log.Debug(sb);

                return response;
            }
            catch (Exception ex)
            {
                _log.Error(sb, ex);
                throw;
            }
        }

        private void WriteRequest(StringWriter sb, HttpRequestMessage request)
        {
            sb.WriteLine("{0} {1}", request.Method, request.RequestUri);
            if (request.Headers.Any())
                sb.WriteLine();
            foreach (var entry in request.Headers)
            {
                foreach (var value in entry.Value)
                    sb.WriteLine("{0}: {1}", entry.Key, value);
            }
        }
    }
}