﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace iRentKiosk.Core.Util
{
    public static class StringExtensions
    {
        public static DateTime? ToDate(this string self, string format)
        {
            if (self == null)
                return null;

            DateTime ret;
            if (DateTime.TryParseExact(self, format, null, DateTimeStyles.AllowWhiteSpaces, out ret))
                return ret;

            return null;
        }

        public static string Extract(this string self, string regex)
        {
            if (self == null)
                return null;
            var match = Regex.Match(self, regex);
            if (match.Success)
                return match.Value;
            return null;
        }
    }
}