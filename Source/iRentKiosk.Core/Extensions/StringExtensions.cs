﻿using System;
using System.Globalization;

namespace iRentKiosk.Core.Extensions
{
    public static class StringExtensions
    {
        public static bool IsDate(this string value, string format)
        {
            DateTime tmp;
            return DateTime.TryParseExact(value, format, null, DateTimeStyles.None, out tmp);
        }
         
    }
}