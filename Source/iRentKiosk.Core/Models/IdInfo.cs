﻿using iRent2.Contracts.Enums;

namespace iRentKiosk.Core.Models
{
    public class IdInfo
    {
        public IdInfo(string idNumber, IdType type = IdType.ID, string country = "ZA")
        {
            IdNumber = idNumber;
            Type = type;
            Country = country;
        }

        public string IdNumber { get; set; }
        public IdType Type { get; set; }
        public string Country { get; set; }

        public override string ToString()
        {
            return $"{Type}:{Country}:{IdNumber}";
        }
    }
}