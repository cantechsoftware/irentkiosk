﻿using System;
using System.Xml.Linq;
using System.Xml.XPath;

namespace iRentKiosk.Core.Models
{
    public class Workflow
    {
        private readonly XElement _definition;

        public Workflow(XElement definition)
        {
            _definition = definition;
        }

        public string ContinueWith { get; set; }
        public string Name => (string) _definition.Attribute("name");

        public XElement FindWorkflow(string name)
        {
            var def = _definition.Document.XPathSelectElement("//Workflow[@name='" + name + "']");

            if (def == null)
                throw new ApplicationException("Could not find workflow " + name + " in " + _definition);
            return def;
        }

        public XElement StartCommand()
        {
            var startName = (string)_definition.Attribute("start");

            return GetCommand(startName);
        }

        public XElement GetCommand(string name)
        {
            var command = _definition.Element(name);
            return command;
        }
    }
}