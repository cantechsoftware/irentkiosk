﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iRentKiosk.Core.Models
{
    public class PayGateRequest
    {
        public string PAYGATE_ID { get; set; }
        public string REFERENCE { get; set; }
        public string AMOUNT { get; set; }
        public string CURRENCY { get; set; }
        public string RETURN_URL { get; set; }
        public string TRANSACTION_DATE { get; set; }
        public string LOCALE { get; set; }
        public string COUNTRY { get; set; }
        public string EMAIL { get; set; }
        public string NOTIFY_URL { get; set; }
        public string USER1 { get; set; }
        public string CHECKSUM { get; set; }

    }
}
