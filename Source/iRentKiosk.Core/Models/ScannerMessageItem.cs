﻿using iRent2.Contracts.Models;

namespace iRentKiosk.Core.Models
{
    public class ScannerMessageItem
    {
        public string msg { get; set; }
        //public CancomWebService.LicenseAndImage obj { get; set; }
        public DriverModel Driver { get; set; }
    }
}
