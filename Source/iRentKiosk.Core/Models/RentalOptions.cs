﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iRent2.Contracts.Rental;
using iRent2.Contracts.PayGate;
using iRent2.Contracts.Models;
using iRent2.Contracts.AdoModels;
using RentalOptionModel = iRent2.Contracts.AdoModels.RentalOptionModel;


namespace iRentKiosk.Core.Models
{
    public class RentalOptions
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public RentalVehicleGroupResponse[] VehicleGroups { get; set; }
        public RentalSearchResponse[] Vehicles { get; set; }
        public RentalSearchResponse SelectedVehicle { get; set; }
        public RentalQuoteBookingResponse QuoteData { get; set; }
        public RentalBookingResponse BookingInfo { get; set; }
        public GetPayTokensResponse[] PaygateTokens { get; set; }
        public WaiverOptionModel[] WaiverOptions { get; set; }
        public WaiverOptionModel SelectedWaiver { get; set; }
        public RentalOptionModel[] Options { get; set; }
        public RentalOptionModel[] SelectedOptions { get; set; }
    }
}
