﻿using System.Windows.Forms;

namespace iRentKiosk.Core.Models
{
    public interface ICommand
    {
        Form MainForm { get; set; }
        void Invoke();
    }
}