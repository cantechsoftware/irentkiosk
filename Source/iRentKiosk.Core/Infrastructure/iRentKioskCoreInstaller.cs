﻿using CanTech.Common.Extensions;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace iRentKiosk.Core.Infrastructure
{
    public class iRentKioskCoreInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.RegisterComponents<iRentKioskCoreInstaller>();
        }
    }
}