﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Castle.Core;
using Castle.MicroKernel;
using iRentKiosk.Core.Models;
using iRentKiosk.Core.Util;
using log4net;
using SmartFormat;
using System.Windows.Forms;


namespace iRentKiosk.Core.Services
{
    [CastleComponent]
    public class CommandFactory
    {
        private static readonly ILog _log = Log.For<CommandFactory>();
        private readonly XElementBinder _binder;
        private readonly IDictionary<string, Type> _commandTypes;
        private readonly IKernel _kernel;
        public Form Mainform { get; set; }

        public CommandFactory(IKernel kernel, XElementBinder binder)
        {
            _kernel = kernel;
            _binder = binder;
            _commandTypes = kernel.GetAssignableHandlers(typeof(ICommand))
                .ToDictionary(x => x.ComponentModel.Name,
                    x => x.ComponentModel.Implementation);
        }

        public ICommand GetCommand(string name, XElement config = null, Form main = null)
        {
            Type type;
            ICommand val = null;
            if (_commandTypes.TryGetValue(name + "Command", out type))
            {
                val = (ICommand)_kernel.Resolve(type, new { config });
                val.MainForm = main;
            }
            return val;
        }

        public void Invoke<TCommand>() where TCommand : ICommand
        {
            var cmd = _kernel.Resolve<TCommand>();
            cmd.Invoke();
        }

        public void InvokeCommand(XElement config)
        {
            var name = (string) config.Attribute("command") ?? config.Name.LocalName;
            var copy = Smart.Format(config.ToString(), ContextData.Instance);
            var configCopy = XElement.Parse(copy);

            _log.DebugFormat("InvokeCommand: {0}, {1}", name, configCopy);

            var cmd = GetCommand(name, configCopy, Mainform);
            if (cmd == null)
                throw new ApplicationException("Could not find command " + name);

            _binder.Bind(configCopy, cmd);

            cmd.Invoke();
        }
    }
}