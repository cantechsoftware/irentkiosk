﻿using System;
using System.Linq;
using Castle.Core;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Services.Api;
using System.Threading;

namespace iRentKiosk.Core.Services
{
    [CastleComponent]
    public class CountryService : IStartable
    {
        private readonly CountryApi _countryApi;
        private Lazy<CountryModel[]> _countries;

        public CountryService(CountryApi countryApi)
        {
            _countryApi = countryApi;
            _countries = new Lazy<CountryModel[]>(_countryApi.Countries, true);
            DefaultCode = "ZA";
        }

        public string DefaultCode { get; set; }

        public void Start()
        {
            try
            {
                var resolved = _countries.Value;
            }
            catch
            {
                _countries = new Lazy<CountryModel[]>(_countryApi.Countries, true);
            }
        }

        public void Stop()
        {
        }

        public CountryModel[] Countries()
        {
            return _countries.Value;
        }

        public CountryModel GetDefaultCountry()
        {
            return Countries().FirstOrDefault(x => x.Code == DefaultCode);
        }
    }
}