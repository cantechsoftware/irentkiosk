﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using iRentKiosk.Core.Models;
using Castle.Core;
using Castle.Windsor;
using iRent2.Contracts.PayGate;
using iRent2.Contracts;

namespace iRentKiosk.Core.Services.Api
{
    [CastleComponent]
    public class PayGateApi
    {
        public PayGateApi(ApiClient apiClient)
        {
            _apiclient = apiClient;
        }

        private ApiClient _apiclient;

        public RegisterPaymentResponse RegisterPayment(RegisterPaymentRequest request)
        {
            try
            {
                return _apiclient.PostJson<RegisterPaymentResponse>(
                        "api/v3/paygate/registerPayment", request
                    );
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }

        }
    }
}
