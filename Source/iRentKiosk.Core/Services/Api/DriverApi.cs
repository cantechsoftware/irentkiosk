﻿using System.Net;
using System.Security.Cryptography.X509Certificates;
using Castle.Core;
using iRent2.Contracts;
using iRent2.Contracts.Drivers;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Models;

namespace iRentKiosk.Core.Services.Api
{
    [CastleComponent]
    public class DriverApi
    {
        private readonly ApiClient _client;

        public DriverApi(ApiClient client)
        {
            _client = client;
        }

        public DriverModel SavePassport(SavePassportRequest request)
        {
            try
            {
                return _client.PostJson<DriverModel>("api/v1/drivers/savePassport", request);
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }

        }

        public DriverModel GetDriver(int driverId)
        {
            try
            {
                return _client.GetJson<DriverModel>("api/v1/drivers/" + driverId);
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }

        }

        public string SendOTP(string cellnumber)
        {
            //api/v2/drivers/sendotp?cellnumber=0824641358
            try
            {
                return _client.GetJson<string>("api/v2/drivers/sendotp?cellnumber=" + cellnumber);
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }

        }

        public bool CheckOTP(string cellnumber, string otp)
        {
            //api/v2/drivers/checkotp?cellnumber=0824641358&otp=1234
            try
            {
                return _client.GetJson<bool>("api/v2/drivers/checkotp?cellnumber=" + cellnumber + "&otp=" + otp);
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return false;
                throw;
            }

        }

        public void SaveNewPassword(ChangePasswordRequest driver)
        {
            try
            {
                _client.PostJson<object>("api/v2/drivers/changePassword", driver);
            }
            catch (ApiException ex)
            {
                //if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                //    return null;
                //throw;
            }
        }

        public DriverModel Save(DriverModel driver)
        {
            try
            {
                return _client.PostJson<DriverModel>("api/v1/drivers/save", driver);
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }
        }

        public DriverModel DecryptAndSaveLicense(byte[] buffer)
        {
            try
            {
                return _client.PostJson<DriverModel>(
                    "api/v1/drivers/decryptLicence",
                    new DecryptLicenceRequest
                    {
                        EncryptedData = buffer
                    });
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }
        }

        public DriverModel GetByIdNo(IdInfo id)
        {
            try
            {
                return _client.GetJson<DriverModel>(
                    $"api/v1/drivers/byIdNo/{id.Type}/{id.Country}/{id.IdNumber}");
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }
        }

        public DriverModel Terminate(int driverId)
        {
            try
            {
                return _client.GetJson<DriverModel>("api/v1/drivers/terminate/" + driverId);
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }

        }

        public ApiResponse StaffNumberAuthentication(StaffAuthenticationRequest request)
        {
            try
            {
                return _client.PostJson<ApiResponse>(
                    "api/v1/drivers/staffAuthentication",
                    request);
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }

        }

        public X509Certificate2 GetCertificate()
        {
            try
            {
                var data = _client.GetJson<byte[]>("api/v1/certificates/password");
                var cert = new X509Certificate2(data);
                return cert;
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }
        }
    }
}