﻿using System.Collections.Generic;
using Castle.Core;
using iRent2.Contracts.Enums;
using iRent2.Contracts.Fingerprints;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Models;

namespace iRentKiosk.Core.Services.Api
{
    [CastleComponent]
    public class FingerprintApi
    {
        private readonly ApiClient _client;

        public FingerprintApi(ApiClient client)
        {
            _client = client;
        }

        public void Enroll(IdInfo id, FingerCode fingerCode, byte[] buffer)
        {
            _client.PostJson<object>("api/v1/fingerprints/save",
                                     new FingerprintModel
                                         {
                                             IdNumber = id.IdNumber,
                                             IdType = id.Type,
                                             IdCountry = id.Country,
                                             Code = fingerCode,
                                             Template = buffer
                                         });
        }

        public IList<FingerprintModel> GetPrints(IdInfo id)
        {
            return _client.PostJson<List<FingerprintModel>>(
                "api/v1/fingerprints/search",
                new SearchFingerprintsRequest
                    {
                        IdNumber = id.IdNumber,
                        IdType = id.Type,
                        IdCountry = id.Country
                    });
        }
    }
}