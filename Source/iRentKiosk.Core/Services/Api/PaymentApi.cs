﻿using Castle.Core;
using iRent2.Contracts.Payments;
using iRent2.Contracts;
using System.Net;

namespace iRentKiosk.Core.Services.Api
{
    [CastleComponent]
    public class PaymentApi
    {
        private readonly ApiClient _client;

        public PaymentApi(ApiClient client)
        {
            _client = client;
        }

        public CreatePaymentResponse Create(CreatePaymentRequest request)
        {
            try
            {
                return _client.PostJson<CreatePaymentResponse>("api/v1/payment/create", request);
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }
        }
    }
}