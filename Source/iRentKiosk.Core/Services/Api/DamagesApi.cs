﻿using Castle.Core;
using iRent2.Contracts.Models;
using System.Net;
using iRent2.Contracts;

namespace iRentKiosk.Core.Services.Api
{
    [CastleComponent]
    public class DamagesApi
    {
        private readonly ApiClient _client;

        public DamagesApi(ApiClient client)
        {
            _client = client;
        }

        public DamageTypeModel[] DamageTypes(string location)
        {
            try
            {
                return _client.GetJson<DamageTypeModel[]>("api/v1/damages/types/" + location);
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }

        }

        public DamageModel[] GetDamages(int VehicleId)
        {
            try
            {
                //http://localhost:23139/api/v1/damages/getdamages?VehicleId=122
                return _client.GetJson<DamageModel[]>("api/v1/damages/getdamages?VehicleId=" + VehicleId.ToString());
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }

        }
    }
}