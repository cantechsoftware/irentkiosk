﻿using Castle.Core;
using iRent2.Contracts.Rental;
using iRent2.Contracts.PayGate;
using iRent2.Contracts.Models;
using System.Net;
using iRent2.Contracts;
using iRent2.Contracts.Finance;
using RentalOptionModel = iRent2.Contracts.AdoModels.RentalOptionModel;


namespace iRentKiosk.Core.Services.Api
{
    [CastleComponent]
    public class RentalApi
    {
        private readonly ApiClient _client;

        public RentalApi(ApiClient client)
        {
            _client = client;
        }

        public RentalSearchResponse[] RentalSearch(RentalSearchRequest request)
        {
            try
            {
                return _client.PostJson<RentalSearchResponse[]>("api/v3/rental/rentalVehiclesearch", request);
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }

        }

        public RentalVehicleGroupResponse[] RentalVehicleClass(RentalVehicleGroupRequest request)
        {
            try
            {
                return _client.PostJson<RentalVehicleGroupResponse[]>("api/v3/rental/rentalvehiclegroup", request);
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }

        }

        public ChargeReIssueCardResponse CardReissueCharge(ChargeReIssueCardRequest request)
        {
            try
            {
                return _client.PostJson<ChargeReIssueCardResponse>("api/v3/rental/chargeRFIDCardReissue", request);
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }

        }

        public RentalBookingResponse RentalBooking(RentalBookingRequest request)
        {
            try
            {
                return _client.PostJson<RentalBookingResponse>("api/v3/rental/rentalbooking", request);
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }

        }

        public RentalDriverResponse RentalDriverConfirm(RentalDriverRequest request)
        {
            try
            {
                return _client.PostJson<RentalDriverResponse>("api/v3/rental/rentaldriverconfirm", request);
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }

        }

        public RentalPaymentResponse RentalMakePayment(RentalPaymentRequest request)
        {
            try
            {
                return _client.PostJson<RentalPaymentResponse>("api/v3/rental/rentalmakepayment", request);
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }

        }

        public RentalReturnResponse RentalReturn(RentalReturnRequest request)
        {
            try
            {
                return _client.PostJson<RentalReturnResponse>("api/v3/rental/rentalreturn", request);
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }

        }

        public RentalInspectionResponse RentalInspection(RentalInspectionRequest request)
        {
            try
            {
                return _client.PostJson<RentalInspectionResponse>("api/v3/rental/rentalinspection", request);
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }

        }

        public RentalDamageTypeContract[] RetrieveDamageAreas()
        {
            try
            {
                return _client.PostJson<RentalDamageTypeContract[]>("api/v3/rental/damageareas", null);
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }

        }

        public RentalQuoteBookingResponse RentalQuoteBooking(RentalQuoteBookingRequest request)
        {
            try
            {
                return _client.PostJson<RentalQuoteBookingResponse>("api/v3/rental/rentalquotebooking", request);
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }

        }

        public GetPayTokensResponse[] RetrievePayTokens(GetPayTokensRequest request)
        {
            try
            {
                return _client.PostJson<GetPayTokensResponse[]>("api/v3/paygate/getpaytokens", request);
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }

        }


        public FeeCostingResult GetFeeCosting(FeeCostingRequest request)
        {
            try
            {
                return _client.PostJson<FeeCostingResult>("api/v3/rental/feeCosting", request);
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }

        }


        public RentalOptionModel[] GetRentalOptions()
        {
            try
            {
                return _client.GetJson<RentalOptionModel[]>("api/v3/rental/getRentalOptions");
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }

        }

        public WaiverOptionModel[] GetWaiverOptions()
        {
            try
            {
                return _client.GetJson<WaiverOptionModel[]>("api/v3/rental/getWaiverOptions");
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }

        }

        public void CancelRental(int bookingid)
        {
            try
            {
                _client.PostJson<CustomerFeedbackRequest>("api/v3/rental/rentalcancel", bookingid);
            }
            catch (ApiException ex)
            {
                //if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                //    return null;
                //throw;
            }

        }


        public void CustomerFeedback(CustomerFeedbackRequest req)
        {
            try
            {
                _client.PostJson<CustomerFeedbackRequest>("api/v3/rental/customerFeedback", req);
            }
            catch (ApiException ex)
            {
                //if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                //    return null;
                //throw;
            }

        }

        public string KioskDamageNotify(string bookingid)
        {
            try
            {
                //api/v2/drivers/sendotp?cellnumber=0824641358
                return _client.GetJson<string>("api/v3/rental/KioskDamageNotify?bookingid=" + bookingid);
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }

        }

    }
}
