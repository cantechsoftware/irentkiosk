﻿using Castle.Core;
using iRent2.Contracts.Models;
using iRent2.Contracts.Rfid;
using iRent2.Contracts;
using System.Net;

namespace iRentKiosk.Core.Services.Api
{
    [CastleComponent]
    public class RfidApi
    {
        private readonly ApiClient _client;

        public RfidApi(ApiClient client)
        {
            _client = client;
        }

        public RfidModel CancelCard(CancelRfidRequest request)
        {
            try
            {
                return _client.PostJson<RfidModel>("api/v1/rfid/cancel", request);
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }

        }
    }
}