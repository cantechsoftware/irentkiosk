﻿using Castle.Core;
using iRent2.Contracts.Models;
using System.Net;
using iRent2.Contracts;

namespace iRentKiosk.Core.Services.Api
{
    [CastleComponent]
    public class CountryApi
    {
        private readonly ApiClient _client;

        public CountryApi(ApiClient client)
        {
            _client = client;
        }

        public CountryModel[] Countries()
        {
            try
            {
                return _client.GetJson<CountryModel[]>("api/v1/countries/list");
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }
        }
    }
}