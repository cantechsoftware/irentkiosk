﻿using System;
using System.Linq;
using System.Net;
using CanTech.Common.Extensions;
using iRent2.Contracts;

namespace iRentKiosk.Core.Services.Api
{
    public class ApiException : Exception
    {
        public ApiException(HttpStatusCode statusCode, string result)
            : base( "Error:" + statusCode+ "\r\n"+ result)
        {
            HttpStatusCode = statusCode;
            Result = result;
        }

        public ApiException(HttpStatusCode statusCode, ApiResponse<object> result)
            :base(result.StatusCode  + ":\r\n"
                  + result.Messages.Select(x => x.Field + ": " + x.Message).Join("\r\n")
                  + " - " + result.Data)
        {
            HttpStatusCode = statusCode;
            ApiStatusCode = result.StatusCode;
            Messages = result.Messages;
        }

        public HttpStatusCode HttpStatusCode { get; private set; }
        public ApiStatusCode ApiStatusCode { get; private set; }
        public string Result { get; private set; }
        public ApiResponseMessage[] Messages { get; private set; }
    }
}