﻿using Castle.Core;
using System.Net;
using iRent2.Contracts;

namespace iRentKiosk.Core.Services.Api
{
    [CastleComponent]
    public class DataApi
    {
        private readonly ApiClient _client;

        public DataApi(ApiClient client)
        {
            _client = client;
        }

        public byte[] Download(int documentId)
        {
            try
            {
                using (var cl = _client.GetClient())
                {
                    return cl.GetByteArrayAsync("api/v1/data/" + documentId).Result;
                }
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }

        }
    }
}