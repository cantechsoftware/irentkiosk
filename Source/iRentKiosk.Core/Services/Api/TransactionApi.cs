﻿using Castle.Core;
using iRent2.Contracts;
using iRent2.Contracts.Models;
using iRent2.Contracts.Transactions;
using System.Net;

namespace iRentKiosk.Core.Services.Api
{
    [CastleComponent]
    public class TransactionApi
    {
         private readonly ApiClient _client;

        public TransactionApi(ApiClient client)
        {
            _client = client;
        }
 
        public TransactionModel Update(TransactionModel request)
        {
            try
            {
                return _client.PostJson<TransactionModel>("api/v1/transactions/update", request);
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }

        }

        public TransactionModel GetLatestByDriver(int driverId)
        {
            try
            {
                return _client.GetJson<TransactionModel>("api/v1/transactions/latestByDriver/" + driverId);
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound)
                    return null;
                throw;
            }
        }

        public void Activate(int id, bool notify)
        {
            try
            {
                _client.PostJson<object>("api/v1/transactions/activate",
                                         new ActivateTransactionRequest
                                         {
                                             TransactionId = id,
                                             NotifyDriver = notify
                                         });
            }
            catch (ApiException ex)
            {
                //if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                //    return null;
                //throw;
            }

        }

        public TransactionModel Swap(int transactionId, int branchId,
                                     string rfid = null, int? vehicleGroupId = null, int? vehicleId = null)
        {
            try
            {
                return _client.PostJson<TransactionModel>("api/v1/transactions/swap",
                                                          new SwapTransactionRequest
                                                          {
                                                              BranchId = branchId,
                                                              Rfid = rfid,
                                                              TransactionId = transactionId,
                                                              VehicleGroupId = vehicleGroupId,
                                                              NewVehicleId = vehicleId
                                                          });
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }

        }
    }
}