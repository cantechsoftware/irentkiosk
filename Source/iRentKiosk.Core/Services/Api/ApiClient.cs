﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using CanTech.Common.Extensions;
using Castle.Core;
using iRent2.Contracts;
using iRentKiosk.Core.Util;

namespace iRentKiosk.Core.Services.Api
{
    [CastleComponent]
    public class ApiClient
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string BaseUrl { get; set; }
        public bool LogCalls { get; set; }

        public virtual string Token { get; set; }

        private HttpMessageHandler CreateHandler()
        {
            if (LogCalls)
                return new LoggingHandler(new HttpClientHandler());
            return new HttpClientHandler();
        }


        public HttpClient GetClient()
        {
            var client = new HttpClient(CreateHandler());
            client.Timeout = TimeSpan.FromMinutes(Debugger.IsAttached ? 10 : 1);
            client.BaseAddress = new Uri(BaseUrl);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(
                "Basic",
                Convert.ToBase64String(Encoding.ASCII.GetBytes(Username + ":" + Password))
            );
            return client;
        }

        public virtual TResult WithClient<TResult>(Func<HttpClient, Task<HttpResponseMessage>> execute)
        {
            try
            {
                using (var client = GetClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    if (Token.HasValue())
                        client.DefaultRequestHeaders.Add("X-Authorisation-Token", Token);
                    var rsp = execute(client).Result;
                    if (!rsp.IsSuccessStatusCode)
                        throw new ApiException(rsp.StatusCode, rsp.Content.ReadAsStringAsync().Result);

                    var rsp1 = rsp.Content.ReadAsAsync<ApiResponse<dynamic>>()
                        .ConfigureAwait(false)
                        .GetAwaiter()
                        .GetResult();

                    if (rsp1.StatusCode != ApiStatusCode.Success)
                        throw new ApiException(rsp.StatusCode, rsp1);

                    if (rsp1.Data == null)
                        return default(TResult);
                    var fmt = new JsonMediaTypeFormatter();
                    using (var ms = new MemoryStream())
                    {
                        fmt.WriteToStream(rsp1.Data.GetType(), rsp1.Data, ms, Encoding.Default);
                        ms.Seek(0, SeekOrigin.Begin);
                        return (TResult)fmt.ReadFromStream(typeof(TResult), ms, Encoding.Default, null);
                    }
                }
            }
            catch
            {
                return default(TResult);
            }

        }

        public virtual TResult GetJson<TResult>(string url)
        {
            return WithClient<TResult>(x => x.GetAsync(url));
        }

        public virtual TResult PostJson<TResult>(string url, object request)
        {
            return WithClient<TResult>(x => x.PostAsJsonAsync(url, request));
        }

        public virtual dynamic PostJson(string url, object request)
        {
            return PostJson<dynamic>(url, request);
        }
    }
}