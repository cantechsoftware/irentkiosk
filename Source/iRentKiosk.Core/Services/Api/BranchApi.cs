﻿using Castle.Core;
using iRent2.Contracts.Branches;
using iRent2.Contracts.Models;
using iRent2.Contracts;
using System.Net;

namespace iRentKiosk.Core.Services.Api
{
    [CastleComponent]
    public class BranchApi
    {
        private readonly ApiClient _client;

        public BranchApi(ApiClient client)
        {
            _client = client;
        }

        public string[] GetProvinces()
        {
            try
            {
                return _client.GetJson<string[]>("api/v1/branches/provinces");
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }

        }

        public BranchModel[] GetBranches(string province)
        {
            try
            {
                return _client.PostJson<BranchModel[]>(
                    "api/v1/branches/list", new ListBranchesRequest
                    {
                        Province = province
                    });
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }
        }
    }
}