﻿using System;
using System.Net;
using Castle.Core;
using iRent2.Contracts;
using iRent2.Contracts.Bookings;
using iRent2.Contracts.Models;

namespace iRentKiosk.Core.Services.Api
{
    [CastleComponent]
    public class BookingApi
    {
        private readonly ApiClient _client;

        public BookingApi(ApiClient client)
        {
            _client = client;
        }

        public BookingModel GetBooking(int id)
        {
            try
            {
                return _client.GetJson<BookingModel>("api/v1/bookings/" + id);
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }
        }

        public BookingModel[] FindBookings(SearchBookingsRequest request)
        {
            try
            {
                return _client.PostJson<BookingModel[]>("api/v1/bookings/search", request);
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }

        }
        public WaiverOptionModel[] GetWaiverOptions(int billingModelId)
        {
            try
            {
                return _client.GetJson<WaiverOptionModel[]>("api/v1/bookings/waiverOptions/" + billingModelId);
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }

        }

        public PaymentMethodModel GetPaymentMethod(string code)
        {
            try
            {
                return _client.GetJson<PaymentMethodModel>("api/v1/bookings/paymentMethods/" + code);
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }

        }

        public QuoteModel GetQuote(BookingModel booking)
        {
            try
            {
                return _client.PostJson<QuoteModel>("api/v1/bookings/quote", booking);
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }

        }

        public BookingModel Save(BookingModel booking)
        {
            try
            {
                return _client.PostJson<BookingModel>("api/v1/bookings/save", booking);
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }

        }

        public void Activate(BookingModel booking, string rfid, Guid? paymentKey)
        {
            try
            {
                _client.PostJson<object>("api/v1/bookings/activate", new ActivateBookingRequest
                {
                    BookingId = booking.Id,
                    Rfid = rfid,
                    PaymentKey = paymentKey
                });
            }
            catch (ApiException ex)
            {
                //if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                //    return null;
                //throw;
            }

        }
    }
}