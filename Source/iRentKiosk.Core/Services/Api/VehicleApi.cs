﻿using System;
using Castle.Core;
using iRent2.Contracts.Models;
using iRent2.Contracts.Vehicles;
using iRent2.Contracts;
using System.Net;

namespace iRentKiosk.Core.Services.Api
{
    [CastleComponent]
    public class VehicleApi
    {
        private readonly ApiClient _client;

        public VehicleApi(ApiClient client)
        {
            _client = client;
        }

        public VehicleGroupsResponse VehicleGroups()
        {
            try
            {
                return _client.PostJson<VehicleGroupsResponse>("api/v1/vehicles/groups",
                    new GetVehicleGroupsRequest
                    {
                        BranchId = ContextData.Kiosk.BranchId
                    });
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }

        }

        public VehicleModel GetByRfid(string rfid)
        {
            try
            {
                if (rfid == null)
                    return null;
                return _client.GetJson<VehicleModel>("api/v1/vehicles/rfid/" + rfid);
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }

        }

        public VehicleModel GetById(int id)
        {
            try
            {
                return _client.GetJson<VehicleModel>("api/v1/vehicles/" + id);
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }

        }

        public VehicleModel UpdateRfid(VehicleRfidModel request)
        {
            try
            {
                return _client.PostJson<VehicleModel>("api/v1/vehicles/updateRfid", request);
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }

        }

        public VehicleModel[] GetByGroupId(int groupId)
        {
            try
            {
                return _client.PostJson<VehicleModel[]>("api/v1/vehicles/search",
                    new SearchVehiclesRequest
                    {
                        Groups = new[] { groupId },
                        Branches = new[] { ContextData.Kiosk.BranchId.Value },
                        Available = true
                    });
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }

        }

        public void ReturnVehicle(string rfid, bool returnCard)
        {
            try
            {
                _client.PostJson<object>("api/v1/vehicles/return", new ReturnVehicleRequest
                {
                    Rfid = rfid,
                    ReturnCard = returnCard,
                    ReturnDate = DateTime.Now,
                    BranchId = ContextData.Kiosk.BranchId.Value
                });
            }
            catch (ApiException ex)
            {
                //if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                //    return null;
                //throw;
            }

        }
    }
}