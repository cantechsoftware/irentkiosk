﻿using Castle.Core;
using iRent2.Contracts.Kiosks;
using iRent2.Contracts;
using System.Net;

namespace iRentKiosk.Core.Services.Api
{
    [CastleComponent]
    public class KioskApi
    {
        private readonly ApiClient ApiClient;

        public KioskApi(ApiClient apiClient)
        {
            ApiClient = apiClient;
        }

        public ProvisionKioskResponse Provision(ProvisionKioskRequest request)
        {
            try
            {
                return ApiClient.PostJson<ProvisionKioskResponse>(
                    "api/v1/kiosks/provision",
                    request);
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }
        }
    }
}