﻿using Castle.Core;
using iRent2.Contracts.Models;
using iRent2.Contracts;
using System.Net;

namespace iRentKiosk.Core.Services.Api
{
    [CastleComponent]
    public class SmsApi
    {
        private readonly ApiClient _client;

        public SmsApi(ApiClient client)
        {
            _client = client;
        }

        public SmsModel SendClientSms(SmsModel request)
        {
            try
            {
                return _client.PostJson<SmsModel>("api/v1/sms/send/", request);
            }
            catch (ApiException ex)
            {
                if (ex.ApiStatusCode == ApiStatusCode.NotFound || ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }

        }
    }
}