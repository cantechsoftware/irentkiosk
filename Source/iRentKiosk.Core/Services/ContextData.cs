﻿using System;
using System.Collections.Generic;
using System.Drawing;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Models;
using iRent2.Contracts.Rental;
using SmartFormat;

namespace iRentKiosk.Core.Services
{
    public static class ContextData
    {
        static ContextData()
        {
            Instance = new InstanceWrapper();
            DynamicData = new Dictionary<string, dynamic>();
        }

        public static IDictionary<string, object> Values { get; } = new Dictionary<string, object>();

        public static string VehicleName { get; set; }
        public static DateTime? VehicleReturnDate { get; set; }
        public static int AdditionalDriverCost { get; set; }
        public static string SelectedOffice { get; set; }
        public static string SelectedCountry { get; set; }
        public static bool SwapCar { get; set; }
        public static string PreviousRfid { get; set; }
        public static IdInfo DriverID { get; private set; }
        public static VehicleModel Vehicle { get; set; }

        public static int? VehicleId
        {
            get { return Vehicle != null ? Vehicle.Id : (int?)null; }
        }

        public static decimal VehicleCost { get; set; }

        public static VehicleGroupModel VehicleGroup { get; private set; }

        public static RentalOptions RentalOptions { get; private set; }

        public static int? VehicleGroupId => VehicleGroup?.Id;

        public static DriverModel Driver { get; private set; }

        public static string DriverName => Driver.Initials + " " + Driver.Surname;

        public static BookingModel Booking { get; private set; }

        public static InstanceWrapper Instance { get; private set; }

        public static KioskModel Kiosk { get; set; }

        public static BranchModel ReturnBranch { get; set; }

        public static Image Signature { get; set; }

        public static Guid? PaymentKey { get; set; }

        public static TransactionModel Transaction { get; set; }

        public static Dictionary<string, dynamic> DynamicData { get; set; }

        public static RentalInspectionRequest RentalInspection { get; set; }

        public static void Reset()
        {
            Values.Clear();
            Booking = null;
            SwapCar = false;
            DriverID = null;
            Driver = null;
            VehicleName = null;
            VehicleReturnDate = null;
            AdditionalDriverCost = 0;
            SelectedOffice = null;
            SelectedCountry = null;
            PreviousRfid = null;
            Vehicle = null;
            VehicleGroup = null;
            PaymentKey = null;
            Transaction = null;
            RentalInspection = null;
        }

        public static void SetRentalInspection(RentalInspectionRequest request)
        {
            RentalInspection = request;
        }

        public static void SetDriver(DriverModel driver)
        {
            DriverID = new IdInfo(driver.IdNumber, driver.IdType, driver.IdCountry);
            if(driver != null && driver.PermanentRfid == null)
            {
                driver.PermanentRfid = "";
            }
            Driver = driver;
        }

        public static void Set(RentalOptions options)
        {
            RentalOptions = options;
        }

        public static void SetDynamicData(string Name, dynamic data)
        {
            if (DynamicData.ContainsKey(Name))
            {
                DynamicData[Name] = data;
            }
            else
            {
                DynamicData.Add(Name, data);
            }
        }

        public static void SetBooking(BookingModel booking)
        {
            Booking = booking;
            if (booking != null)
            {
                Set(booking.VehicleGroup);
                ReturnBranch = booking.ReturnBranch;
                VehicleReturnDate = booking.ReturnDate;
            }
        }

        public static void Set(VehicleGroupModel vehicleGroup)
        {
            VehicleGroup = vehicleGroup;
            VehicleCost = vehicleGroup?.Cost ?? 0;
        }

        public static string Format(string message)
        {
            return Smart.Format(message, Instance);
        }

        public static void Set(TransactionModel transaction)
        {
            Transaction = transaction;
            if (transaction == null)
                return;

            Set(transaction.Vehicle);
            Set(transaction.VehicleGroup);
        }

        public static void Set(VehicleModel vehicle)
        {
            Vehicle = vehicle;
            VehicleName = $"{vehicle.Colour} {vehicle.Make}-{vehicle.Model} {vehicle.RegNo}";
        }

        public class InstanceWrapper
        {
            public IDictionary<string, object> Values => ContextData.Values;
            public string VehicleName => ContextData.VehicleName;
            public DateTime? VehicleReturnDate => ContextData.VehicleReturnDate;
            public int AdditionalDriverCost => ContextData.AdditionalDriverCost;
            public string SelectedOffice => ContextData.SelectedOffice;
            public string SelectedCountry => ContextData.SelectedCountry;
            public bool SwapCar => ContextData.SwapCar;
            public string PreviousRfid => ContextData.PreviousRfid;
            public IdInfo DriverID => ContextData.DriverID;
            public VehicleModel Vehicle => ContextData.Vehicle;
            public int? VehicleId => ContextData.VehicleId;
            public decimal VehicleCost => ContextData.VehicleCost;
            public VehicleGroupModel VehicleGroup => ContextData.VehicleGroup;
            public int? VehicleGroupId => ContextData.VehicleGroupId;
            public DriverModel Driver => ContextData.Driver;
            public string DriverName => ContextData.DriverName;
            public BookingModel Booking => ContextData.Booking;
            public KioskModel Kiosk => ContextData.Kiosk;
            public BranchModel ReturnBranch => ContextData.ReturnBranch;
            public Image Signature => ContextData.Signature;
            public Guid? PaymentKey => ContextData.PaymentKey;
            public TransactionModel Transaction => ContextData.Transaction;
        }
    }
}