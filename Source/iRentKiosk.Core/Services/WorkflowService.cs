﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using CanTech.Common.Extensions;
using Castle.Core;
using iRentKiosk.Core.Models;
using iRentKiosk.Core.Util;
using log4net;

namespace iRentKiosk.Core.Services
{
    [CastleComponent]
    public class WorkflowService
    {
        private static readonly ILog _log = Log.For<WorkflowService>();

        private readonly CommandFactory _commandFactory;
        private readonly FileSystemWatcher _fsw;
        private readonly string _path;
        private readonly Stack<Workflow> _workflowStack = new Stack<Workflow>();
        private XElement _definition;

        public WorkflowService(string path, CommandFactory commandFactory)
        {
            _commandFactory = commandFactory;
            _path = Path.GetFullPath(path);
            _fsw = new FileSystemWatcher(Path.GetDirectoryName(_path), Path.GetFileName(_path));
            _fsw.Changed += OnFileChanged;
            _fsw.Created += OnFileChanged;
            _fsw.Deleted += OnFileChanged;
        }

        public Workflow Current => _workflowStack.Peek();

        private void OnFileChanged(object sender, FileSystemEventArgs e)
        {
            _definition = null;
        }

        private XElement GetWorkflowDefinition()
        {
            var wf = _definition;
            if (wf != null)
                return wf;

            lock (this)
            {
                wf = _definition;
                if (wf == null)
                {
                    wf = XElement.Load(_path);
                    new XDocument(wf); // wrap in a document to make it easier to get to root
                    _definition = wf;
                }
                return wf;
            }
        }

        public void Start()
        {
            _workflowStack.Clear();

            var def = GetWorkflowDefinition();
            if (def == null)
                throw new ApplicationException("Could not find workflow " + _path);

            var wf = new Workflow(def);
            StartWorkflow(wf);
        }

        private void StartWorkflow(Workflow workflow)
         {
            _log.DebugFormat("StartWorkflow: {0}", workflow.Name);
            _workflowStack.Push(workflow);
            var cmd = workflow.StartCommand();
            _commandFactory.InvokeCommand(cmd);
        }

        public void StartWorkflow(string name, string next)
        {            
            var childDef = Current.FindWorkflow(name);

            var wf = new Workflow(childDef) {ContinueWith = next};
            StartWorkflow(wf);
        }

        public void Execute(string commandName)
        {
            var cmd = Current.GetCommand(commandName);
            if (cmd != null)
                _commandFactory.InvokeCommand(cmd);
            else
            {
                var inst = _commandFactory.GetCommand(commandName, null, _commandFactory.Mainform );
                if (inst == null)
                    throw new ApplicationException("Could not find command " + commandName);
                inst.Invoke();
            }
        }

        public void CompleteWorkflow()
        {
            var wf = _workflowStack.Pop();
            _log.DebugFormat("CompleteWorkflow: {0}, continue with: {1}", wf.Name, wf.ContinueWith);
            if (wf.ContinueWith.HasValue())
                Execute(wf.ContinueWith);
            else
                Start();
        }
    }
}