﻿using System;
using System.IO;
using System.Security.Cryptography;
using Castle.Core;
using iRent2.Contracts.Models;
using iRent2.Contracts.Payments;
using iRentKiosk.Core.Services.Api;

namespace iRentKiosk.Core.Services
{
    [CastleComponent]
    public class PaymentService
    {
        private readonly ApiClient _client;
        private readonly PaymentApi _paymentApi;

        public PaymentService(ApiClient client, PaymentApi paymentApi)
        {
            _client = client;
            _paymentApi = paymentApi;
        }

        public byte[] Encode(string cc, string cvv, int year, int month, decimal amount)
        {
            var salt = new byte[32];
            using (var rng = RandomNumberGenerator.Create())
                rng.GetNonZeroBytes(salt);

            using (var ms = new MemoryStream())
            using (var aes = Aes.Create())
            {
                ms.Write(salt, 0, salt.Length);
                aes.Key = GetAesKey(amount, salt);
                aes.GenerateIV();
                ms.Write(aes.IV, 0, aes.IV.Length);

                using (var enc = aes.CreateEncryptor())
                {
                    using (var cs = new CryptoStream(ms, enc, CryptoStreamMode.Write))
                    using (var bw = new BinaryWriter(cs))
                    {
                        bw.Write(cc);
                        bw.Write(cvv);
                        bw.Write(year);
                        bw.Write(month);
                    }
                }

                return ms.ToArray();
            }
        }

        private byte[] GetAesKey(decimal amount, byte[] salt)
        {
            var key = GetPasswordKey(amount, ContextData.Kiosk.MacAddress, _client.Password);
            try
            {
                using (var pdb = new Rfc2898DeriveBytes(key, salt, 1000))
                    return pdb.GetBytes(32);
            }
            finally
            {
                Array.Clear(key, 0, key.Length);
            }
        }

        private byte[] GetPasswordKey(decimal amount, string p1, string p2)
        {
            var buff = new byte[p1.Length + p2.Length];
            var foil = BitConverter.GetBytes((int) (amount*100));
            for (var i = 0; i < p1.Length; i++)
                buff[i] = (byte) (p1[i] ^ foil[i%foil.Length]);
            for (var i = 0; i < p2.Length; i++)
                buff[p1.Length + i] = (byte) (p2[i] ^ foil[i%foil.Length]);
            return buff;
        }

        public CreatePaymentResponse Submit(BookingModel booking, decimal amount,
                                            string cc, string cvv, int year, int month)
        {
            var req = new CreatePaymentRequest
                {
                    BookingId = booking.Id,
                    Amount = amount,
                    Data = Encode(cc, cvv, year, month, amount),
                    KioskId = ContextData.Kiosk.Id
                };

            return _paymentApi.Create(req);
        }
    }
}