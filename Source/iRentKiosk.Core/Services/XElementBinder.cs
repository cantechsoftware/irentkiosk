﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using System.Xml.Serialization;
using Castle.Core;

namespace iRentKiosk.Core.Services
{
    [CastleComponent]
    public class XElementBinder
    {
        private static readonly ConcurrentDictionary<Type, XmlSerializer> _serializers
            = new ConcurrentDictionary<Type, XmlSerializer>();

        private static readonly IDictionary<Type, MethodInfo> _converters
            = typeof (XElement).GetMethods()
                               .Where(x => x.Name == "op_Explicit")
                               .ToDictionary(x => x.ReturnType);

        private static readonly IDictionary<Type, MethodInfo> _attributeConverters
            = typeof (XAttribute).GetMethods()
                                 .Where(x => x.Name == "op_Explicit")
                                 .ToDictionary(x => x.ReturnType);

        public void Bind(XElement source, object target)
        {
            var props = target.GetType()
                              .GetProperties()
                              .ToDictionary(x => x.Name, StringComparer.CurrentCultureIgnoreCase);

            foreach (var attr in source.Attributes())
            {
                if (attr.Name == "name")
                    continue;

                PropertyInfo prop;
                if (props.TryGetValue(attr.Name.LocalName, out prop) && prop.CanWrite)
                {
                    var value = GetValue(attr, prop.PropertyType);
                    prop.SetValue(target, value);
                }
            }

            foreach (var elt in source.Elements())
            {
                PropertyInfo prop;
                if (props.TryGetValue(elt.Name.LocalName, out prop) && prop.CanWrite)
                {
                    var value = GetValue(elt, prop.PropertyType);
                    prop.SetValue(target, value);
                }
            }
        }

        public object GetValue(XAttribute attr, Type ofType)
        {
            MethodInfo method;
            if (_attributeConverters.TryGetValue(ofType, out method))
                return method.Invoke(null, new[] {attr});
            if (ofType.IsEnum)
            {
                var strVal = (string) attr;
                return Enum.Parse(ofType, strVal);
            }
            throw new ApplicationException("Cannot convert XAttribute to " + ofType);
        }

        public object GetValue(XElement elt, Type ofType)
        {
            MethodInfo method;
            if (_converters.TryGetValue(ofType, out method))
                return method.Invoke(null, new[] {elt});

            var xser = GetSerializer(ofType);

            var root = new XElement(elt);
            root.Name = "Root";

            using (var xr = root.CreateReader())
            {
                var ret = xser.Deserialize(xr);
                return ret;
            }
        }

        private static XmlSerializer GetSerializer(Type modelType)
        {
            return _serializers.GetOrAdd(modelType,
                                         t => new XmlSerializer(t, new XmlRootAttribute("Root")));
        }
    }
}