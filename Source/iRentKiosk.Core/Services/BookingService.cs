﻿using System;
using Castle.Core;
using iRent2.Contracts.Models;
using iRentKiosk.Core.Services.Api;

namespace iRentKiosk.Core.Services
{
    [CastleComponent]
    public class BookingService
    {
        private readonly BookingApi _bookingApi;

        public BookingService(BookingApi bookingApi)
        {
            _bookingApi = bookingApi;
        }

        public BookingModel SaveBooking()
        {
            var booking = ContextData.Booking ?? new BookingModel {PickupDate = DateTime.Now};
            booking.VehicleGroup = ContextData.VehicleGroup;
            booking.ReturnBranch = ContextData.ReturnBranch;
            booking.ReturnDate = ContextData.VehicleReturnDate.Value;
            booking.DriverId = ContextData.Driver.Id;
            if (booking.PickupBranch == null)
                booking.PickupBranch = ContextData.Kiosk.Branch;

            booking = _bookingApi.Save(booking);
            ContextData.SetBooking(booking);

            return booking;
        }
    }
}